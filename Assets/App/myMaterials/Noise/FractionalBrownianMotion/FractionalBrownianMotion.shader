﻿Shader "Custom/FractionalBrownianMotion" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Division ("Division", int) = 16
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
        int _Division;
        
		struct Input {
			float2 uv_MainTex;
		};
  
        // 砂嵐 ランダムな値を返す
        fixed2 random2(fixed2 st){
            st = fixed2(dot(st,fixed2(127.1,311.7)),
                        dot(st,fixed2(269.5,183.3)));
            return -1.0 + 2.0 * frac(_Time.r * sin(st) * 43758.5453123);
        }
        
        float perlinNoise(fixed2 st){
            fixed2 p = floor(st);
            fixed2 f = frac(st);
            fixed2 u = f * f * (3.0 - 2.0 * f);
            
            float v00 = random2(p+fixed2(0,0));
            float v10 = random2(p+fixed2(1,0));
            float v01 = random2(p+fixed2(0,1));
            float v11 = random2(p+fixed2(1,1));
            
            return lerp( lerp( dot( v00, f - fixed2(0,0) ), dot( v10, f - fixed2(1,0) ), u.x ),
                         lerp( dot( v01, f - fixed2(0,1) ), dot( v11, f - fixed2(1,1) ), u.x ), 
                         u.y)+0.5f;
        }
        
        /*http://nn-hokuson.hatenablog.com/entry/2017/01/27/195659#fBm%E3%83%8E%E3%82%A4%E3%82%BA
        上のプログラムでは、４つの解像度のノイズのテクスチャを次のような割合で合成しています。
        ノイズの解像度が高まるごとに、パーシステンスをべき乗した値を合成する割合として使用しています。最も解像度の低いテクスチャを0.5、次に解像度が低いテクスチャを0.25、次のテクスチャを0.125、最も解像度の高いテクスチャを0.0625の割合で合成しています。
        */
        float fBm (fixed2 st) 
        {
            float f = 0;
            fixed2 q = st;

            f += 0.5000*perlinNoise( q ); q = q*2.01;
             f += 0.2500*perlinNoise( q ); q = q*2.02;
            f += 0.1250*perlinNoise( q ); q = q*2.03;
            f += 0.0625*perlinNoise( q ); q = q*2.01;

            return f;
        }

        void surf (Input IN, inout SurfaceOutputStandard o) {
            float c = fBm(IN.uv_MainTex * _Division);    
            o.Albedo = fixed4(c,c,c,1);
            o.Metallic = 0;
            o.Smoothness = 0;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}