﻿Shader "Custom/ValueNoise" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Division ("Division", int) = 16
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;
        int _Division;

		struct Input {
			float2 uv_MainTex;
		};
        
        // 砂嵐 ランダムな値を返す
        float random(fixed2 p){
            return frac(sin(_Time.r * dot(p, fixed2(12.9898,78.233))) * 43758.5453);
        }
        
        // ブロック floorでst以下の整数にして整数ごとに区分を分ける。今回使ってない。
        float noise(fixed2 st){
            fixed2 p = floor(st);
            return random(p);
        }
        
        // 砂嵐とブロックを合わせる 引数をfixedにするとラインノイズになります。
        float valueNoise(fixed2 st){
            fixed2 p = floor(st);   // floorでst以下の整数にして整数ごとに区分を分ける
            fixed2 f = frac(st);    // fracで小数点以下を取り出す
            
            float v00 = random(p+fixed2(0,0));
            float v10 = random(p+fixed2(1,0));
            float v01 = random(p+fixed2(0,1));
            float v11 = random(p+fixed2(1,1));
            
            /*
            このオフセット値を -2f^3 + 3f^2という数式を使って補間します。
            この数式をグラフ化すると次のようになり、0~1の間の値を緩やかに補間できることがわかります（この行をu=f;にすると線形補間になります）
            */
            fixed2 u = f * f * (3.0 - 2.0 * f);            

            float v0010 = lerp(v00, v10, u.x);
            float v0111 = lerp(v01, v11, u.x);
            return lerp(v0010, v0111, u.y);
        }
  
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
            float c = valueNoise(IN.uv_MainTex * _Division);
			o.Albedo = fixed4(c,c,c,1);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
