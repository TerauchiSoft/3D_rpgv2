﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// 変数の操作。
/// </summary>
public class EVVariable : Instruction
{
    private static System.Random rand;

    private int[] _args;
    private int changeValNo;
    private int changeValNoEnd;
    public enum Way { Substitution, Addition, Subtraction, Multiplication, Division, Remainder }
    private Way way;
    private int operandValue;

    private void GetChangeVal(ref int[] args)
    {
        if (args[0] == 0)
        {
            changeValNo = args[1];
            changeValNoEnd = changeValNo;
        }
        else if (args[0] == 1)
        {
            changeValNo = args[1];
            changeValNoEnd = args[2];

            if (changeValNo > changeValNoEnd)
            {
                var a = changeValNoEnd;
                changeValNoEnd = changeValNo;
                changeValNo = a;
            }
        }
        else if (args[0] == 2)
        {
            changeValNo = VariableObject.Instance.GetVal(args[1]);
            changeValNoEnd = changeValNo;
        }
    }

    private int GetItemOperand(ref int[] args)
    {
        return 0;
        //CommonDatas.
    }

    private int GetPlayerOperand(ref int[] args)
    {
        int playerNo = args[5];
        StatusClass charadata = PlayerDatas.Instance.p_chara[playerNo].Ply_st;
        switch (args[6])
        {
            case 0://レベル
                return charadata.Lv;
            case 1://経験値
                return charadata.Exp;
            case 2://HP
                return charadata.Hp;
            case 3://MP
                return charadata.Mp;
            case 4://M_HP
                return charadata.M_Hp;
            case 5://M_MP
                return charadata.M_Mp;
            case 6://STR
                return charadata.Str;
            case 7://AGI
                return charadata.Agi;
            case 8://INI
                return charadata.Int;
            case 9://VIT
                return charadata.Vit;
            case 10://LUK
                return charadata.Luk;
            case 11://CRI
                return charadata.Cri;
            case 12://Fig
                return charadata.Fatigue;
            default:
                throw new System.Exception("EV変数の操作で主人公のパラメータが範囲外です。");
        }
    }

    /// <summary>
    ///TODO:マップの状態変数の取得ができるようにする。
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    private int GetCharaConditionOperand(ref int[] args)
    {
        int charaNo = args[5];
        switch (args[6])
        {
            case 0:
                return 0;// MapID
            case 1:
                return 0;// X
            case 2:
                return 0;// Y
            case 3:
                return 0;// Direction
            default:
                throw new System.Exception("EV変数の操作でキャラ状態のパラメータが範囲外です。");
        }
    }

    private void GetOperandVal(ref int[] args)
    {
        //0で定数、1で変数、2で変数番の変数、3で乱数、4でアイテム、5で主人公、
        //6でキャラ、7でその他
        switch (args[4])
        {
            case 0://0で定数
                operandValue = args[5];
                break;
            case 1://1で変数
                operandValue = VariableObject.Instance.GetVal(args[5]);
                break;
            case 2://2で変数番の変数
                operandValue = VariableObject.Instance.GetVal(VariableObject.Instance.GetVal(args[5]));
                break;
            case 3://3で乱数
                if (args[5] > args[6])
                {
                    var a = args[5];
                    args[6] = args[5];
                    args[6] = a;
                }
                operandValue = rand.Next(args[5], args[6] + 1);
                break;
            case 4://4でアイテム 所持数
                operandValue = GetItemOperand(ref args);
                break;
            case 5://5で主人公
                operandValue = GetPlayerOperand(ref args);
                break;
            case 6://6でキャラ
                operandValue = GetCharaConditionOperand(ref args);
                break;
            case 7://7でその他
                operandValue = 0;
                break;
            default:
                operandValue = 0;
                break;
        }
    }

    // args[0] = 指定する変数No, args[0] = 指定する変数Noの範囲, args[2] = 指定する変数が指定する変数No, 
    public EVVariable(int[] args)
    {
        if (rand == null)
            rand = new System.Random();

        way = (Way)args[3];
        _args = args;
    }
    
    public override ControlReq Run()
    {
        GetChangeVal(ref _args);
        GetOperandVal(ref _args);
        int[] values = VariableObject.Instance.GetValArray(changeValNo, changeValNoEnd - changeValNo + 1);
        for (int i = 0; i < values.Length; i++)
        {
            switch (way)
            {
                case Way.Substitution:
                    values[i] = operandValue;
                    break;
                case Way.Addition:
                    values[i] = values[i] + operandValue;
                    break;
                case Way.Subtraction:
                    values[i] = values[i] - operandValue;
                    break;
                case Way.Multiplication:
                    values[i] = values[i] * operandValue;
                    break;
                case Way.Division:
                    if (operandValue != 0)
                        values[i] = values[i] / operandValue;
                    else
                        values[i] = 0;
                    break;
                case Way.Remainder:
                    values[i] = values[i] % operandValue;
                    break;
            }
        }
        VariableObject.Instance.SetValArray(changeValNo, values);

        return ControlReq.None;
    }
}
