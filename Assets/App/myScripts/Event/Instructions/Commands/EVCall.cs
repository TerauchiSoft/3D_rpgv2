﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ほかイベントの呼び出しで、独立してMapEventを作成して実行する。
/// </summary>
public class EVCall : Instruction
{
    string _eventName;
    string _label;
    int _numLabel;

    GameObject ob_call;

    public EVCall(string[] source, string str)
    {
        var strs = str.Split(',');
        _label = strs[0];
        _eventName = strs[1];
        _numLabel = int.Parse(_label.Remove(0, 2));
    }

    /// <summary>
    /// シーン上のMapEventのEventIDを検索して実行する。
    /// コモンイベント目的が多い。
    /// </summary>
    /// <returns></returns>
    public override ControlReq Run()
    {
        ob_call = GameObject.Instantiate(ev_pref.evcallobject[0], com_parent);
        var call = ob_call.GetComponent<CallObject>();
        var evt = GameObject.FindObjectsOfType<MapEvent>().First(e => e.eventID == _numLabel);

        Instruction[] insts = evt.instructions;
        string[] sources = evt.evSources;
        //sources = Instruction.CutFirstInst(sources);
        //insts = Instruction.GetInstructions(sources);

        call.StartInstruction(insts, sources);
        return ControlReq.Call;
    }
}
