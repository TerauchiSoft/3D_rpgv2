﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アイテムを追加する。
/// TODO:現状アイテムがいっぱいの処理は作っていない。
/// </summary>
public class EVItem : Instruction
{
    int sign;
    int itemid;
    int num;
    Item item;

    public EVItem(int[] Args)
    {
        sign = (Args[0] == 0) ? 1 : -1;
        if (Args[1] == 0)
            itemid = Args[2];
        else
            itemid = VariableObject.Instance.GetVal(Args[2]);
        if (Args[3] == 0)
            num = sign * Args[4];
        else
            num = sign * VariableObject.Instance.GetVal(Args[4]);

        // 0(指定がなかった)の場合1にする。
        if (num == 0)
        {
            num = 1;
        }
        //TODO:この実装ではアイテムのIDが行番号と変わらないので
        // 名前で検索する方法に変えるかも。
        item = ItemTable.Instance.GetItemForId(itemid + 1);
        item.ItemNum = num;
    }

    public override ControlReq Run()
    {
        PlayerDatas.Instance.AddItemForAll(item); // TODO:アイテムの追加チェックはやらないといけないね
        return ControlReq.None;
    }
    
}
