﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 全回復させる。
/// 対象と効果を指定できる。
/// </summary>
public class EVFullCare : Instruction
{
    int cid;
    public EVFullCare(int[] args)
    {
        if (args[0] == 0)
            cid = -1;
        else if (args[0] == 1)
            cid = args[1];
        else// if (args[0] == 2)
            cid = VariableObject.Instance.GetVal(args[1]);
    }

    public override ControlReq Run()
    {
        PlayerDatas.Instance.CareFull(cid);
        return ControlReq.None;
    }
    
}
