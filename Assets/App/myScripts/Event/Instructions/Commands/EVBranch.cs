﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Switch文や、Elseの分岐で別れた処理群。
/// 呼ばれるとCallObjectを発行して
/// MapEventを独立して実行させる。
/// </summary>
public class EVBranch : Instruction
{
    /// <summary>
    /// 子のインストラクション。
    /// </summary>
    [SerializeField]
    private Instruction[] instructions;
    [SerializeField]
    private string[] instsource;
    [SerializeField]
    private ChoiceObject.Mode mode;

    // ChoiceObjectComponent
    private ChoiceObject choice;

    public EVBranch(string[] source, int[] args)
    {
        mode = (ChoiceObject.Mode)args[0];
        source = Instruction.CutFirstInst(source);
        instsource = source;
        instructions = Instruction.GetInstructions(source);
    }

    private void init()
    {
        var obs = GameObject.FindGameObjectsWithTag("ChoiceObject");
        choice = obs[obs.Length - 1].GetComponent<ChoiceObject>();
    }

    public override ControlReq Run()
    {
        init();
        // 命令スタート
        choice.StartInstruction(instructions, instsource);
        return ControlReq.Branch;
    }
}
