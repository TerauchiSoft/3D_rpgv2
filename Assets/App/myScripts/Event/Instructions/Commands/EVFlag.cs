﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// フラグの操作。
/// </summary>
public class EVFlag : Instruction
{
    int m = -1;
    int n = -1;

    int flagNo;
    bool value;
    bool isrev;
    public EVFlag(int[] args)
    {
        if (args[0] == 0)
        {
            flagNo = args[1];
        }
        else if (args[0] == 1)
        {
            m = args[1];
            n = args[2];
        }
        else
        {
            flagNo = VariableObject.Instance.GetVal(args[1]);
        }
        value = args[3] == 0;
        isrev = args[3] == 2;
    }

    private void FlagRev(int fno)
    {
        var b = FlagObject.Instance.GetFlag(fno);
        FlagObject.Instance.SetFlag(fno, b ^ b);
    }

    public override ControlReq Run()
    {
        if (m != -1)
        {
            for (int i = m; i <= n; m++)
            {
                if (isrev)
                    FlagRev(i);
                else
                    FlagObject.Instance.SetFlag(i, value);
            }
            return ControlReq.None;
        }
        if (isrev)
            FlagRev(flagNo);
        else
            FlagObject.Instance.SetFlag(flagNo, value);

        return ControlReq.None;
    }
}
