﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲーム内のメソッドを呼び出す。
/// 現状GameMethodCallのprivateメソッドでなければならない。
/// </summary>
public class EVGameMethod : Instruction {
    public string _methodName;

    public EVGameMethod(string methodName) {
        _methodName = methodName;
    }

    /// <summary>
    /// MapEventからMap_GameControl.GotoBattleSystemを実行。
    /// </summary>
    /// <returns></returns>
    public override ControlReq Run() {
        GameMethodCall.RunGameMethod(_methodName);
        return ControlReq.None;
    }
}
