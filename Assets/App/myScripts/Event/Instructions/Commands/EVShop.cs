﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ショップ機能を開始させる。
/// </summary>
public class EVShop : Instruction {

    private string[] strArgs;
    private int[] buyItems;
    private GameObject ob_shop;
    private TextWin textwin;

    public EVShop(string str, int[] args)
    {
        strArgs = str.Split('/');
        buyItems = args;
    }

    // 出現するオブジェクトを管理
    private void init()
    {
        var ob = GameObject.FindGameObjectWithTag("ShopObject");
        if (ob == null)
            ob_shop = GameObject.Instantiate(ev_pref.evshop[0], com_parent);
        else
            ob_shop = ob;

        var ob2 = GameObject.FindGameObjectWithTag("MesWin");
        if (ob2 == null || ob2.activeSelf == false)
        {
            textwin = GameObject.Instantiate(ev_pref.evtext[0], com_parent_overlay).GetComponent<TextWin>();
        }
        else
        {
            textwin = ob2.GetComponent<TextWin>();
        }
    }

    private void shopRun(ShopObject so)
    {
        so.RunInit(strArgs, buyItems, textwin);
    }

    public override ControlReq Run()
    {
        //MapComponentsContainar.DeleteOneComponent(MapComponent.Name.TextWin);
        init();
        var shop = ob_shop.GetComponent<ShopObject>();
        shopRun(shop);
        return ControlReq.Shop;
    }

}
