﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 会話などの文章を表示する。
/// </summary>
public class EVText : Instruction {
    private GameObject textWin;
    [SerializeField]
    private string Str;

    public EVText(string[] srs)
    {
        Vector2 range = Instruction.GetTextInstGroup(0, srs, "SubT");
        string str = GetText(srs, range);
        Str = str;
    }


    private string GetText(string[] srs, Vector2 range)
    {
        string str = "";
        int i = (int)range.x;
        for ( ; i <= (int)range.y; i++)
        {
            string src = Instruction.RemoveFirstUnderbar(srs[i]);
            var inst = InstructionAnalysis.GetInstructionNameAndArgs(src);
            if (inst != null)
                str += inst.Value.ArgStr + '\n';
        }
        if (i > (int)range.x + 1)
            str = str.Remove(str.Length - 1, 1);
        return str;
    }

    public override ControlReq Run()
    {
        init();
        var win = textWin.GetComponentInChildren<TextWin>();
        StartText(win);

        return ControlReq.TextWin;
    }

    private void init()
    {
        var ob = GameObject.FindGameObjectWithTag("MesWin");
        if (ob == null)
            textWin = GameObject.Instantiate(ev_pref.evtext[0], com_parent_overlay);
        else
            textWin = ob;
    }

    private void StartText(TextWin win)
    {
        win.StartTextPlay(Str);
    }

}
