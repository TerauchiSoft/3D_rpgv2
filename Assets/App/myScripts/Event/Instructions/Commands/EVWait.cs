﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ただ待ち続ける命令。
/// </summary>
public class EVWait : Instruction {
    float time;

    public override ControlReq Run() {
        MapEvent.SetWaitAndMethod(time, null);
        return ControlReq.Wait;
    }

    public EVWait(int[] args) {
        time = args[0] * 0.1f; // 10s -> s
    }
}
