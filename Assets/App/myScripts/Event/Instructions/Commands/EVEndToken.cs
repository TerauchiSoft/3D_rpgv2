﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 制御系イベント用のトークン。
/// </summary>
[System.Serializable]
public class EVEndToken : Instruction
{
    public override ControlReq Run()
    {
        return ControlReq.EndToken;
    }
    
}
