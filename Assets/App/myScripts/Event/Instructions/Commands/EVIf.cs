﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// 条件にあったものを実行させる識別子になる。
/// 条件にあったときに実行する命令群を含む。
/// 呼ばれると、CallObjectを発行して
/// MapEventを独立して実行させる。
/// </summary>
public class EVIf : Instruction
{
    /// <summary>
    /// 子のインストラクション。
    /// </summary>
    [SerializeField]
    private GameObject ob_if;
    [SerializeField]
    private Instruction[] instructions;
    [SerializeField]
    private string[] instsource;
    [SerializeField]
    private int[] cond;
    [SerializeField]
    private bool isElseOn = false;
    
    public EVIf(string[] source, int[] args)
    {
        cond = args;
        isElseOn = System.Convert.ToBoolean(args[5]);
        source = Instruction.CutFirstInst(source);
        instsource = source;
        instructions = Instruction.GetInstructions(source);
    }
    

    private bool CheckFlag()
    {
        bool bl = FlagObject.Instance.GetFlag(cond[1]);
        if (cond[2] == 1)
            bl = bl ^ true;

        return bl;
    }

    private bool CheckVariable()
    {
        int v = VariableObject.Instance.GetVal(cond[1]);
        int c;
        if (cond[2] == 0)  // 定数でチェック
        {
           c  = cond[3];
        }
        else               // 変数の値でチェック
        {
            c = VariableObject.Instance.GetVal(cond[3]);
        }

        switch (cond[4])
        {
            case 0: // v == c
                return (v == c);
            case 1:
                return (v >= c);
            case 2:
                return (v <= c);
            case 3:
                return (v > c);
            case 4:
                return (v < c);
            default:
                throw new System.Exception("範囲外の変数範囲分岐ですよ。");
        }
    }

    // cond { 5, Menber, Status, Value }
    private bool CheckStatus()
    {
        var peason = PlayerDatas.Instance.p_chara[cond[1]];
        switch(cond[2])
        {
            case 0:     // メンバーに居るか
                return peason.isJoined;
            //case 1:   // 名前が(未実装)
                //return;
            case 2:     // レベルが~~以上
                return peason.LV >= cond[3];
            case 3:     // HPが~~以上
                return peason.HP >= cond[3];
            //TODO:ID問題を修正しないといけない
            case 4:     // スキル~~を取得
                return peason.Ply_skl.Any(s => s.SkillID == cond[3]);
            case 5:     // アイテム~~を持っている
                return peason.Ply_item.Any(i => i.ItemID == cond[3]);
            //case 6:     // 状態~~ である
            //    return peason. .Ply_st.Equals(cond[3]);
            default:
                return false;
        }
    }

    private bool Checkcond()
    {
        switch (cond[0])
        {
            case 0:// flag
                return CheckFlag();
            case 1:
                return CheckVariable();
            //case 2:
            //    return CheckTimer();
            //case 3:
            //    return CheckMoney();
            //case 4:
            //    return CheckItem();
            case 5:
                return CheckStatus();
            //case 6:
            //    return CheckChara();
            default:
                throw new System.Exception("今の所対応外か、範囲外の条件分岐ですよ。");
        }
    }

    /// <summary>
    /// 条件からElseに飛ぶか実行するか見る。
    /// </summary>
    /// <returns></returns>
    public override ControlReq Run()
    {
        if (Checkcond() == true)
        {
            // IF命令スタート
            ob_if = GameObject.Instantiate(ev_pref.evifobject[0], com_parent);
            ob_if.GetComponent<IfObject>().StartInstruction(instructions, instsource);
            return ControlReq.If;
        }
        else
        {
            // EVElseで命令スタートさせる
            if (isElseOn)
                return ControlReq.Else;
            else
                return ControlReq.None;
        }

    }
}
