﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// イベントを削除し、イベントを強制終了する。
/// </summary>
public class EVDelete : Instruction
{
    public override ControlReq Run()
    {
        RunEventOnTrigger.isEventOnCollide = false;
        return ControlReq.Delete;
    }
}
