﻿using UnityEngine;
using Minimap;

/// <summary>
/// 場所移動をする。
/// 層、階数を0にすればマップの読み直しなしに場所のみ移動。
/// 層、階数を指定すればマップを読み直し、イベントも読み直される。
/// フロア数が15を超えるとアプリを終了する。
/// </summary>
public class EVMovePlace : Instruction
{
    private const float DELAY = 1.0f;
    private bool isVariableValue = false;
    private int _floor = 0;
    private int _hierarchy = 0;
    private Vector2Int vec = Vector2Int.zero;

    public EVMovePlace(int[] Args)
    {
        isVariableValue = (Args[0] == 1);
        if (isVariableValue == false)
        {
            _hierarchy = Args[1];
            _floor = Args[2];
            vec = new Vector2Int(Args[3], Args[4]);
        }
        else
        {
            _hierarchy = VariableObject.Instance.GetVal(Args[1]);
            _floor = VariableObject.Instance.GetVal(Args[2]);
            vec = new Vector2Int(VariableObject.Instance.GetVal(Args[3]),
                                 VariableObject.Instance.GetVal(Args[4]));
        }
    }
    
    public override ControlReq Run()
    {
        // 階数を指定してマップの読み直し
        // フロア数が15を超えるとアプリを終了する。
        if (_floor > 0)
        {
            CameraMove.Instance.Move_Arrow = CameraMove.Arrow.WARP;
            var warpob = GameObject.Instantiate(ev_pref.evwarp[0], com_parent);
            var warp = warpob.GetComponent<WarpObject>();
            warp.SetInit(DELAY, new Vector2Int(vec.x, vec.y), () =>
            {
                if (_floor > 15)
                {
                    #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                    #elif UNITY_STANDALONE
                    Application.Quit();
                    #endif
                    return;
                }

                var nowhierarchy = MapTest_Legacy.Instance.GetMap_HierarchyNum();
                MapTest_Legacy.MapLoad(_hierarchy, _floor);
                MinimapManager.Instance.StepFloor(new Position(vec.x, vec.y, CommonData.Instance.Posd,_floor, _hierarchy));
                Map_Audio.Instance.AudioPlay(_floor - 1, _hierarchy);
                if (_hierarchy != nowhierarchy)
                    Map_EventAnalysisExecution.Instance.LoadMapEvents();
                //Map_WallView.MapObjectViewForIsMapView();
            });
            return ControlReq.Warp;
        }

        // 同じ回数で場所移動し、ロードは起こらない
        Map_WallView.ToLock(true);  // 踏破記録機能のロック
        GameObject.FindGameObjectWithTag("CharaView_CameraMoveObject").transform.position = new Vector3(vec.x, 0, -vec.y);
        var now = CommonData.Instance.IntPos;
        //CommonData.Instance.BeforeFlamePos = new Position(vec.x, vec.y, now.direction, now.hierarchy, now.floor);
        CommonData.Instance.SetBeforeLocation(vec.x, vec.y, now.Direction, now.Floor, now.Hierarchy);
        Map_WallView.ToLock(false); // 踏破記録機能ロック解除
        return ControlReq.Break;
    }
}
