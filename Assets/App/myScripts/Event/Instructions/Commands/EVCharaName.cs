﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// テキスト上部のネームプレートに名前を表示する。
/// </summary>
public class EVCharaName : Instruction
{
    private string Name;

    public override ControlReq Run()
    {
        var ob = GameObject.FindGameObjectWithTag("CharaName");
        if (ob == null)
        {
            ob = GameObject.Instantiate(ev_pref.evcharaname[0], com_parent_overlay);
        }
        ob.GetComponent<CharaName>().SetCharaName(Name);

        return ControlReq.None;
    }

    public EVCharaName(string sarg)
    {
        Name = sarg;
    }
}
