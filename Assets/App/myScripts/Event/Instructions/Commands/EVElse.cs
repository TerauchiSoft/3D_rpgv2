﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Elseに囲まれた命令群。
/// </summary>
public class EVElse : Instruction
{
    /// <summary>
    /// 子のインストラクション。
    /// </summary>
    [SerializeField]
    private Instruction[] instructions;
    [SerializeField]
    private string[] instsource;

    public EVElse(string[] source)
    {
        source = Instruction.CutFirstInst(source);
        instsource = source;
        instructions = Instruction.GetInstructions(source);
    }

    public override ControlReq Run()
    {
        // Else命令スタート
        var ob_if = GameObject.Instantiate(ev_pref.evifobject[0], com_parent);
        var ifobject = ob_if.GetComponent<IfObject>();
        ifobject.StartInstruction(instructions, instSources);
                
        return ControlReq.If;
    }
}
