﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 立ち絵の対象キャラを削除。
/// </summary>
public class EVCharaDel : Instruction
{
    private int[] Args;
    private GameObject charaView;
    public override ControlReq Run()
    {
        charaView = GameObject.FindGameObjectWithTag("CharaView");
        if (charaView == null)
            return ControlReq.None;

        var cview = charaView.GetComponent<CharaView>();
        cview.DeleteCharacter(Args);

        return ControlReq.Chara;
    }

    public EVCharaDel(int[] args)
    {
        Args = args;
    }
    
}
