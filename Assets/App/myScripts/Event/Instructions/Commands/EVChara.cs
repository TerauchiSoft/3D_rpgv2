﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// キャラクターの立ち絵を表示させる。
/// </summary>
public class EVChara : Instruction
{
    private int[] Args;
    private GameObject charaView;
    public override ControlReq Run()
    {
        init();
        var cview = charaView.GetComponent<CharaView>();
        showChara(cview);

        return ControlReq.Chara;
    }

    public EVChara(int[] args)
    {
        Args = args;
    }

    private void init()
    {
        var ob = GameObject.FindGameObjectWithTag("CharaView");
        if (ob == null)
        {
            charaView = GameObject.Instantiate(ev_pref.evcharaview[0], com_parent);
            if (MapComponentsContainar.Instance.ExistComponent("CharaViewCamera") == false)
            {
                var cameratrs = GameObject.Find("PlayerCamera").transform;
                GameObject.Instantiate(ev_pref.evcharaview[1], cameratrs.position, cameratrs.rotation, cameratrs);
            }
        }
        else
        {
            charaView = ob;
        }
    }

    private void showChara(CharaView cview)
    {
        cview.ShowCharacter(Args);
    }

}
