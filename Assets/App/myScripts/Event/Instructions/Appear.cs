﻿/// <summary>
/// イベントの実行条件
/// </summary>
[System.Serializable]
public class Appear : Instruction
{
    public override ControlReq Run()
    {
        throw new System.NotImplementedException();
    }

    // イベントの実行方法
    public enum PlayCond { Investigation = 0, Collision, Auto }
    public PlayCond playCond;

    // イベントタグ マップアイコンの識別など
    public int eventTagNo;

    // 出現条件フラグ
    public bool checkFlag;
    public int flagNo;
    public bool isFlagCondTrue;

    // 出現条件変数
    public bool checkVariable;
    public int valNo;
    public int valLimit;
    public enum Condition { Eq, More, OneOrLess, Greater, Less, Other }
    public Condition condition;

    // 出現条件メンバー
    public bool checkMen;
    public int menNo;

    // 出現フロアとポジション
    public int floor;
    public int posX;
    public int posY;

    /// <summary>
    /// 出現条件セット
    /// </summary>
    public Appear(string[] strs)
    {
        /*
         *  playcond(0)
         *  flag(0,0,0)
	     *  val(0,0,0,0)
	     *  menm(0,0)
         *  position(0,0,0)
         *  eventtag(0)        
         */

        instSources = Instruction.CodeToForInstruction(strs);

        InstructionAnalysis.InstructionVariable?[] vals = new InstructionAnalysis.InstructionVariable?[6];
        vals[0] = InstructionAnalysis.GetInstructionNameAndArgs(strs[0]);
        vals[1] = InstructionAnalysis.GetInstructionNameAndArgs(strs[1]);
        vals[2] = InstructionAnalysis.GetInstructionNameAndArgs(strs[2]);
        vals[3] = InstructionAnalysis.GetInstructionNameAndArgs(strs[3]);
        vals[4] = InstructionAnalysis.GetInstructionNameAndArgs(strs[4]);
        if (strs.Length == 6)
            vals[5] = InstructionAnalysis.GetInstructionNameAndArgs(strs[5]);

        playCond = (PlayCond)vals[0].Value.Args[0];

        if (strs.Length == 6)
            eventTagNo = vals[5].Value.Args[0];

        checkFlag = System.Convert.ToBoolean(vals[1].Value.Args[0]);
        flagNo = vals[1].Value.Args[1];
        isFlagCondTrue = vals[1].Value.Args[2] == 1;

        checkVariable = System.Convert.ToBoolean(vals[2].Value.Args[0]);
        valNo = vals[2].Value.Args[1];
        valLimit = vals[2].Value.Args[2];
        condition = (Condition)vals[2].Value.Args[3];

        checkMen = System.Convert.ToBoolean(vals[3].Value.Args[0]);
        menNo = vals[3].Value.Args[1];

        floor = vals[4].Value.Args[0];
        posX = vals[4].Value.Args[1];
        posY = vals[4].Value.Args[2];
    }
}
