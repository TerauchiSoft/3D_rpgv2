﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// イベントコンテンツの各コマンドの親クラス。
/// </summary>
[SerializeField]
public abstract class Instruction
{
    /// <summary>
    /// 各命令で使用するプレハブが保存された参照。Resourcesのマスターデータ。
    /// </summary>
    protected EventInstructionDatas ev_pref;

    /// <summary>
    /// コンポーネント格納先。
    /// </summary>
    protected Transform com_parent;

    /// <summary>
    /// overlayコンポーネント格納先。
    /// </summary>
    protected Transform com_parent_overlay;

    /// <summary>
    /// 1命令ごとのソース。
    /// </summary>
    [SerializeField]
    protected string[] instSources;
    
    /// <summary>
    /// 子の命令表示用。
    /// </summary>
    public string[] view_c_Insts;

    /// <summary>
    /// イベントの処理によってコマンドに対しての操作リクエスト。
    /// </summary>
    public enum ControlReq
    {
        None, Wait, TextWin, Chara, Choice, Branch, EndToken,
        If, Else, Call, Shop, Warp, MovePlace, Break, Delete,
        Label, LabelJump
    }

    /// <summary>
    /// 子の命令クラスからイベントを実行するため。
    /// </summary>
    public abstract ControlReq Run();


    public Instruction()
    {
        ev_pref = Map_EventAnalysisExecution.EvPref;
        com_parent = Map_EventAnalysisExecution.ComParent; //GameObject.FindGameObjectWithTag("Map_UI").transform;
        com_parent_overlay = Map_EventAnalysisExecution.ComParentOverlay;
    }

    /// <summary>
    /// 命令取得。
    /// 命令の子クラスからも使用。
    /// </summary>
    /// <param name="eventsrc"></param>
    /// <returns></returns>
    public static Instruction[] GetInstructions(string[] eventsrc)
    {
        // イベントコマンドセット
        List<Instruction> instdat = new List<Instruction>();
        string[] evs = Instruction.CodeToForInstruction(eventsrc);
        for (int i = 0; i < evs.Length; i++)
        {
            string inst = RemoveFirstUnderbar(evs[i]);
            // イベント名
            var Instdat = InstructionAnalysis.GetInstructionNameAndArgs(inst);
            if (Instdat == null) continue;
            // 命令単体ごとの抜き出す範囲
            var range = Instruction.GetInstructionGroupRangeNum(i, evs, Instdat.Value.InstName);
            i = (int)range.Value.y;
            // 抜き出したソース
            string[] addsource = evs.Skip((int)range.Value.x).Take((int)(range.Value.y - range.Value.x) + 1).ToArray();

            instdat.Add(Instruction.GetInst(Instdat, addsource));
        }
        return instdat.ToArray();
    }

    public static string RemoveFirstUnderbar(string inst)
    {
        string str = null;
        for(int i = 0; i < inst.Length; i++)
        {
            if(inst[i] != '_')
            {
                str = inst.Substring(i, inst.Length - i);
                return str;
            }
        }
        return inst;
    }

    /// <summary>
    /// 命令の情報から各命令のクラスを返す。
    /// </summary>
    /// <param name="instDat"></param>
    /// <param name="sources"></param>
    public static Instruction GetInst(InstructionAnalysis.InstructionVariable? instDat, string[] srs)
    {
        // instDatの命令名を見て任意の子クラスを作成するメソッドからInstを獲得する。
        Instruction inst = GetInstSubClass(instDat, srs);
        if (inst == null)
            return null;

        inst.instSources = srs;

        return inst;
    }

    /// <summary>
    /// 命令名から命令の枠組みを作成。
    /// </summary>
    /// <param name="instName">命令名</param>
    /// <param name="srs">ソース。If文などは更にこのデータから命令を登録。</param>
    /// <returns></returns>
    private static Instruction GetInstSubClass(InstructionAnalysis.InstructionVariable? instVal, string[] srs)
    {
        switch(instVal.Value.InstName)
        {
            case "Text":
                return new EVText(srs);
            case "Choice":
                return new EVChoice(instVal.Value.ArgStr, instVal.Value.Args);
            case "Call":
                return new EVCall(srs, instVal.Value.ArgStr);
            case "Branch":
                return new EVBranch(srs, instVal.Value.Args);
            case "EndChoice":
                return new EVEndToken();
            case "If":
                return new EVIf(srs, instVal.Value.Args);
            case "Else":
                return new EVElse(srs);
            case "Endif":
                return new EVEndToken();
            case "Flag":
                return new EVFlag(instVal.Value.Args);
            case "Variable":
                return new EVVariable(instVal.Value.Args);
            case "FullCare":
                return new EVFullCare(instVal.Value.Args);
            case "PlayBGM":
                return new EVBGM(instVal.Value.ArgStr, instVal.Value.Args);
            case "Item":
                return new EVItem(instVal.Value.Args);
            case "PlaySE":
                return new EVSE(instVal.Value.ArgStr, instVal.Value.Args);
            case "CharaView":
                return new EVChara(instVal.Value.Args);
            case "CharaDelete":
                return new EVCharaDel(instVal.Value.Args);
            case "CharaFront":
                return new EVCharaFront(instVal.Value.Args);
            case "CharaBack":
                return new EVCharaBack(instVal.Value.Args);
            case "CharaName":
                return new EVCharaName(instVal.Value.ArgStr);
            case "Shop":
                return new EVShop(instVal.Value.ArgStr, instVal.Value.Args);
            case "MovePlace":
                return new EVMovePlace(instVal.Value.Args);
            case "ScreenTone":
                return new EVScreenTone(instVal.Value.Args);
            case "Break":
                return new EVBreak();
            case "Delete":
                return new EVDelete();
            case "GameMethod":
                return new EVGameMethod(instVal.Value.ArgStr);
            case "Label":
                return new EVLabel(instVal.Value.Args);
            case "LabelJump":
                return new EVLabelJump(instVal.Value.Args);
            case "Wait":
                return new EVWait(instVal.Value.Args);
            default:
                return new EVNull();
        }
    }

    /// <summary>
    /// グループ命令(If Choice)の先頭行の削除。
    /// </summary>
    /// <returns></returns>
    public static string[] CutFirstInst(string[] inst)
    {
        string[] strs = new string[inst.Length - 1];
        for(int i = 0; i < strs.Length; i++)
        {
            strs[i] = inst[i + 1];
        }
        return strs;
    }


    /// <summary>
    /// インデント後の命令を見て、選択された一つの命令群の範囲を返す。返り値(idx, からridx)
    /// </summary>
    /// <param name="idx"></param>
    public static Vector2? GetInstructionGroupRangeNum(int idx, string[] Codes, string instName)
    {
        if (instName == "Text")
            return GetTextInstGroup(idx, Codes, "SubT");
        else
            return GetBranchInstGroup(idx, Codes);

    }
    /// <summary>
    /// インデント後の命令を見て、選択された一つの命令群の範囲を返す。返り値(idx, からridx)
    /// </summary>
    /// <param name="idx"></param>
    public static Vector2 GetTextInstGroup(int idx, string[] Codes, string ckinst)
    {
        string inst = RemoveFirstUnderbar(Codes[idx]);
        var Instdat = InstructionAnalysis.GetInstructionNameAndArgs(inst);
        string sarg = Instdat.Value.ArgStr;

        int ridx = idx + 1;
        while (ridx < Codes.Length)
        {
            try
            {
                inst = RemoveFirstUnderbar(Codes[ridx]);
                Instdat = InstructionAnalysis.GetInstructionNameAndArgs(inst);
            }
            catch
            {
                throw new System.Exception("イベントの解析エラー。");
            }
            if (Instdat != null && Instdat.Value.InstName == ckinst)
                ridx++;
            else
                break;
        }
        ridx--;
        return new Vector2(idx, ridx);
    }

    private static Vector2 GetBranchInstGroup(int idx, string[] Codes)
    {
        int stck = 0;
        int ridx = idx;
        if (idx == Codes.Length - 1)
            return new Vector2(idx, ridx);

        // Indexofではだめ。
        for (int i = 0; i < Codes[idx].Length; i++)
        {
            if (Codes[idx][i] != '_')
            {
                stck = i;
                break;
            }
        }

        int seachidx = ridx + 1;
        string str = Codes[seachidx];

        while (Codes.Length > seachidx && str.Length > stck && str[stck] == '_')
        {
            seachidx++;
            str = Codes[seachidx];
        }
        ridx = seachidx - 1;
        return new Vector2(idx, ridx);
    }


    /// <summary>
    /// sourceを読み取り用のデータに変換
    /// </summary>
    public static string[] CodeToForInstruction(string[] strs)
    {
        // \tを省く
        strs = RemoveTab(strs);
        // インデント用の全角スペースを追加する。
        strs = AddUnderbar(strs);

        return strs;
    }

    /// <summary>
    /// タブ文字の消去
    /// </summary>
    /// <param name="strs"></param>
    private static string[] RemoveTab(string[] strs)
    {
        for (int i = 0; i < strs.Length; i++)
            strs[i] = CollectionTool.RemoveChar(strs[i], '\t');

        return strs;
    }

    /// <summary>
    /// 条件分岐で_の追加
    /// </summary>
    /// <param name="strs"></param>
    public static string[] AddUnderbar(string[] strs)
    {
        int stck = 0;

        for (int i = 0; i < strs.Length; i++)
        {
            int h = 0;
            string c;
            if (i < strs.Length)
            {
                c = CheckWithTabString(strs[i]);
                if ((c == "Else") || (c == "Endif") || (c == "Branch") || (c == "EndCho"))
                {
                    h++;
                }
            }

            for (; h < stck; h++)
            {
                strs[i] = "_" + strs[i];
            }

            // 今の行を見てstckを変更。
            c = CheckWithTabString(strs[i]);
            if (c != null)
            {
                switch (c)
                {
                    case "If":
                    case "Choice":
                        stck++;
                        break;
                    case "Endif":
                    case "EndCho":
                        stck--;
                        break;
                    default:
                        break;
                }
            }
        }
        return strs;
    }

    /// <summary>
    /// 全角アンダーバーを追加する文字列をチェック
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private static string CheckWithTabString(string str)
    {
        str = CollectionTool.RemoveChar(str, '_');
        string[] words = { "If", "Else", "Endif", "Branch", "EndCho", "Choice" };
        for (int i = 0; i < words.Length; i++)
        {
            string s = (str.Length > 6) ? str.Substring(0, 6) : str;
            if (s.IndexOf(words[i]) != -1)
                return words[i];
        }
        return null;
    }


}
