﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EventSign : Singleton<EventSign> {
    Animator anim;

    private void Start()
    {
        instance = this;
        anim = GetComponentInChildren<Animator>();
        gameObject.SetActive(false);
    }

    private bool isAp;
    public void Appear()
    {
        Debug.Log("Appear");
        gameObject.SetActive(true);
        anim.Play("Ap", 0);
        isAp = true;
    }

    public void Fadeout()
    {
        if (isAp)
        {
            gameObject.SetActive(true);
            anim.Play("Fadeout", 0);
            isAp = false;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
