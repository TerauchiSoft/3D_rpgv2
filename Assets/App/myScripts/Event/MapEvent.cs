﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Map_EventDataの上で動くマップイベント。
/// 分岐命令では他の
/// イベント単位で、イベントコンテンツ群を含む。
/// </summary>
public class MapEvent : MonoBehaviour
{
    public bool isRunning = false;

    private MapTest map;
    private MapTest_Legacy l_map;
    private static Vector2Int mapSize = new Vector2Int(-1, -1);
    private static byte[] evMap = new byte[0];

    //　イベント条件監視
    public RunEventConditionChacker RunEventConditionChacker;

    // 調べるイベントの決定キー監視
    public RunEventOnPushEnter RunEventOnPushEnter;

    /// <summary>
    /// 実行中イベントから派生したネストイベント。スタックのように新しいBranchが生成されたらコンポーネントから呼ぶ。
    /// </summary>
    public static List<MapEvent> pb_childEvent = new List<MapEvent>();

    /// <summary>
    /// デバッグ用に子のEventDataObjectを表示。debug
    /// </summary>
    [SerializeField]
    private string[] childEventStrs;

    /// <summary>
    /// デバッグ用
    /// </summary>
    /// <returns></returns>
    public string[] getChildEventStrs() {
        string[] strs = new string[pb_childEvent.Count];
        for (int i = 0; i < pb_childEvent.Count; i++) {
            strs[i] = pb_childEvent[i].ToString();
        }
        return strs;
    }

    /// <summary>
    /// イベントのネスト。
    /// </summary>
    public int eventNest = 0;

    /// <summary>
    /// コンポーネントで生成されたイベント。
    /// </summary>
    public bool isBranch = false;

    /// <summary>
    /// 選択肢の表示で生成されたオブジェクト。
    /// </summary>
    public static List<ChoiceObject> choiceObjects = new List<ChoiceObject>();

    /// <summary>
    /// EVIfで生成されたオブジェクト。
    /// </summary>
    public static List<IfObject> ifObjects = new List<IfObject>();


    public class Label
    {
        public int _labelNo = -1;    // はじめに設定
        public MapEvent mapEvent = null;// ラベル設置後のInstructionOperationで設定
        public int _labelIdx = -1;   // ラベル設置後のInstructionOperationで設定
        public bool _doJump = false;    // ジャンプ時に設定 InstructionOperationで値をみてジャンプする
        public Label(int lno) {
            _labelNo = lno;
        }
        public void SetEventAndIdx(MapEvent me, int idx) {
            // 二重登録禁止
            if (me == mapEvent && idx == _labelIdx)
                return;

            mapEvent = me; _labelIdx = idx;
            return;
        }
        public void JumpIdx() {
            // インデックスを登録して、次のコマンドへ移動可能にする。
            mapEvent.instIdx = _labelIdx;
            mapEvent.req = Instruction.ControlReq.None;
            _doJump = false;
        }
    }
    /// <summary>
    /// ラベルの設置のデータ。
    /// EVLabelから追加される。
    /// </summary>
    public static List<Label> _labels = new List<Label>();

    /// <summary>
    /// イベントのタイマー専用の汎用ウェイト。
    /// 各イベントから設定する。
    /// </summary>
    private static int WaitCount;

    /// <summary>
    /// イベントのタイマー専用の汎用メソッド。
    /// WaitCount == 0で実行される。
    /// </summary>
    private static System.Action WaitAction;

    public static void SetWaitAndMethod(float sec, System.Action act = null) {
        WaitCount = (int)(sec * 60);
        WaitAction = act;
    }

    /// <summary>
    /// コンポーネントで生成されたイベントの削除許可。
    /// </summary>
    public bool AllowDelete = false;

    /// <summary>
    /// イベントID
    /// </summary>
    public int eventID;
    /// <summary>
    /// イベントの名前(デバッグ用)
    /// </summary>
    public string eventName = "";

    // 命令、出現ソース。
    [SerializeField]
    public string[] apSource;
    [SerializeField]
    public string[] evSources;

    /// <summary>
    /// イベント出現条件。
    /// イベントの配置に必要。
    /// </summary>
    public Appear appearConditions;

    /// <summary>
    /// イベントコマンド。
    /// Choiceなどから直接セットして実行することも可能。
    /// </summary>
    public Instruction[] instructions;

    /// <summary>
    /// イベントコマンドInspector表示用。
    /// </summary>
    public string[] view_instructions;

    private void Awake() {
        map = FindObjectOfType<MapTest>();
        l_map = FindObjectOfType<MapTest_Legacy>();
    }
    
    /// <summary>
    /// EVBreak命令
    /// </summary>
    public static void ForceBreak() {
        MapComponentsContainar.Instance.DeleteAll();
        for (int i = pb_childEvent.Count - 1; i >= 0; i--) {
            if (pb_childEvent[i] != null)
                pb_childEvent[i].EventEndProcessing();
        }
        pb_childEvent.Clear();
    }

    /// <summary>
    /// EVDelete命令
    /// </summary>
    public static void ForceDelete() {
        MapComponentsContainar.Instance.DeleteAll();
        for (int i = pb_childEvent.Count - 1; i >= 0; i--)
        {
            if (pb_childEvent[i] != null)
            {
                if (pb_childEvent[i].isBranch)
                {
                    pb_childEvent[i].EventEndProcessing();
                }
                else
                {
                    var ob = pb_childEvent[i].gameObject;
                    pb_childEvent[i].EventEndProcessing();
                    Destroy(ob);
                }
            }
        }
        pb_childEvent.Clear();
    }

    /// <summary>
    /// EVBranch->ChoiceObject, EVIf->JudgObjectから呼び出す時に使う。
    /// </summary>
    /// <param name="inst"></param>
    public void RunEventFromComponents(Instruction[] inst) {
        instructions = inst;
        view_instructions = new string[inst.Length];
        for (int i = 0; i < inst.Length; i++) {
            view_instructions[i] = inst[i].ToString();
        }
        RunEvent();
    }


    /// <summary>
    /// 2次ソースから出現条件と命令を保存。
    /// </summary>
    public void MapEventDataset(int eventid, string eventTitle, List<string> apcont, List<string> evcont) {
        eventID = eventid;
        eventName = eventTitle;
        apSource = apcont.ToArray();
        evSources = evcont.ToArray();

        // 出現条件セット
        appearConditions = new Appear(apSource);

        instructions = Instruction.GetInstructions(evSources);
        view_instructions = GetInstName(instructions);
        transform.position = new Vector3(appearConditions.posX, 0, -appearConditions.posY);
    }


    private string[] GetInstName(Instruction[] insts) {
        List<string> strs = new List<string>();
        foreach (var a in insts) {
            if (a != null)
                strs.Add(a.ToString());
            else
                strs.Add("null");
        }
        return strs.ToArray();
    }

    public enum EventCont
    { Start, Next, Wait, EndToken, End, Delete }

    [SerializeField]
    private EventCont phese;  // イベントのウェイト制御
    [SerializeField]          // イベントコマンドからのウェイトリクエスト
    Instruction.ControlReq req = Instruction.ControlReq.None;
    [SerializeField]
    private int befInstIdx = -1;
    [SerializeField]
    private int instIdx = -1;
    /// <summary>
    /// MapToBattleから行呼び出し。
    /// 初めに実行される。
    /// </summary>
    public void RunEvent() {
        Debug.Log("RunEvent");
        isRunning = true;
        PlayerController.Instance.isMenuAllow = false;
        PlayerController.Map_EventConfigSet();
        // Map_UI.StartEvent(); 旧版
        TextAreaStatic.StartEvent();
        Map_WallView.HideMapObject();   // マップ表示の一次消去

        befInstIdx = -1;
        instIdx = 0;
        phese = EventCont.Start;
        pb_childEvent.Add(this);
        eventNest = pb_childEvent.Count - 1;
    }

    public List<MapEvent> _sub; // デバッグ用
    private void Update() {
        _sub = MapEvent.pb_childEvent;
        //childEventStrs = getChildEventStrs();
        if (instIdx == -1 || CameraMove.Instance.Move_Arrow != CameraMove.Arrow.NONE)
            return;

        while ((phese == EventCont.Start || phese == EventCont.Next) && befInstIdx != instIdx)
        {
            // instructionsの端
            if (instIdx >= instructions.Length)
            {
                EventEndProcessing();
                return;
            }

            req = RunEventInterpret(instIdx);
            phese = GetPheseForReq(req);
            phese = AddInstIdx(phese);
        }

        if (phese != EventCont.Next && phese != EventCont.End) {
            // Nextになるまで繰り返す
            phese = GetPheseForReq(req);
            phese = AddInstIdx(phese);
        }

    }


    private Instruction.ControlReq RunEventInterpret(int idx) {
        var rq = Instruction.ControlReq.None;
        if (idx < instructions.Length && instructions[idx] != null) {
            print(this.ToString() + ":Do:" + idx + instructions[idx].ToString());
            rq = instructions[idx].Run();
        } else {
            print(this.ToString() + "Error" + idx + instructions[idx].ToString());
            rq = Instruction.ControlReq.None;
        }
        return rq;
    }

    private EventCont GetPheseForReq(Instruction.ControlReq rq) {
        return InstructionOperation(rq);
    }


    void CheckIdxAndEndProcess() {
        if (instIdx >= view_instructions.Length) {
            EventEndProcessing();
        }
    }

    private EventCont AddInstIdx(EventCont p) {
        if (p == EventCont.Next) {
            befInstIdx = instIdx;
            instIdx++;
            return p;
        } else if (p == EventCont.EndToken) {
            req = Instruction.ControlReq.None;
            befInstIdx = instIdx;

            while (view_instructions[instIdx] != "EVEndToken") {
                instIdx++;
                CheckIdxAndEndProcess();
            }
            return EventCont.Next;
        } else if (p == EventCont.End) {
            EventEndProcessing();
        } else if (p == EventCont.Delete) {
            ForceDelete();
        }
        return p; // 設定なしの場合
    }

    private void EventEndProcessing() {
        Debug.LogError("EventEndProcessing");
        req = Instruction.ControlReq.None;
        phese = EventCont.End;
        instIdx = -1;
        befInstIdx = -1;
        isRunning = false;
        if (isBranch == true)
        {
            Debug.Log("EventEndProcessing isBranch End");
            pb_childEvent.Remove(this);
            Destroy(gameObject);
        }
        else
        {
            choiceObjects.Clear();
            ifObjects.Clear();
            _labels.Clear();
            Map_Audio.Instance.ReplayBeforeAudio();
            MapComponentsContainar.Instance.DeleteAll();
            PlayerController.Map_InputConfigSet();
            Map_WallView.MapObjectViewForIsMapView();   // 設定値の通りマップを表示。
            EventSign.Instance.Fadeout();
            pb_childEvent.Clear();

            Debug.Log("EventEndProcessing End");
        }
    }

    /// <summary>
    /// 命令のウェイト、インデックスを飛ばす、順序などの指令を返す。
    /// </summary>
    private EventCont InstructionOperation(Instruction.ControlReq req) {
        switch (req) {
            // 文章の表示
            case Instruction.ControlReq.TextWin:
                if (!TextWin.isCanSendMes)
                    return EventCont.Wait;
                else
                    return EventCont.Next;
            // キャラの表示
            case Instruction.ControlReq.Chara:
                if (CharaView.isWaiting)
                    return EventCont.Wait;
                else
                    return EventCont.Next;
            // 選択肢の処理
            case Instruction.ControlReq.Choice:
                if (choiceObjects[choiceObjects.Count - 1].isChoicing)
                    return EventCont.Wait;
                else {
                    int jmp = (int)choiceObjects[choiceObjects.Count - 1].JumpBranch;
                    instIdx += jmp;
                    return EventCont.Next;
                }
            // 選択肢の処理 分岐先
            case Instruction.ControlReq.Branch:
                if (eventNest + 1 < pb_childEvent.Count && pb_childEvent[eventNest + 1] != null)
                    return EventCont.Wait;
                else {
                    return EventCont.EndToken;
                }
            // 条件分岐
            case Instruction.ControlReq.If:
                if (eventNest + 1 < pb_childEvent.Count && pb_childEvent[eventNest + 1] != null)
                    return EventCont.Wait;
                else {
                    return EventCont.EndToken;
                }
            // 条件分岐 Else
            case Instruction.ControlReq.Else: // Ifの次
                return EventCont.Next;
            // イベント呼び出し
            case Instruction.ControlReq.Call:
                if (eventNest + 1 < pb_childEvent.Count && pb_childEvent[eventNest + 1] != null)
                    return EventCont.Wait;
                else {
                    return EventCont.Next;
                }
            // 条件分岐 Endif 選択肢の処理 EndChoice
            case Instruction.ControlReq.EndToken:
                return EventCont.Next;
            // お店の処理
            case Instruction.ControlReq.Shop:
                if (ShopObject.isShoping)
                    return EventCont.Wait;
                else
                    return EventCont.Next;
            // ワープ(未使用)
            case Instruction.ControlReq.Warp:
                if (WarpObject.isWarping == true)
                    return EventCont.Wait;
                else
                    return EventCont.End;
            // 場所移動
            case Instruction.ControlReq.MovePlace:
            // イベント中断
            case Instruction.ControlReq.Break:
                return EventCont.End;
            // イベントの一時削除
            case Instruction.ControlReq.Delete:
                return EventCont.Delete;
            // シーン上のイベントオブジェクトによる処理
            case Instruction.ControlReq.Wait:
                if (WaitCount > 0) {
                    WaitCount--;
                    return EventCont.Wait;
                }
                if (WaitAction != null) {
                    WaitAction.Invoke();
                    WaitAction = null;
                }
                return EventCont.Next;
            // ラベルの設置
            case Instruction.ControlReq.Label:
                _labels.Last().SetEventAndIdx(this, instIdx);
                req = Instruction.ControlReq.None;
                return EventCont.Next;
            case Instruction.ControlReq.LabelJump:
                Label lbl;
                try {
                    lbl = _labels.First(l => l._doJump == true);
                } catch {
                    print("ジャンプするラベルがありません。");
                    throw;
                }
                lbl.JumpIdx();
                // 他のマップイベントに行くとき
                if (lbl.mapEvent != this) {
                    EventEndProcessing();
                }
                req = Instruction.ControlReq.None;
                return EventCont.Next;
            // 次へ進む
            case Instruction.ControlReq.None:
            default:
                return EventCont.Next;
        }
    }

    /// <summary>
    /// マップ上のイベントオブジェクト表示を切り替える。
    /// 接触型、調べる方で分岐する。
    /// </summary>
    /// <returns>(イベントを実行したか, イベントサインを表示するか)</returns>
    public (bool, bool) OnCollision()
    {
        Vector3Int evPos = new Vector3Int(appearConditions.posX, appearConditions.posY, appearConditions.floor);
        Vector3Int nowPos = CommonData.Instance.IntPos.XYF;
        if (!gameObject.activeSelf || !RunEventConditionChacker.CheckEventCondition() || evPos != nowPos)
        {
            RunEventOnPushEnter.SetActive(false);
            return (false, false);
        }

        var playCond = appearConditions.playCond;
        if (playCond == Appear.PlayCond.Auto)
        {
            Debug.LogError("Appear.PlayCond.Auto");
            // 即実行
            RunEvent();
            // イベントサインを隠す
            EventSign.Instance.Fadeout();
            return (true, false);
        }
        else if (playCond == Appear.PlayCond.Collision && RunEventConditionChacker.CheckEventCondition())
        {
            // 即実行
            RunEvent();
            // イベントサインを隠す
            EventSign.Instance.Fadeout();
            return (true, false);
        }
        else if (playCond == Appear.PlayCond.Investigation && RunEventConditionChacker.CheckEventCondition())
        {
            Debug.Log("Appear.PlayCond.Investigation");
            RunEventOnPushEnter.SetActive(true);
            // イベントサインを表示する
            EventSign.Instance.Appear();
            return (false, true);
        }
        return (false, false);
    }
}
