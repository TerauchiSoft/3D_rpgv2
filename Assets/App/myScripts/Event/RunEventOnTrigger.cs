﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// イベントの接触、決定ボタンでの実行元
/// RunEventOnPushEnterとペアで使用する。
/// イベントの実行方法での分岐もする。
/// </summary>
public class RunEventOnTrigger : MonoBehaviour {
    public static bool isEventOnCollide = false;
    private MapEvent ev;
    GameObject runEnterObj;
	// Use this for initialization
	void Awake ()
    {
        ev = GetComponentInParent<MapEvent>();
        runEnterObj = transform.GetChild(0).gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {   
        if (ev == null)
            ev = GetComponentInParent<MapEvent>();

        if (other.tag == "MainCamera")
        {
            if (ev.appearConditions.playCond == Appear.PlayCond.Collision)
            {
                isEventOnCollide = true;
                ev.RunEvent();
            }
            else
            {
                RunEventOnPushEnter.isCollide = true;
                EventSign.Instance.Appear();
                runEnterObj.SetActive(true);
            }
        }
    }

    private void OnDestroy()
    {
        RunEventOnPushEnter.isCollide = false;
        isEventOnCollide = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            RunEventOnPushEnter.isCollide = false;
            isEventOnCollide = false;

            EventSign.Instance.Fadeout();
            runEnterObj.SetActive(false);
        }
    }
}
