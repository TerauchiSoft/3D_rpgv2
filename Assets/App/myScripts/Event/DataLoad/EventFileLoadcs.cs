﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class EventFileLoadcs
{
    //読み込み関数
    public static string[] ReadFile(string Directory, string fnameWithExtension)
    {
        //ファイル情報取得
        FileStream variable = new FileStream(Application.streamingAssetsPath + "/" + Directory + "/" + fnameWithExtension, FileMode.Open, FileAccess.Read);
        StreamReader streamReader = new StreamReader(variable, Encoding.UTF8);

        List<string> strs = new List<string>();
        while (!streamReader.EndOfStream)
            strs.Add(streamReader.ReadLine());

        streamReader.Dispose();
        variable.Dispose();

        return strs.ToArray();
    }
}