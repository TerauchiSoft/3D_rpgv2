﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;
using MapVariable;

/// <summary>
/// バトルの呼び出し部
/// </summary>
public class MapToBattle : MonoBehaviour
{
    static MapToBattle instance;
    public static MapToBattle Instance { get { if (instance == null) instance = FindObjectOfType<MapToBattle>(); return instance; } }
    /*~----------------------------------/
    :                                    :
    :           外部のクラス             :
    :                                    :
    /~----------------------------------*/

    // 共通データ
    private CommonData cd;

    // マップデータ
    private MapTest map;

    // マップデータ
    private MapTest_Legacy l_map;

    // イベントデータ
    private Map_EventAnalysisExecution eventdatas;

    // コントローラー
    private PlayerController plycnt;

    /*~----------------------------------/
    :                                    :
    :        内部のクラス 変数           :
    :                                    :
    /~----------------------------------*/
    private const int MAX_Probability = 100;

    // イベント
    private MapEvent[] events;

    // イベントマップデータ
    //private List<Byte> iv_Map;

    // キャラ分岐用の乱数取得
    private System.Random rd = new System.Random();
    private int val_rand;
    
    // Use this for initialization
    void Start ()
    {
        // マップデータ
        map = GetComponent<MapTest>();
        l_map = GetComponent<MapTest_Legacy>();

        // イベントデータ
        eventdatas = gameObject.GetComponent<Map_EventAnalysisExecution>();

        // 共通データ
        cd = CommonData.Instance;

        // 前行動時の位置初期化
        cd.SetBeforeLocation(-1, -1, 0, 1, 1);
        
        // コントローラー
        plycnt = PlayerController.Instance;
        plycnt.isMoveAllow = true;

        // 敵マップレイヤー、敵テーブルリスト読み込み
        ReadBattleLayer();
    }

    /*~----------------------------------/
    :                                    :
    :        メインのメソッド            :
    :                                    :
    /~----------------------------------*/
    // 乱数更新
    private void Update()
    {
        val_rand = rd.Next() % 100;
        // マップイベント
        if (events == null)
        {
            events = FindObjectsOfType<MapEvent>();
            PlayerController.Map_InputConfigSet();
            Map_WallView.MapObjectViewForIsMapView();
        }
    }

    float max = 0.3f;
    async Task<bool> gotoBattleFirst()
    {
        Map_Sound.Instance.PlayStartBattle();
        await Task.Delay(100);

        _blackMask.DOColor(new Color(1, 1, 1, 0.2f), 0.2f);
        await Task.Delay(200);

        _blackMask.DOColor(new Color(0,0,0,0.8f), 0.44f);
        _noiseMeshMap.transform.DOPunchPosition(Vector3.one * 0.1f, 0.44f);
        _noiseMeshMap.material.DOFloat(max, "_DistortionPower", 0.44f);
        _noiseMeshBattle.material.DOFloat(max, "_DistortionPower", 0.44f);

        await Task.Delay(440);

        _noiseMeshMap.material.DOFloat(max/2f, "_DistortionPower", 0.08f);
        _noiseMeshBattle.material.DOFloat(max/2f, "_DistortionPower", 0.08f);

        await Task.Delay(80);

        // AudioListener切り替え
        CameraMove.Instance.SetAudioListener(false);

        return true;
    }

    string enemygroup;
    /// <summary>
    /// 敵グループの登録。
    /// </summary>
    void RegistEnemy()
    {
        enemygroup = MapVariableManager.Instance.GetBattleGroup();
        Debug.Log("enemygroup : " + enemygroup);
        Battle.PrefabLoader.Instance.RegistEnemies(enemygroup);
    }

    // バトルシーンに移行
    public SpriteRenderer _blackMask;
    public MeshRenderer _noiseMeshMap;
    public MeshRenderer _noiseMeshBattle;
    public async Task<bool> GotoBattleScene()
    {
        // 地図アウトビュー
        FrontUIView.Instance.DisplayAnime(false);

        // 動作禁止
        plycnt.isMoveAllow = false;

        // メニュー禁止
        plycnt.isMenuAllow = false;

        _noiseMeshMap.gameObject.SetActive(true);
        _noiseMeshBattle.gameObject.SetActive(true);

        MapUI.Instance.Battle(true);
        CameraMove.Instance.enabled = false;
        // 画面移行
        await gotoBattleFirst();
        
        // フェーズ遷移
        PheseManager.Instance.ChangeToGameMode("Battle", () =>
        {
            /*動作許可*/
            plycnt.isMoveAllow = true;
            _blackMask.DOFade(0f, 0.1f);
        });

        return true;
    }

    /// <summary>
    /// バトルキャラ取得
    /// TODO:とりあえず仮実装。
    /// </summary>
    /// <returns></returns>
    //string GetEnemyGroup(Position po)
    //{
        //var i = val_rand % 3;
        //// まだまだ途中
        //switch (po.Floor)
        //{
        //    case 1:
        //        if (i == 0) { return "FlowerSlime"; }
        //        if (i == 1) { return "FlowerSlime"; }
        //        if (i == 2) { return "FlowerSlime"; }
        //        return "";
        //    case 2:
        //        if (i == 0) { return "Wolf"; }
        //        if (i == 1) { return "Wolf"; }
        //        if (i == 2) { return "FlowerSlime"; }
        //        return "";
        //    case 3:
        //        if (i == 0) { return "Wolf2"; }
        //        if (i == 1) { return "DragonbabyWolf"; }
        //        if (i == 2) { return "Dragonbaby2"; }
        //        return "";
        //    case 4:
        //        if (i == 0) { return "Wolf2"; }
        //        if (i == 1) { return "Dragonbaby2"; }
        //        if (i == 2) { return "Dragonbaby2"; }
        //        return "";
        //    case 5:
        //        return "Dragonbaby";
        //    /*
        //    if (i == 0) { PlayerDatas.Instance.p_charaSet(3); return "Wolf"; }
        //    if (i == 1) { PlayerDatas.Instance.p_charaSet(1); return "FlowerSlime"; }
        //    if (i == 2) { PlayerDatas.Instance.p_charaSet(20); return "Harpy"; }
        //    if (i == 3) { PlayerDatas.Instance.p_charaSet(30); return "Yashigani"; }
        //    if (i == 4) { PlayerDatas.Instance.p_charaSet(30); return "Alraune"; }
        //    return "";
        //    */
        //    default:
        //        return "Wolf";
        //}
    //}

    /// <summary>
    /// バトルに移行するか判定・エンカウント
    /// </summary>
    public void ToBattle()
    {
        if (plycnt.isMoveAllow == false || RunEventOnPushEnter.isCollide == true || RunEventOnTrigger.isEventOnCollide == true)
            return;

        // 前回と同一座標ならreturn
        if (cd.IntPos.X == cd.BeforeFlamePos.X &&
            cd.IntPos.Y == cd.BeforeFlamePos.Y)
        {
            return;
        }
        
        // エンカウント判定
        if (isBattle && (isBattleEncount100 || MapVariableManager.Instance.EnemyCumulativeProbability >= MAX_Probability))
        {
            BattleEncounter();
            MapVariableManager.Instance.ResetEnemyCumulativeProbability();
        }
        return;
    }

    [SerializeField]
    private bool isBattle = true;
    [SerializeField]
    private bool isBattleEncount100 = false;
    [SerializeField]
    private bool isEvent = false;
    /// <summary>
    /// バトルシーンに移行する
    /// </summary>
    public void BattleEncounter()
    {
        // データに現在座標をSave
        cd.SetReturnLocation(cd.IntPos);

        // 敵キャラリストから敵グループ(prefab_Name)を読み込み
        RegistEnemy();
        cd.BattleEnemyGroupName = enemygroup;

        // バトルシーンに移行開始
        GotoBattleScene();
    }

    /// <summary>
    /// マップ上の敵グループレイヤーを読み込む
    /// </summary>
    private void ReadBattleLayer()
    {

    }
}
