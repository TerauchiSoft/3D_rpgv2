﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventInfo
{
    public bool isMainEvent;
    public int eventID;
    public string eventTitle;
    public string eventExplanatory;
    public bool isDone;
}
