﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

/// <summary>
/// MapEventからGameMethodが呼ばれたら
/// リフレクションで関数を探して実行する。
/// </summary>
public partial class GameMethodCall : MonoBehaviour {
    private static GameMethodCall _gameMethodCall;

	// Use this for initialization
	void Awake () {
        _gameMethodCall = this;
	}

    public static void RunGameMethod(string methodName) {
        _gameMethodCall.Run(methodName);
    }

    private void Run(string methodName) {
        Type type = _gameMethodCall.GetType();
        var methodInfo = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
        methodInfo.Invoke(_gameMethodCall, null);
    }
}
