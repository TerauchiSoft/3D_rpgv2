﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameMethodCall {
    private void BossDragon() {
        // データに現在座標をSave

        // 敵キャラリストから敵グループ(prefab_Name)を読み込み
        CommonData.Instance.BattleEnemyGroupName = "Dragon";
        Battle.PrefabLoader.Instance.RegistEnemies(CommonData.Instance.BattleEnemyGroupName);

        // バトルシーンに移行開始
        MapToBattle.Instance.GotoBattleScene();
    }
}
