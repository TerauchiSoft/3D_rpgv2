﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameMethodCall
{
    private void AddPetil() {
        CommonData.Instance._joinPetil = true;
        CommonData.Instance.Petil.isJoined = true;
    }

    private void AddErina() {
        CommonData.Instance._joinErina = true;
        CommonData.Instance.Erina.isJoined = true;
    }
}
