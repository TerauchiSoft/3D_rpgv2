﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class StringBuild
{
    private static StringBuilder _strBuilder = new StringBuilder();
    public static StringBuilder GetStrBunler() { return _strBuilder; }
    public static string GetString() { return _strBuilder.ToString(); }
    public static string GetStringWithClear() {
        string str = _strBuilder.ToString();
        _strBuilder.Clear();
        return str;
    }
    
    public static StringBuilder ClearStr()
    {
        _strBuilder.Clear();
        return _strBuilder;
    }

    public static StringBuilder Append(string str)
    {
        _strBuilder.Append(str);
        return _strBuilder;
    }

    public static StringBuilder AppendLine()
    {
        _strBuilder.AppendLine();
        return _strBuilder;
    }

    public static StringBuilder AppArgs(params string[] vs)
    {
        foreach (var s in vs)
            _strBuilder.Append(s);
        return _strBuilder;
    }

    public static string GetStringFromArgs(params string[] vs) {
        _strBuilder.Clear();
        AppArgs(vs);
        return _strBuilder.ToString();
    }

    public static StringBuilder Replace(string oldstr, string newstr)
    {
        _strBuilder.Replace(oldstr, newstr);
        return _strBuilder;
    }

    public static StringBuilder Replace(char oldchar, char newchar)
    {
        _strBuilder.Replace(oldchar, newchar);
        return _strBuilder;
    }

    public static StringBuilder Replace(string oldstr, string newstr, int startidx, int count)
    {
        _strBuilder.Replace(oldstr, newstr, startidx, count);
        return _strBuilder;
    }

    public static StringBuilder Replace(char oldchar, char newchar, int startidx, int count)
    {
        _strBuilder.Replace(oldchar, newchar, startidx, count);
        return _strBuilder;
    }

    public static StringBuilder Remove(int startidx, int count)
    {
        _strBuilder.Remove(startidx, count);
        return _strBuilder;
    }
}
