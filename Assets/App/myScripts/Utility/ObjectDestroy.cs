﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 親につけよう。
/// </summary>
public class ObjectDestroy : MonoBehaviour {
    public float time = 0;
    public bool CollisionDestroy = false;
    public string targetTag = "";
	// Use this for initialization
	private void Start()
    {
        SetUpDestroy(time);
	}
	
    public void SetUpDestroy(float tm)
    {
        if (tm > 0)
        {
            Destroy(transform.gameObject, tm);
        }
    }

    public void DestroyObjectWithDelay(float delay)
    {
        Destroy(this.gameObject, delay);
    }

    public void DestroyObject()
    {
        Destroy(this.gameObject);
    }

    // 未使用
    public void DestroyParentObject()
    {
        Destroy(transform.parent.gameObject);
    }
    public void DestroyTargetObject(GameObject target) {
        Destroy(target.gameObject);
    }
    public void DestroyTargetObjectWithDelay(GameObject target, float delay)
    {
        Destroy(target.gameObject, delay);
    }
}
