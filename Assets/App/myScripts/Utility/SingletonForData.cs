﻿using UnityEngine;

/// <summary>
/// なかったらnewで初期化する前提のシングルトンオブジェクト
/// </summary>
/// <typeparam name="T"></typeparam>
public class SingletonForData<T> where T : new()
{
    protected static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new T();
                Debug.Log(typeof(T).Name + "クラス初期化");
            }

            if (instance == null)
                Debug.LogWarning(typeof(T) + "クラスのインスタンスがありません。");

            return instance;
        }
    }
}
