﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 警告メッセージ。
/// </summary>
public class Dialog : Singleton<Dialog> {
    [SerializeField] TextWin textWin;
    [SerializeField] Image backImg;

	// Use this for initialization
	void Awake () {
        instance = this;
        DontDestroyOnLoad(this);
	}

    /// <summary>
    /// ダイアログメッセージを表示する。
    /// </summary>
    /// <param name="str">String.</param>
    public void ViewDialogMessage(string str)
    {
        Debug.LogError("DialogOpen" + str);
        backImg.gameObject.SetActive(true);
        textWin.StartTextPlayNoSpan(str, () =>
        {
            backImg.gameObject.SetActive(false);
            textWin.gameObject.SetActive(false);
        });
        gameObject.transform.SetAsLastSibling();
    }
}
