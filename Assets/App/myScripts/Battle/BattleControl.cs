//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System.Threading.Tasks;
//using DG.Tweening;

//[System.Serializable]
//public partial class BattleControl : MonoBehaviour
//{
//    public static BattleControl btlcnt;

//    // -------------- 計算、値取得用メソッド --------------

//    BattleMessage _battleMessage = new BattleMessage();

//    // --------------------------------------------------

//    /*~----------------------------------/
//    :                                    :
//    :           外部のクラス             :
//    :                                    :
//    /~----------------------------------*/
//    // コモンデータ
//    private CommonData cd;
//    // バトルカメラクラス
//    private BattleCamera b_cam;
//    // エフェクト
//    private Effect efk;
//    // 入力
//    private PlayerController plycnt;
//    // フェーズマネージャー
//    public PheseManager pm;
//    // テストのステータスバー
//    public LV testview;
//    // テストのステータスバー
//    public LV testview_enm;
//    /*~----------------------------------/
//    :                                    :
//    :           非同期処理用             :
//    :          毎フレーム更新            :
//    :                                    :

//    private void UpdateAsynchronousValue()
//    {

//    }
//    /*~----------------------------------------------/
//    :                                                :
//    : バトルフェイズに関係するオブジェクト、メソッド :
//    :                                                :
//    /~----------------------------------------------*/
//    /// <summary>
//    /// 読み込み完了
//    /// </summary>
//    private bool isEndRead = false;
//    private bool GetEndRead()
//    {
//        return isEndRead;
//    }

//    [SerializeField]
//    private ST Phese;                       // 現在のバトルの状態、外からも変更可能
//    private GameObject[] EnemyPrefab;       // 敵キャラのゲームオブジェクト
//    private GameObject[] PlayerPrefab;      // プレイヤーのゲームオブジェクト
//    private List<Battle_Status> Enemys = new List<Battle_Status>();     // 敵キャラのステータス
//    private List<Battle_Status> Players = new List<Battle_Status>();    // プレイヤーのステータス
//    private List<GameObject> ob_Btlobject = new List<GameObject>();  // バトルが終わったら削除するオブジェクトを登録

//    public ST GetPhese() { return Phese; }
//    public ST SetPhese(ST ps) { Phese = ps; return Phese; }
//    public GameObject GetEnemyPrefab(int n) { return EnemyPrefab[n]; }
//    public GameObject GetPlayerPrefab(int n) { return PlayerPrefab[n]; }
//    public List<Battle_Status> GetEnemys() { return Enemys; }
//    public List<Battle_Status> GetPlayers() { return Players; }
//    public Battle_Status GetEnemy(int n) { return Enemys[n]; }
//    public Battle_Status GetPlayer(int n) { return Players[n]; }
//    public void AddBtlObject(GameObject ob) { ob_Btlobject.Add(ob); }
    
//    public int NumCharacter;
//    public int Enemy_Num;
//    public int Player_Num;

//    /// <summary>
//    /// ランダム
//    /// </summary>
//    System.Random rand;

//    /// <summary>
//    /// カーソルの選択遷移
//    /// </summary>    
//    public enum ST
//    {
//        SPAWN = -2, WAIT = -1, INIT = 0, WINLOSEJUDG = 1, SELECT = 2, BATTLE = 3, BTL_ROT = 4,
//        SKILL = 5, REACTION = 10, OUTREACTION = 11, HPMPMES = 12, OUTHPMPMES = 13
//    }

//    /// <summary>敵のカメラの状態</summary>
//    public bool Camera_EnemyEnabled
//    {
//        get
//        { return b_cam.GetEnabledEnemyObjectCamera(); }
//        set
//        {
//            b_cam.SetEnabledEnemyObjectCamera(value);
//            b_cam.SetEnabledEnemyBackCamera(value);
//        }
//    }

//    /// <summary>プレイヤーのカメラの状態</summary>
//    public bool Camera_PlayerEnabled
//    {
//        get
//        { return b_cam.GetEnabledPlayerObjectCamera(); }
//        set
//        {
//            b_cam.SetEnabledPlayerObjectCamera(value);
//            b_cam.SetEnabledPlayerBackCamera(value);
//        }
//    }

//    // 各要素のゲームオブジェクト
//    // GameObject EnemyMes;
//    // GameObject PlayerMes;
//    private GameObject MenuUI;

//    // テキストウィンドウ操作クラス
//    public Battle_TextWin InfoMes;
//    public Battle_TextWin TextMes;
    

//    // Use this for initialization
//    void OnEnable()
//    {
//        btlcnt = this;
//        Phese = ST.INIT;

//        if (isEndRead == false)
//        {
//            rand = new System.Random();

//            // 外部クラス
//            cd = CommonData.Common;
//            plycnt = CommonData.Plycnt;
//            efk = GetComponent<Effect>();
//            b_cam = GetComponent<BattleCamera>();
//            //pm = GameObject.Find("PheseManager").GetComponent<PheseManager>();

//            MenuUI = GameObject.Find("MenuUI");

//            EnemyPrefab = new GameObject[10];
//            PlayerPrefab = new GameObject[10];
//            isEndRead = true;
//        }
        
//        //InitBattleObject();
//        // メッセージウィンドウの位置とメッセージの一文字当たりの時間間隔の記憶
//        // InfoMes = new Battle_TextWin("Top", 2);
//        TextMes = new Battle_TextWin("Bottom", 2);
        
//        plycnt.SetKeyDownMode(true);

//        Phese = ST.SPAWN;
        
//        SetPrefab();
//    }

//    private void OnDisable() {
//        Resources.UnloadUnusedAssets();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        // prefabセット後、INITの処理へ
//        if (Phese == ST.INIT)               // 1st Phese
//        {
//            InitStatusAssignment();

//            // 1.8秒待ってフェーズ変更
//            WaitPheseWait(1.8f, ST.WINLOSEJUDG);
//        }

//        if (Phese == ST.WINLOSEJUDG)        // 2nd Phese
//        {
//            if (Enemys.Count <= 0)
//            {
//                Win();
//            }
//            else
//            if (Players.Count <= 0)         // 2nd Phese
//            {
//                Lose();

//            }
//            else
//            {
//                if (Camera_EnemyEnabled) b_cam.CameraChange();             // 3rd Phese

//                var x = PlayerPrefab[0].transform.localPosition.x;  // カメラを第一プレイヤーに向ける
//                Vector3 xvec = new Vector3(x, 0, 0);

//                // カメラのx座標 = "セレクト"の座標 + x
//                b_cam.CameraMoveToSelect(xvec);

//                Players[0].SetMenuBar();

//                for (int i = 0; i < Players.Count; i++)
//                    Players[i].NextAct = Battle_Status.Act.none;

//                TextMes.TextPlayNoSpan("どうする？");
                
//                // StartCoroutine(InfoMes.TextSetContinuity("..."));

//                Phese = ST.SELECT;
//            }
//        }

//        // バトルフェイズ
//        if (Phese == ST.BATTLE)             // 4th Phese
//        {
//            // 全員の行動セットか見る
//            for (int i = 0; i < Players.Count; i++)
//                if (Players[i].NextAct == Battle_Status.Act.none) Phese = ST.SELECT; 

//            SetEnemyAct();
//            SetEnmStatusInPlayer();

//            // プレイヤーの選択が終わったらカメラをノーマルに戻す
//            b_cam.CameraMoveToNormal();

//            // Destroy(select);
//            Phese = ST.BTL_ROT;
//            StartCoroutine(Battle_ActSet());
//        }
//    }


//    // バトルフェイズ
//    IEnumerator Battle_ActSet()        // 5th Phese
//    {
//        // 押しっぱなしで会話飛ばせる
//        plycnt.SetKeyDownMode(false);
     
//        // カメラ移動待ち
//        yield return new WaitForSeconds(0.6f);

//        SetBehaviorPriority();      // 行動優先度を算出

//        NumCharacter = Players.Count + Enemys.Count;

//        Battle_Status[] bs = new Battle_Status[NumCharacter];   // 0から行動順にセットする配列
//        for (int i = 0; i < NumCharacter; i++)                  // bs[]に全てのキャラクターを入れる。
//        {
//            if (i < Players.Count) bs[i] = Players[i];
//            else bs[i] = Enemys[i - Players.Count];
//        }

//        bs = PrioritySort(bs);       // 行動優先度順に並び替え

//        // 行動振り分け
//        int n = 0;
//        Battle_Status.Act act;
//        while (n < bs.Length)
//        {
//            if (bs[n].Hp <= 0)
//            {
//                n++;
//                continue;
//            }
//            print(bs[n].CharaName + "のターン！");

//            act = bs[n].NextAct;

//            object do_act;
//            if (act == Battle_Status.Act.Item)
//                do_act = new Item();
//            else if (act == Battle_Status.Act.Skill)
//                do_act = new Skill();
//            else
//                do_act = null;

//            // スキルかアイテムの選択
//            if (bs[n].NextAct == Battle_Status.Act.Skill)
//            {
//                do_act = bs[n].SelectedSkl;
//                bs[n].SelectedSkl = (Skill)do_act;
//            }
//            else if (bs[n].NextAct == Battle_Status.Act.Item)
//            {
//                do_act = bs[n].SelectedItem;
//                bs[n].SelectedItem = (Item)do_act;
//            }

            
//            // 標的のターゲットの名前取得
//            bs[n].SelectedTgtName = SearchTgtName(bs[n].TargetObject, bs[n].SelectedTgtNum);
//            // 標的のターゲットの情報取得
//            //bs[n].EnmStatus = GetEnmStatus(bs[n].TargetObject, bs[n].SelectedTgtNum);
            

//            // バトルルーチン開始
//            Phese = ST.BTL_ROT;
//            StartCoroutine(Battle_Routine(bs[n], do_act));
//            yield return new WaitForSeconds(0.5f);

//			while (Phese != ST.WAIT)
//            {
//                yield return 0;
//            }

//            n++;
//        }


//        Phese = ST.WINLOSEJUDG;
//        yield return 0;
//    }

//    /// <summary>
//    /// バトルの進行ルーチン、戻るときはyield break + Phese == Wait
//    /// </summary>
//    IEnumerator Battle_Routine(Battle_Status bs, object do_act)        // 6th Phese
//    {
//        // 使用者にカメラ移動
//        //int ischange = b_cam.CameraMoveToCharacter(bs.PlyOrEnm, bs.PlyOrEnm, bs.ObjectID);

//        // カメラ移動待ち
//        //if (ischange > 0) yield return new WaitForSeconds(0.5f);

//        Skill skl = new Skill();
//        Item itm = new Item();
//        if (bs.NextAct == Battle_Status.Act.Skill)
//            skl = (Skill)do_act;
//        else if (bs.NextAct == Battle_Status.Act.Item)
//            itm = (Item)do_act;

//        switch (bs.NextAct)
//        {
//            case Battle_Status.Act.Skill:
//                // メッセージ開始
//                bs.TalkText = NameSubstitution(skl.SkillMessages, bs.CharaName, bs.SelectedTgtName);
//                var sk_tex = TextSetContinuity(bs.TalkText);
//                // StartCoroutine(InfoMes.TextSetContinuity(skl.SkillName));
//                TextMes.TextPlayNoSpan(sk_tex);

//                // スキル開始
//                GameObject sk_usr;     // スキル使用者
//                GameObject[] sk_enm;   // 対象(複数)

//                if (bs.PlyOrEnm == Battle_Status.Target.Player) sk_usr = PlayerPrefab[bs.ObjectID];
//                else sk_usr = EnemyPrefab[bs.ObjectID];
                
//                if (bs.TargetObject == Battle_Status.Target.Player) sk_enm = PlayerPrefab;
//                else if (bs.TargetObject == Battle_Status.Target.Enemy) sk_enm = EnemyPrefab;
//                else // bs.TargetObject == Battle_Status.Target.MySelf
//                {
//                    if (bs.PlyOrEnm == Battle_Status.Target.Player) sk_enm = PlayerPrefab;
//                    else sk_enm = EnemyPrefab;
//                }

//                // スキル発動、発動アニメーション
//                Phese = ST.SKILL;
//                StartCoroutine(StartSkill(skl, sk_usr, sk_enm, bs.PlyOrEnm, bs.SelectedTgtNum));       // 8th Phese
//                break;
//            case Battle_Status.Act.Item:
//                // メッセージ開始
//                bs.TalkText = NameSubstitution(itm.ItemMessages, bs.CharaName, bs.SelectedTgtName);
//                var it_tex = TextSetContinuity(bs.TalkText);
//                // StartCoroutine(InfoMes.TextSetContinuity(skl.SkillName));
//                TextMes.TextPlayNoSpan(it_tex);

//                // スキル開始
//                GameObject it_usr;     // スキル使用者
//                GameObject[] it_enm;   // 対象(複数)


//                if (bs.PlyOrEnm == Battle_Status.Target.Player) it_usr = PlayerPrefab[bs.ObjectID];
//                else it_usr = EnemyPrefab[bs.ObjectID];

//                if (bs.TargetObject == Battle_Status.Target.Player) it_enm = PlayerPrefab;
//                else if (bs.TargetObject == Battle_Status.Target.Enemy) it_enm = EnemyPrefab;
//                else // bs.TargetObject == Battle_Status.Target.MySelf
//                {
//                    if (bs.PlyOrEnm == Battle_Status.Target.Player) it_enm = PlayerPrefab;
//                    else it_enm = EnemyPrefab;
//                }

//                // スキル発動、発動アニメーション
//                Phese = ST.SKILL;
//                // StartCoroutine(StartSkill(skl, it_usr, it_enm, bs.SelectedTgtNum));       // 8th Phese
//                WaitPhese(0.3f, ST.REACTION);
//                break;
//            case Battle_Status.Act.RunAway:
//                EndProcessingWhenBattleEnd();
//                break;
//            case Battle_Status.Act.PowerUp:
//                break;
//            case Battle_Status.Act.Status:
//                break;
//        }


//		while (Phese != ST.REACTION || TextMes.GetIsMesPlaying() == true || plycnt.isFire1 == false)
//        {
//            yield return 0;
//        }


//        // 対象にカメラ移動
//        //ischange = b_cam.CameraMoveToCharacter(bs.PlyOrEnm, bs.TargetObject, bs.SelectedTgtNum);
//        //if (ischange > 0) yield return new WaitForSeconds(0.6f);
//        // スキルエフェクト(対象に)再生
//        var name = (bs.NextAct == Battle_Status.Act.Skill) ? skl.EffectName : itm.EffectName;
//        var trs = ((bs.TargetObject == Battle_Status.Target.Player) || (bs.TargetObject == Battle_Status.Target.Players)) ? 
//            PlayerPrefab : EnemyPrefab;
//        efk.PlayActivateEffect(name, trs, bs.TargetObject, bs.PlyOrEnm, bs.SelectedTgtNum);

//        int efectFromAct = (bs.NextAct == Battle_Status.Act.Skill) ? (int)skl.effects[0] : (int)itm.effects[0];
//        // 攻撃スキルなら被ダメージプロセス//TODO:ここは複数のエフェクトに対応させる
//        if ((TableValues.Effect)efectFromAct == TableValues.Effect.Attack)
//        {
//            SkillAttackProcessing(bs, skl);
//        }
//        // 回復のスキル(アイテム)のプロセス
//        else if ((TableValues.Effect)efectFromAct == TableValues.Effect.HPRecover)
//        {
//            SkillRecoverProcessing(bs, skl, itm);
//        }
//        else if ((TableValues.Effect) efectFromAct == TableValues.Effect.VITUp) { 
//            SkillVITUpProcessing(bs, skl);
//        }

//		while (Phese != ST.OUTREACTION || TextMes.GetIsMesPlaying() == true || plycnt.isFire1 == false)                                // 9th Phese
//        {
//            yield return 0;
//        }

//        if (bs.EnmStatus[bs.SelectedTgtNum].Hp > 0)
//            TextMes.TextPlayNoSpan(GetHpStr(bs.EnmStatus[bs.SelectedTgtNum]));

//        Phese = ST.HPMPMES;
//        WaitPhese(0.3f, ST.OUTHPMPMES);

//        while (Phese != ST.OUTHPMPMES || TextMes.GetIsMesPlaying() == true || plycnt.isFire1 == false)                                // 9th Phese
//        {
//            yield return 0;
//        }
        
//        BattleEndAction();
//        Phese = ST.WAIT;
//    }

//    /// <summary>
//    /// スキルアタックの攻撃処理
//    /// </summary>
//    void SkillAttackProcessing(Battle_Status bs, Skill skl)
//    {
//        string str = "";
//        int damage = 0;
//        for (int i = 0; i < bs.EnmStatus.Count; i++)
//        {
//            var localdamage = skl.skl_method.Invoke(bs, bs.EnmStatus[i]);
//            //TODO: 状態異常にも対応する

//            // 瀕死でない時体力を減らす
//            if (bs.EnmStatus[i].B_Abn != StatusTable.Abnormality.Down)
//                bs.EnmStatus[i].Hp -= localdamage;

//            // ダウン状態にする。
//            if (bs.EnmStatus[i].Hp <= 0)
//                ToDisableCombat(bs, i);

//            damage += localdamage;
//            StartCoroutine(DamageAnimationPlay(bs, bs.TargetObject, bs.SelectedTgtNum));       // 9th Phese
//        }
//        damage /= bs.EnmStatus.Count;
//        str = GetDamageStr(bs, bs.ObjectID, damage)/* + "\n" + damage.ToString() + "\n" + bs.EnmStatus[0].Hp + " / " + bs.EnmStatus[0].M_Hp*/;// GetDamageStr(bs, damage);
        

//        TextMes.TextPlayNoSpan(str);
//    }

//    /// <summary>
//    /// 戦闘不能にする。
//    /// </summary>
//    /// <param name="bs"></param>
//    /// <param name="i"></param>
//    void ToDisableCombat(Battle_Status bs, int i)
//    {
//        bs.EnmStatus[i].Hp = 0;
//        bs.EnmStatus[i].B_Abn = StatusTable.Abnormality.Down;
//    }

//    /// <summary>
//    /// 回復の処理
//    /// </summary>
//    void SkillRecoverProcessing(Battle_Status bs, Skill skl, Item itm)
//    {
//        string str = "";
//        int recover = 0;
//        for (int i = 0; i < bs.EnmStatus.Count; i++)
//        {
//            if (bs.NextAct == Battle_Status.Act.Skill) recover = skl.skl_method.Invoke(bs, bs.EnmStatus[i]);
//            else recover = itm.itm_method.Invoke(bs, bs.EnmStatus[i]);
//            //TODO: 状態異常にも対応する

//            Phese = ST.REACTION;
//            // recover = bs.M_Hp;// 30回復
//            bs.EnmStatus[bs.SelectedTgtNum].Hp += recover;
//            if (bs.EnmStatus[bs.SelectedTgtNum].Hp > bs.EnmStatus[bs.SelectedTgtNum].M_Hp)
//                bs.EnmStatus[bs.SelectedTgtNum].Hp = bs.EnmStatus[bs.SelectedTgtNum].M_Hp;
//            // TextMes.TextPlayNoSpan("体力全快！\n" + recover / recover));
//        }
//        recover /= bs.EnmStatus.Count;
//        str = GetRecoverStr(bs, bs.ObjectID, recover)/* + "\n" + recover.ToString() + "\n" + bs.EnmStatus[0].Hp + " / " + bs.EnmStatus[0].M_Hp*/;// GetDamageStr(bs, damage);
        
//        WaitPhese(0.3f, ST.OUTREACTION);
//        TextMes.TextPlayNoSpan(str);
//    }

//    /// <summary>
//    /// 防御アップの処理
//    /// </summary>
//    void SkillVITUpProcessing(Battle_Status bs, Skill skl)
//    {
//        string str = "";
//        int damage = 0;
//        for (int i = 0; i < bs.EnmStatus.Count; i++)
//        {
//            var localdamage = skl.skl_method.Invoke(bs, bs.EnmStatus[i]);
//            //TODO: 状態異常にも対応する

//            // 瀕死でない時防御力を上げる
//            if (bs.EnmStatus[i].B_Abn != StatusTable.Abnormality.Down)
//                bs.EnmStatus[i].B_Vit += localdamage;

//            // ダウン状態にする。
//            if (bs.EnmStatus[i].Hp <= 0)
//                ToDisableCombat(bs, i);

//            damage += localdamage;
//            StartCoroutine(DamageAnimationPlay(bs, bs.TargetObject, bs.SelectedTgtNum));       // 9th Phese
//        }
//        damage /= bs.EnmStatus.Count;
//        str = "防御力アップ！"/* + "\n" + damage.ToString() + "\n" + bs.EnmStatus[0].Hp + " / " + bs.EnmStatus[0].M_Hp*/;// GetDamageStr(bs, damage);


//        TextMes.TextPlayNoSpan(str);
//    }


//    /*~----------------------------------/
//    :                                    :
//    :             雑多な処理             :
//    :                                    :
//    /~----------------------------------*/
//    /// <summary>
//    /// エネミーの行動をセット
//    /// ここでエネミーの行動のすべてが決まる
//    /// </summary>
//    public void SetEnemyAct()
//    {
//        for (int i = 0; i < Enemys.Count; i++)
//        {
//            var skl = EnemyPrefab[i].GetComponent<Battle_EnemyAct>();
//            Enemys[i] = skl.AIMethodRun(Enemys[i], GetEnemys(), GetPlayers(), skl);
//        }
//    }
//    /// <summary>
//    /// ブックでプレイヤーの行動をセット
//    /// したあと対象のステータスセット
//    /// </summary>
//    public void SetEnmStatusInPlayer()
//    {
//        for (int i = 0; i < Players.Count; i++)
//        {
//            if (Players[i].TargetObject == Battle_Status.Target.Myself)
//                Players[i].SetEnemyStatus(Players[i], Players, Players[i].ObjectID);
//            else if (Players[i].TargetObject == Battle_Status.Target.Enemy)
//                Players[i].SetEnemyStatus(Players[i], Enemys, Players[i].SelectedTgtNum);
//            else if (Players[i].TargetObject == Battle_Status.Target.Enemys)
//                Players[i].SetEnemyStatus(Players[i], Enemys);
//            else if (Players[i].TargetObject == Battle_Status.Target.Player)
//                Players[i].SetEnemyStatus(Players[i], Players, Players[i].SelectedTgtNum);
//            else if (Players[i].TargetObject == Battle_Status.Target.Players)
//                Players[i].SetEnemyStatus(Players[i], Players);
//        }
//    }


//    /*~----------------------------------/
//    :                                    :
//    :             雑多な処理             :
//    :                                    :
//    /~----------------------------------*/
//    /// <summary>
//    /// スキルの使用
//    /// </summary>
//    /// <param name="usr"></param>
//    /// <param name="enm"></param>
//    /// <param name="skl"></param>
//    /// <returns></returns>
//    IEnumerator StartSkill(Skill skl, GameObject usr, GameObject[] enm, Battle_Status.Target plyorenm, int tgt)    // 8th Phese
//    {
//        print(skl.EffectName);

//        Animator anim = usr.GetComponent<Animator>();
//        if (plyorenm == Battle_Status.Target.Enemy)
//            anim.Play(skl.EffectName, 0);
//        else
//            anim.Play("Attack");

//        yield return new WaitForSeconds(0.3f);

//        float bf = 0.0f;
//        float af = 0.1f;
//        float a = 0.1f;
//        float b = 0;
//        while (b < a && a < 1.0f)
//        {
//            if (plycnt.isFire3 == true)
//                anim.speed = 2.0f;
//            else
//                anim.speed = 1.0f;

//            print(a);
//            bf = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
//            yield return new WaitForSeconds(0.1f);
//            af = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
//            a = af;
//            b = bf;

//            if (anim.GetCurrentAnimatorStateInfo(0).IsName(skl.EffectName) == false)
//            {
//                Phese = ST.REACTION;
//                yield break;
//            }
//        }
        
//        Phese = ST.REACTION;
//    }


//    /// <summary>
//    /// ダメージアニメの再生
//    /// </summary>
//    /// <param name="hp"></param>
//    /// <param name="tgt"></param>
//    /// <param name="idx"></param>
//    /// <returns></returns>
//    IEnumerator DamageAnimationPlay(Battle_Status bs, Battle_Status.Target tgt, int idx)    // 8th Phese
//    {
//        var usr = (tgt == Battle_Status.Target.Player || tgt == Battle_Status.Target.Players) ? PlayerPrefab[idx] : EnemyPrefab[idx];
//        Animator anim = usr.GetComponent<Animator>();
//        if (bs.EnmStatus[(int)idx].Hp > 0)
//            anim.Play("Damage", 0);
//        else
//            anim.Play("Down", 0);

//        yield return new WaitForSeconds(0.1f);

//        float bf = 0.0f;
//        float af = 0.1f;
//        float a = 0.1f;
//        float b = 0;
//        while (b < a && a < 1.0f)
//        {
//            if (plycnt.isFire3 == true)
//                anim.speed = 2.0f;
//            else
//                anim.speed = 1.0f;

//            print(a);
//            bf = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
//            yield return new WaitForSeconds(0.1f);
//            af = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
//            a = af;
//            b = bf;

//            if (/*anim.GetCurrentAnimatorStateInfo(0).IsName("Damage") == false &&*/ 
//                anim.GetCurrentAnimatorStateInfo(0).IsName("Down") == true)
//            {
//                Phese = ST.OUTREACTION;
//                yield break;
//            }
//        }
        
//        Phese = ST.OUTREACTION;
//    }
    
//    /// <summary>
//    /// 文字を連結
//    /// </summary>
//    /// <returns></returns>
//    public string TextSetContinuity(string[] st)
//    {
//        string str = "";
//        for (int i = 0; i < st.Length; i++)
//        {
//            str += st[i] + "\r";
//        }
//        return str;
//    }

//    /// <summary>
//    /// 対象の名前を取得
//    /// </summary>
//    /// <param name=""></param>
//    /// <param name=""></param>
//    /// <returns></returns>
//    string SearchTgtName(Battle_Status.Target tgt, int idx)
//    {
//        if (tgt == Battle_Status.Target.Player)
//            return Players[idx].CharaName;
//        else
//            return Enemys[idx].CharaName;
//    }

//    /// <summary>
//    /// Hpごとの状態メッセージ取得
//    /// </summary>
//    /// <returns></returns>
//    public static string GetHpStr(Battle_Status bs)
//    {
//        return GetHpStr(bs.Hp, bs.M_Hp, bs.CharaName, bs.PlyOrEnm);
//    }

//    /// <summary>
//    /// Hpごとの状態メッセージ取得
//    /// </summary>
//    /// <returns></returns>
//    public static string GetHpStr(int hp, int m_hp, string name, Battle_Status.Target isenm)
//    {
//        string[] strs;
//        if (isenm == Battle_Status.Target.Enemy)
//            strs = hpstrs;
//        else
//            strs = hpstrsplyr;

//        string EnmName = name;
//        float hp_per_mhp = (float)hp / (float)m_hp;
//        return StageCalculation(12, hp_per_mhp, strs, EnmName);
//    }

//    static string[] hpstrs = { "闘志満々！" ,
//                             "元気、元気、元気！！！",
//                             "元気、元気！！",
//                             "元気！",
//                             "普通",
//                             "モンスターはいきり立ってるよ",
//                             "魔物はクラクラだよ",
//                             "足にキてるようだ...。",
//                             "少し弱ってきたよ",
//                             "魔物は苦しそうだ",
//                             "魔物はフラフラだよ",
//                             "バタンキュー寸前" };

//    static string[] hpstrsplyr = { "闘志満々！" ,
//                             "とっても元気だよ！",
//                             "元気、元気！！",
//                             "元気！",
//                             "まだいけるよ！",
//                             "ちょっと苦しくなってきたよ",
//                             "くらくらするよ",
//                             "結構きいてきたみたい...。",
//                             "痛い...。",
//                             "苦しい...。",
//                             "ふぅ...ふぅ...。",
//                             ".......。" };

//    /// <summary>
//    /// Mpごとの状態メッセージ取得
//    /// 主人公専用
//    /// </summary>
//    /// <returns></returns>
//    public static string GetMpStr(Battle_Status bs)
//    {
//        return GetMpStr(bs.Mp, bs.M_Mp, bs.CharaName);
//    }

//    /// <summary>
//    /// Mpごとの状態メッセージ取得
//    /// 主人公専用
//    /// </summary>
//    /// <returns></returns>
//    public static string GetMpStr(int mp, int m_mp, string name)
//    {
//        string EnmName = name;
//        float mp_per_mmp = (float)mp / (float)m_mp;
//        return StageCalculation(12, mp_per_mmp, mpstrsplyr, EnmName);
//    }
    
//    static string[] mpstrsplyr = { "魔力満タン！" ,
//                             "魔力たっぷり！",
//                             "魔力はまだまだあるよ！",
//                             "魔力はまだあるよ！",
//                             "魔力あるよ！",
//                             "魔力は残っているよ！",
//                             "魔力節約しよ！",
//                             "魔力少なくなってきた",
//                             "魔力少ないよ",
//                             "魔力なくなってきた...",
//                             "魔力無いかも...",
//                             "魔力全然ない...。"};


//    /// <summary>
//    /// ダメージのメッセージ取得
//    /// 対象が複数、単体でグループ名と敵名を分ける。
//    /// </summary>
//    /// <returns></returns>
//    static string GetDamageStr(Battle_Status bs, int myselfnum, int damage)
//    {
//        string EnmName = "";
//        if (bs.EnmStatus.Count > 1)
//            EnmName = bs.EnmStatus[myselfnum].GroupName + '達';
//        else
//            EnmName = bs.EnmStatus[myselfnum].CharaName;

//        float dmg_per_hp = (float)damage / (float)bs.EnmStatus[bs.SelectedTgtNum].M_Hp;
//        // return StageCalculation(12, dmg_per_hp, damstrs, EnmName);
//        return GetDamageStr(damage, bs.EnmStatus[bs.SelectedTgtNum].M_Hp, EnmName);
//    }

//    /// <summary>
//    /// ダメージのメッセージ取得
//    /// 対象が複数、単体名とグループ名は直接入力。
//    /// </summary>
//    /// <returns></returns>
//    static string GetDamageStr(int damage, int m_hp, string name)
//    {
//        string EnmName = name;

//        float dmg_per_hp = (float)damage / (float)m_hp;
//        return StageCalculation(12, dmg_per_hp, damstrs, EnmName);
//    }

//    static string[] damstrs = { "に超ウルトラ特大ダメージ！！！" ,
//                             "にウルトラ特大ダメージ！！",
//                             "にウルトラ特大ダメージ！",
//                             "に特大ダメージ！",
//                             "に大ダメージ！",
//                             "に結構大きなダメージ！",
//                             "に結構なダメージ！",
//                             "に結構効いたよ！",
//                             "にダメージ！",
//                             "にダメージ",
//                             "に少しダメージ",
//                             "にあまり効いていない...。"};

//    /// <summary>
//    /// 回復度の文章取得
//    /// </summary>
//    /// <param name="bs"></param>
//    /// <param name="recover"></param>
//    /// <returns></returns>
//    static string GetRecoverStr(Battle_Status bs, int myselfnum, int recover)
//    {

//        string EnmName = "";
//        if (bs.EnmStatus.Count > 1)
//            EnmName = bs.EnmStatus[myselfnum].GroupName + '達';
//        else
//            EnmName = bs.EnmStatus[myselfnum].CharaName;

//        // return StageCalculation(12, dmg_per_hp, hpstrs, EnmName);
//        return GetRecoverStr(recover, bs.EnmStatus[bs.SelectedTgtNum].M_Hp, EnmName);
//    }
    
//    static string GetRecoverStr(int recover, int m_hp, string name)
//    {
//        string EnmName = name;

//        float rec_per_hp = (float)recover / (float)m_hp;
//        return StageCalculation(12, rec_per_hp, recstrs, EnmName);
//    }


//    static string[] recstrs = { "の体力が全快した！！！" ,
//                             "の体力がかなり回復した！！",
//                             "の体力がかなり回復した！",
//                             "の体力が結構回復した！",
//                             "の体力が結構回復した。",
//                             "の体力が回復した。",
//                             "の体力が回復した。",
//                             "の体力が少し回復した。",
//                             "の体力が少し回復した。",
//                             "の体力がほんのり回復した。",
//                             "の体力が少しだけ回復した。",
//                             "の体力はあまり回復していない。"};

//    private static string StageCalculation(int n, float m, string[] strs, string en)
//    {
//        for (int i = n; i > 0; i--)
//        {
//            if (m > 0.0833333f * i)
//            {
//                print("d / hp : " + m);
//                return en + strs[n - i];
//            }
//        }
//        return en + strs[strs.Length - 1];
//    }

//    /// <summary>
//    /// 文字を対象、使用者に置き換え
//    /// </summary>
//    /// <returns></returns>
//    public string[] NameSubstitution(string[] str, string usr_str, string en_str)
//    {
//        for (int i = 0; i < str.Length; i++)
//        {
//            var newstr = str[i];
//            newstr = newstr.Replace("*Usr", usr_str);
//            newstr = newstr.Replace("*Enm", en_str);
//            str[i] = newstr;
//        }

//        return str;
//    }

//    /*~----------------------------------/
//    :                                    :
//    :    オブジェクト&クラス関係の操作   :
//    :                                    :
//    /~----------------------------------*/
//    /// <summary>最初のオブジェクトをセット</summary>
//    /// <returns></returns>
//    private void SetPrefab()
//    {
//        // 敵キャラクター読み込み
//        GameObject prefabSource = Resources.Load("ResouresPrefab/Battle/EnemyCharacters/EnemyGroups/" + cd.BattleEnemyGroupName) as GameObject;
//        GameObject ob = Instantiate(prefabSource);
//        AddBtlObject(ob);

//        print(EnemyPrefab.Length);

//        int i = 0;
//        int j = ob.GetComponent<BattlePrefabSource>().prefab.Length;
//        for (i = 0; i < j; i++)
//        {
//            var spawnpoint = b_cam.GetEnemyObjectCamera().transform.position;
//            spawnpoint.z = -8.0f;
//            GameObject ob2 = Instantiate(ob.GetComponent<BattlePrefabSource>().prefab[i], spawnpoint, Quaternion.identity, b_cam.ob_EnemyObject.transform);
//            EnemyPrefab[i] = ob2;
//            AddBtlObject(ob2);
//        }

//        Enemy_Num = i;

//        // プレイヤーキャラクター読み込み
//        prefabSource = Resources.Load("ResouresPrefab/Battle/PlayerCharacters/PlayerGroups/" + "PlayerOne") as GameObject;
//        ob = Instantiate(prefabSource);
//        AddBtlObject(ob);

//        i = 0;
//        j = ob.GetComponent<BattlePrefabSource>().prefab.Length;
//        for (i = 0; i < j; i++)
//        {
//            var spawnpoint = b_cam.GetPlayerObjectCamera().transform.position;
//            spawnpoint.z = -8.0f;
//            GameObject ob2 = Instantiate(ob.GetComponent<BattlePrefabSource>().prefab[i], spawnpoint, Quaternion.identity, b_cam.ob_PlayerObject.transform);
//            PlayerPrefab[i] = ob2;
//            AddBtlObject(ob2);
//        }

//        // 1 ~ 3
//        Player_Num = i;

//        Phese = ST.INIT;
//    }

//    /// <summary>
//    /// 初期ステータス代入
//    /// </summary>
//    /// <returns></returns>
//    void InitStatusAssignment()      // 1st Phese
//    {
//        EnemyStatusAssignment();
//        PlayerStatusAssignment();
//    }

    
//    /// <summary>
//    /// 敵キャラ出現
//    /// </summary>
//    void EnemyStatusAssignment()
//    {
//        // 敵キャラ存在
//        if (EnemyPrefab[0] == true) print("存在");

//        for (int i = 0; i < Enemy_Num; i ++)
//        {
//            Enemys.Add(EnemyPrefab[i].GetComponent<Battle_Status>());
//            Enemys[i].StatusInit(i, Battle_Status.Target.Enemy);
//            // テスト
//            testview_enm.btlst = Enemys[i];
//        }
//        // 事前にEditor上でStatus入力

//        TextMes.TextPlayNoSpan(Enemys[0].CharaName + "が躍り出た。");
//   }

//    /// <summary>
//    /// 主人公キャラ出現
//    /// </summary>
//    void PlayerStatusAssignment()
//    {
//        // 主人公キャラ存在
//        if (PlayerPrefab[0] == true) print("存在");

//        for (int i = 0; i < Player_Num; i++)
//        {
//            Players.Add(PlayerPrefab[i].GetComponent<Battle_Status>());
//            CommonData.Instance.SetCommonStatusToBattlePlayer(Players[i], cd.PlayerCharaData[i]);
//            Players[i].StatusInit(i, Battle_Status.Target.Player);
//            testview.btlst = Players[i];
//        }
        
//        // StartCoroutine(TextMes.TextSetContinuity(Players[0].Status.CharaName + "の登場だ。"));

//    }

//    /// <summary>
//    /// バトルステータスからプレイヤーデータに
//    /// レベルアップした分もここで代入する。
//    /// </summary>
//    /// <param name="bp"></param>
//    /// <param name="pd"></param>
//    public void SetBattlePlayerToCommonStatusAll(List<Battle_Status> bps, PlayerCharaData[] pd)
//    {
//        for (int i = 0; i < bps.Count; i++)
//        {
//            // bp.CharaName = pd.PlayerName;
//            pd[i].LV = bps[i].Lv;
//            pd[i].HP = bps[i].Hp;
//            pd[i].M_HP = bps[i].M_Hp;
//            pd[i].MP = bps[i].Mp;
//            pd[i].M_MP = bps[i].M_Mp;
//            pd[i].STR = bps[i].Str;
//            pd[i].AGI = bps[i].Agi;
//            pd[i].INT = bps[i].Int;
//            pd[i].VIT = bps[i].Vit;
//            pd[i].LUK = bps[i].Luk;
//            pd[i].CRI = bps[i].Cri;
//            // bp.Fatigue = 0;
//        }
//    }


//    /*~----------------------------------/
//    :                                    :
//    :   バトルの判定及び進行処理等       :
//    :                                    :
//    /~----------------------------------*/
//    /// <summary>
//    /// ターン終わりの処理(勝敗、状態異常、イベント呼び出しetc.)
//    /// </summary>
//    void BattleEndAction()
//    {
//        for (int i = 0; i < Players.Count; i++)
//        {
//            testview.HpSliderHenkou(Players[i].Hp);
//            testview.MpSliderHenkou(Players[i].Mp);
//        }
//        for (int i = 0; i < Enemys.Count; i++)
//        {
//            testview_enm.HpSliderHenkou(Enemys[i].Hp);
//            testview_enm.MpSliderHenkou(Enemys[i].Mp);
//        }

//        // 人数確認
//        int cnt_ply = 0;
//        for (int i = 0; i < Players.Count; i++)
//            cnt_ply += (Players[i].B_Abn != StatusTable.Abnormality.Down) ? 1 : 0;
//        int cnt_enm = 0;
//        for (int i = 0; i < Enemys.Count; i++)
//            cnt_enm += (Enemys[i].B_Abn != StatusTable.Abnormality.Down) ? 1 : 0;

//        if (cnt_enm != 0 && cnt_ply != 0) return;

//        StopAllCoroutines();

//        if (cnt_ply == 0) {
//            Phese = ST.WAIT;
//            Lose();
//        }
//            if(cnt_enm == 0) {
//            Phese = ST.WAIT;
//            Win();
//        }

//    }

//    /// <summary>
//    /// バトルターンへ移行前処理
//    /// </summary>
//    public void Battle_Turn()
//    {
//        Phese = ST.BATTLE;
//    }

//    /// <summary>
//    /// 行動優先度決定
//    /// </summary>
//    void SetBehaviorPriority()
//    {
//        for (int j = 0; j < Enemys.Count; j++)
//        {
//            // Agi * (192 + (1 ~ 64)) / 256
//            Enemys[j].B_Priority = (int)((float)Enemys[j].B_Agi * (rand.Next(64) + 192) / 256);
//            print("E" + Enemys[j].B_Priority);
//        }

//        for (int j = 0; j < Players.Count; j++)
//        {
//            // Agi * (192 + (1 ~ 64)) / 256
//            Players[j].B_Priority = (int)((float)Players[j].B_Agi * (rand.Next(64) + 192) / 256);
//            print("P" + Players[j].B_Priority);
//        }
//    }
    
//    /// <summary>
//    /// 勝利処理
//    /// </summary>
//    /// <returns></returns>
//    private void Win()
//    {
//        TextMes.TextPlayNoSpan("戦いに勝利した");
//        SetBattlePlayerToCommonStatusAll(Players, cd.PlayerCharaData);
//        WaitPheseWait(3.0f, ST.WAIT, "EndProcessingWhenBattleEnd");
//    }

//    /// <summary>
//    /// 敗戦処理
//    /// </summary>
//    /// <returns></returns>
//    private void Lose()
//    {
//        TextMes.TextPlayNoSpan("戦いに敗北した");

//        WaitPheseWait(3.0f, ST.WAIT, "EndProcessingWhenBattleEnd");
//        ;

//    }

//    /// <summary>
//    /// 優先度順にソート
//    /// </summary>
//    /// <param name="bs"></param>
//    public Battle_Status[] PrioritySort(Battle_Status[] bs)
//    {
//        int j = 0;
//        while (j < bs.Length - 1)     // 行動優先度順に並び替え
//        {
//            if (bs[j + 1].B_Priority > bs[j].B_Priority)
//            {
//                var a = bs[j];
//                bs[j] = bs[j + 1];
//                bs[j + 1] = a;

//                j = 0;
//                continue;
//            }
//            j++;
//        }
//        return bs;
//    }

//    public void Nigeru()
//    {
//        StartCoroutine(Dash());
//    }

//    IEnumerator Dash()
//    {
//        TextMes.TextPlayNoSpan(Players[0].CharaName + "は逃げ出した");

//        // 2秒待つ
//        yield return new WaitForSeconds(2f);
//        SetBattlePlayerToCommonStatusAll(Players, cd.PlayerCharaData);
//        EndProcessingWhenBattleEnd();
//    }
    
//    /// <summary>
//    /// バトル後の初期化
//    /// </summary>
//    public void EndProcessingWhenBattleEnd()
//    {
//        foreach(var a in ob_Btlobject)
//        {
//            Destroy(a);
//        }
//        ob_Btlobject.Clear();


//        for (int i = 0; i < EnemyPrefab.Length; i++)
//        {
//            EnemyPrefab[i] = null;
//            PlayerPrefab[i] = null;
//        }

//        Enemys.Clear();
//        Players.Clear();

//        NumCharacter = 0;
//        Enemy_Num = 0;
//        Player_Num = 0;

//        PlayerController.Map_InputConfigSet();
//        pm.ChangeToGameMode("Map", null);
//    }

//}
