﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// 未使用っぽい  
/// </summary>
// テキストウィンドウの操作クラス
public class Battle_TextWin
{
	private PlayerController plycnt;

    // 操作対象のGameObject
    private GameObject ob;

    // 上側か下側か
    [SerializeField]
    private string Position = "Top";

    // 連続表示のスパンデフォルト(フレーム)
    [SerializeField]
    private readonly int ViewSpanDefault = 4;

    // 連続表示のスパン(フレーム)
    [SerializeField]
    private int ViewSpan = 4;

    // 連続表示のカウント
    [SerializeField]
    private int ViewCount = 4;

    // ボタンでスキップ可能か
    [SerializeField]
    private bool isAllowSkip = true;

    // メッセージ再生中か
    [SerializeField]
    private bool isMesPlaying = false;

    TextMeshProUGUI textmesh;

    public bool GetIsMesPlaying()
    {
        return isMesPlaying;
    }

	// ディレイ
	private float delay = 0;
	private readonly int delayflame = 10;

    private void Awake()
    {
        textmesh = ob.GetComponentInChildren<TextMeshProUGUI>();
    }
    
    // 位置、連続文字表示の時間間隔
    public Battle_TextWin(string po, int tm)
    {
        SetViewSpanAndPosition(po, tm);
        plycnt = PlayerController.Instance;
    }

    public void SetViewSpanAndPosition(string po, int tm)
    {
        Position = po;
        SetViewSpan(tm);

        // 操作オブジェクト取得
        if (po == "Top")
            ob = GameObject.Find("InfoMes");
        if (po == "Bottom")
            ob = GameObject.Find("Mes");
    }


    // マップ用
    public Battle_TextWin(GameObject go, int tm)
    {
        SetViewSpanAndPosition(go, tm);
        plycnt = PlayerController.Instance;
    }

    public void SetViewSpanAndPosition(GameObject go, int tm)
    {
        Position = "Map";
        SetViewSpan(tm);

        // 操作オブジェクト取得
        ob = go;

    }

    /// <summary>
    /// 表示間隔の設定
    /// </summary>
    /// <param name="tm"></param>
    public void SetViewSpan(int tm)
    {
        ViewSpan = tm;
        ViewCount = tm;
    }
    

    /// <summary>
    /// 連続文字表示のメソッド
    /// </summary>
    /// <returns></returns>
    public IEnumerator TextPlay(string st)
    {
        yield return new WaitForSeconds(delay);

		isMesPlaying = true;

        // 文字数の配列添え字
        int strcnt = 1;

        // 今の文字
        string str = "";

        // 全ての文字を読み込むまで繰り返す
        while (strcnt <= st.Length)
        {
            // 一文字の時間間隔待ち
            if (ViewCount > 0)
            {
                ViewCount--;

                // 一回休み
                yield return 0;
                continue;
            }

			if (isAllowSkip == true && plycnt.isFire1 == true)
			{
				for (int i = strcnt; i <= st.Length; i++)
					str += st[i - 1];

                textmesh.text = str;
				endTextSetContinuity();
				yield break;
			}
			else
			{
				// 表示文字に1文字足す
				str += st[strcnt - 1];
                textmesh.text = str;
			}
            

            // 文字間隔リセット
            ViewCount = ViewSpan;

            // 次の文字
            strcnt++;

            yield return 0;
        }
		endTextSetContinuity();
    }

	private void endTextSetContinuity()
	{

		isMesPlaying = false;
		plycnt.SetInputDelay();
		SetMesStartDelay();
        SetViewSpan(ViewSpanDefault);
    }

	private void SetMesStartDelay()
	{
		delay = (delayflame / 60);
	}
    
    /// <summary>
    /// 瞬間表示のメソッド
    /// </summary>
    /// <returns></returns>
    public void TextPlayNoSpan(string st)
    {
        textmesh.text = st;
    }

}