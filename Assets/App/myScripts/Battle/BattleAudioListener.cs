﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルのオーディオリスナー
/// </summary>
public class BattleAudioListener : Singleton<BattleAudioListener>
{
    [SerializeField]
    private AudioListener battleAudioListener;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    
    /// <summary>
    /// BattleのAudioListenerを設定する
    /// </summary>
    /// <param name="toEnable"></param>
    public void SetAudioListener(bool toEnable)
    {
        battleAudioListener.enabled = toEnable;
    }
}
