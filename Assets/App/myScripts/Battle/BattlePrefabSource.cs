﻿using UnityEngine;

namespace Battle
{
    /// <summary>
    /// バトルグループ
    /// </summary>
    public class BattlePrefabSource : MonoBehaviour
    {
        public string GroupName;
        public BattlePrefab[] _battlePrefabs;

        [ContextMenu("nameApply")]
        public void ttt()
        {
            GroupName = this.gameObject.name;
        }
    }

    /// <summary>
    /// 敵単体名前
    /// </summary>
    [System.Serializable]
    public class BattlePrefab
    {
        public GameObject _chara;
        public Vector3 _position;
    }
}
