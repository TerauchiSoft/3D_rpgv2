﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace Battle
{
    /// <summary>
    /// プレイヤー、敵のデバッグ用
    /// </summary>
    public partial class BattleActManager
    {
        bool skipEnemyTurn;
        bool skipPlayerTurn;
    }
}