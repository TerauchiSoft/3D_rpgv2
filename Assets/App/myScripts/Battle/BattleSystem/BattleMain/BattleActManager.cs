﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace Battle
{
    /// <summary>
    /// バトルターン中の行動の管理。
    /// </summary>
    public partial class BattleActManager : MonoBehaviour, IBattleEndProcessableObject
    {
        // --------------- IBattleEndProcessableObject ------------

        public void EndProcessing()
        {

        }

        // --------------------------------------------------------

        // ---------------- Field ----------------------

        public bool _isRunning;

        // ---------------- Behaviour ------------------

        [SerializeField] private BattleMessageBehaviour _message;
        [SerializeField] private EffectPlayAll _effectPlayAll;

        // ---------------- Characters -----------------

        private List<BattleCharacter> _sortBattleCharacter = new List<BattleCharacter>();
        private List<PlayerCharacter> _players;
        private List<EnemyCharacter> _enemies;
        private System.Random rand = new System.Random();

        // ---------------- 初期化 ----------------------

        /// <summary>
        /// 行動順セット。
        /// </summary>
        private void OrderRegistration()
        {
            _sortBattleCharacter.Clear();
            _sortBattleCharacter.AddRange(_players);
            _sortBattleCharacter.AddRange(_enemies);
            
            foreach (var c in _sortBattleCharacter)
            {
                c.OrderAgi = c.BattleStatus.B_Agi + rand.Next(10);
            }
            _sortBattleCharacter.Sort();    // 行動優先度の早い順に並び替える。
        }

        /// <summary>
        /// 処理を開始させる。終了もここでさせる。
        /// </summary>
        /// <returns></returns>
        private async Task<bool> startAction()
        {
            OrderRegistration();
            _isRunning = true;
            var task = await CharasProccessing();   // 終了した時に_isRunning = false。
            _isRunning = false;
            endAction();
            return true;
        }

        /// <summary>
        /// ターン毎のバトルの開始処理
        /// </summary>
        /// <param name="players"></param>
        /// <param name="enemies"></param>
        /// <param name="skipEnemyDebug"></param>
        /// <param name="skipPlayerDebug"></param>
        /// <returns></returns>
        public async Task<bool> BattleAction(IEnumerable<PlayerCharacter> players, IEnumerable<EnemyCharacter> enemies, bool skipEnemyDebug = false, bool skipPlayerDebug = false)
        {
            _players = players.ToList();
            _enemies = enemies.ToList();

            skipEnemyTurn = skipEnemyDebug;
            skipPlayerTurn = skipPlayerDebug;

            await startAction();
            return true;
        }

        // ----------------- 終了処理 ---------------------
        
        /// <summary>
        /// このクラスを終了するときの処理
        /// </summary>
        private void endAction()
        {
            _isRunning = false;
        }

        /// <summary>
        /// 逃げるとき
        /// </summary>
        /// <returns>The end.</returns>
        private IEnumerator toEnd()
        {
            BattleMain.Instance.StatusToCommon();
            BattleMain.Instance.End(true); // このクラスのEndProcessingはBattleMain.End()から呼ばれる。
            yield return 0;
        }

        // --------------- private --------------------------

        /// <summary>
        /// 逃げられるか判定
        /// </summary>
        /// <returns></returns>
        private bool getCanRunaway()
        {
            //TODO:実実装をする。
            int playersLV = 50;
            foreach (var p in _players)
            {
                playersLV += p.BattleStatus.Lv;
            }

            int enemiesLV = 0;
            foreach (var e in _enemies)
            {
                enemiesLV += e.BattleStatus.Lv;
            }
            if (playersLV > enemiesLV)
                return true;
            else
                return false;
        }

        // --------------- 本体で呼ばれる処理 ------------------

        /// <summary>
        /// 行動はじめの時のメッセージ再生。
        /// </summary>
        /// <returns>The act message.</returns>
        /// <param name="chara">Chara.</param>
        private async Task<bool> playActMessage(BattleCharacter chara)
        {
            if (chara.BattleStatus.NextAct == Battle_Status.Act.Skill)
                await _message.PlayAttack(chara.BattleStatus);
            else if (chara.BattleStatus.NextAct == Battle_Status.Act.Item)
                await _message.PlayItem(chara.BattleStatus);
            else if (chara.BattleStatus.NextAct == Battle_Status.Act.RunAway)
            {
                var canRunaway = getCanRunaway();
                await _message.PlayRunaway(canRunaway);
                if (canRunaway)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 敵の行動を決定
        /// </summary>
        /// <param name="c"></param>
        private void decideEnemyAct(EnemyCharacter c)
        {
            var enm = c as EnemyCharacter;
            enm.BattleEnemyAct.AIMethodRun(enm, _players, _enemies, enm.BattleEnemyAct);
        }

        /// <summary>
        /// ダメージ、回復量、ステータス上昇下降などの計算。
        /// 登録してある対象全てに対して計算が行われる。
        /// </summary>
        /// <param name="chara"></param>
        private void calculation(BattleCharacter chara)
        {
            foreach (var enm in chara.BattleStatus.EnmStatus)
            {
                if (enm != null)
                    chara.BattleStatus.SelectedSkl.skl_method.Invoke(chara.BattleStatus, enm);
            }
        }

        /// <summary>
        /// エフェクトによって敵全体、味方全体のキャラクターを取得。
        /// </summary>
        /// <returns>The targets.</returns>
        /// <param name="usechara">Usechara.</param>
        private List<BattleCharacter> getTargets(Battle_Status usechara)
        {
            var tgtIsPlayer = usechara.GetTargetsEnum();
            if (tgtIsPlayer == Battle_Status.PLYorENM.PLY)
                return new List<BattleCharacter>(_players);
            else
                return new List<BattleCharacter>(_enemies);
        }

        /// <summary>
        /// 指定のプレイヤーのステータスのみ取得
        /// </summary>
        /// <param name="battlestatus"></param>
        /// <returns></returns>
        private PlayerCharacter getPlayerForStatus(Battle_Status battlestatus)
        {
            foreach (var p in _players)
            {
                if (p.BattleStatus == battlestatus)
                {
                    return p;
                }
            }
            return null;
        }

        // ********** エフェクトの再生、アニメーションの再生類 **********

        /// <summary>
        /// 攻撃アニメーションなどを再生 ハンドル取得
        /// </summary>
        /// <param name="tgtsts"></param>
        /// <param name="animationName"></param>
        /// <returns></returns>
        private Animator playAttackAnimation(Battle_Status tgtsts, string animationName)
        {
            if (tgtsts.PlyOrEnm == Battle_Status.PLYorENM.ENM)
            {
                tgtsts.anim.Play(animationName);
                return tgtsts.anim;
            }
            return null;
        }

        /// <summary>
        /// エフェクトを再生させる対象キャラを取得
        /// </summary>
        /// <param name="chara"></param>
        /// <returns></returns>
        private IEnumerable<BattleCharacter> getEffectTargets(BattleCharacter chara)
        {
            List<BattleCharacter> ret = new List<BattleCharacter>();
            var groups = getTargets(chara.BattleStatus);
            var statustgt = chara.BattleStatus.GetTargets();
            foreach (var g in groups)
            {
                foreach (var t in statustgt)
                {
                    if (g.BattleStatus == t)
                        ret.Add(g);
                }
            }
            return ret;
        }

        // ********** アニメーション ************

        /// <summary>
        /// TODO:死エフェクト 追加予定
        /// </summary>
        /// <param name="battletgt"></param>
        private void setDeadEffect(BattleCharacter battletgt)
        {
            if (battletgt.BattleStatus.Hp <= 0)
                battletgt.IsDeadEffect = true;
        }

        /// <summary>
        /// エフェクト再生
        /// </summary>
        /// <param name="battletgt"></param>
        /// <param name="efcname"></param>
        /// <param name="viewtype"></param>
        /// <returns></returns>
        private EffekseerEmitter playEffect(BattleCharacter battletgt, string efcname, TableValues.ViewType viewtype)
        {
            if (viewtype == TableValues.ViewType.Multiple)
            {
                if (battletgt.BattleStatus.PlyOrEnm == Battle_Status.PLYorENM.ENM)
                {
                    return _effectPlayAll.PlayEffectEnemyAll(efcname);
                } 
                else
                {
                    return _effectPlayAll.PlayEffectUIAll(efcname);
                }
            }

            battletgt.EffectPlay.CalculatePos();
            return battletgt.EffectPlay.PlayEffect(efcname);
        }

        /// <summary>
        /// ダメージアニメ再生
        /// </summary>
        /// <param name="battlechara"></param>
        private void playDamageAnim(BattleCharacter battlechara)
        {
            if (battlechara.BattleStatus.Hp > 0)
                battlechara.BattleStatus.anim?.Play("Damage");
            else
                battlechara.BattleStatus.anim?.Play("Down");
        }

        // ********** 音声再生 ダメージ揺らし **********

        /// <summary>
        /// damage / MHP
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="calculatevalue"></param>
        /// <returns></returns>
        private float getDmgPerMhp(BattleCharacter chara, int calculatevalue)
        {
            float targetsValues = 0;
            int num = 0;
            var targets = chara.BattleStatus.GetTargets();
            if (targets.Any() == false)
                return 0;

            foreach (var t in targets)
            {
                targetsValues += t.M_Hp;
                num++;
            }
            return calculatevalue / (targetsValues / num);
        }

        /// <summary>
        /// プレイヤーキャラクターを揺らす
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="effect"></param>
        /// <param name="chara"></param>
        /// <param name="calculatevalue"></param>
        private void playCharaShaking(IEnumerable<Battle_Status> targets, TableValues.Effect effect, BattleCharacter chara, int calculatevalue)
        {
            foreach (var t in targets)
            {
                if (t.PlyOrEnm == Battle_Status.PLYorENM.PLY)
                {
                    var player = getPlayerForStatus(t);
                    if (player == null) continue;
                    float dmgpermhp = getDmgPerMhp(chara, calculatevalue);
                    if (effect == TableValues.Effect.Attack)
                    {
                        player.BattleFaceStatus.DamageFaceChange(dmgpermhp);   // 顔の更新 : ダメージの揺れ
                    }
                    else
                    {
                        player.BattleFaceStatus.FaceChange(dmgpermhp);  // 顔の更新 : 回復
                    }
                }
            }
        }

        private const float CAMERASHAKE = 20f;
        /// <summary>
        /// ダメージ時などの画面の揺れ
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="effect"></param>
        /// <param name="chara"></param>
        /// <param name="calculatevalue"></param>
        private void playUIShaking(IEnumerable<Battle_Status> targets, TableValues.Effect effect, BattleCharacter chara, int calculatevalue)
        {
            if (effect != TableValues.Effect.Attack || targets.Any() == false)
                return;

            int num = 0;
            float alldmgperhp = 0;
            foreach (var t in targets)
            {
                if (t.PlyOrEnm == Battle_Status.PLYorENM.PLY)
                {
                    var player = getPlayerForStatus(t);
                    if (player == null) continue;
                    alldmgperhp += getDmgPerMhp(chara, calculatevalue);
                    num++;
                }
            }
            if (num == 0)
                return;

            var cameratrs = BattleMain.Instance.PlayerSpawnDir;

            var dmgperhp = alldmgperhp / num;
            var shakevalue = Mathf.Clamp(dmgperhp * CAMERASHAKE, 0, CAMERASHAKE);

            cameratrs.DOPunchPosition(Vector3.one * shakevalue, 0.58f, 20);
        }

        // ----

        /// <summary>
        /// スキルの効果音を再生
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="calculatevalue"></param>
        private void playSE(BattleCharacter chara, int calculatevalue)
        {
            var effect = chara.BattleStatus.GetEffects().First();
            if (effect == TableValues.Effect.Attack)
            {
                float dmgpermhp = getDmgPerMhp(chara, calculatevalue);
                BattleSound.PlayDamage(dmgpermhp);
            }
        }

        /// <summary>
        /// プレイヤーダメージ時の画面の揺れ
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="calculatevalue"></param>
        private void playDamagePunchAnim(BattleCharacter chara, int calculatevalue)
        {
            var effect = chara.BattleStatus.GetEffects().First();
            if (effect != TableValues.Effect.Attack && effect != TableValues.Effect.HPRecover &&
                effect != TableValues.Effect.MPRecover && effect != TableValues.Effect.HPMPRecover)
                return;

            var targets = chara.BattleStatus.GetTargets();
            if (targets.Any() == false)
                return;

            // プレイヤーの揺れ
            playCharaShaking(targets, effect, chara, calculatevalue);

            // 画面の揺れ
            playUIShaking(targets, effect, chara, calculatevalue);
        }

        // ********** ダメージの計算類 **********

        /// <summary>
        /// ダメージ量、回復量の計算その2
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="skillValue"></param>
        /// <returns></returns>
        private int targetCalculation(BattleCharacter chara, SkillValue skillValue)
        {
            int idx = 0;
            int value = 0;
            var targets = chara.BattleStatus.GetTargets();  // ターゲットをBattleStatusに設定してある値から分岐して取得。
            if (targets.Any() == false)
                return 0;

            foreach (var t in targets)
            {
                skillValue.SetCalculatedAmount(chara.BattleStatus.Calculate(t));// ターゲットへの影響量を計算して設定。
                skillValue.RunSkillActionToTgt(t); // ターゲットへの影響を与える。
                value += skillValue._calculatedAmount;
                idx++;
            }
            return value / idx;
        }

        /// <summary>
        /// ダメージ量、回復量の計算その1
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="effect"></param>
        /// <returns></returns>
        private int damagePheseProcessing(BattleCharacter chara, TableValues.Effect effect)
        {
            // ダメージ、回復、バフ、デバブはSkillValueでやりましょう。
            var skillValue = SkillValue.CreateSkillValue(effect);
            // 効果量の計算、メッセージ用の効果量の取得
            return targetCalculation(chara, skillValue);
        }

        // ------------------ トリガー ------------------------

        /// <summary>
        /// 行動はじめのアニメーションとエフェクト
        /// </summary>
        /// <param name="chara"></param>
        /// <param name="calvalue"></param>
        /// <returns></returns>
        private IEnumerator playingStartEffectAndAnimation(BattleCharacter chara, int calvalue)
        {
            if (chara.BattleStatus.NextAct == Battle_Status.Act.RunAway)
                yield break;

            _message.LockNextMessage(true); // メッセージを送れない。

            // ** 攻撃アニメーション再生(Enemyのみ)
            var enmanim = playAttackAnimation(chara.BattleStatus, chara.BattleStatus.GetEffectName());
            if (enmanim != null)
            {
                yield return 0; //　stateの更新待ち
                var state = enmanim.GetCurrentAnimatorStateInfo(0);
                while (state.normalizedTime < 0.75f)
                {
                    yield return 0;
                    state = enmanim.GetCurrentAnimatorStateInfo(0);
                }
            }
            else
            {
                yield return new WaitForSecondsRealtime(0.2f);
                while (_message.IsMesPrompt == false) { yield return 0; }  // テキスト再生待ち
            }

            // ** ここで攻撃エフェクト、Damageアニメーション、Dieアニメーションを再生。の再生。
            var effecttgt = getEffectTargets(chara);
            foreach (var t in effecttgt)
            {
                t.ResurectChara();
                if (t.IsDeadEffect == true)
                    continue;

                // すでに死亡しているときはもう一度エフェクトを再生しないようにする。
                setDeadEffect(t);

                // エフェクト 現状演出を飛ばす
                var efk = playEffect(t, chara.BattleStatus.GetEffectName(), chara.BattleStatus.GetEffectType()); 
                //yield return 0;
                //while (efk.exists == true) { yield return 0; }

                if (t.BattleStatus.PlyOrEnm == Battle_Status.PLYorENM.PLY)
                    continue;

                // アニメ
                playDamageAnim(t);
            }

            // ダメージ音再生
            playSE(chara, calvalue);
            playDamagePunchAnim(chara, calvalue);

            _message.LockNextMessage(false); // メッセージを送れる。
        }

        /// <summary>
        /// ダメージ、回復、バフ、デバブなどの計算
        /// </summary>
        /// <param name="chara"></param>
        /// <returns></returns>
        private int attackPheseCalculating(BattleCharacter chara)
        {
            if (chara.BattleStatus.NextAct == Battle_Status.Act.none || chara.BattleStatus.NextAct == Battle_Status.Act.RunAway)
                return -1;

            int firstValue = -1;

            var effects = chara.BattleStatus.GetEffects();
            if (effects.Any() == false)
                return 0;

            foreach (var e in effects)
            {
                if (firstValue == -1)
                    firstValue = damagePheseProcessing(chara, e);
                else
                    damagePheseProcessing(chara, e);
            }
            if (chara.BattleStatus.SelectedItem.ItemID != 0)
                chara.BattleStatus.DecSelectedItem();

            return firstValue;
        }

        /// <summary>
        /// アイテムドロップ
        /// </summary>
        /// <param name="player">攻撃をしたプレイヤー</param>
        /// <returns></returns>
        private async Task<bool> ItemDrop(Battle_Status player, Battle_Status target)
        {
            // とりあえずLvをみてレア度が高いアイテムを落とす。
            Item[] items = ItemTable.Instance.Items.Where(it => it.ItemID == it.MaterialItemIDs[0]).ToArray();
            Item item = items[rand.Next(items.Length)];

            bool isAdded = false;
            string charaName = string.Empty;
            if (PlayerDatas.Instance.CanAddItem(item, 1, player.ObjectID))
            {
                // 攻撃したキャラにアイテム追加できる場合
                PlayerDatas.Instance.p_chara[player.ObjectID].AddItem(item);
                isAdded = true;
                charaName = player.CharaName;
            }
            else
            {
                // 攻撃したキャラがアイテム一杯の場合
                PlayerCharaData data = PlayerDatas.Instance.p_chara.FirstOrDefault(p => p.AddItem(item) != -1);
                isAdded = data != null ? true : false;
                charaName = data != null ? data.PlayerName : string.Empty;
            }
            await _message.PlayItemDrop(item);
            await _message.PlayItemHave(isAdded, item.ItemName, charaName);

            target.SetDieAttackCount(0);
            return true;
        }

        // ----------------- ここからうごかす -----------------------

        /// <summary>
        /// キャラクターの行動をさせる
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CharasProccessing()
        {
            foreach (var c in _sortBattleCharacter)
            {
                // 敵の行動をルーチンに選択させる。死んでいても実行する。
                if (c is EnemyCharacter)
                {
                    // デバッグスイッチが有効なら戻る
                    if (skipEnemyTurn)
                        continue;

                    decideEnemyAct(c as EnemyCharacter);
                }
                else
                {
                    // デバッグスイッチが有効なら戻る
                    if (skipPlayerTurn)
                        continue;
                }


                // 生存していたら行動に関する動作をする。
                if (c.BattleStatus.Hp > 0)
                {
                    //ダメージ、回復、バフ、デバブなどの計算。死亡、状態異常もつける。
                    var value = attackPheseCalculating(c);
                    Debug.Log("ダメージ値 : "+ value);

                    // エフェクト再生、アニメーション。死亡時は消滅アニメーション。
                    StartCoroutine(playingStartEffectAndAnimation(c, value));

                    // メッセージ再生。(TODO:逃げる判定もしているので分ける。)
                    var isRun = await playActMessage(c);
                    if (isRun)
                    {   // 逃げることができると、BattleMainのEnd()を呼び出してバトル終了。
                        await toEnd();
                        return false;
                    }

                    // なにもしなかった(逃げる失敗も)
                    if (value == -1)
                        return false;

                    // 計算後、対象と得られた値でメッセージ表示。
                    var effects = c.BattleStatus.GetEffects().ToList();
                    var targets = c.BattleStatus.GetTargets().ToList();

                    if (targets.Any() == true)
                        await _message.PlayMessageForEffect(effects, c.BattleStatus, value, targets);

                    if (c.BattleStatus.PlyOrEnm == Battle_Status.PLYorENM.ENM || !effects.Any(e => e == TableValues.Effect.Attack))
                        continue;
                    
                    // しかばねドロップ
                    for (int i = 0; i < targets.Count(); i++)
                    {
                        if (targets[i].Hp <= 0 && targets[i].GetSikabaneCount() > 0)
                        {
                            // 屍処理
                            await ItemDrop(c.BattleStatus, targets[i]);
                        }
                    }
                }
            }
            return false;
        }

    }
}
