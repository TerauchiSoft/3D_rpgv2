﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

namespace Battle
{
    public class ResultAddExpHandle : MonoBehaviour, IBattleEndProcessableObject
    {
        public BattleMessageBehaviour _message;
        public List<PlayerCharacter> _players;
        public List<EnemyCharacter> _enemies;
        public void Init(BattleMessageBehaviour message, List<PlayerCharacter> players, List<EnemyCharacter> enemies)
        {
            _message = message;
            _players = players;
            _enemies = enemies;
        }

        public void EndProcessing() { }

        public async Task<bool> AddExpPhese()
        {
            AddExp();
            AddG();
            await Task.Delay(300);
            int idx = 0;
            foreach (var p in _players)
            {   // バトルステータスデータを共通データに上書きする。
                CommonData.SetBattleStatusToCommonPlayer(p.BattleStatus, idx);
                idx++;
            }
            await Task.Delay(300);
            await CheckLVUP();
            await _message.PlayG(addG);
            return true;
        }

        // ------------- 経験値演出 --------------

        /// <summary>
        /// 経験値を追加する。Sliderの値が振り切るとレベルアップ。
        /// Sliderからレベルアップを実行させる。
        /// </summary>
        /// <returns>The exp.</returns>
        void AddExp()
        {
            int addExp = 0;
            foreach (var e in _enemies)
            {
                addExp += e.BattleStatus.Exp;
            }
            var idx = 0;
            foreach (var p in _players)
            {
                var id = idx;
                var player = p;
                // ダミーデータの場合スキップ
                if (!p.BattleStatus.isDammy)
                {
                    p.ExpSlider.AddLevelUpAct(() => LevelUp(player, id));
                    p.BattleStatus.Exp += addExp;
                }
                idx++;
            }
        }

        async void LevelUp(PlayerCharacter p, int id)
        {
            BattleSound.PlayLVUP();
            CommonData.Instance.LvUp(id);
            string skillName = CommonData.Instance.GetSkillName(id);
            await _message.PlayLVUP(p);
            if (!string.IsNullOrEmpty(skillName))
            {
                BattleSound.PlayGetSkill();
                await _message.PlayGetSkill(p, skillName);
            }
            Map_Exp.IsNowLevelUp = false;
        }

        IEnumerator CheckLVUP()
        {
            var sliders = _players.Select(p => p.ExpSlider);
            while (true)
            {
                var num = 0;
                foreach (var s in sliders)
                {
                    if (s.isValueChange == true)
                    {
                        num++;
                    }
                }
                if (num == 0)
                    yield break;

                yield return new WaitForSeconds(0.2f);
            }
        }

        // ------------- G取得演出 ---------------

        int addG;
        void AddG()
        {
            addG = 0;
            foreach (var e in _enemies)
            {
                addG += e.BattleStatus.G;
            }
            _players[0].BattleStatus.G += addG;
        }
    }
}