﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// デバッグの変数やトグルを定義
    /// </summary>
    public partial class BattleMain
    {
        // --------------- デバッグスイッチ --------------------

        // 敵        
        public bool SkipEnemyTurn;
        public bool EnemyHpInfinity;
        public bool EnemyMpInfinity;

        // プレイヤー
        public bool SkipPlayerTurn;
        public bool PlayerHpInfinity;
        public bool PlayerMpInfinity;

        // --------------------------------------------------

        /// <summary>
        /// 毎ターン必要な処理をここで実行する。
        /// </summary>
        public void DebugRun()
        {
            // 敵
            if (EnemyHpInfinity || EnemyMpInfinity)
            {
                foreach (var e in _enemyCharacter)
                {
                    if (EnemyHpInfinity)
                        e.BattleStatus.Hp = e.BattleStatus.M_Hp;

                    if (EnemyMpInfinity)
                        e.BattleStatus.Mp = e.BattleStatus.M_Mp;
                }
            }

            // プレイヤー
            if (PlayerHpInfinity || PlayerHpInfinity)
            {
                foreach (var p in _playerCharacter)
                {
                    if (PlayerHpInfinity)
                        p.BattleStatus.Hp = p.BattleStatus.M_Hp;

                    if (PlayerMpInfinity)
                        p.BattleStatus.Mp = p.BattleStatus.M_Mp;
                }
            }

        }
    }
}
