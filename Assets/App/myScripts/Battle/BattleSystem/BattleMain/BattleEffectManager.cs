﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Battle
{
    /// <summary>
    /// 毒などのターン毎の状態変化を管理する。敵全員死亡、味方全員死亡でバトルを抜ける。
    /// </summary>
    public class BattleEffectManager : IBattleEndProcessableObject
    {
        // ---------------------------------------

        BattleMessageBehaviour _message;
        List<PlayerCharacter> _players = new List<PlayerCharacter>();
        List<EnemyCharacter> _enemies = new List<EnemyCharacter>();

        // ---------------------------------------

        public BattleEffectManager(BattleMessageBehaviour message, IEnumerable<PlayerCharacter> players, IEnumerable<EnemyCharacter> enemies)
        {
            _message = message;
            _players.AddRange(players);
            _enemies.AddRange(enemies);
        }

        // --------------- IBattleEndProcessableObject ------------

        public void EndProcessing()
        {
        }

        // ------------------ 各処理 ------------------

        void CheckStatusCalculate()
        {
            foreach (var p in _players)
            {
                p.BattleStatus.Turn();
            }
        }

        async Task<bool> CheckWin()
        {
            foreach (var e in _enemies)
            {
                if (e.BattleStatus.Hp > 0)
                {
                    return false;
                }
            }
            BattleMusic.PlayWin();
            await _message.PlayWin();
            return true;
        }

        async Task<bool> CheckLose()
        {
            foreach (var p in _players)
            {
                if (p.BattleStatus.Hp > 0)
                {
                    return false;
                }
            }
            BattleMusic.PlayLose();
            await _message.PlayLose();
            BattleMain.Instance.End(false); // このクラスのEndProcessingはBattleMain.End()から呼ばれる。
            return true;
        }

        async Task<bool> CheckPoison()
        {
            return false;
        }

        // ----------------- 本体 ----------------

        bool isWin;
        bool isEnd;
        /// <summary>
        /// ターンごとの処理を行い、勝利している場合は勝利を返す。
        /// </summary>
        /// <returns>The turn process.</returns>
        public async Task<bool> EachTurnProcess()
        {
            CheckStatusCalculate(); // ターンごとの計算。
            isEnd = false;
            if (!isEnd)
            {
                isEnd = await CheckLose();
            }
            if (!isEnd)
            {
                isEnd = await CheckWin();
                isWin = isEnd;
            }
            if (!isEnd)
            {
                isEnd = await CheckPoison();
            }
            return isWin;
        }
    }
}