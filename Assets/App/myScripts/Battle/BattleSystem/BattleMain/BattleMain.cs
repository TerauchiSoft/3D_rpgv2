﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Battle
{
    /// <summary>
    /// Ballte main.
    /// </summary>
    public partial class BattleMain : MonoBehaviour {
        public static BattleMain Instance;
        bool IsBattleEnd;

        // ----------------- 処理に必要なクラスインスタンス ------------------

        // 敵と主人公のステータス。
        public List<PlayerCharacter> _playerCharacter = new List<PlayerCharacter>();
        public List<EnemyCharacter> _enemyCharacter = new List<EnemyCharacter>();
        [SerializeField] RectTransform _playerSpawnDir;
        [SerializeField] Transform _enemySpawnDir;
        
        // ステータス初期化
        StatusLoader _statusLoader;

        // -------------- ゲームオブジェクトにアタッチされたスクリプト ---------

        // メッセージ再生
        [SerializeField] BattleMessageBehaviour _message;
        // カメラの操作
        [SerializeField] BattleCameraBehaviour _camera;
        // プレイヤー行動セレクタ
        [SerializeField] PlayerActSelectManager _playerActSelectManager;
        // 行動選択時のキャラ選択マネージ
        [SerializeField] SelectEnemyTarget _selectEnemyTarget;
        [SerializeField] SelectPlayerTarget _selectPlayerTarget;
        // バトル行動選択マネージャー
        [SerializeField] Battle_UseBookAct _battle_UseBookAct;
        // バトル命令マネージャー
        [SerializeField] BattleActManager _battleActManager;
        // バトル効果マネージャー
        BattleEffectManager _battleEffectManager;
        // 経験値、G取得
        [SerializeField] ResultAddExpHandle _resultAddExpHandle;
        // フェードアウト用のイメージ
        [SerializeField] Image _blackMask;

        // --------------- 初期化 ------------------

        bool isWakedup = false;
        // Use this for initialization
        async void Start() {
            isWakedup = true;

            Battle.BattleMusic.Instance.SetVolume(Map_Audio.Instance.GetVolumeCache());
            Battle.BattleSound.Instance.SetVolume(Map_Sound.Instance.GetVolumeCache());

            _blackMask.color = Color.clear;
            BattleMusic.PlayBattle1();

            Instance = this;
            IsBattleEnd = false;

            SetUpComponent();   // 各種コンポーネントの変数の初期化など
            SetBattleRefObject();   // 共通使用物のロード

            // 顔更新
            foreach (var p in _playerCharacter)
            {
                if (p.BattleStatus.isDammy)
                    continue;

                var dmgpermhp = p.BattleStatus.Hp / p.BattleStatus.M_Hp;
                p.BattleFaceStatus.DamageFaceChange(dmgpermhp);   // 顔の更新
            }

            // 敵出現メッセージ
            await _message.PlayAppear(new Battle_Status[] { _enemyCharacter[0].BattleStatus });

            PlayerController.Instance.ResetInput(30);
            _message.HideWindow();

            WakeBattleThread();
        }

        async void OnEnable()
        {
            if (isWakedup == false)
                return;

            Battle.BattleMusic.Instance.SetVolume(Map_Audio.Instance.GetVolumeCache());
            Battle.BattleSound.Instance.SetVolume(Map_Sound.Instance.GetVolumeCache());

            _blackMask.color = Color.clear;
            BattleMusic.PlayBattle1();

            Instance = this;
            IsBattleEnd = false;

            SetUpComponent();   // 各種コンポーネントの変数の初期化など
            SetBattleRefObject();   // 共通使用物のロード

            // 顔更新
            foreach (var p in _playerCharacter)
            {
                if (p.BattleStatus.isDammy)
                    continue;

                var dmgpermhp = p.BattleStatus.Hp / p.BattleStatus.M_Hp;
                p.BattleFaceStatus.DamageFaceChange(dmgpermhp);   // 顔の更新
            }

            // 敵出現メッセージ
            await _message.PlayAppear(new Battle_Status[] { _enemyCharacter[0].BattleStatus });

            PlayerController.Instance.ResetInput(30);
            _message.HideWindow();

            WakeBattleThread();
        }

        // ---------------- 終了処理 -----------------

        public void StatusToCommon()
        {
            int idx = 0;
            foreach (var p in _playerCharacter)
            {   // 非常時バトルステータスデータを共通データに上書きする。
                CommonData.SetBattleStatusToCommonPlayer(p.BattleStatus, idx);
                idx++;
            }
        }

        /// <summary>
        /// 全ての演出が終了してから呼び出す。
        /// </summary>
        /// <param name="isAlive">If set to <c>true</c> is alive.</param>
        public void End(bool isAlive)
        {
            EndProcessing(isAlive);
        }

        /// <summary>
        /// バトル終了時の処理
        /// </summary>
        /// <param name="isAlive"></param>
        public void EndProcessing(bool isAlive)
        {
            _playerCharacter.Clear();
            _enemyCharacter.Clear();
            
            _statusLoader = null;

            _playerActSelectManager.EndProcessing();

            _battle_UseBookAct.EndProcessing();
            _battleActManager.EndProcessing();
            _battleEffectManager.EndProcessing();
            UITray._current = null;// メニュー開けるように

            IsBattleEnd = true; // whileループを抜ける。

            // フェードアウト
            _blackMask.DOColor(Color.black, 1.0f).OnComplete(() =>
            {
                if (!isAlive) {
#if PLATFORM_STANDALONE
                    Application.Quit();
#endif
#if UNITY_EDITOR
                    EditorApplication.isPlaying = false;
#endif
                }
                _camera.EndProcessing();
                PheseManager.Instance.ChangeToGameMode("Map", () => BattleEndCallback());
                BattleMain.Instance.DeleteRegistedObject();
                PlayerController.Map_InputConfigSet();
            });
        }

        /// <summary>
        /// バトル終了時処理
        /// </summary>
        private void BattleEndCallback()
        {
            MapUI.Instance.Battle(false);
            CameraMove.Instance.enabled = true;
        }

        // ------------- コンポーネントのセットアップ ---------------

        private void SetUpComponent()
        {
            // プレハブをスポーンさせて表示。
            PrefabLoader.Instance.RegistPlayers(CommonData.Instance.PlayerNum, CommonData.Instance.Erina.isJoined);
            _playerCharacter = PrefabLoader.Instance.SpawnPlayerPrefabs(_playerSpawnDir).ToList();
            _enemyCharacter = PrefabLoader.Instance.SpawnEnemyPrefabs(_enemySpawnDir).ToList();

            // ステータスをロードさせる。
            _statusLoader = new StatusLoader(_playerCharacter);
            _statusLoader.InitBattleStatusParams(_enemyCharacter);

            // セレクタマネージャーにセレクタをセットする
            _playerActSelectManager.Init(_playerCharacter, _playerCharacter.Select(pl => pl.PlayerActSelecter));

            // カメラの設定。
            _camera.Init();
            _camera.AppearShader();
            _camera.SetOrthographicSize(BattleCamera.Phese.BTLINIT);
            _camera.Idle();

            // 行動選択時のキャラ選択時用のキャラクターパッケージの登録。
            _selectPlayerTarget.Init(_playerCharacter, _enemyCharacter);
            _selectEnemyTarget.Init(_playerCharacter, _enemyCharacter);
            _selectPlayerTarget.InitCursorObject(_playerCharacter);
            _selectEnemyTarget.InitCursorObject(_enemyCharacter);

            // バトル行動選択マネージャーにプレイヤー登録
            _battle_UseBookAct.Init(_playerCharacter);

            // 状態異常、勝利、敗北処理
            _battleEffectManager = new BattleEffectManager(_message, _playerCharacter, _enemyCharacter);

            // 経験値、G取得
            _resultAddExpHandle.Init(_message, _playerCharacter, _enemyCharacter);
        }
    

        // --------------- バトル時の総合処理管理 ---------------

        private void WakeBattleThread()
        {
            BattleThread();
        }

        private async void BattleThread()
        {
            while (!IsBattleEnd)
            {
                DebugRun(); // デバッグスイッチにより色々

                // プレイヤー行動選択
                if (SkipPlayerTurn != true)
                {
                    await _playerActSelectManager.WakeupSelecters();// 行動選択初期化
                    await SelecterMonitoring();                     // 行動選択終了監視
                }

                await _battleActManager.BattleAction(_playerCharacter, _enemyCharacter, SkipEnemyTurn, SkipPlayerTurn); // 開始トリガー。終了を検知するまで待つ
                bool isWin = await _battleEffectManager.EachTurnProcess();
                if (isWin)
                {
                    await _resultAddExpHandle.AddExpPhese();
                    BattleMain.Instance.End(true);
                }
                _message.HideWindow();
                //await _message.PlayAppear(new Battle_Status[] { _enemyCharacter[0].BattleStatus }); // テスト
            }
        }

        // --------------- IEnumerator --------------

        // セレクタが選択状態の時監視
        IEnumerator<int> SelecterMonitoring()
        {
            yield return 30;
            while (_playerActSelectManager._isWakeup == true)
            {
                yield return 0;
            }
        }
    }
}