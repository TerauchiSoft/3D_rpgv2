﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Battle
{
    public class SelectPlayerTarget : SelectTarget
    {
        public override void InitCursorObject(IEnumerable<BattleCharacter> charas)
        {
            _character = charas.ToList();
            foreach (var chara in _character)
            {
                chara.CursorAnimeObject = chara.BehaviourObject.GetComponent<GenericFlame>();
                    //Instantiate(_pref_CursorFlame, chara.BehaviourObject.transform.position, Quaternion.identity, _spawnCursorFlame)
                    //.GetComponent<GenericFlame>();
            }
            _objectFlame = _character.Select(ch => ch.CursorAnimeObject).ToArray();
            base.Start();   // _objectFlame最大数登録。
        }

        // -------------- プレイヤーの登録 ------------------

        public override void RegistTarget()
        {
            _callerChara.SetEnemiesStatus(_callerChara, _playerStatuses);
            base.RegistTarget();
        }


        // -------------- Update -------------------

        public void Update()
        {
            if (!_isRunning)
                return;

            base.CursorMove(_plycnt.joy_Axis_buf);
            base.GotoNextTray(_plycnt.isFire1);
            base.GotoBackTray(_plycnt.isFire2);
        }
    }
}