﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Battle
{
    /// <summary>
    /// バトルの行動選択時、スキルで敵を選択する時に使用。
    /// </summary>
    public class SelectEnemyTarget : SelectTarget
    {
        /// <summary>
        /// カーソルアニメをするGenericFlameのオブジェクト。
        /// Init時にキャラの数だけ配置される。
        /// 選択すると対象が決定される。
        /// </summary>
        [SerializeField] GameObject _pref_CursorFlame;
        public override void InitCursorObject(IEnumerable<BattleCharacter> charas)
        {
            _character = charas.ToList();
            foreach (var chara in _character)
            {
                chara.CursorAnimeObject =
                    Instantiate(_pref_CursorFlame, chara.BehaviourObject.transform.position + Vector3.back, Quaternion.identity, _spawnCursorFlame)
                    .GetComponent<GenericFlame>();
                BattleMain.Instance.RegistDeleteObject(chara.CursorAnimeObject.gameObject);
            }
            _objectFlame = _character.Select(ch => ch.CursorAnimeObject).ToArray();
            base.Start();   // _objectFlame最大数登録。
        }

        public override void OnOpenTray()
        {
            _nowFlameCursor = 0;
            // 生存ターゲットを初期カーソルにする。
            if (_character[_nowFlameCursor].BattleStatus.Hp <= 0)
            {

                int idx = 0;
                foreach (var o in _objectFlame)
                {
                    LeaveAnime(idx);
                }

                idx = 0;
                foreach (var c in _character)
                {
                    if (c.BattleStatus.Hp > 0)
                    {
                        _nowFlameCursor = idx;
                        CursorAnime(_nowFlameCursor);
                        return;
                    }
                    idx++;
                }
            }
        }

        // -------------- 敵の登録 ------------------

        public override void RegistTarget()
        {
            _callerChara.SetEnemiesStatus(_callerChara, _enemyStatuses);
            base.RegistTarget();
        }

        public void CursorMoveEnemy(Vector2 input, int numOfline = 1, bool isSound = true)
        {
            if (_target == TableValues.Tgt.Me || _target == TableValues.Tgt.EnemyAll || _target == TableValues.Tgt.PlayerAll)
            {
                return;
            }

            if (_character[_nowFlameCursor].BattleStatus.Hp <= 0)
            {
                var arrow = _nowFlameCursor - _beforeIdx;
                base.CursorMoveSelect(Vector2.down * arrow, numOfline, isSound);
            }

            base.CursorMoveSelect(input, numOfline);
        }

        // -------------- Update -------------------

        public void Update()
        {
            if (!_isRunning)
                return;

            CursorMoveEnemy(_plycnt.joy_Axis_buf, 1, false);
            base.GotoNextTray(_plycnt.isFire1);
            base.GotoBackTray(_plycnt.isFire2);
        }
    }
}