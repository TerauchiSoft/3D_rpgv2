﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// バトル中のダメージ音など
    /// </summary>
    public class BattleSound : MonoBehaviour, IAudioSaveLoad
    {
        static BattleSound instance;
        public static BattleSound Instance 
        { 
            get { if (instance == null) instance = FindObjectOfType<BattleSound>(); return instance; }
        }
        AudioSource _audioSource;

        private void OnEnable()
        {
            instance = this;
            _audioSource = GetComponent<AudioSource>();
            _audioSource.loop = false;
        }

        // ------------- ダメージ ---------------

        /// <summary>
        /// ダメージ具合で1 ~ 8までのダメージ音。
        /// </summary>
        /// <returns>The index for damage per hp.</returns>
        /// <param name="dmgperhp">Dmgforhp.</param>
        static int GetIdxForDamagePerHp(float dmgperhp)
        {
            var chip = 0.124999f;
            for (int i = 1; i <= 7; i++)
            {
                if (chip * i > dmgperhp)
                {
                    return i - 1;
                }
            }
            return 8 - 1;
        }

        // -------------- 再生 -----------------

        [SerializeField] AudioClip[] _damages = new AudioClip[8];
        public static void PlayDamage(float dmgperhp)
        {
            Instance._audioSource.Stop();
            var idx = GetIdxForDamagePerHp(dmgperhp);
            Instance._audioSource.clip = Instance._damages[idx];
            Instance._audioSource.Play();
        }

        [SerializeField] AudioClip[] _bossdamages = new AudioClip[8];
        public static void PlayBossDamage(float dmgperhp)
        {
            Instance._audioSource.Stop();
            var idx = GetIdxForDamagePerHp(dmgperhp);
            Instance._audioSource.clip = Instance._bossdamages[idx];
            Instance._audioSource.Play();
        }

        [SerializeField] AudioClip _lvup;
        public static void PlayLVUP()
        {
            Instance._audioSource.clip = Instance._lvup;
            Instance._audioSource.Play();
        }

        [SerializeField] AudioClip _getskill;
        public static void PlayGetSkill()
        {
            Instance._audioSource.clip = Instance._getskill;
            Instance._audioSource.Play();
        }

        // --------------- セーブロード -------------------

        /// <summary>
        /// コンフィグ変更時、ロード時、ボリュームを変更。
        /// </summary>
        /// <param name="volume">Volume.</param>
        public void SetVolume(int volume)
        {
            _audioSource.volume = volume * 0.01f;
        }

        /// <summary>
        /// セーブ時に現在のボリューム値を取得。
        /// </summary>
        /// <returns>The volume cache.</returns>
        public int GetVolumeCache()
        {
            return (int)(_audioSource.volume * 100);
        }
    }
}
