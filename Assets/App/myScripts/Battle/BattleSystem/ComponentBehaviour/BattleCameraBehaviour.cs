﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Battle
{
    /// <summary>
    /// バトルカメラ
    /// </summary>
    public class BattleCameraBehaviour : MonoBehaviour, IBattleEndProcessableObject
    {
        // --------------- IBattleEndProcessableObject ------------

        public void EndProcessing()
        {
            audioListener.enabled = false;
            _battleBackCamera.ToEndPos();
            _backcamera.gameObject.SetActive(false);
        }

        // --------------------------------------------------------

        void Start()
        {
            BattleMain.Instance.SetBattleCamera(_camera);
        }

        // --------------------------------------------------------

        [SerializeField] Camera _backcamera;
        [SerializeField] Camera _camera;
        [SerializeField] AudioListener audioListener;
        BattleCamera _battleCamera;
        BattleBackCamera _battleBackCamera;
        public void Init()
        {
            audioListener.enabled = true;
            _backcamera.gameObject.SetActive(true);
            _battleCamera = new BattleCamera(_camera);
            _battleBackCamera = new BattleBackCamera(_backcamera);
            _battleBackCamera.ToInitPos();
        }

        public MeshRenderer _noiseMeshMap;
        public MeshRenderer _noiseMeshBattle;
        public Image _noiseMeshCanvas;
        public void AppearShader()
        {
            _battleCamera.AppearShader(_noiseMeshMap, _noiseMeshBattle, _noiseMeshCanvas);
        }

        public void SetOrthographicSize(BattleCamera.Phese phese)
        {
            _battleCamera.SetOrthographicSize(phese);
        }

        public async void Zoom(float z, float wait = 1.0f)
        {
            await _battleCamera.Zooming(z, wait);
        }

        public async void Idle(float m = -1f)
        {
            await _battleCamera.Idling(m);
        }

        public IEnumerator Shake(float phese)
        {

            yield return 0;
        }
    }
}