﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

namespace Battle
{
    public class BattleMessageBehaviour : MonoBehaviour, IBattleEndProcessableObject
    {
        // --------------- IBattleEndProcessableObject ------------

        public void EndProcessing()
        {

        }

        // --------------------------------------------------------

        [SerializeField] TextWin _textWin;
        private BattleMessage _message;

        private void Awake()
        {
            _message = new BattleMessage();
        }


        // ----------------- メッセージ待ち --------------------

        public void LockNextMessage(bool iscannext)
        {
            if (iscannext == false)
            {
                _textWin.UnLockNext();
            }
            else
            {
                _textWin.LockNext();
            }
        }

        public bool GetMessageEnd()
        {
            if (TextWin.isMesPlaying == false && PlayerController.Instance.isFire1 == true)
                return true;
            else
                return false;
        }

        public bool IsMesPlaying
        {
            get { return TextWin.isMesPlaying == false; }
        }

        public bool IsMesPrompt
        {
            get { return TextWin.state == TextWin.State.Prompt; }
        }

        // ----------------- メッセージを見せる ----------------

        private void Show(bool isNoSpan)
        {
            _textWin.ShowAnimation(isNoSpan);
        }

        // ----------------- メッセージ閉じる ------------------

        private void Close()
        {
            
        }

        public void HideWindow()
        {
            _textWin.HideAnimation();
        }

        // ----------------- メッセージ再生 --------------------
        // **状態

        public IEnumerator PlayHp(Battle_Status battle_status)
        {
            if (battle_status.PlayedEndMessage)
                yield break;

            Show(false);
            _textWin.StartTextPlayBattle(_message.GetHpStr(battle_status), 0, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        public IEnumerator PlayMp(Battle_Status battle_status)
        {
            if (battle_status.PlayedEndMessage)
                yield break;

            Show(false);
            _textWin.StartTextPlayBattle(_message.GetMpStr(battle_status), 0, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **ダメージ具合
        public IEnumerator PlayDamage(Battle_Status battle_status, int damage)
        {
            int num = 0;
            var targets = battle_status.GetTargets();
            foreach (var e in targets)
            {
                if (e.PlayedEndMessage)
                {
                    num++;
                }
            }
            var mes = _message.GetDamageStr(battle_status, damage);
            // 屍しかない場合
            if (num == targets.Count())
                mes = _message.GetSikabaneStr();

            Show(true);
            _textWin.StartTextPlayBattle(mes , -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **回復
        public IEnumerator PlayRecover(Battle_Status battle_status, int recover)
        {
            if (battle_status.PlayedEndMessage)
                yield break;

            Show(true);
            _textWin.StartTextPlayBattle(_message.GetRecoverStr(battle_status, recover), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **能力アップ仮実装
        public IEnumerator PlayStatusUp(Battle_Status battle_status)
        {
            if (battle_status.PlayedEndMessage)
                yield break;

            Show(true);
            _textWin.StartTextPlayBattle("能力アップ！", -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **~が現れた
        public IEnumerator PlayAppear(IEnumerable<Battle_Status> en_Statused)
        {
            Show(false);
            _textWin.StartTextPlayBattle(_message.GetAppearMessage(en_Statused.ToArray()), 2);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **スキル行動
        public IEnumerator PlayAttack(Battle_Status battle_status)
        {
            Show(false);
            _textWin.StartTextPlayBattle(_message.GetSkillMessage(battle_status), 0, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **アイテム行動
        public IEnumerator PlayItem(Battle_Status battle_status)
        {
            Show(false);
            _textWin.StartTextPlayBattle(_message.GetItemMessage(battle_status), 0, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **逃げるTODO:実実装して
        public IEnumerator PlayRunaway(bool canRunaway)
        {
            Show(true);
            _textWin.StartTextPlayBattle(_message.GetRunaway(canRunaway), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **勝利
        public IEnumerator PlayWin()
        {
            Show(true);
            _textWin.StartTextPlayBattle(_message.GetWinMes(), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **レベルアップ
        public IEnumerator PlayLVUP(PlayerCharacter p)
        {
            Show(true);
            _textWin.StartTextPlayBattle(_message.GetLVUPStr(p.BattleStatus), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **スキル取得
        public IEnumerator PlayGetSkill(PlayerCharacter p, string skillname)
        {
            Show(true);
            _textWin.StartTextPlayBattle(_message.GetSkillStr(p.BattleStatus.CharaName, skillname), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **ゴールド取得
        public IEnumerator PlayG(int g)
        {
            Show(true);
            _textWin.StartTextPlayBattle(StringBuild.GetStringFromArgs(g.ToString(), "Gを手に入れた"), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **敗北
        public IEnumerator PlayLose()
        {
            Show(true);
            _textWin.StartTextPlayBattle(_message.GetLoseMes(), -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **アイテムドロップ
        public IEnumerator PlayItemDrop(Item item)
        {
            Show(true);
            string dropMes = string.Format(_message.GetItemDrop(), item.ItemName);
            _textWin.StartTextPlayBattle(dropMes, -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **アイテム取得メッセージ
        public IEnumerator PlayItemHave(bool isAdded, string itemName, string charaName)
        {
            Show(true);
            string haveMes = _message.GetItemHave(isAdded);
            if (isAdded)
                haveMes = string.Format(haveMes, charaName, itemName);
            _textWin.StartTextPlayBattle(haveMes, -1, true);
            yield return new WaitForSecondsRealtime(0.4f);

            while (GetMessageEnd() == false)
                yield return 0;

            Close();
        }

        // **エフェクト再生 bsに
        // 原則として対象のBattle_Statusが引数
        public async Task PlayMessageForEffect(IEnumerable<TableValues.Effect> effects, Battle_Status bs, int value, IEnumerable<Battle_Status> second_bs = null)
        {
            foreach (var e in effects)
            {
                switch(e)
                {
                    case TableValues.Effect.HPRecover:
                        await PlayRecover(bs, value); // 回復メッセージ
                        foreach (var sbs in second_bs)
                            await PlayHp(sbs);             // 選択した対象の回復後のHPの程度を確かめる
                        break;
                    case TableValues.Effect.MPRecover: // アイテム使用後MPの程度を確かめる
                        foreach (var sbs in second_bs)
                            await PlayMp(sbs);              // 選択した対象の回復後のMPの程度を確かめる
                        break;
                    case TableValues.Effect.HPMPRecover:
                        await PlayRecover(bs, value); // (bs.EnmStatus)回復メッセージ
                        foreach (var sbs in second_bs)
                            await PlayHp(sbs);             // 選択した対象の回復後のHPの程度を確かめる
                        break;
                    case TableValues.Effect.Attack: // 選択した敵(bs.EnmStatus)へのダメージ
                        await PlayDamage(bs, value);
                        foreach (var sbs in second_bs)
                        {
                            await PlayHp(sbs);             // 選択した対象の回復後のHPの程度を確かめる
                            if (sbs.Hp <= 0)
                            {
                                sbs.IncDieAttackCount();
                                sbs.PlayedEndMessage = true;    // バタンキュー再生
                            }
                        }
                        if (bs.PlyOrEnm == Battle_Status.PLYorENM.PLY && bs.NextAct == Battle_Status.Act.Skill && bs.SelectedSkl.mpCost > 0)
                            await PlayMp(bs);          // プレイヤーが魔法を使ったらMP消費を再生する。
                        break;
                    case TableValues.Effect.AGIDown:
                    //    break;
                    case TableValues.Effect.AGIUp:
                    //    break;
                    case TableValues.Effect.CRIDown:
                    //    break;
                    case TableValues.Effect.CRIUp:
                    //    break;
                    case TableValues.Effect.Event:
                    //    break;
                    case TableValues.Effect.LUCKDown:
                    //    break;
                    case TableValues.Effect.LUCKUp:
                    //    break;
                    case TableValues.Effect.MPDown:
                    //    break;
                    case TableValues.Effect.STRDown:
                    //    break;
                    case TableValues.Effect.STRUp:
                    //    break;
                    case TableValues.Effect.VITDown:
                    //    break;
                    case TableValues.Effect.VITUp:
                        //    break;
                        await PlayStatusUp(bs); // 回復メッセージ
                        break;
                }
            }
        }
    }
}