﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class EffectBehaviour : MonoBehaviour
    {
        [SerializeField]
        private GameObject ob_CameraEffectPlayer;
        [SerializeField]
        private GameObject ob_CameraEffectEnemy;

        private CommonData cd;
        public bool isEndRead = false;
        public EffekseerHandle handle;

        private void Start()
        {
            cd = CommonData.Instance;
            handle = new EffekseerHandle();
        }

        // Use this for initialization
        void Update()
        {
            if (cd.isEndRead == true && this.isEndRead == false)
            {
                for (int i = 0; i < SkillTable.Instance.enm_skills.Count; i++)
                    EffekseerSystem.LoadEffect(SkillTable.Instance.enm_skills[i].EffectName);
                for (int i = 0; i < SkillTable.Instance.ply_skills.Count; i++)
                    EffekseerSystem.LoadEffect(SkillTable.Instance.ply_skills[i].EffectName);
                isEndRead = true;
            }
        }

        /// <summary>
        /// エフェクトの開始位置を設定
        /// 味方単体、味方全体、敵単体、敵全体、自分自身
        /// </summary>
        Vector3 EffectPositionSet(TableValues.Tgt tgt, int obnum, GameObject[] trs)
        {
            Vector3 vec = new Vector3();
            if (tgt == TableValues.Tgt.Player)
            {
                vec = trs[obnum].transform.position;
                vec.z = -9.0f;
            }
            if (tgt == TableValues.Tgt.PlayerAll)
            {
                vec = ob_CameraEffectPlayer.transform.position;
            }
            if (tgt == TableValues.Tgt.Enemy)
            {
                vec = trs[obnum].transform.position;
                vec.z = -9.0f;
            }
            if (tgt == TableValues.Tgt.EnemyAll)
            {
                vec = ob_CameraEffectEnemy.transform.position;
            }
            else
            {
                vec = trs[obnum].transform.position;
                vec.z = -9.0f;
            }

            return vec;
        }


        /// <summary>
        /// 発動エフェクト再生
        /// </summary>
        public void PlayActivateEffect(string efkname, GameObject[] trs, TableValues.Tgt tgt, TableValues.Tgt myself, int selectob)
        {
            if (tgt == TableValues.Tgt.Me)
                tgt = myself;

            var vec = EffectPositionSet(tgt, selectob, trs);

            for (int i = 0; i < SkillTable.Instance.enm_skills.Count; i++)
            {
                if (efkname == SkillTable.Instance.enm_skills[i].EffectName)
                {
                    handle = EffekseerSystem.PlayEffect(efkname, vec);
                    return;
                }
            }
            for (int i = 0; i < SkillTable.Instance.ply_skills.Count; i++)
            {
                if (efkname == SkillTable.Instance.ply_skills[i].EffectName)
                {
                    handle = EffekseerSystem.PlayEffect(efkname, vec);
                    return;
                }
            }
        }


    }
}