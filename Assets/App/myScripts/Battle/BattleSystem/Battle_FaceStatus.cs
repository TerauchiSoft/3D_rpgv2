﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Battle {
    public class Battle_FaceStatus : FaceStatus {
        Battle_Status _battleStatus;
        public override void Start()
        {
            _battleStatus = GetComponentInParent<Battle_Status>();
        }

        public override void Update() { }

        void SetFaceAt(int idx)
        {
            foreach (var o in ob_Face)
            {
                o.SetActive(false);
            }
            ob_Face[idx].SetActive(true);
        }

        // ----------- BattleActManagerから ------------

        readonly float MAGNIFICATION = 70;

        /// <summary>
        /// ダメージでの顔変更。
        /// </summary>
        /// <param name="dmgpermhp">Dmgpermhp.</param>
        public void DamageFaceChange(float dmgpermhp)
        {
            SetFaceAt(3);
            var mag = Mathf.Clamp(dmgpermhp * MAGNIFICATION, 0, MAGNIFICATION);
            transform.parent.DOPunchPosition(Vector3.one * mag, 0.62f, 15).OnComplete(() => CharacterFaceSet(_battleStatus.M_Hp, _battleStatus.Hp));
        }

        /// <summary>
        /// 通常での顔変更。
        /// </summary>
        /// <param name="dmgpermhp">Dmgpermhp.</param>
        public void FaceChange(float dmgpermhp)
        {
            CharacterFaceSet(_battleStatus.M_Hp, _battleStatus.Hp);
        }
    }
}