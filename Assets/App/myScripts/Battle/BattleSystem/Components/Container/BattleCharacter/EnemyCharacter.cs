﻿using System;
using UnityEngine;

namespace Battle
{
    public class EnemyCharacter : BattleCharacter
    {
        // public GameObject BehaviourObject { get; set; }
        // public Battle_Status BattleStatus { get; set; }  
        public Battle_EnemyAct BattleEnemyAct { get; set; }

        public EnemyCharacter() : base()
        {
        }

        /// <summary>
        /// 画面ベースのキャラの選択座標を取得する。
        /// </summary>
        /// <returns>The chara select vector.</returns>
        public Vector2 GetCharaSelectVector()
        {
            var camera = BattleMain.Instance.UseCamera;
            var pos = camera.WorldToScreenPoint(BehaviourObject.transform.position);
            return pos;
        }
    }
}