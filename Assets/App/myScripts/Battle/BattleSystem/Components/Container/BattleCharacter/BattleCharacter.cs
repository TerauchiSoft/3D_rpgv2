﻿using UnityEngine;
using System;
using System.Collections;

namespace Battle
{
    public class BattleCharacter : IComparable
    {
        public bool IsDeadEffect { get; set; }  // 死亡時にエフェクトを再生済み
        public GameObject BehaviourObject { get; set; }
        public Battle_Status BattleStatus { get; set; }
        public EffectPlay EffectPlay { get; set; }
        // SelectTarget時に設定されるGenericFlameオブジェクト
        public　GenericFlame CursorAnimeObject { get; set; }
        public int OrderAgi { get; set; }   // 行動優先度

        public BattleCharacter()
        {
        }

        /// <summary>
        /// 生きているときの復活処理
        /// </summary>
        public void ResurectChara()
        {
            if (BattleStatus.Hp > 0)
            {
                IsDeadEffect = false;
                BattleStatus.PlayedEndMessage = false;
                BattleStatus.SetDieAttackCount(0);
            }
        }

        /// <summary>
        /// Agiの順番をBattleCharacter[]でソートできるようにする。
        /// </summary>
        /// <returns>The IC omparable. compare to.</returns>
        /// <param name="obj">Object.</param>
        int IComparable.CompareTo(object obj)
        {
            if (obj == null) 
                return -1;

            BattleCharacter otherBattleCharacter = obj as BattleCharacter;
            if (otherBattleCharacter != null)
                return -this.OrderAgi.CompareTo(otherBattleCharacter.OrderAgi);
            else
                throw new ArgumentException("Object is not a BattleCharacter");
        }
    }
}