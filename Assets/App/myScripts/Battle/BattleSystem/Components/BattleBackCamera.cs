﻿using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

namespace Battle
{
    public class BattleBackCamera
    {
        public Camera _camera;

        public BattleBackCamera(Camera cam)
        {
            _camera = cam;
        }

        public void ToInitPos()
        {
            _camera.transform.DOLocalMoveZ(-0.52f, 4.0f);
        }

        public void ToEndPos()
        {
            _camera.transform.DOLocalMoveZ(-0.28f, 0.5f);
        }
    }
}