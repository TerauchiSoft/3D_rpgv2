﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle
{
    /// <summary>
    /// 全体化エフェクトの再生。
    /// </summary>
    public class EffectPlayAll : MonoBehaviour
    {
        public EffectPlayForUI _uiAll;
        public EffectPlayForObject _enemyAll;

        public EffekseerEmitter PlayEffectUIAll(string effectName)
        {
            _uiAll.CalculatePos();
            return _uiAll.PlayEffect(effectName);
        }

        public EffekseerEmitter PlayEffectEnemyAll(string effectName)
        {
            _enemyAll.CalculatePos();
            return _enemyAll.PlayEffect(effectName);
        }
    }
}