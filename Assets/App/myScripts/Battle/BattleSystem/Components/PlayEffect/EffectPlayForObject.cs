﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class EffectPlayForObject : EffectPlay
    {
        public float _mag = 1.0f;

        float z;
        float magScale;
        // Use this for initialization
        public override void CalculatePos()
        {
            z = -transform.position.z / 10.0f;
            magScale = 1 + z * z * 1.225f;

            //var efkOb = new GameObject();
            //var efk = efkOb.AddComponent<EffekseerEmitter>();
            //efk.effectName = this.effectName;
            //efk.playOnStart = this.playOnStart;
            //efk.loop = this.loop;
            //efkOb.transform.position = transform.position;
            //efkOb.transform.localScale = new Vector3(magScale * 1.0f, magScale * 1.0f, magScale * 1.0f);
            //gameObject.transform.localScale = new Vector3(_mag, _mag, _mag);
        }

        //private void Update()
        //{
            //var z = -transform.position.z / 10.0f;
            //var magScale = 1 + z * z * 1.225f;
            //gameObject.transform.localScale = new Vector3(magScale, magScale, magScale);
        //}

        public override EffekseerEmitter PlayEffect(string effectName)
        {
            // エラーログ用
            effectname = effectName;

            // オブジェクト生成
            var efkOb = new GameObject(effectName);

            //　オブジェクト消去
            var dest = efkOb.AddComponent<ObjectDestroy>();
            dest.SetUpDestroy(4f);

            // 出現機構設定
            var efk = efkOb.AddComponent<EffekseerEmitter>();
            efk.effectName = effectName;    //this.effectName;
            efk.playOnStart = false;    //this.playOnStart;
            efk.loop = false;   //this.loop;

            // 位置合わせ
            efkOb.transform.position = transform.position;
            efkOb.transform.localScale = new Vector3(magScale * 1.0f, magScale * 1.0f, magScale * 1.0f);
            gameObject.transform.localScale = new Vector3(_mag, _mag, _mag);

            // 再生
            StartCoroutine(Playing(efk));
            return efk;
        }

        string effectname;
        IEnumerator Playing(EffekseerEmitter effect)
        {
            yield return 2;
            try
            {
                effect.Play();
            }
            catch
            {
                throw new System.Exception("エフェクトの名前 : " + effectname);
            }
        }
    }
}