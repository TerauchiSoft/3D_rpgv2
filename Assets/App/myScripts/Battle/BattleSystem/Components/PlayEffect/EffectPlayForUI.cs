﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle
{
    public class EffectPlayForUI : EffectPlay
    {
        private RectTransform _parent;
        private RectTransform _rectUI;

        public Rect parentSize;
        public float pX;
        public float pY;
        public Vector2 viewPos;
        public Vector3 pos;

        public override void CalculatePos()
        {
            _useCamera = BattleMain.Instance.UseCamera;
            _rectUI = transform as RectTransform;
            _parent = transform.parent.transform as RectTransform;
            parentSize = _parent.rect;
            pX = _rectUI.pivot.x * _rectUI.rect.width / _parent.rect.width;
            pY = _rectUI.pivot.y * _rectUI.rect.height / _parent.rect.height;
            viewPos = new Vector2(pX, pY);
            pos = _useCamera.ViewportToWorldPoint(_rectUI.anchorMin + viewPos);
            ///GameObject go = new GameObject();
            //var efk = go.AddComponent<EffekseerEmitter>();
            //efk.effectName = this.effectName;
            //efk.playOnStart = this.playOnStart;
            //efk.loop = this.loop;
            //go.transform.position = pos;
            //Instantiate(go, pos, Quaternion.identity);
        }

        public override EffekseerEmitter PlayEffect(string effectName)
        {
            // オブジェクト生成
            GameObject go = new GameObject(effectName);

            // オブジェクト消去
            var destroy = go.AddComponent<ObjectDestroy>();
            destroy.SetUpDestroy(4);

            // エフェクト出現機構
            var efk = go.AddComponent<EffekseerEmitter>();
            efk.effectName = effectName;//this.effectName;
            efk.playOnStart = false;    // this.playOnStart;
            efk.loop = false;   //this.loop;

            // 位置合わせ
            go.transform.position = pos;

            // 再生
            StartCoroutine(Playing(efk));
            return efk;
        }

        IEnumerator Playing(EffekseerEmitter effect)
        {
            yield return 0;
            effect.Play();
        }
    }
}