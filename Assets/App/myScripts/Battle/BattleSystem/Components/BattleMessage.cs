﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Reflection;

namespace Battle
{
    public class BattleMessage : SingletonForData<BattleMessage>
    {
        private const string BATTLEMES_PATH = "/Tables/BattleMessages.csv";

        // --------------- Behaviour ----------------------

        /// <summary>
        /// バトルメッセージの初期化
        /// </summary>
        public BattleMessage()
        {
            ReadBattleMessages();
        }


        /// <summary>
        /// バトル説明文をcsvから読み込む
        /// </summary>
        private void ReadBattleMessages()
        {
            string path = Application.streamingAssetsPath + BATTLEMES_PATH;
            string[] strs = File.ReadAllLines(path, System.Text.Encoding.UTF8);
            var type = this.GetType();

            for (int i = 0; i < strs.Length; i++)
            {
                List<string> strSplit = strs[i].Split(',').ToList();
                string tag = strSplit[0];
                var fieldInfo = type.GetField(tag, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

                if (!string.IsNullOrEmpty(strSplit[2]))
                {
                    // 複数の文字列
                    strSplit.RemoveRange(0, 2);
                    fieldInfo.SetValue(this, strSplit.ToArray());
                }
                else
                {
                    // 単数文字列
                    fieldInfo.SetValue(this, strSplit[1]);
                }
            }
        }

        public BattleMessageBehaviour _messageBehaviour;

        // --------------- メッセージリスト ------------------

        private string RUNAWAY;// = "逃げきれた...。";
        private string RUNAWAY_FAILED;// = "逃げ切れない！";

        private string[] APPEAR_STRS;
                                        //    {
                                        //"が躍り出た",
                                        //"の登場だ",
                                        //"は軽快な動き",
                                        //"が襲いかかってきた",
                                        //"と遭遇した",
                                        //"との戦いがはじまった",
                                        //"が現れた",
                                        //"との命のやりとり",
                                        //"とのバトル",
                                        //"とのぶつかり稽古の始まりだ",
                                        //"が道を阻んできた",
                                        //"が絡んできた"
                                        //};

        private string[] HP_STRS_ENEMY;
                                        //   {"体力満タンだよ",
                                        //"とても元気そうだよ",
                                        //"まだまだ元気そうだよ",
                                        //"元気そうだよ",
                                        //"いきり立っている",
                                        //"少し弱ってきた",
                                        //"すこしクラクラだ",
                                        //"クラクラだ",
                                        //"足にきているようだ",
                                        //"苦しそうだ",
                                        //"フラフラだ",
                                        //"もう倒れそう",
                                        //"ばたんきゅ~だ" };

        private string[] HP_STR_ARESTA;
                                        //   {"体力満タンだよ",
                                        //"まだまだ余裕だよ!",
                                        //"まだまだやれるよ!",
                                        //"こんなのかすり傷だよ",
                                        //"いいかんじ",
                                        //"まだやれるよ",
                                        //"ちょっと疲れたかも",
                                        //"傷が痛み始めた",
                                        //"ちょっとクラクラする",
                                        //"苦しい...",
                                        //"もうだめだー...",
                                        //"...",
                                        //"もうだめばたんきゅ~...。" };

        private string[] HP_STR_ERINA;
                                        //   {"体力満タンですわ",
                                        //"まだまだ余裕ですわ!",
                                        //"まだまだやれますわよ!",
                                        //"こんなのかすり傷ですわ!",
                                        //"まだまだですわ!",
                                        //"なかなか手間取りますわね",
                                        //"少し疲れましたわ",
                                        //"体が痛い",
                                        //"ふらつくわね...",
                                        //"体が重いですわ...",
                                        //"うう...",
                                        //"...",
                                        //"私のしたことが...。" };

        private string[] HP_STR_PETIL;
                                        //   {"体力満タンだよ",
                                        //"全然平気だよ!",
                                        //"平気だよ!",
                                        //"まだまだ!",
                                        //"大丈夫だよ",
                                        //"まだやれる",
                                        //"ちょっと辛いかも",
                                        //"足が動かない",
                                        //"ふらふらする",
                                        //"辛い...",
                                        //"ふう...ふう...",
                                        //"...",
                                        //"ヨロレイヒ~...。" };

        private string[] MP_STR_ARESTA;
                                        //   {"魔力満タンだよ",
                                        //"魔力まだまだあるよ!",
                                        //"魔力まだあるよ!",
                                        //"魔力あるよ",
                                        //"魔力ちょっと減ってきたよ",
                                        //"魔力減ってきたよ",
                                        //"魔力けっこう減ってきたよ",
                                        //"魔力節約しないと",
                                        //"魔力少ないよ",
                                        //"魔力あんまりないかも...",
                                        //"魔力もうほとんどない...",
                                        //"魔力全然ない...",
                                        //"魔力空っぽだ...。" };

        private string[] MP_STR_ERINA;
                                        //   {"魔力満タンですわ",
                                        //"魔力まだまだ余裕ですわ!",
                                        //"魔力まだありますわよ!",
                                        //"魔力ちょっと減ってきましわ",
                                        //"魔力減ってきましたわ",
                                        //"魔力半分くらいですわ",
                                        //"魔力半分くらいですわ",
                                        //"魔力少ないですわ",
                                        //"魔力もう残り少ないですわ...",
                                        //"魔力少ししかありませんわ...",
                                        //"魔力あとわずかですわ!",
                                        //"魔力もうありませんわよ!",
                                        //"魔力空っぽですわ...。" };

        private string[] MP_STR_PETIL;
                                        //   {"弾は満タンだよ!",
                                        //"弾はまだまだあるよ!",
                                        //"弾はまだまだ残ってるよ",
                                        //"弾ちょっと減ってきた",
                                        //"弾が減ってきた",
                                        //"弾はあと半分ある",
                                        //"弾はあと半分ほどある",
                                        //"弾がかなり減ってきた",
                                        //"弾は残り少ない",
                                        //"弾は残り少ない",
                                        //"弾は残りわずか",
                                        //"もう弾がない...",
                                        //"弾切れだ...。" };

        private string[] DAMAGE_STR;
                                        //    {
                                        //"超ウルトラ特大ダメージ!!!",
                                        //"ウルトラ特大ダメージ!!",
                                        //"特大ダメージ",
                                        //"かなり大きなダメージ",
                                        //"大ダメージ",
                                        //"大きなダメージ",
                                        //"結構なダメージ",
                                        //"いいダメージ",
                                        //"ダメージ",
                                        //"ほんのりダメージ",
                                        //"ちいさなダメージ",
                                        //"あまり効いていない",
                                        //"全く効いていないようだ...。"
                                        //};

        private string[] RECOVER_STR;
        //    {"体力全快!",
        //"かなり回復",
        //"とても回復",
        //"結構回復",
        //"まあまあの回復",
        //"はんぶん回復",
        //"回復",
        //"少し回復",
        //"ほんのり回復",
        //"ちょっと回復",
        //"ちょっぴり回復",
        //"すすめの涙ほどの回復",
        //"回復しなかった...。"
        //};

        private string[] ENM_STRENGTH_ARESTA;
        private string[] ENM_STRENGTH_ERINA;
        private string[] ENM_STRENGTH_PETIL;
        //    {
        //"相手にとって不足なし",
        //"めちゃくちゃ強そう",
        //"とても強そう",
        //"強そう",
        //"ちょっと強そう",
        //"苦戦するかも",
        //"けっこう拮抗してるかも",
        //"消耗するかも",
        //"魔力つかうかも",
        //"魔力節約しよ",
        //"弱そう",
        //"とても弱そう",
        //"かわいそうだよ、逃げよう"
        //};

        private string ARESTA_LVUP;// "アレスタ「やった！レベルアップだよ！」";
        private string ERINA_LVUP;// "エリナ「レベルアップですわ！」";
        private string PETIL_LVUP;// "ペティル「レベルアップしたよ」";

        private string SIKABANEMES;// "しかばねに強烈な一撃！";
        private string GETITEM;// "魔物は{0}を落とした。";
        private string GETITEM_HAVE;// "{0}は{1}を手に入れた。";
        private string GETITEM_CANNOT;// "しかしアイテムをこれ以上持てない！";
        private string WINMES;// "戦いに勝った！";
        private string LOSEMES;// "戦いに敗北した...。";

        private string GETEDSKILL;// "を覚えた！";

        private string ASESTES;// "アレスタたちに";
        private string MONSTERS;// "魔物たちに";

        private string BATTLE_FUKIDASHI_TEMP;

        private const string USR_TAG = "*Usr";
        private const string PLY_TAG = "*Ply";
        private const string ENM_TAG = "*Enm";

        // --------------- privateメソッド ------------------

        /// <summary>
        /// キャラごとの状態文字列テーブルを取得。
        /// </summary>
        /// <returns>The HPS trings for player chara.</returns>
        /// <param name="charaName">Chara name.</param>
        private string[] GetHPStringsForPlayerChara(string charaName)
        {
            if (charaName == "アレスタ")
                return HP_STR_ARESTA;
            else if (charaName == "エリナ")
                return HP_STR_ERINA;
            else if (charaName == "ペティル")
                return HP_STR_PETIL;
            else
                return HP_STR_ARESTA;
        }

        /// <summary>
        /// キャラごとの状態文字列テーブルを取得。
        /// </summary>
        /// <returns>The HPS trings for player chara.</returns>
        /// <param name="charaName">Chara name.</param>
        private string[] GetMPStringsForPlayerChara(string charaName)
        {
            if (charaName == "アレスタ")
                return MP_STR_ARESTA;
            else if (charaName == "エリナ")
                return MP_STR_ERINA;
            else if (charaName == "ペティル")
                return MP_STR_PETIL;
            else
                return MP_STR_ARESTA;
        }

        /// <summary>
        /// キャラごとのレベルアップ文字列を取得。
        /// </summary>
        /// <returns>The HPS trings for player chara.</returns>
        /// <param name="charaName">Chara name.</param>
        private string GetLVUPStringForPlayerChara(string charaName)
        {
            if (charaName == "アレスタ")
                return ARESTA_LVUP;
            else if (charaName == "エリナ")
                return ERINA_LVUP;
            else if (charaName == "ペティル")
                return PETIL_LVUP;
            else
                return ARESTA_LVUP;
        }

        /// <summary>
        /// 選択した敵の名前を取得。
        /// </summary>
        /// <returns>The enm name for status.</returns>
        private string GetEnmNameForStatus(Battle_Status playerstatus)
        {
            var enmcount = playerstatus.EnmStatus.Count;
            string enmname;
            if (playerstatus.TargetObject == TableValues.Tgt.Player || playerstatus.TargetObject == TableValues.Tgt.Enemy)
            {
                enmname = StringBuild.GetStringFromArgs(playerstatus.EnmStatus[playerstatus.SelectedTgtNo].CharaName, "に");
                return enmname;
            }
            else if (enmcount == 1)
            {
                enmname = StringBuild.GetStringFromArgs(playerstatus.EnmStatus[0].CharaName, "に");
                return enmname;
            }
            else if (playerstatus.TargetObject == TableValues.Tgt.PlayerAll)
            {
                enmname = ASESTES;
                return enmname;
            }
            else if (enmcount > 1)
            {
                enmname = MONSTERS;
                return enmname;
            }
            throw new System.IndexOutOfRangeException("選択した敵の名前を取得できませんでした。");
        }

        /// <summary>
        /// 標的に与えたダメージ割合を取得
        /// </summary>
        /// <returns>The damage calculation.</returns>
        private float GetDamagePercent(Battle_Status playerstatus, float damage)
        {
            if (playerstatus.TargetObject == TableValues.Tgt.Enemy)
            {
                float dmg_per_hp = damage / playerstatus.EnmStatus[playerstatus.SelectedTgtNo].M_Hp;
                return dmg_per_hp;
            }
            else
            {
                float dmg_per_hp = 0;
                foreach (var enm in playerstatus.EnmStatus)
                {
                    dmg_per_hp += damage / enm.M_Hp;
                }
                return dmg_per_hp / playerstatus.EnmStatus.Count;
            }
        }

        /// <summary>
        /// ステータスの減少具合で特定の文字列を返す。
        /// </summary>
        /// <returns>The calculation.</returns>
        /// <param name="per_max">Hp per mhp.</param>
        /// <param name="strs">Strs. 13個の要素</param>
        /// <param name="en">En.</param>
        private string GetStatusMes(float per_max, string[] strs, string en = "")
        {
            Debug.Log("per_max" + per_max);

            if (per_max <= 0)
                return en + strs[strs.Length - 1];

            float div = 1f / strs.Length;
            for (int i = strs.Length; i >= 2; i--)
            {
                if (per_max > div * i - 0.01f)
                {
                    Debug.Log("per_max > div * i" + "per_max:" + per_max + "div:" + div + "i:" + i);
                    return en + strs[strs.Length - i];
                }
            }
            return en + strs[strs.Length - 2];
        }

        /// <summary>
        /// スキル使用時のメタ文字を使用者の名前に変換する。
        /// *Usr *Enmを変換。
        /// </summary>
        /// <returns>The trans skill text to person.</returns>
        /// <param name="skillTexts">Skill texts.</param>
        private IEnumerable<string> GetTransSkillTextToPerson(IEnumerable<string> skillTexts, string replaceName)
        {
            List<string> _texts = new List<string>();
            foreach (var text in skillTexts)
            {
                var t = text.Replace(USR_TAG, replaceName);
                t = t.Replace(ENM_TAG, replaceName);
                _texts.Add(t);
            }
            return _texts;
        }

        /// <summary>
        /// アイテム使用時のメタ文字を対象の名前に変換する。
        /// *Usr *Enmを変換。
        /// </summary>
        private IEnumerable<string> GetTransItemTextToPerson(IEnumerable<string> itemTexts, IEnumerable<Battle_Status> targets)
        {
            string replaceName = targets.First().CharaName;
            if (targets.Count() > 1)
                replaceName += "達";

            List<string> _texts = new List<string>();
            foreach (var text in itemTexts)
            {
                var t = text.Replace(USR_TAG, replaceName);
                t = t.Replace(ENM_TAG, replaceName);
                _texts.Add(t);
            }
            return _texts;
        }

        // --------------- 文章取得メソッド ------------------

        /// <summary>
        /// 勝利メッセージを取得。
        /// </summary>
        /// <returns>The window mes.</returns>
        public string GetWinMes()
        {
            return WINMES;
        }

        /// <summary>
        /// 負けメッセージ取得。
        /// </summary>
        /// <returns>The lose mes.</returns>
        public string GetLoseMes()
        {
            return LOSEMES;
        }

        /// <summary>
        /// アイテムドロップメッセージを取得
        /// </summary>
        /// <returns>The lose mes.</returns>
        public string GetItemDrop()
        {
            return GETITEM;
        }

        /// <summary>
        /// アイテムドロップアイテム取得メッセージを取得
        /// </summary>
        /// <param name="isAdded"></param>
        /// <returns></returns>
        public string GetItemHave(bool isAdded)
        {
            if (isAdded)
                return GETITEM_HAVE;
            else
                return GETITEM_CANNOT;
        }

        /// <summary>
        /// hpごとのメッセージを取得。
        /// </summary>
        /// <returns>The hp string.</returns>
        /// <param name="battle_status">Battle_Status.</param>
        public string GetHpStr(Battle_Status battle_status)
        {
            var isenm = battle_status.PlyOrEnm;
            var name = battle_status.CharaName;
            int hp = battle_status.Hp, m_hp = battle_status.M_Hp;

            var strs = (isenm == Battle_Status.PLYorENM.ENM) ? HP_STRS_ENEMY : GetHPStringsForPlayerChara(name);
            float hp_per_mhp = (float)hp / m_hp;
            return GetStatusMes(hp_per_mhp, strs);
        }

        /// <summary>
        /// mpごとのメッセージを取得。
        /// </summary>
        /// <returns>The mp string.</returns>
        public string GetMpStr(Battle_Status battle_status)
        {
            var name = battle_status.CharaName;
            int mp = battle_status.Mp, m_mp = battle_status.M_Mp;

            var strs = GetMPStringsForPlayerChara(name);
            float mp_per_mmp = (float)mp / m_mp;
            return GetStatusMes(mp_per_mmp, strs);
        }

        /// <summary>
        /// LVUPごとのメッセージを取得。
        /// </summary>
        /// <returns>The mp string.</returns>
        public string GetLVUPStr(Battle_Status battle_status)
        {   
            return GetLVUPStringForPlayerChara(battle_status.CharaName);
        }

        /// <summary>
        /// スキル取得時のメッセージを取得。
        /// </summary>
        /// <param name="charaName">Chara name.</param>
        /// <param name="skillName">Skill name.</param>
        public string GetSkillStr(string charaName, string skillName)
        {
            return StringBuild.GetStringFromArgs(charaName, "は、", skillName, GETEDSKILL);
        }

        /// <summary>
        /// 選択した敵(EnmStatus)についてのダメージメッセージを取得する。
        /// </summary>
        /// <returns>The damage string.</returns>
        /// <param name="playerstatus">Btlstatus.</param>
        /// <param name="myselfnum">Myselfnum.</param>
        /// <param name="damage">Damage.</param>
        public string GetDamageStr(Battle_Status playerstatus, int damage)
        {
            var enmname = GetEnmNameForStatus(playerstatus);
            if (string.IsNullOrEmpty(enmname))
                return enmname;

            float dmg_per_mhp = GetDamagePercent(playerstatus, damage);

            return StringBuild.GetStringFromArgs(enmname, GetStatusMes(dmg_per_mhp, DAMAGE_STR));
        }

        /// <summary>
        /// しかばねに攻撃したときのメッセージ取得。
        /// </summary>
        /// <returns>The sikabane string.</returns>
        public string GetSikabaneStr()
        {
            return SIKABANEMES;
        }

        /// <summary>
        /// 選択した敵(EnmStatus)についての回復メッセージを取得する。
        /// </summary>
        /// <returns>The damage string.</returns>
        /// <param name="playerstatus">Btlstatus.</param>
        /// <param name="myselfnum">Myselfnum.</param>
        /// <param name="recover">Damage.</param>
        public string GetRecoverStr(Battle_Status playerstatus, int recover)
        {
            var enmname = GetEnmNameForStatus(playerstatus);
            if (string.IsNullOrEmpty(enmname))
                return enmname;

            float dmg_per_mhp = GetDamagePercent(playerstatus, recover);

            return StringBuild.GetStringFromArgs(enmname, GetStatusMes(dmg_per_mhp, RECOVER_STR));
        }

        /// <summary>
        /// 敵強さのメッセージ取得
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetStrangthMessage(PlayerCharaData.Name name)
        {
            int plv = BattleMain.Instance._playerCharacter[(int)name].BattleStatus.Lv;
            var enemies = BattleMain.Instance._enemyCharacter;

            int elv = 0;
            int count = 0;
            foreach (var e in enemies)
            {
                elv += e.BattleStatus.Lv;
                count++;
            }
            elv /= count;

            int plv_diff_elv = Mathf.RoundToInt(Mathf.Clamp((plv - elv) + 6, 0, 11));
            if (name == PlayerCharaData.Name.アレスタ)
                return ENM_STRENGTH_ARESTA[plv_diff_elv];
            else if (name == PlayerCharaData.Name.エリナ)
                return ENM_STRENGTH_ERINA[plv_diff_elv];
            else// (player.ObjectID == 2)
                return ENM_STRENGTH_PETIL[plv_diff_elv];
        }

        /// <summary>
        /// バトル吹き出しのステータス表示文言取得
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetFukidashiMes(PlayerCharaData.Name name)
        {
            Battle_Status bs = BattleMain.Instance._playerCharacter[(int)name].BattleStatus;

            string temp = BATTLE_FUKIDASHI_TEMP.Replace("\\n", Environment.NewLine);
            string hpMes = GetHpStr(bs);
            string mpMes = GetMpStr(bs);
            string strangth = GetStrangthMessage(name);

            return string.Format(temp, hpMes, mpMes, "", strangth);
        }

        /// <summary>
        /// ランダムで敵の登場セリフを取得。
        /// </summary>
        /// <returns>The appear message.</returns>
        public string GetAppearMessage(params Battle_Status[] enemies)
        {
            StringBuild.ClearStr();
            StringBuild.Append(enemies[0].CharaName);
            if (enemies.Length > 1)
                StringBuild.Append("達");

            var rand = new System.Random();
            StringBuild.Append(APPEAR_STRS[rand.Next(0, 11)]);
            return StringBuild.GetString();
        }

        /// <summary>
        /// スキル使用時のメッセージを取得。
        /// </summary>
        /// <returns>The skill message.</returns>
        /// <param name="battleStatus">Battle status.</param>
        public string GetSkillMessage(Battle_Status battleStatus)
        {
            IEnumerable<string> strs = GetTransSkillTextToPerson(battleStatus.SelectedSkl.SkillMessages, battleStatus.CharaName);
            string str;
            StringBuild.ClearStr();
            StringBuild.Append(battleStatus.CharaName);
            StringBuild.Append("「");
            foreach (var s in strs)
            {
                StringBuild.Append(s);
                StringBuild.Append("」");
                StringBuild.AppendLine();
            }
            str = StringBuild.GetString();
            return str;
        }

        /// <summary>
        /// アイテム使用時のメッセージを取得。
        /// </summary>
        /// <returns>The skill message.</returns>
        /// <param name="battleStatus">Battle status.</param>
        public string GetItemMessage(Battle_Status battleStatus)
        {
            var targets = battleStatus.GetTargets();
            IEnumerable<string> strs = GetTransItemTextToPerson(battleStatus.SelectedItem.ItemMessages, targets);
            string str;
            StringBuild.ClearStr();
            foreach (var s in strs)
            {
                StringBuild.Append(s);
                StringBuild.AppendLine();
            }
            str = StringBuild.GetString();
            return str;
        }

        /// <summary>
        /// 逃走のメッセージを取得。
        /// </summary>
        /// <returns>The runaway.</returns>
        /// <param name="canrunaway">If set to <c>true</c> canrunaway.</param>
        public string GetRunaway(bool canrunaway)
        {
            if (canrunaway)
                return RUNAWAY;
            else
                return RUNAWAY_FAILED;
        }
    }
}