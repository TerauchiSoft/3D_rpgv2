﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Battle
{
    [System.Serializable]
    public class MagicElementBattle : MonoBehaviour
    {
        [SerializeField]
        private Battle_Status bs;

        private Image img_Magic;
        private ParticleSystem part_Effect;
        private float emissionLevel = 1.0f; // 0 ~ 1.0f;揺れ幅
        private Color amplitube = new Color(0.8f, 0.8f, 0.8f, 1);
        private readonly Color baseColor = new Color(0.2f, 0.2f, 0.2f, 1);
        private Color nowcolor = new Color(0.2f, 0.2f, 0.2f, 1);

        // Use this for initialization
        void Start()
        {
            img_Magic = GetComponent<Image>();
            part_Effect = GetComponent<ParticleSystem>();
        }

        // Update is called once per frame
        void Update()
        {
            emissionLevel = GetEmissionLevel();
            nowcolor = baseColor + amplitube * emissionLevel;// 色 : 基本色 + 振幅の半分 * -1.0 ~ 1.0f * 0 ~ 1.0f
            nowcolor.a = 1;

            ResetParticleNum();
            img_Magic.color = nowcolor;
        }

        private bool isChange = false;
        private float GetEmissionLevel()
        {
            float m_mp;
            float mp;
            float mppermax;
            m_mp = bs.M_Mp;
            mp = bs.Mp;
            mppermax = mp / m_mp;

            if (mppermax != emissionLevel)
                isChange = true;

            return mppermax;
        }

        private void ResetParticleNum()
        {
            if (!isChange)
                return;

            ParticleSystem.EmissionModule EmObj = part_Effect.emission;
            EmObj.rateOverTime = new ParticleSystem.MinMaxCurve(emissionLevel * 10);
        }
    }
}