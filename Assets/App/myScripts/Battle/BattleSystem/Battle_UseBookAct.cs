﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using Battle;

/// <summary>
/// バトルの選択から使用。
/// アイテムかスキルの入力値に従って、アイテム、スキルの
/// アクションをそれぞれ実行する。味方に加えて敵の選択にも対応。
/// Book系から実行。
/// デリゲートメソッドを受け取り
/// 対象を選択できた時に実行、キャンセルなどの時はデリゲートメソッドを実行せずそのまま戻る。
/// </summary>
[System.Serializable]
public class Battle_UseBookAct : MonoBehaviour, IBattleEndProcessableObject
{
    // --------------- IBattleEndProcessableObject ------------

    public void EndProcessing()
    {

    }

    // --------------------------------------------------------

    public static bool isRunnning = false;

    public void SetIsRunning(bool b) { isRunnning = b; SetPhase(Phase.NONE); }

    private CommonData cd;

    // public UITray _characterBarTray;
    private UITray _callerTray;
    private PlayerCharaData.Name callerCharaName;
    private IEnumerable<PlayerCharacter> _playerCharas; 
    private BattleCharacter callerBattleCharaStatus;
    private Item selectedItem;
    private Skill selectedSkill;

    public enum Phase { NONE, USESELECT, ITEMCHARASELECT, SKILLCHARASELECT, ITEMHANDOVER, ITEMTHROWAWAY }
    private Phase phase = Phase.NONE;
    private void SetPhase(Phase p) { phase = p; }

    /// <summary>
    /// 使う、捨てる、やめる。の選択UI。
    /// </summary>
    [SerializeField]
    private GameObject pref_UseItemUI;
    /// <summary>
    /// アイテム、スキル使用時のキャラ選択UI。
    /// </summary>
    [SerializeField]
    private GameObject pref_CharaSelectUI;
    /// <summary>
    /// スキル使用時、残り魔力を示す魔石のエレメントと文章。
    /// スポーンさせて表示。キャラごとに変更。
    /// </summary>
    [SerializeField]
    private GameObject[] pref_MagicStones;
    /// <summary>
    /// スキル使用時、使用スキルで消費されるMPの具合を表示するエレメントと文章。
    /// スポーンさせて使用させる。
    /// </summary>
    [SerializeField]
    private GameObject pref_UseMagicLevel;
    /// <summary>
    /// 上記オブジェクトのスポーン先。
    /// </summary>
    [SerializeField]
    private Transform spwnDirectory;
    /// <summary>
    /// アイテム使用UIのスポーン先。
    /// </summary>
    [SerializeField]
    private Transform spwnuseitemui;

    [SerializeField]
    private GameObject pref_HpRecoverEfc;
    [SerializeField]
    private GameObject pref_MpRecoverEfc;

    [SerializeField]
    private TextWin textWin;

    [SerializeField] SelectPlayerTarget _selectPlayerTarget;
    [SerializeField] SelectEnemyTarget _selectEnemyTarget;

    private UIWindowElements useSelectUI;
    private UIWindowElements charaSelectUI;
    private CharaSelectMes _charaSelectMes;
    private UIWindowElements magicStoneNow;
    private ViewUseMagicCost useMagicLevel;

    // ---------------- 初期化 終了処理 --------------------

    /// <summary>
    /// BattleMainからキャラ変数のセットアップ。
    /// </summary>
    public void Init(IEnumerable<PlayerCharacter> players)
    {
        _playerCharas = players;
    }

    /// <summary>
    /// これを呼ぶとどこからでも終了できる。
    /// </summary>
    public void CloseEndProcessing(bool isDecideEnd) {
        ResetDatas();
        ResetObject();
        phase = Phase.NONE;
        SetIsRunning(false);

        if (isDecideEnd) // 行動を選択した終了。
        {
            _forIncSelecterAction.Invoke();
        }
        else             // キャンセルでの終了。
        {
            _forIncSelecterAction = null;
            _callerTray._isRunning = true;
            _callerTray.gameObject.SetActive(true);
        }
        //callerBook.SetisCanSelect(true);
    }

    /// <summary>
    /// 選択しているデータをリセットする。
    /// </summary>
    private void ResetDatas()
    {
        selectedItem = new Item();
        selectedSkill = new Skill();
    }

    /// <summary>
    /// スポーンディレクトリ以下のごみオブジェクトをすべて消去。
    /// </summary>
    public void ResetObject() {
        DestroyUIWIndow();

        var obs = spwnDirectory.GetComponentsInChildren<Transform>();
        for (int i = 1; i < obs.Length; i++)
            Destroy(obs[i].gameObject);

        useSelectUI = null;
        charaSelectUI = null;
        magicStoneNow = null;
        useMagicLevel = null;
    }
    
    private void DestroyUIWIndow()
    {
        if (useSelectUI != null)
            Destroy(useSelectUI.gameObject);
        if (charaSelectUI != null)
            Destroy(charaSelectUI.gameObject);
        if (magicStoneNow != null)
            Destroy(magicStoneNow.gameObject);
        if (useMagicLevel != null)
            Destroy(useMagicLevel.gameObject);
    }

    // -------------- 呼び出し処理 -------------------

    /// <summary>
    /// セレクタを進ませるメソッド。
    /// = PlayerActSelecter.EndSelecting()
    /// </summary>
    System.Action _forIncSelecterAction;

    /// <summary>
    /// スキルを使用する時はじめに呼ぶ。
    /// 共通:選択時に対象を選択できる。選択時に魔力の消費具合をメッセージで
    /// 現在の魔力量を魔石で表現。
    /// 味方に加えて敵の選択にも対応。
    /// </summary>
    /// <param name="skl"></param>
    public void RunSkill(PlayerCharaData.Name charaname, Skill skl, UITray tray, System.Action _forIncSelecterIdxAct) {
        _forIncSelecterAction = _forIncSelecterIdxAct;  // セレクタを進ませるメソッド。行動決定時のみ実行。

        _callerTray = tray;
        if (skl.effects[0] == TableValues.Effect.Event) {
            TextPlay("ここでは使えません",
                () => CloseEndProcessing(false));
            textWin.transform.SetAsLastSibling();
            return;
        }

        callerCharaName = charaname;
        callerBattleCharaStatus = _playerCharas.First(pl => pl.PlayerCharaName == callerCharaName);
        callerBattleCharaStatus.BattleStatus.SetSkill(skl);

        ResetDatas();
        selectedSkill = skl;
        ResetObject();

        if (skl.Target == TableValues.Tgt.Enemy || skl.Target == TableValues.Tgt.EnemyAll)
        {
            _selectEnemyTarget.Wakeup(callerBattleCharaStatus.BattleStatus, skl.Target);
            _selectEnemyTarget.OpenNewTray(_callerTray, () => CloseEndProcessing(true));
        }
        else if (skl.Target == TableValues.Tgt.Player || skl.Target == TableValues.Tgt.PlayerAll || skl.Target == TableValues.Tgt.Me)
        {
            _selectPlayerTarget.Wakeup(callerBattleCharaStatus.BattleStatus, skl.Target);
            _selectPlayerTarget.OpenNewTray(_callerTray, () => CloseEndProcessing(true));
        }
        isRunnning = true;
        return;
    }


    /// <summary>
    /// アイテム選択処理
    /// </summary>
    /// <param name="charaname"></param>
    /// <param name="itm"></param>
    /// <param name="tray"></param>
    /// <param name="_forIncSelecterIdxAct"></param>
    public void RunItem(PlayerCharaData.Name charaname, Item itm, UITray tray, System.Action _forIncSelecterIdxAct)
    {
        _forIncSelecterAction = _forIncSelecterIdxAct;  // セレクタを進ませるメソッド。行動決定時のみ実行。

        _callerTray = tray;
        _callerTray._isRunning = false;

        callerCharaName = charaname;
        callerBattleCharaStatus = _playerCharas.First(pl => pl.PlayerCharaName == callerCharaName);
        callerBattleCharaStatus.BattleStatus.SetItem(itm);

        ResetDatas();
        selectedItem = itm;
        ResetObject();

        _selectPlayerTarget.Wakeup(callerBattleCharaStatus.BattleStatus, itm.Target);
        _selectPlayerTarget.OpenNewTray(_callerTray, () => CloseEndProcessing(true));
        isRunnning = true;
        return;

        // 使用選択ウィンドウを表示。
        useSelectUI = LoadUIWindowBook(pref_UseItemUI, useSelectUI, Phase.USESELECT, spwnuseitemui);

        isRunnning = true;
    }

    // -------------- ウィンドウの読み込み -----------------

    /// <summary>
    /// UIWindowElements要素の読み込み。
    /// </summary>
    /// <param name="pref"></param>
    /// <param name="ui"></param>
    /// <param name="p"></param>
    /// <param name="sptrs"></param>
    /// <returns></returns>
    public UIWindowElements LoadUIWindowBook(GameObject pref, UIWindowElements ui, Phase p, Transform sptrs = null) {
        phase = p;
        return UIWindowElements.LoadUIWindow(pref, ui, (sptrs == null) ? spwnDirectory : sptrs);
    }

    // --------------- 文章の表示 --------------------

    /// <summary>
    /// メッセージ再生。メッセージ再生後にSystem.Action発火可能。
    /// </summary>
    /// <param name="str"></param>
    private void TextPlay(string str, System.Action forIncIdxAct = null)
    {
        textWin.gameObject.SetActive(true);
        textWin.StartTextPlayNoSpan(str, forIncIdxAct);
    }

    // ---------------- 第1選択処理 Update()で実行 ----------------------

    /// <summary>
    /// 使用、渡す、捨てるの実行。アイテムのみで実行する。
    /// </summary>
    private void UseSelectProcessing() {
        if (phase == Phase.USESELECT && useSelectUI.isChoicing == false) {
            switch (useSelectUI.DecisionIndex) {
                case 0:// 決定
                    // 重要アイテム
                    if (selectedItem.rarity == TableValues.Rarity.Event) {
                        TextPlay("このアイテムは使えません！",
                            () => CloseEndProcessing(false));
                        return;
                    }
                    OpenUseItemUI();
                    return;
                case 1:// やめる
                default:// (-1)キャンセル
                    CloseEndProcessing(false);
                    return;
            }
        }
    }

    /// <summary>
    /// スキルの場合、第一選択で実行処理に入る。
    /// 選択処理はここで全て完結。
    /// </summary>
    private void SelectSkillCharaProcessing() {
        if (phase == Phase.SKILLCHARASELECT && charaSelectUI.isChoicing == false) {
            int dectidx = charaSelectUI.DecisionIndex;
            switch (dectidx) {
                case 0:// アレスタ
                case 1:// エリナ
                case 2:// ペティル
                    SkillAction(dectidx);
                    charaSelectUI.ToActiveChoicing();
                    CloseEndProcessing(true);
                    return;
                default://-1
                    CloseEndProcessing(false);
                    return;
            }
        }
    }

    // ---------------- アイテム使用選択モードへ -------------------

    /// <summary>
    /// アイテム使用選択モードにする。
    /// </summary>
    private void ToUseSelectPhese() {
        ResetObject();
        useSelectUI = LoadUIWindowBook(pref_UseItemUI, useSelectUI, Phase.USESELECT, spwnuseitemui);
    }
    
    // ---------------- 第1選択処理完了 ------------------

    private void OpenCharaSelect(CharaSelectMes.Mode mode) {
        charaSelectUI = LoadUIWindowBook(pref_CharaSelectUI, charaSelectUI, Phase.ITEMCHARASELECT, spwnDirectory.parent);
        charaSelectUI.SetIsHideWhenDecided(false);
        _charaSelectMes = charaSelectUI.GetComponent<CharaSelectMes>();
        _charaSelectMes?.SetCharaSetectMode(mode);
    }

    private void InitViewMagic(Skill skl, int cost) {
        skl.mpCost = cost;
        useMagicLevel = Instantiate(pref_UseMagicLevel, spwnDirectory.parent).GetComponent<ViewUseMagicCost>();
        useMagicLevel.SpawnMagicStone((int)callerCharaName, ref skl);
    }
    
    // アイテム:使う
    private void OpenUseItemUI() {
        switch (selectedItem.effects[0]) {
            case TableValues.Effect.HPRecover:
            case TableValues.Effect.MPRecover:
                // キャラセレクトウィンドウを開く
                OpenCharaSelect(CharaSelectMes.Mode.Use);
                // Mpの消費量と現在のMpを表示する。
                InitViewMagic(Skill.GetNullSkill(), 0);
                // 全体選択
                if (charaSelectUI != null && selectedItem.Target == TableValues.Tgt.PlayerAll) {
                    charaSelectUI.SetIsAllSelectAtCharaSlct(true);
                }// インデックス固定
                else if (charaSelectUI != null && selectedItem.Target == TableValues.Tgt.Me) {
                    charaSelectUI.ToIndexLock((int)callerCharaName);    
                }
                break;
            case TableValues.Effect.Event://TODO:イベントはまた作らないと...。
                CloseEndProcessing(false);
                return;
            default:
                CloseEndProcessing(false);
                return;
        }
    }

    // アイテム:渡す
    private void OpenHandverItemUI() {
        // キャラセレクトウィンドウを開く
        OpenCharaSelect(CharaSelectMes.Mode.HandOver);
    }

    // アイテムを捨てる。
    private void OpenThrowAwayItemUI() {
        // キャラセレクトウィンドウを開く
        OpenCharaSelect(CharaSelectMes.Mode.HandOver);
        _charaSelectMes.OpenSelectThrowAway(selectedItem, () => {
            var throwAwayNum = _charaSelectMes.DecideValue;
            ThrowAway(throwAwayNum);
        });
    }

    private void ThrowAway(int throwawaynum) {
        // 捨てる数が1以上
        if (throwawaynum >= 1) {
            PlayerDatas.Instance.ThrowItem((int)callerCharaName, selectedItem, throwawaynum);
            TextPlay(StringBuild.GetStringFromArgs(selectedItem.ItemName, "を", throwawaynum.ToString(), "捨てました。"));
            _callerTray.OnOpenTray();
            CloseEndProcessing(true);
        } else { // 捨てる数が0、キャンセル
            ToUseSelectPhese();
        }
        _charaSelectMes.CloseTray();
    }


    // ---------------- 第2選択処理 アイテムのみ ----------------------

    /// <summary>
    /// UseSelectProcessingで選択されたあとのメソッド。
    /// ここで各メニューで個別に選択処理。
    /// </summary>
    private void SelectItemCharaProcessing() {
        if (phase == Phase.ITEMCHARASELECT && charaSelectUI.isChoicing == false) {
            switch (_charaSelectMes._mode) {
                case CharaSelectMes.Mode.Use:
                    UseItemAct();
                    return;
                default:
                    ToUseSelectPhese();
                    return;
            }
        }
    }

    private void UseItemAct() {
        int dectidx = charaSelectUI.DecisionIndex;
        switch (dectidx) {
            case 0:// アレスタ
            case 1:// エリナ
            case 2:// ペティル
                ItemAction(dectidx);
                return;
            default://-1
                ToUseSelectPhese();
                return;
        }
    }

    // ------------------ 使用処理 --------------------------

    private int MPDec() {
        int mp = PlayerDatas.Instance.p_chara[(int)callerCharaName].MP - selectedSkill.mpCost;
        PlayerDatas.Instance.p_chara[(int)callerCharaName].MP = Mathf.Clamp(mp, 0, PlayerDatas.Instance.p_chara[(int)callerCharaName].M_MP);
        return mp;
    }

    /// <summary>
    /// 威力の計算にBattleControl, Battle_Statusの動きをエミュレートしている。不毛。
    /// </summary>
    /// <param name="tgtidx"></param>
    /// <returns></returns>
    private int EffectQuantityCalculation(int tgtidx) {
        // Battle_Status用のダミーを作成。
        GameObject[] ob_dammy = new GameObject[3];
        List<Battle_Status> bss = new List<Battle_Status>();
        for (int i = 0; i < 3; i++) {
            ob_dammy[i] = new GameObject();
            bss.Add(ob_dammy[i].AddComponent<Battle_Status>());
        }
        // プレイヤーデータからBattle_Statusに代入。
        CommonData.Instance.SetCommonStatusToBattlePlayer(bss[(int)callerCharaName], (int)callerCharaName);
        CommonData.Instance.SetCommonStatusToBattlePlayer(bss[tgtidx], tgtidx);
        bss[(int)callerCharaName].StatusInit((int)callerCharaName, Battle_Status.PLYorENM.PLY);
        bss[tgtidx].StatusInit(tgtidx, Battle_Status.PLYorENM.PLY);
        // ターゲットを指定。計算
        bss[(int)callerCharaName].SetEnemyStatus(bss[(int)callerCharaName], bss, tgtidx);
        int ret = -1;
        if (selectedItem.ItemID != 0) {
            bss[(int)callerCharaName].SetItem(selectedItem);
            ret = selectedItem.itm_method.Invoke(bss[(int)callerCharaName], bss[tgtidx]);
        } else if (selectedSkill.SkillID != 0) {
            bss[(int)callerCharaName].SetSkill(selectedSkill);
            ret = selectedSkill.skl_method.Invoke(bss[(int)callerCharaName], bss[tgtidx]);
        }

        // 計算が終わったら空GameObjectは用済み。
        foreach (var a in ob_dammy)
            Destroy(a);

        return ret;
    }

    private void ItemDec(int n) {
        // アイテムを減らして0になったら画面を閉じる。
        int itemnum = PlayerDatas.Instance.p_chara[(int)callerCharaName].DecItem(selectedItem, n);
        PlayerDatas.Instance.p_chara[(int)callerCharaName].ReSortItemIdx();
        _callerTray.OnOpenTray();
        charaSelectUI.isChoicing = true;
        if (itemnum <= 0) {
            // 選ぶの禁止
            charaSelectUI.isChoicing = false;
            isRunnning = false;
            DOVirtual.DelayedCall(1.3f, () => {
                CloseEndProcessing(true);
            });
        }
    }

    /// <summary>
    /// スキルの使用効果。
    /// </summary>
    /// <param name="tgtidx">Tgtidx.</param>
    private void SkillAction(int tgtidx) {
        if (selectedSkill.effects[0] == TableValues.Effect.Event) {
            TextPlay("何もおこらなかった...。");
        }

        int val = EffectQuantityCalculation(tgtidx);
        int mp = MPDec();
        useMagicLevel.SetMpCostMes((int)callerCharaName, ref selectedSkill);
        // MP足りなければ威力を1 / 10にする。
        val = (mp >= 0) ? val : (int)(val / 10.0f);
        // 全効果をここで計算。
        foreach (var a in selectedSkill.effects)
            EffectAct(a, val, tgtidx);

    }

    /// <summary>
    /// アイテムの使用効果。
    /// </summary>
    /// <param name="tgtidx">Tgtidx.</param>
    private void ItemAction(int tgtidx) {
        int val = EffectQuantityCalculation(tgtidx);
        // 全効果をここで計算。
        foreach (var a in selectedItem.effects)
            EffectAct(a, val, tgtidx);

        // アイテムの増減
        ItemDec(1);
    }

    // ----------------- 回復エフェクト、メッセージ -----------------------
    
    /// <summary>
    /// 回復メッセージ。
    /// </summary>
    /// <param name="isHp"></param>
    /// <param name="tgtidx"></param>
    /// <param name="efcval"></param>
    /// <returns></returns>
    private string GetRecoverMes(bool isHp, int tgtidx, int efcval)
    {
        if (isHp)
        {
            if (PlayerDatas.Instance.p_chara[tgtidx].HP >= PlayerDatas.Instance.p_chara[tgtidx].M_HP)
                return "体力全快！";
            var hppermhp = (float)efcval / PlayerDatas.Instance.p_chara[tgtidx].M_HP;
            if (hppermhp < 0.2f)
                return "少し回復！";
            if (hppermhp < 0.4f)
                return "ほどほどに回復！";
            if (hppermhp < 0.6f)
                return "けっこう回復！";
            //if (hppermhp < 0.8f)
                return "かなり回復！";
        }
        else
        {
            if (PlayerDatas.Instance.p_chara[tgtidx].MP >= PlayerDatas.Instance.p_chara[tgtidx].M_MP)
                return "魔力全快！";
            var hppermmp = (float)efcval / PlayerDatas.Instance.p_chara[tgtidx].M_MP;
            if (hppermmp < 0.2f)
                return "少し回復！";
            if (hppermmp < 0.4f)
                return "ほどほどに回復！";
            if (hppermmp < 0.6f)
                return "けっこう回復！";
            //if (hppermmp < 0.8f)
                return "かなり回復！";
        }
    }

    public Vector2 _defaultEffectVec;
    public Vector2 _spanEffectVec;
    private void HPRecover(int efcval, int tgtidx)
    {
        // 回復
        PlayerDatas.Instance.p_chara[tgtidx].AddHp(efcval);
        // 回復エフェクト
        var ob = Instantiate(pref_HpRecoverEfc, spwnDirectory.parent);
        ob.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = GetRecoverMes(true, tgtidx, efcval);
        var rt = ob.GetComponent<RectTransform>();
        rt.anchoredPosition = rt.anchoredPosition + _defaultEffectVec + _spanEffectVec * (-1 + tgtidx);
    }

    private void MPRecover(int efcval, int tgtidx)
    {
        // 回復
        PlayerDatas.Instance.p_chara[tgtidx].AddMp(efcval);
        // 回復エフェクト
        var ob = Instantiate(pref_HpRecoverEfc, spwnDirectory.parent);
        ob.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = GetRecoverMes(false, tgtidx, efcval);
        var rt = ob.GetComponent<RectTransform>();
        rt.anchoredPosition = rt.anchoredPosition + _defaultEffectVec + _spanEffectVec * (-1 + tgtidx);
    }

    private void EffectAct(TableValues.Effect ef, int efcval, int tgtidx)
    {
        switch(ef)
        {
            case TableValues.Effect.HPRecover:
                HPRecover(efcval, tgtidx);
                print(efcval+"HP回復");
                return;
            case TableValues.Effect.MPRecover:
                MPRecover(efcval, tgtidx);
                print(efcval + "MP回復");
                return;
            default:
                //TODO:何も起きない
                return;
        }
    }

    // ------------- Start Update ----------------

    private void Start()
    {
        cd = CommonData.Instance;
    }

    private void Update()
    {
        if (!isRunnning)
            return;

        UseSelectProcessing();
        SelectItemCharaProcessing();
        SelectSkillCharaProcessing();
    }
}
