﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Battle
{
    /// <summary>
    /// 主人公のバトル時の行動を決定する。
    /// 正常終了時のCloseActで次のキャラの行動選択に進ませる。
    /// キャンセルキーが押されると実行せずそのまま戻る。
    /// </summary>
    public class Battle_SkillTray : UITray
    {
        [SerializeField] PlayerActSelectManager _playerActSelectManager;

        public Transform _trsIconTray;
        public SkillFlame[] _iconFlames;
        public Battle_UseBookAct _useAct;

        public TextMeshProUGUI _skillName;
        public TextMeshProUGUI _skillText;

        private PlayerController plycnt;

        public TextMeshProUGUI _text_skillName;
        public TextMeshProUGUI _text_skillExplanatory;

        // --------------------- override --------------------------

        public override void LeaveAnime(int befidx)
        {
            _iconFlames[befidx]._anim.Play("UnZoom");
        }

        public override void CursorAnime(int nowidx)
        {
            _iconFlames[nowidx]._anim.Play("Zoom");
            SetSkillText();
        }

        /// <summary>
        /// トレーにアイテムをセット。
        /// アイテムスプライトと、エフェクトスプライト、個数をセット。
        /// </summary>
        public override void OnOpenTray()
        {
            // Battle_UseBookActから戻る時に有効化
            if (gameObject.activeSelf == false)
            {
                gameObject.SetActive(true);
            }

            plycnt = PlayerController.Instance;
            _iconFlames = _trsIconTray.GetComponentsInChildren<SkillFlame>();
            var playerskills = PlayerDatas.Instance.p_chara[_playerActSelectManager._idx].Ply_skl;
            for (int i = 0; i < _iconFlames.Length; i++)
            {
                if (i < playerskills.Length)
                {
                    _iconFlames[i].SetData(playerskills[i]);
                }
                else
                {
                    _iconFlames[i].SetData(Skill.GetNullSkill());
                }
            }
            CursorAnime(_nowFlameCursor);
        }

        // --------------------- Next Back ---------------------------

        public override void GotoNextTray(bool isgoto, Action act = null)
        {
            if (!isgoto)
                return;

            var skl = _iconFlames[_nowFlameCursor].skill;
            if (skl.SkillID == 0)
                return;

            Map_Sound.Instance.DecisionSoundPlay();

            // バトル時の選択ではウィンドウを閉じる。
            gameObject.SetActive(false);
            // スキル、対象が決定された場合、_closeAction (= PlayerActSelecterの（）=> EndSelecting()を実行する。)
            _useAct.RunSkill((PlayerCharaData.Name)_playerActSelectManager._idx, skl, this, _closeAction);
        }

        public override void GotoBackTray(bool isgoto)
        {
            if (!isgoto)
                return;

            Map_Sound.Instance.CancelSoundPlay();
            EndProcessManual(); // キャンセル時はPlayerActSelecterの（）=> EndSelecting()を実行しない。
            gameObject.SetActive(false);
        }

        // -------------------------------------------------------------

        private int _NumOfLine = 8;
        public override void CursorMove(Vector2 input, int numOfLine = 1)
        {
            if (input.x == 0 && input.y != 0)
            {
                base.CursorMove(input, _NumOfLine);
                LoadItemInfomationOnChangeCursor(_nowFlameCursor);
                return;
            }
            base.CursorMove(input);
            LoadItemInfomationOnChangeCursor(_nowFlameCursor);
        }

        // ------------------- BattleSkillTray -----------------------

        private void SetSkillText()
        {
            _skillName.text = _iconFlames[_nowFlameCursor].skill.SkillName;
            _skillText.text = _iconFlames[_nowFlameCursor].skill.ExplanatoryText;
        }

        /// <summary>
        /// カーソル変更時にアイテム情報を読み込み、右トレイに表示。
        /// </summary>
        private void LoadItemInfomationOnChangeCursor(int idx)
        {
            var skl = _iconFlames[idx].skill;
            _text_skillName.text = skl.SkillName;
            _text_skillExplanatory.text = skl.ExplanatoryText;
        }

        // ----------------------------------------------------

        private void Update()
        {
            if (!_isRunning)
                return;

            CursorMove(plycnt.joy_Axis_buf);
            GotoNextTray(plycnt.isFire1);
            GotoBackTray(plycnt.isFire2);
        }
    }
}