﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Battle
{
    /// <summary>
    /// 主人公のバトル時の行動を決定する。
    /// 正常終了時のCloseActで次のキャラの行動選択に進ませる。
    /// キャンセルキーが押されると実行せずそのまま戻る。
    /// </summary>
    public class Battle_ItemTray : UITray
    {
        [SerializeField] PlayerActSelectManager _playerActSelectManager;

        public Transform _trsIconTray;
        public ItemFlame[] _iconFlames;
        public Battle_UseBookAct _useAct;

        public TextMeshProUGUI _itemName;
        public TextMeshProUGUI _itemText;

        private PlayerController plycnt;

        public TextMeshProUGUI _text_itemName;
        public TextMeshProUGUI _text_itemExplanatory;

        // --------------------- override --------------------------

        public override void LeaveAnime(int befidx)
        {
            _iconFlames[befidx]._anim.Play("UnZoom");
        }

        public override void CursorAnime(int nowidx)
        {
            _iconFlames[nowidx]._anim.Play("Zoom");
            SetItemText();
        }

        /// <summary>
        /// トレーにアイテムをセット。
        /// アイテムスプライトと、エフェクトスプライト、個数をセット。
        /// </summary>
        public override void OnOpenTray()
        {
            // Battle_UseBookActから戻る時に有効化
            if (gameObject.activeSelf == false)
            {
                gameObject.SetActive(true);
            }

            plycnt = PlayerController.Instance;
            _iconFlames = _trsIconTray.GetComponentsInChildren<ItemFlame>();
            var playeritems = PlayerDatas.Instance.p_chara[_playerActSelectManager._idx].Ply_item;
            for (int i = 0; i < _iconFlames.Length; i++)
            {
                if (i < playeritems.Length)
                {
                    _iconFlames[i].SetData(playeritems[i]);
                }
                else
                {
                    _iconFlames[i].SetData(Item.GetNullItem());
                }
            }
            CursorAnime(_nowFlameCursor);
        }

        // --------------------- Next Back ---------------------------

        public override void GotoNextTray(bool isgoto, Action act = null)
        {
            if (!isgoto)
                return;

            var item = _iconFlames[_nowFlameCursor].item;
            if (item.ItemID == 0)
                return;

            Map_Sound.Instance.DecisionSoundPlay();

            // バトル時の選択ではウィンドウを閉じる。
            gameObject.SetActive(false);
            // アイテム、対象が決定された場合、_closeAction (= PlayerActSelecterの（）=> EndSelecting()を実行する。)
            _useAct.RunItem((PlayerCharaData.Name)_playerActSelectManager._idx, item, this, _closeAction);
        }

        public override void GotoBackTray(bool isgoto)
        {
            if (!isgoto)
                return;

            Map_Sound.Instance.CancelSoundPlay();
            EndProcessManual(); // キャンセル時はPlayerActSelecterの（）=> EndSelecting()を実行しない。
            gameObject.SetActive(false);
        }

        // -------------------------------------------------------------

        private int _NumOfLine = 8;
        public override void CursorMove(Vector2 input, int numOfLine = 1)
        {
            if (input.x == 0 && input.y != 0)
            {
                base.CursorMove(input, _NumOfLine);
                LoadItemInfomationOnChangeCursor(_nowFlameCursor);
                return;
            }
            base.CursorMove(input);
            LoadItemInfomationOnChangeCursor(_nowFlameCursor);
        }

        // ------------------- BattleItemTray -----------------------

        /// <summary>
        /// カーソル変更時にアイテム情報を読み込み、右トレイに表示。
        /// </summary>
        private void LoadItemInfomationOnChangeCursor(int idx)
        {
            var itm = _iconFlames[idx].item;
            _text_itemName.text = itm.ItemName;
            _text_itemExplanatory.text = itm.ExplanatoryText;
        }

        private void SetItemText()
        {
            _itemName.text = _iconFlames[_nowFlameCursor].item.ItemName;
            _itemText.text = _iconFlames[_nowFlameCursor].item.ExplanatoryText;
        }

        // ----------------------------------------------------

        private void Update()
        {
            if (!_isRunning)
                return;

            CursorMove(plycnt.joy_Axis_buf);
            GotoNextTray(plycnt.isFire1);
            GotoBackTray(plycnt.isFire2);
        }
    }
}