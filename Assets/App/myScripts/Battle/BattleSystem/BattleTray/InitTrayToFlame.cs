﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class InitTrayToFlame : MonoBehaviour
    {
        public string _gotoTray;
        public ObjectFlame _flame;

        public void Start()
        {
            if (_gotoTray == "Skill")
            {
                _flame._gotoTray = BattleMain.Instance.SkillTray;
            }
            if (_gotoTray == "Item")
            {
                _flame._gotoTray = BattleMain.Instance.ItemTray;
            }
            if (_gotoTray == "Run")
            {
                _flame._gotoTray = BattleMain.Instance.RunAwayTray;
            }

            if (_flame._gotoTray == null)
                throw new System.NullReferenceException("InitTrayToFlameエラー");
        }
    }
}