﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace Battle
{
    public class PrefabLoader : SingletonForData<PrefabLoader>
    {
        private string _registEnemyGroup = "Wolf";
        public void RegistEnemies(string enemyGroupName)
        {
            _registEnemyGroup = enemyGroupName;
        }

        private string _registPlayers = "PlayerOne";
        public void RegistPlayers(int numofplayer, bool erinajoined)
        {
            if (numofplayer == 1)
            {
                _registPlayers = "PlayerOne";
            }
            else if (numofplayer == 2)
            {
                if (erinajoined)
                    _registPlayers = "PlayerDouble";
                else
                    _registPlayers = "PlayerDouble2";
            }
            else if (numofplayer == 3)
            {
                _registPlayers = "PlayerTriple";
            }
        }

        /// <summary>
        /// UIキャラクターの位置(screenpos)に合わせるように出現位置(worldpos)を調整。
        /// </summary>
        /// <returns>The player prefabs.</returns>
        public IEnumerable<PlayerCharacter> SpawnPlayerPrefabs(RectTransform spwn)
        {
            var players = BattlePrefabLoader.Instance.GetBattlePrefab(_registPlayers);
            //var players = Resources.Load<BattlePrefabSource>("ResouresPrefab/Battle/PlayerCharacters/PlayerGroups/" + _registPlayers);
            var playercharacters = new List<PlayerCharacter>();
            int idx = 0;
            foreach (var p in players._battlePrefabs)
            {
                var spawnob = GameObject.Instantiate(p._chara, spwn);
                BattleMain.Instance.RegistDeleteObject(spawnob);
                var player = new PlayerCharacter
                {
                    BehaviourObject = spawnob.GetComponentInChildren<PlayerCursorElement>().gameObject,
                    RectTransformPlayer = spawnob.GetComponentsInChildren<RectTransform>().First(rt => rt.tag == "Player"),
                    EffectPlay = spawnob.GetComponentInChildren<EffectPlayForUI>(),
                    PlayerActSelecter = spawnob.GetComponentInChildren<PlayerActSelecter>(),
                    BattleStatus = spawnob.GetComponent<Battle_Status>(),
                    PlayerCharaName =  (PlayerCharaData.Name)idx,
                    BattleFaceStatus = spawnob.GetComponentInChildren<Battle_FaceStatus>(),
                    ExpSlider = spawnob.GetComponentInChildren<Map_Exp>()
                };
                playercharacters.Add(player);
                CommonData.Instance.SetCommonStatusToBattlePlayer(playercharacters[idx].BattleStatus, idx);
                idx++;
            }
            return playercharacters;
        }

        public IEnumerable<EnemyCharacter> SpawnEnemyPrefabs(Transform spwn)
        {
            var enemies = BattlePrefabLoader.Instance.GetBattlePrefab(_registEnemyGroup);
            //var enemies = Resources.Load<BattlePrefabSource>("ResouresPrefab/Battle/EnemyCharacters/EnemyGroups/" + _registEnemyGroup);
            var enemycharacters = new List<EnemyCharacter>();
            foreach (var e in enemies._battlePrefabs)
            {
                var spawnob = GameObject.Instantiate(e._chara, e._position, Quaternion.identity, spwn);
                BattleMain.Instance.RegistDeleteObject(spawnob);
                enemycharacters.Add(new EnemyCharacter {
                    BehaviourObject = spawnob,
                    EffectPlay = spawnob.AddComponent<EffectPlayForObject>(), // AddComponent
                    BattleEnemyAct = spawnob.GetComponent<Battle_EnemyAct>(),
                    BattleStatus = spawnob.GetComponent<Battle_Status>()
                });
            }
            return enemycharacters;
        }
    }
} 