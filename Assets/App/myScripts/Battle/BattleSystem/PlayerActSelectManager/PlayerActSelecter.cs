﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Threading.Tasks;
using UnityEngine.Events;
using DG.Tweening;

public class PlayerActSelecter : UITray
{

    // --------------------- UITray ------------------------

    public override void LeaveAnime(int befidx)
    {
        if (_isSwaping)
        {
            return;
        }

        _objectFlame[befidx]._anim.Play("UnZoom");
    }

    public override void CursorAnime(int nowidx)
    {
        if (_isSwaping)
        {
            _nowFlameCursor = _selectedIdx;
            return;
        }

        if (twenners[0].IsActive())
        {
            return;
        }
        _selectedIdx = nowidx;
        SetFlamePos(_selectedIdx);
        _objectFlame[_selectedIdx]._anim.Play("Zoom");
    }

    public override void OnOpenTray()
    {
        plycnt = PlayerController.Instance;
        plycnt.ResetInput(12);
        SetFlamePos(_selectedIdx);
        _objectFlame[_selectedIdx]._anim.Play("Zoom");

        _isRunning = true;
        _isSelecting = true;
        _isDecided = false;
        
        InitFlashObject();
    }

    // ------------------ PlayerActSelecter ---------------------

    private const int NUM_OF_FLAME = 4;
    private const float ROTWAIT_1 = 0.21f;
    private const float ROTWAIT_2 = 0.12f;
    private bool isSecondMove;
    public bool _isSelecting;
    public bool _isDecided;
    public int _selectedIdx;

    // 選択カーソル●
    public RectTransform _flashCursor;
    private PlayerController plycnt;
    private UnityAction canselAction;
    [SerializeField] RectTransform centerObject;
    [SerializeField] Vector3[] _rotVectors = new Vector3[NUM_OF_FLAME];

    // キャラクターカーソル
    private const int YET = 0;
    private const int ONCURSOR = 1;
    private const int DONE = 2;

    // ------------------ private --------------------------

    private async void SetCenterPos()
    {
        await ToCenter();
    }

    private void SetFlamePos(int selectidx)
    {
        ToIdxPos(selectidx);
    }

    private void SetInitPos(int selectidx)
    {
        EndFlashObject();
        objectflamerects = GetObjectFlameRect().ToArray();
        ToIdxPos(selectidx);
    }

    private void InitFlashObject()
    {
        _anim.SetInteger("State", ONCURSOR);
        _flashCursor.gameObject.SetActive(true);
        _flashCursor.localPosition = centerObject.localPosition + _rotVectors[0];
    }

    private void EndFlashObject()
    {
        _flashCursor.gameObject.SetActive(false);
    }

    private IEnumerable<RectTransform> GetObjectFlameRect()
    {
        RectTransform[] rects = new RectTransform[_objectFlame.Length];
        for (int i = 0; i < diffcenter.Length; i++)
        {
            rects[i] = _objectFlame[i].GetComponent<RectTransform>();
        }
        return rects;
    }

    // ------------------- IEnumerator ---------------------

    bool _isSwaping = false;
    RectTransform[] objectflamerects = new RectTransform[NUM_OF_FLAME];    // 各フレームのRectTransform
    [SerializeField] Vector3[] diffcenter = new Vector3[NUM_OF_FLAME];                      // 中心からの差分
    IEnumerator ToCenter()
    {
        _isSwaping = true;

        var div = 10;
        for (int i = 0; i < diffcenter.Length; i++)             // 中心からの距離
        {
            diffcenter[i] = (centerObject.localPosition - objectflamerects[i].localPosition) / div;
        }

        for (int i = 0; i < div; i++)                           // 中心へ寄せていく
        {
            var idx = 0;
            foreach (var obrect in objectflamerects)
            {
                obrect.localPosition += diffcenter[idx];
                idx++;
            }
            yield return 0;
        }

        foreach (var obrect in objectflamerects)
        {
            obrect.localPosition = centerObject.localPosition;
        }

        _isSwaping = false;
    }

    private Tweener[] twenners = new Tweener[NUM_OF_FLAME];
    private void ToIdxPos(int selectidx)
    {
        _isSwaping = true;

        int[] rotIdx = new int[NUM_OF_FLAME];
        for (int i = 0; i < NUM_OF_FLAME; i++)
        {
            rotIdx[i] = (i - selectidx) >= 0 ? (i - selectidx) % NUM_OF_FLAME : NUM_OF_FLAME + (i - selectidx);
        }

        var idx = 0;
        foreach (var rect in objectflamerects)
        {
            float wait = isSecondMove ? ROTWAIT_2 : ROTWAIT_1;
            twenners[idx] = rect.DOAnchorPos(_rotVectors[rotIdx[idx]], wait).SetEase(Ease.Linear);
            idx++;
        }

        if (!isSecondMove)
            isSecondMove = true;
        _isSwaping = false;
    }

    // ------------------- 初期化 キャンセル  -------------------------

    /// <summary>
    /// セレクタの立ち上げ時、キャンセル処理で処理が移った時に呼び出される。
    /// </summary>
    /// <returns>The init.</returns>
    /// <param name="cancelAction">Cancel action.</param>
    public async Task<bool> Init(UnityAction cancelAction)
    {
        _isSelecting = false;
        _isDecided = false;
        this.canselAction = cancelAction;

        int idx = 0;
        foreach (var flame in _objectFlame)
        {
            _objectFlame[idx]._anim.Play("UnZoom");
            idx++;
        }
        _anim.SetInteger("State", YET);
        SetInitPos(_selectedIdx);
        _isCanOpenTray = true;  // キャンセル時Offになっているのでセット
        return true;
    }

    /// <summary>
    /// キャンセルするときの処理
    /// </summary>
    public void CanselSelecting()
    {
        _isDecided = false;
        _isSelecting = false;
        EndFlashObject();
    }

    // --------------------- 開始、終了のトリガー -----------------------

    /// <summary>
    /// セレクタのインデックスがキャラを指した時に呼び出され、選択処理が開始される。
    /// </summary>
    public async void StartSelecting()
    {
        await Task.Delay(100);
        OnOpenTray();
    }

    /// <summary>
    /// 決定キーを押して終了する時の処理。セレクタのインデックスを1進める。(_isDecided == true && _isSelecting == false)
    /// なお、処理のスタックが深くなるにつれてSystem.Actionが次々に伝承される。
    /// _closeAct = nullダメ絶対。
    /// </summary>
    public void EndSelecting()
    {
        _isDecided = true;
        _isSelecting = false;
        EndFlashObject();
        _anim.SetInteger("State", DONE);
    }

    // --------------- overrite ------------------------

    /// <summary>
    /// カーソルを動かした時
    /// </summary>
    /// <param name="inputHV"></param>
    public override void CursorMove(Vector2 inputHV, int numOfline = 1)
    {
        if (inputHV.x == 0 && inputHV.y == 0)
        {
            isSecondMove = false;
            return;
        }

        if (twenners[0].IsActive())
            return;
        
        int befidx = _nowFlameCursor;

        bool ischange = false;
        if (inputHV.y > 0 || inputHV.x < 0)
        {
            ChangeIdxFromCursorMove(-numOfline);
            ischange = true;
        }
        if (inputHV.y < 0 || inputHV.x > 0)
        {
            ChangeIdxFromCursorMove(numOfline);
            ischange = true;
        }

        if (_nowFlameCursor >= _numFlame)
            _nowFlameCursor = _nowFlameCursor % numOfline;
        if (_nowFlameCursor < 0)
        {
            _nowFlameCursor = (_numFlame + _nowFlameCursor) % _numFlame;
        }

        if (ischange)
        {
            if (befidx != _nowFlameCursor)
                LeaveAnime(befidx);
            CursorAnime(_nowFlameCursor);
            Map_Sound.Instance.CursorMoveSoundPlay();
        }
    }

    /// <summary>
    /// EndSelectingはジャンプ先のトレイに実行してもらう。
    /// act = EndSelecting() 
    /// (actはCloseActなので正常終了時にEndSelecting
    /// (キャンセルボタンが押された時はジャンプ先のトレイに独自に終了処理を書く。
    /// </summary>
    /// <param name="isgoto">If set to <c>true</c> isgoto.</param>
    /// <param name="act">Act.</param>
    public override void GotoNextTray(bool isgoto, Action act = null)
    {
        UITray._isCanOpenTray = true;
        base.GotoNextTray(isgoto, act);
    }

    public override void GotoBackTray(bool isgoto)
    {
        if (isgoto && canselAction != null)
        {
            canselAction.Invoke();  // セレクタのインデックスを戻す。
            foreach (var flame in _objectFlame)
            {
                flame.PlayAnimeLeaveCursor();
            }
            CanselSelecting();
            _anim.SetInteger("State", YET);
            base.GotoBackTray(isgoto);
        }
    }

    // ----------------- 更新処理 ------------------------

    // Update is called once per frame
    void Update()
    {
        if (!_isSelecting || !_isRunning)
            return;

        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");
        this.CursorMove(new Vector2(h, v));
        this.GotoNextTray(plycnt.isFire1, () => EndSelecting());
        this.GotoBackTray(plycnt.isFire2);
    }

}
