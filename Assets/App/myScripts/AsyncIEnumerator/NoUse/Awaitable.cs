﻿using System;
using System.Collections;
using System.Threading.Tasks;
/*
public class Awaitable
{
    /// <summary>
    /// このスクリプトからシーン上のMainThreadDispatcherにコルーチンを
    /// 登録、保持、実行するよ
    /// </summary>
    /// <returns>The create.</returns>
    /// <param name="creation">Creation.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static Task<T> Create<T>(Func<TaskCompletionSource<T>, IEnumerator> creation)
    {
        var tcs = new TaskCompletionSource<T>();

        // コルーチンを生成
        var coroutine = creation(tcs);

        MainThreadDispatcher.Instance.RegisterCoroutine(coroutine);

        return tcs.Task;
    }
}

*/