﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// 独自定義。
/// UniRxにもコルーチン動作機構があるので注意。
/// </summary>
public class MainThreadDispatcher : MonoBehaviour
{
    private static MainThreadDispatcher instance;

    public static MainThreadDispatcher Instance
    {
        get
        {
            if (instance == null)
            {
                //とりあえず雑に
                instance = FindObjectOfType<MainThreadDispatcher>();
            }
            return instance;
        }
    }


    // -------------- コルーチンの実行でここで２レイヤー -----------------
    // -------------- 変換も含めれば何レイヤーあるか... -----------------

    /// <summary>
    /// Task以外の定義からコルーチンを実行するときの結果受け取りをコールバックでする。
    /// 登録されたこルーチンを実行する。
    /// </summary>
    /// <param name="coroutine">Coroutine.</param>
    /// <param name="callback">Callback.</param>
    public void RegisterCoroutine(IEnumerator coroutine, Action callback) 
    {
        StartCoroutine(WorkCoroutine(coroutine, callback));
    }

    /// <summary>
    /// Taskからコルーチンの実行。
    /// 登録されたコルーチンを実行する
    /// </summary>
    /// <param name="coroutine">Coroutine.</param>
    public void RegisterCoroutine(IEnumerator coroutine)
    {
        StartCoroutine(coroutine);
    }

    /// <summary>
    /// コルーチンのコールバックを実行する。
    /// </summary>
    /// <returns>The coroutine.</returns>
    /// <param name="target">Target.</param>
    /// <param name="callback">Callback.</param>
    private IEnumerator WorkCoroutine(IEnumerator target, Action callback)
    {
        yield return target;    // 結果を返した時がtargetの終わり。
        callback?.Invoke();     // コールバック
    }

    private void Awake()
    {
        instance = this;
    }
}
