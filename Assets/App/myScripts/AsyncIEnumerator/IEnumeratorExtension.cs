﻿using System.Collections;
using UnityEngine;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

/// <summary>
/// IEnumeratorの拡張メソッド。awaitするときに自動的にGetAwaiterが呼ばれる。
/// </summary>
public static class IEnumeratorExtension
{
    // Taskに非同期処理をラップすることも可能。その場合はAwaitableEnumeratorの定義もいらない。
    public static TaskAwaiter<bool> GetAwaiter(this IEnumerator coroutine)
    {
        // TaskCompletionSourceを生成
        // 非ジェネリック版のTaskCompletionSourceは存在しないので、便宜上boolで代用 tcsはTaskが走る
        var tcs = new TaskCompletionSource<bool>();

        MainThreadDispatcher.Instance.RegisterCoroutine(coroutine, () =>
        {
        // コルーチン終了時に値を設定
        tcs.TrySetResult(true);
        });

        return tcs.Task.GetAwaiter();//awaitの機能により、tcs.Taskが完了されたら返される。
    }
}

/*

public static class IEnumeratorExtension
{
    /// <summary>
    /// awaitを実行する時にこれが呼ばれる。
    /// https://ufcpp.net/study/csharp/sp5_awaitable.html
    /// によれば型は問わず、GetAwaiterメソッド、特定メンバ(以下に記述)があればいい。
    /// </summary>
    /// <returns>The awaiter.</returns>
    /// <param name="coroutine">Coroutine.</param>
    public static AwaitableEnumerator GetAwaiter(this IEnumerator coroutine)
    {
        return new AwaitableEnumerator(coroutine);
    }
}

/*
 * // 同名のメソッドを持っていれば型は問わない。
 Taskだと以下のクラス、メソッドをすでに持っている。

class Awatable // class TaskAwaiter Task.GetAwaiter
{
    public Awaiter GetAwaiter() { }
}

// 同上、同名のメソッドを持っていれば型は問わない。
struct Awaiter // TaskAwaiter
{
    public bool IsCompleted { get; }
    public void OnCompleted(Action continuation) { }
    public T GetResult() { }
}*/
