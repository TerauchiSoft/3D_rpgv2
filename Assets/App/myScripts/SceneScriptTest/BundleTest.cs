﻿using UnityEngine;
using SE;

public class BundleTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        BundleCaches.Init();
        var audiomixer = BundleCaches.LoadAudioMixer();
        var icondata = BundleCaches.LoadIconDatas();
        var stagemap = BundleCaches.LoadMapHierarchyMasterDatas(1);
        var eventinstructiondatas = BundleCaches.LoadEventInstructionDatas();
        var audioclip = SEManager.Instance.GetSE("step13a");
        //BundleCaches.LoadEffects();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
