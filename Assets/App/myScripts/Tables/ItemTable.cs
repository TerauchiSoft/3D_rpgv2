﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Battle;

public class ItemTable : Singleton<ItemTable>
{
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み終了
    /// </summary>
    public bool isEndRead;

    /// <summary>
    /// 武器テーブル。武器の定義、機能はここから。
    /// </summary>
    public WeponTable weponTable;

    /// <summary>
    /// アイテムのテーブル
    /// </summary>
    public List<Item> Items = new List<Item>();

    public Item GetItemForId(int id)
    {
        try
        {
            return Instance.Items.First(x => x.ItemID == id);
        }
        catch
        {
            return Item.GetNullItem();
        }
    }

    public int lineidx = 0;
    /// <summary>
    /// csv読み込み
    /// </summary>
    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        instance = this;
        WeponLoad();
        ItemLoad();
    }

    private void ItemLoad()
    {
        var strs = File.ReadAllLines(Application.streamingAssetsPath + "/Tables/ItemTablev2.csv", System.Text.Encoding.UTF8);
        var icondata = BundleCaches.LoadIconDatas();
        for (int i = 2; i < strs.Length; i++)
        {
            var line = strs[i];
            // データを分ける
            var ar = line.Split(',');
            lineidx++;

            if (ar[0][0] == '0')
                continue;

            ActMethod.act mtd = Item.GetItemMethod(int.Parse(ar[0]));
            var item = new Item(ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6], ar[7], ar[8], ar[9], ar[10], ar[11], ar[12], ar[13], mtd, icondata);
            Items.Add(item);
        }

        Item.SetNullItem();
        isEndRead = true;
    }

    private void WeponLoad()
    {
        weponTable = new WeponTable();
        weponTable.Load();
    }
}

/*~----------------------------------/
:                                    :
:    アイテムそのものの構造体        :
:                                    :
/~----------------------------------*/
[System.Serializable]
public struct Item
{
    //************************* csvデータから***********************************

    /// <summary>
    /// アイテムの名前
    /// </summary>
    public string ItemName;

    /// <summary>
    /// アイテムのID、効果の呼び出しに使う
    /// </summary>
    public int ItemID;

    /// <summary>
    /// アイテムの効果量(effectの値で用途が変わる)
    /// </summary>
    public int effectAmount;

    /// <summary>
    /// アイテムの貴重度
    /// </summary>
    public TableValues.Rarity rarity;

    /// <summary>
    /// アイテムの種類
    /// </summary>
    public TableValues.Kind kind;

    /// <summary>
    /// アイテムの効果
    /// </summary>
    public TableValues.Effect[] effects;

    /// <summary>
    /// アイテムの属性
    /// </summary>
    public TableValues.Attribute attribute;

    /// <summary>
    /// ターゲット
    /// </summary>
    public TableValues.Tgt Target;

    /// <summary>
    /// 武器アイテムの場合の武器ステータス。
    /// </summary>
    public Wepon weponStatus;

    /// <summary>
    /// アイテム使用時のメッセージ
    /// *Usr, *Tgtで使用者、対象者の名前を入れるように使う。
    /// 改行はCR(\r)で
    /// </summary>
    public string[] ItemMessages;

    /// <summary>
    /// エフェクトの名前
    /// </summary>
    public string EffectName;

    /// <summary>
    /// ダメージ計算式
    /// </summary>
    public string DamageFormula;

    /// <summary>
    /// アイテムの説明文
    /// </summary>
    public string ExplanatoryText;

    /// <summary>
    /// ショップで販売される値段。
    /// </summary>
    public int Gold;

    public int[] MaterialItemIDs;

    /// <summary>
    /// アイテムの個数
    /// </summary>
    public int ItemNum;

    public void SetItemNum(int n) {
        ItemNum = n;
    }

    //***************************************************************************

    //************************* BattleControlから*******************************

    /// <summary>
    /// アイテムの関数、威力計算に使う
    /// </summary>
    public ActMethod.act itm_method;

    //***************************************************************************

    //*************************** IconDataから **********************************
    /// <summary>
    /// アイコンの画像
    /// </summary>
    public Sprite IconSprite;
    /// <summary>
    /// アイテム効果を示す画像。
    /// </summary>
    public Sprite IconEffectSprite;

    //***************************************************************************

    /// <summary>
    /// アイテム割当
    /// </summary>
    /// <param name="id"></param>
    /// <param name="Name"></param>
    /// <param name="likes"></param>
    /// <param name="amount"></param>
    /// <param name="Atr"></param>
    /// <param name="tgt"></param>
    /// <param name="Text"></param>
    public Item(string id, string Name, string amount, string rary, string kinds, string efc, string atr,
        string tgt, string Text, string efcname, string damage_formula, string exp_txt, string matitems, string gold,
        ActMethod.act method, IconDatas icondt) {
        int ID = int.Parse(id);
        int Amount = int.Parse(amount);
        //int Likes = int.Parse(likes);//
        //int Effect = int.Parse(efc);//
        //int Tgt = int.Parse(tgt);//
        //int Attribute = int.Parse(atr);//

        ItemID = ID;
        ItemName = Name;
        itm_method = method;
        Target = TableValues.GetTgtFormStr(tgt);
        rarity = TableValues.GetRarityFromStr(rary);
        kind = TableValues.GetKindFormStr(kinds);
        effects = TableValues.GetEffectsFormStr(efc);
        effectAmount = (int)Amount;
        DamageFormula = damage_formula;
        ExplanatoryText = exp_txt;
        Gold = int.Parse(gold);
        attribute = TableValues.GetAttributeFormStr(atr);
        EffectName = efcname;

        if (kind == TableValues.Kind.Wepon)
            weponStatus = ItemTable.Instance.weponTable.GetWeponData(ItemID);
        else
            weponStatus = new Wepon("NoInit");

        var ary = Text.Split('/');
        ItemMessages = ary;

        if (matitems != "-") {
            var matitemstrs = matitems.Split('/');
            MaterialItemIDs = new int[matitemstrs.Length];
            for (int i = 0; i < matitemstrs.Length; i++) {
                int i_id = int.Parse(matitemstrs[i]);
                MaterialItemIDs[i] = i_id;
            }
        } else {
            // 錬金できないアイテムは自身のアイテム1種を素材アイテムに指定する。
            MaterialItemIDs = new int[1];
            MaterialItemIDs[0] = ItemID;
        }

        IconSprite = icondt?.GetItemIcon(ItemID);
        IconEffectSprite = icondt?.GetEffectIcon(effects[0]);
        ItemNum = 1;
    }

    /// <summary>
    /// アイテムのスキルの計算式を取得
    /// </summary>
    /// <returns></returns>
    public static ActMethod.act GetItemMethod(int id) {
        //act[] ac = { null, RecHpLv1, RecHpLv2, RecHpLv3 };
        return RecHpLv1;//ac[id];
    }

    /************************************
     * 
     *       アイテムスキル
     * 
     ***********************************/
    public static int RecHpLv1(Battle_Status b_st, Battle_Status b_st_tgt) {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    public static int RecHpLv2(Battle_Status b_st, Battle_Status b_st_tgt) {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    public static int RecHpLv3(Battle_Status b_st, Battle_Status b_st_tgt) {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    // public関数
    private static Item nullItem;
    public static Item GetNullItem() { return nullItem; }
    public static void SetNullItem() {
        nullItem = new Item("0", "", "0", "Material", "Any", "HPRecover", "Normal", "Player", "", "Fire1", "", "", "-", "0", null, null);
        nullItem.ItemNum = 1;
    }
}
