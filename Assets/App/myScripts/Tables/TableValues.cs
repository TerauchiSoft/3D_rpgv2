﻿using System;

public static class TableValues
{
    /// <summary>
    /// ダンジョンの名前
    /// </summary>
    public readonly static string[] DUNGEON_NAMES =
       { "森の迷宮", "洞窟", "石造りの迷宮", "氷の迷宮", "漆黒の迷宮" };

    /// <summary>
    /// ダンジョンフロアの接頭語
    /// </summary>
    public readonly static string[] DUNGEON_FLOOR_NAMES =
        { "F", "B", "B", "F", "B" };

    /// <summary>
    /// お金の名前
    /// </summary>
    public readonly static string MONEY_NAME = "G";

    /// <summary>
    /// 貴重度
    /// </summary>
    public enum Rarity
    {
        Material, Normal, Rare, SuperRare, Event
    }

    /// <summary>
    /// スキル、アイテムの種類
    /// </summary>
    public enum Kind
    {
        Any, Battle, Map, Wepon
    }

    /// <summary>
    /// スキル、アイテムの属性
    /// 取得する際は武器属性と合わせて配列にて取得する。
    /// </summary>
    public enum Attribute
    {
        Normal = 0, Fire = 1, Plant = 2, Water = 3, Light = 4, Dark = 8
    }

    /// <summary>
    /// スキル、アイテムの効果
    /// </summary>
    public enum Effect
    {
        HPRecover = 0, MPRecover = 1, HPMPRecover = 2,
        Attack = 10, MPDown = 11,
        STRUp = 20, STRDown = 30,
        AGIUp = 21, AGIDown = 31,
        VITUp = 22, VITDown = 32,
        LUCKUp = 23, LUCKDown = 33,
        CRIUp = 24, CRIDown = 34,
        Event = 40,
    }

    /// <summary>
    /// Effekseerで再生されるエフェクトのタイプ。
    /// </summary>
    public enum ViewType
    {
        Single, Multiple
    }

    /// <summary>
    /// スキル、アイテムに設定されているターゲット。
    /// Player, PlayerAll = thisの自陣営。
    /// </summary>
    public enum Tgt
    {
        Me, Player, PlayerAll, Enemy, EnemyAll
    }

    /// <summary>
    /// 能力地の種類
    /// </summary>
    public enum AbirityType
    {
        HP, MP, STR, AGI, VIT, LUK, CRI,
    }

    public static Rarity GetRarityFromStr(string str) {
        try 
        {
            return (Rarity)Enum.Parse(typeof(Rarity), str);
        } 
        catch
        {
            throw new Exception("列挙型にParse出来ませんでした。\nGetRarityFormStr");
        }
    }

    public static Kind GetKindFormStr(string str)
    {
        try
        {
            return (Kind)Enum.Parse(typeof(Kind), str);
        }
        catch
        {
            throw new Exception("列挙型にParse出来ませんでした。\nGetLikeFormStr");
        }
    }

    public static Effect[] GetEffectsFormStr(string str)
    {
        string[] strs = str.Split('/');
        Effect[] efcs = new Effect[strs.Length];

        for (int i = 0; i < strs.Length; i++)
        {
            try
            {
                efcs[i] = (Effect)Enum.Parse(typeof(Effect), strs[i]);
            }
            catch
            {
                UnityEngine.Debug.LogError(strs[i]);
                throw new Exception("列挙型にParse出来ませんでした。\nGetEffectsFormStr");
            }
        }
        return efcs;
    }

    public static ViewType GetViewTypeFromStr(string str)
    {
        try
        {
            return (ViewType)Enum.Parse(typeof(ViewType), str);
        }
        catch
        {
            throw new Exception("列挙型にParse出来ませんでした。\nGGetViewTypeFromStr");
        }
    }

    public static Tgt GetTgtFormStr(string str)
    {
        try
        {
            return (Tgt)Enum.Parse(typeof(Tgt), str);
        }
        catch
        {
            throw new Exception("列挙型にParse出来ませんでした。\nGetTgtFormStr");
        }
    }

    public static Attribute GetAttributeFormStr(string str)
    {
        try
        {
            return (Attribute)Enum.Parse(typeof(Attribute), str);
        }
        catch
        {
            throw new Exception("列挙型にParse出来ませんでした。\nGetAttributeFormStr");
        }
    }

}
