﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using Battle;

public class SkillTable : Singleton<SkillTable>
{
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み終了
    /// </summary>
    public bool isEndRead;

    /// <summary>
    /// 敵のスキルのテーブル
    /// </summary>
    public List<Skill> enm_skills = new List<Skill>();

    /// <summary>
    /// プレイヤーのスキルのテーブル
    /// </summary>
    public List<Skill> ply_skills = new List<Skill>();

    /// <summary>
    /// スキルIDからスキルを取得。
    /// プレイヤースキルは-200してから引数に積み込む。
    /// </summary>
    /// <returns>The skill for identifier.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="isEnemy">If set to <c>true</c> is enemy.</param>
    public static Skill GetSkillForId(int id, bool isEnemy)
    {
        if (id == 0)
        {
            return Skill.GetNullSkill();
        }

        try
        {
            if (isEnemy)
                return Instance.enm_skills.First(x => x.SkillID == id);
            else
                return Instance.ply_skills.First(x => x.SkillID == id);
        }
        catch
        {
            throw new System.Exception("スキルIDが境界の範囲外でした。");
        }
    }

    public int lineidx = 0;
    /// <summary>
    /// csv読み込み
    /// </summary>
    private void Awake()
    {
        if (Instance != null)
        {
            return;
        }

        instance = this;
        FileStream table = new FileStream(Application.streamingAssetsPath + "/Tables/SkillTable.csv", FileMode.Open, FileAccess.Read);
        var iconmaster = BundleCaches.LoadIconDatas();
        //抜けると同時に削除
        using (StreamReader sr = new StreamReader(table, System.Text.Encoding.UTF8))
        {
            //ファイルが空の場合
            if (table.Length == 0)
            {
                print("ファイルが空でした。");
                return;
            }

            //一、二行目はスルー
            sr.ReadLine();
            sr.ReadLine();

            //一行ごとに読み込める
            while (!sr.EndOfStream)
            {
                // ファイルから一行読み込む
                var line = sr.ReadLine();

                // データを分ける
                var ar = line.Split(',');
                lineidx++;

                if (ar[0] == "0")
                    continue;

                ActMethod.act mtd = Skill.GetSkillMethod(int.Parse(ar[0]));
                if (int.Parse(ar[0]) < 200)
                    enm_skills.Add(new Skill(ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6], ar[7], ar[8], ar[9], ar[10], ar[11], ar[12], ar[13], mtd, iconmaster));
                else
                    ply_skills.Add(new Skill(ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6], ar[7], ar[8], ar[9], ar[10], ar[11], ar[12], ar[13], mtd, iconmaster));
           }
        }
        Skill.SetNullSkill();
        isEndRead = true;
    }

    /*~----------------------------------/
    :                                    :
    :    外部からスキルを取得する        :
    :                                    :
    /~----------------------------------*/
    public enum SkillID
    {
        none = 0, 鉤爪攻撃, ギロチンハンマー,ウォーター,火炎放射,フレアテール,あわ,
        体当たり,蔓の鞭,荊棘の鞭,抱擁,掴み攻撃,竜巻,槍で突く,吠える,角突,突進,何もしない,マグマなげ,
        胞子,みをまもる,たいあたり,ドラゴンシュレッド,ドラゴンバット,ドラゴンダンス,ドラゴンフレア,
        ドラゴンサンダー,ドラゴンブリザード,妖精の息吹,完コピ,ビンタ,アイスニードル,インブレイスアウト,
        アブソリュートメイデン,パンチ,かたくなる,プレス,プレッサ,プレサージュ,騎士の誇り,
        暗黒魔法インブレイズアウト,暗黒一閃,アイアンパンチ,アイアンタックル,光魔法,
        氷魔法アイスニードル, QED, エターナルエクスプローション, 黒の蠱惑,

        // プレイヤースキル
        フレイム = 201, フレイズ, フリーズ, キュア, カリュプス, クリスト, コールド, バーニング, ハイキュア
    }


    public Skill GetSkillFromEffectName(SkillID id)
    {
        var skls = ((int)id < 200) ? enm_skills : ply_skills;
        int iid = (int)id;
        for (int i = 0; i < skls.Count; i++)
        {
            if (iid == skls[i].SkillID)
                return skls[i];
        }

        return skls[0];
    }
}



/*~----------------------------------/
:                                    :
:    スキルそのものの構造体          :
:                                    :
/~----------------------------------*/
[System.Serializable]
public struct Skill
{
    //***************************** SkillTableから **********************************

    /// <summary>
    /// スキルの名前
    /// </summary>
    public string SkillName;

    /// <summary>
    /// スキルのID、効果の呼び出しに使う
    /// </summary>
    public int SkillID;

    /// <summary>
    /// スキルのmp消費量
    /// </summary>
    public int mpCost;

    /// <summary>
    /// スキルの効果量(effectの値で用途が変わる)
    /// </summary>
    public int effectAmount;

    /// <summary>
    /// スキル習得レベル(プレイヤー)
    /// </summary>
    public int getSkillLevel;

    /// <summary>
    /// アイテムの種類
    /// </summary>
    public TableValues.Kind like;

    /// <summary>
    /// アイテムの効果
    /// </summary>
    public TableValues.Effect[] effects;

    /// <summary>
    /// アイテムの属性
    /// </summary>
    public TableValues.Attribute attribute;

    /// <summary>
    /// ターゲット
    /// </summary>
    public TableValues.Tgt Target;

    /// <summary>
    /// スキル仕様時のメッセージ
    /// *Usr, *Tgtで使用者、対象者の名前を入れるように使う。
    /// 改行はCR(\r)で
    /// </summary>
    public string[] SkillMessages;

    /// <summary>
    /// エフェクトの名前
    /// </summary>
    public string EffectName;

    /// <summary>
    /// エフェクトの表示のしかた
    /// </summary>
    public TableValues.ViewType ViewType;

    /// <summary>
    /// ダメージ計算式
    /// </summary>
    public string DamageFormula;

    /// <summary>
    /// スキルの説明文
    /// </summary>
    public string ExplanatoryText;

    //***************************************************************************

    //**************************** 何に使ってるかわからん ***************************

    /// <summary>
    /// 乱数
    /// </summary>
    public static System.Random rand;

    //*******************************************************************************

    //**************************** BattleControlから ********************************

    /// <summary>
    /// スキルの関数、威力計算に使う
    /// </summary>
    public  ActMethod.act skl_method;

    //*******************************************************************************

    //*************************** IconDataから **************************************
    /// <summary>
    /// アイコンの画像
    /// </summary>
    public Sprite IconSprite;
    /// <summary>
    /// アイコンの効果画像
    /// </summary>
    public Sprite IconEffectSprite;

    //*******************************************************************************

    /// <summary>
    /// スキル割当
    /// </summary>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    /// <param name="mp_cost">Mp cost.</param>
    /// <param name="amount">Amount.</param>
    /// <param name="getskllv">Getskllv.</param>
    /// <param name="likes">Likes.</param>
    /// <param name="efc">Efc.</param>
    /// <param name="atr">Atr.</param>
    /// <param name="tgt">Tgt.</param>
    /// <param name="Text">Text.</param>
    /// <param name="efcname">Efcname.</param>
    /// <param name="viewtype">Viewtype.</param>
    /// <param name="damage_formula">Damage formula.</param>
    /// <param name="exp_txt">Exp text.</param>
    /// <param name="mtd">Mtd.</param>
    /// <param name="icondt">Icondt.</param>
    public Skill(string id, string name, string mp_cost, string amount, string getskllv, string likes, string efc,
                 string atr, string tgt, string Text, string efcname, string viewtype, string damage_formula, string exp_txt,
                  ActMethod.act mtd, IconDatas icondt)
    {
        int Id = int.Parse(id);
        if (Id >= 200) Id -= 200;
        int Mp_Cost = int.Parse(mp_cost);
        int Amount = int.Parse(amount);
        int GetSklLv = int.Parse(getskllv);
        //int Likes = int.Parse(likes);//
        //int Effect = int.Parse(efc);//
        //int Tgt = int.Parse(tgt);//
        //int Attribute = int.Parse(atr);//
        rand = new System.Random();
        mpCost = int.Parse(mp_cost);
        SkillID = Id;
        SkillName = name;
        skl_method = mtd;
        Target = TableValues.GetTgtFormStr(tgt);
        like = TableValues.GetKindFormStr(likes);
        effects = TableValues.GetEffectsFormStr(efc);
        effectAmount = (int)Amount;
        getSkillLevel = (int)GetSklLv;
        DamageFormula = damage_formula;
        ExplanatoryText = exp_txt;
        attribute = TableValues.GetAttributeFormStr(atr);
        EffectName = efcname;
        ViewType = TableValues.GetViewTypeFromStr(viewtype);

        var ary = Text.Split('/');
        SkillMessages = ary;

        IconSprite = icondt.GetSkillIcon(int.Parse(id));
        IconEffectSprite = icondt.GetEffectIcon(effects[0]);
    }

    /// <summary>
    /// 使用スキルと対象者の属性チェックして倍数を返す。
    /// </summary>
    /// <returns></returns>
    public static float MagnificationByAttribute(Battle_Status b_st, TableValues.Attribute atr)
    {
        /*
         * Skill.attribute
         * (TableValues.Attribute)の値を見る。
         * skl = スキル属性:	 enm = 対象属性
         */
        int skl = (int)atr;
        int enm = (int)b_st.EnmStatus[b_st.SelectedTgtNo].Atr;

        // 無属性攻撃または対象が無属性
        if (skl == 0 || enm == 0) return 1.0f;

        // 光闇属性
        if (skl >= 4 && enm >= 4)
            return (skl != enm) ? 1.4f : 0.6f;
        else if (skl >= 4 || enm >= 4)
            return 1.0f;

        // その他3属性
        // 炎の技
        if (skl == 1)
        {
            if (enm == 1) return 1.0f;      //炎→炎
            else if (enm == 2) return 1.4f; //炎→植物
            else return 0.6f;               //炎→水
        }

        // 植物の技
        if (skl == 2)
        {
            if (enm == 1) return 0.6f;      //植物→炎
            else if (enm == 2) return 1.0f; //植物→植物
            else return 1.4f;               //植物→水
        }

        // 水の技
        if (skl == 3)
        {
            if (enm == 1) return 1.4f;      //水→炎
            else if (enm == 2) return 0.6f; //水→植物
            else return 1.0f;               //水→水
        }
        else
            return 1.0f;
        
    }

    /// <summary>
    /// MPを消費させてMPが足りたかを返す。
    /// </summary>
    /// <returns>The mp.</returns>
    /// <param name="b_st">B st.</param>
    static bool SubMP(Battle_Status b_st)
    {
        var substvalue = b_st.Mp - b_st.SelectedSkl.mpCost;
        if (b_st.UseMagic == false)
        {
            b_st.Mp = Mathf.Clamp(substvalue, 0, b_st.Mp);
            b_st.UseMagic = true;
        }
        if (substvalue < 0)
        {
            return false;
        }
        return true;
    }

    //Normal, Fire, Plant, Water, Light, Dark

    /*~----------------------------------/
    :                                    :
    :        スキル計算式の関数          :
    :                                    :
    /~----------------------------------*/
    // ダメージの共通部分として
    // ダメージ基本式 * 属性
    // がある。





    /*~----------------------------------/
    :                                    :
    :             一般式の計算           :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// ダメージ補正
    /// </summary>
    public static float DamageCorrection = 0.3f;
    public static float Randval { get { return 0.00001f * (rand.Next(87000, 113000)); } }

    /// <summary>
    /// ダメージ基本値計算
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    /// 
    /*public static int GetDamageBasicValue(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // ダメージ基本値
        /*
         * ダメージ値算出
         *  イメージとしては、VITが素の防御力、INTは攻撃をかわす賢さ
         *  ダメージ基本値 = ( STR * 2 - (対象VIT + 対象INT) / 8 ) * 0.95 ~ 1.05
    //   
        float randval = 0.00001f * (rand.Next(95000, 105000));
        Console.WriteLine(randval);
        float range = ((float)b_st.B_Str * 2 - (float)(b_st_tgt.B_Vit + b_st_tgt.B_Int) / 8) * (float)randval;
        return (int)range;
    }*/

    // -------------- スキルのActionリスト -----------------
    // -------------- 順番はSkillTable.csvのID順 ------------

    static  ActMethod.act[] ac = { null,
            wolfclaw, Ygirotin, Ywater, FflreBurst, FflreTail, Fawa, Ftaiatari,
            Aturu, Akeikyoku, Ahouyou, Htukami, Htornado, Dreims, Dhoeru, Ekakutuki, Etossin, ORock, MMaguma,
            MHousi, SGuard, Dtaiatari, DClaw, DBat, DDansu, DFlara, DThunder, DBlizzard, FWind, FCopy, FBinta,
            MIceNeedle, MInBreizOut, MAbsolute, SPunch, SGuard, MPurss1, MPurss2, MPurss3, DNight, DInBreizOut,
            DDarkSword, APunch, ATackle, BLight, BIceNeedle, SQED, SEks, SBlack};
    // プレイヤー
    static ActMethod.act[] acp = { null,
            Fire1, Fire2, AFrieze, AHeel, AKhalyps, AChrist, ACold, ABurning, AHiHeel,
            Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,// えりな
            Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,Fire1,// ぺてぃる
             };

    /// <summary>
    /// スキルの計算式を取得
    /// </summary>
    /// <returns></returns>
    public static  ActMethod.act GetSkillMethod(int id)
    {
        //TODO: 特殊メソッドは会話で使うのもありだと思う。staticのInvokeはするものではない...。
        if (id < 200) return ac[id];
        else
        {
            id -= 200;
            return acp[id];
        }
    }
    
    
    /************************************
     * 
     *          スキル
     * 
     ***********************************/

    /// <summary>
    /// 鉤爪攻撃
    /// </summary>
    /// <returns></returns>
    public static int wolfclaw(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }

    /// <summary>
    /// やしがにんギロチンハンマー
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Ygirotin(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// やしがにんウォーター
    /// </summary>
    /// <returns></returns>
    public static int Ywater(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フレイムラット火炎放射
    /// </summary>
    /// <returns></returns>
    public static int FflreBurst(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フレイムラットフレアテール
    /// </summary>
    /// <returns></returns>
    public static int FflreTail(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }
    
    /// <summary>
    /// あわ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Fawa(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フレアテール
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int FfireTail(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// たいあたり
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Ftaiatari(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// アルラウネつるのムチ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Aturu(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// アルラウネいたずらはだめよ?
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Akeikyoku(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// アルラウネ抱擁
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Ahouyou(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ハーピー掴み攻撃
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Htukami(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ハーピー竜巻
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Htornado(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// 犬門番槍でつく
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Dreims(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// 犬門番吠える
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Dhoeru(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// エアレー角付き
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Ekakutuki(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// エアレー突進
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Etossin(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// オートマタ???
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int ORock(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// マッシュメルツマグマ投げ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MMaguma(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// マッシュメルツ胞子
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MHousi(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンベイビーたいあたり
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DGuard(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンベイビーたいあたり
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int Dtaiatari(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンキッズドラゴンスラッシュ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DClaw(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンキッズドラゴンバット
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DBat(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンキッズドラゴンダンズ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DDansu(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンドラゴンフレア
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DFlara(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンドラゴンサンダー
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DThunder(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ドラゴンドラゴンブリザード
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DBlizzard(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フェアリー妖精の息吹
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int FWind(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フェアリー完コピ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int FCopy(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// フェアリービンタ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int FBinta(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// 魔道士アイスニードル
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MIceNeedle(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// 魔道士インブレスアウト
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MInBreizOut(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// 魔道士アブソリュートメイデン
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MAbsolute(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ストーンゴーレムパンチ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int SPunch(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ストーンゴーレムかたくなる
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int SGuard(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ミミックプレス
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MPurss1(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ミミックプレス
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MPurss2(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ミミックプレサージュ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int MPurss3(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// デュラハン騎士の誇り
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DNight(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// デュラハン暗黒魔法(インブレスアウト)
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DInBreizOut(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// デュラハン暗黒一閃
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int DDarkSword(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// アイアンゴーレムアイアンパンチ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int APunch(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// アイアンゴーレムアイアンタックル
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int ATackle(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ブロッサムスライム光魔法
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int BLight(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// ブロッサムスライム氷魔法(アイスニードル)
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int BIceNeedle(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// スフィンクスQED
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int SQED(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// スフィンクスエターナルエクスプローション
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int SEks(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }

    /// <summary>
    /// スフィンクス黒の蠱惑
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int SBlack(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) *DamageCorrection * Randval);
    }
    
    /// <summary>
    /// フレイム
    /// </summary>
    /// <returns></returns>
    public static int Fire1(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        // 基本的に,スキルの効果量(固有値) * DamageFormula(計算式) * ダメージ基本値 * 相性 * ダメージ補正
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }

    /// <summary>
    /// フレイズ
    /// </summary>
    /// <returns></returns>
    public static int Fire2(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        // 基本的に,スキルの効果量(固有値) * DamageFormula(計算式) * ダメージ基本値 * 相性 * ダメージ補正
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }


    /// <summary>
    /// アレスタフリーズ
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int AFrieze(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        // 基本的に,スキルの効果量(固有値) * DamageFormula(計算式) * ダメージ基本値 * 相性 * ダメージ補正
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }

    /// <summary>
    /// アレスタキュア
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int AHeel(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    /// <summary>
    /// アレスタカリュプス
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int AKhalyps(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    /// <summary>
    /// アレスタクリスト
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int AChrist(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    /// <summary>
    /// アレスタコールド
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int ACold(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        // 基本的に,スキルの効果量(固有値) * DamageFormula(計算式) * ダメージ基本値 * 相性 * ダメージ補正
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }

    /// <summary>
    /// アレスタバーニング
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int ABurning(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        // 基本的に,スキルの効果量(固有値) * DamageFormula(計算式) * ダメージ基本値 * 相性 * ダメージ補正
        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt) *
                     MagnificationByAttribute(b_st, b_st.SelectedSkl.attribute) * DamageCorrection * Randval);
    }

    /// <summary>
    /// アレスタハイキュア
    /// 弱点をかけない
    /// </summary>
    /// <param name="b_st"></param>
    /// <returns></returns>
    public static int AHiHeel(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        // MP消費 MPが足りなければ威力1
        if (SubMP(b_st) == false)
            return 1;

        return (int)(BracketCalculation.GetFormulaVal(b_st, b_st_tgt));
    }

    // public関数
    private static Skill nullSkill;
    public static Skill GetNullSkill() { return nullSkill; }
    public static void SetNullSkill() { nullSkill = new Skill(); }
}
