﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// ItemTableのメンバとして振る舞う。
/// </summary>
[System.Serializable]
public class WeponTable
{
    public List<Wepon> wepons = new List<Wepon>();

    /// <summary>
    /// テーブルをロードする。
    /// </summary>
    public void Load()
    {
        var strs = File.ReadAllLines(Application.streamingAssetsPath + "/Tables/WeponTable.csv", System.Text.Encoding.UTF8);
        for (int i = 2; i < strs.Length; i++)
        {
            // [アイテムID   武器の名称]   2 ~ 8 STR(2) AGI INT VIT LUK CRI ATR(8) 装備時のメッセージ(9)
            var vs = strs[i].Split(',');
            wepons.Add(new Wepon(vs[0], vs[1], vs[2], vs[3], vs[4], vs[5], vs[6], vs[7], vs[8], vs[9]));
        }
    }

    /// <summary>
    /// 武器アイテムの場合、武器データをコピーして紐つける。
    /// </summary>
    /// <param name="id">Item.</param>
    public Wepon GetWeponData(int id)
    {
        try
        {
            return wepons.First(wpn => wpn.itemID == id);
        }
        catch
        {
            throw new Exception("武器のIDとアイテムのIDが一致しません。");
        }
    }
}

/// <summary>
/// 武器のデータ
/// </summary>
[System.Serializable]
public struct Wepon
{
    /// <summary>
    /// Inspector表示用武器名前
    /// </summary>
    public string itemName;

    /// <summary>
    /// アイテムのID
    /// </summary>
    public int itemID;

    /// <summary>
    /// 武器の能力
    /// </summary>
    public StatusClass status;

    /// <summary>
    /// 装備時メッセージ
    /// </summary>
    public string equitMes;

    /// <summary>
    /// null
    /// </summary>
    /// <param name="nosign">Nosign.</param>
    public Wepon(string nosign)
    {
        itemName = "NoName"; itemID = 0; status = new StatusClass();
        equitMes = "";
    }

    /// <summary>
    /// 武器
    /// </summary>
    /// <param name="ID">Identifier.</param>
    /// <param name="NAME">Name.</param>
    /// <param name="STR">String.</param>
    /// <param name="AGI">Agi.</param>
    /// <param name="INT">Int.</param>
    /// <param name="VIT">Vit.</param>
    /// <param name="LUK">Luk.</param>
    /// <param name="CRI">Cri.</param>
    /// <param name="ATR">Atr.</param>
    public Wepon(string ID, string NAME, string STR, string AGI, string INT, string VIT, string LUK, string CRI, string ATR, string eqMessage)
    {
        itemName = NAME;
        itemID = int.Parse(ID);
        status = new StatusClass { Str = int.Parse(STR), Agi = int.Parse(AGI), Int = int.Parse(INT), Vit = int.Parse(VIT), Luk = int.Parse(LUK), Cri = int.Parse(CRI), Atr = (TableValues.Attribute)Enum.Parse(typeof(TableValues.Attribute), ATR) };
        equitMes = eqMessage;
    }
}
