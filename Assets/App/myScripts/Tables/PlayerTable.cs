﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;

public class PlayerTable : Singleton<PlayerTable>
{
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み終了
    /// </summary>
    public bool isEndRead;

    /// <summary>
    /// 主人公キャラのテーブル
    /// </summary>
    public List<StatusTable> st_Aresta = new List<StatusTable>();
    public List<StatusTable> st_Erina = new List<StatusTable>();
    public List<StatusTable> st_Petil = new List<StatusTable>();

    /// <summary>
    /// csv読み込み
    /// </summary>
    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        instance = this;

        FileStream table = new FileStream(Application.streamingAssetsPath + "/Tables/PlayerData.csv", FileMode.Open, FileAccess.Read);
        Task.Run(() =>
        {
            int lineno = 0;
            try
            {
                //抜けると同時に削除
                using (StreamReader sr = new StreamReader(table, System.Text.Encoding.UTF8))
                {
                    //ファイルが空の場合
                    if (table.Length == 0)
                    {
                        print("ファイルが空でした。");
                        return;
                    }

                    //一行目はスルー
                    sr.ReadLine();

                    //一行ごとに読み込める
                    while (!sr.EndOfStream)
                    {
                        // ファイルから一行読み込む
                        var line = sr.ReadLine();

                        // データを分ける
                        var ar = line.Split(',');

                        if (ar[0] == "アレスタ")
                            st_Aresta.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                                ar[4], ar[5], ar[6], ar[7],
                                                ar[8], ar[9], ar[10], ar[11], ar[12], ar[13]));
                        if (ar[0] == "エリナ")
                            st_Erina.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                                ar[4], ar[5], ar[6], ar[7],
                                                ar[8], ar[9], ar[10], ar[11], ar[12], ar[13]));
                        if (ar[0] == "ペティル")
                            st_Petil.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                                ar[4], ar[5], ar[6], ar[7],
                                                ar[8], ar[9], ar[10], ar[11], ar[12], ar[13]));
                        lineno++;
                    }

                }
            }
            catch (System.IO.FileNotFoundException ex)
            {
                print("ファイルが見つかりませんでした。");
                throw;
            }
            catch (System.UnauthorizedAccessException ex)
            {
                print("必要なアクセス許可がありません。");
                throw;
            }
            catch
            {
                Debug.Log("LineNo : " + lineno);
                throw new System.Exception("データの並びがおかしいです。");
            }

            isEndRead = true;
        });
    }
}

/*~----------------------------------/
:                                    :
:  レベルごとの増分そのものの構造体  :
:                                    :
/~----------------------------------*/
[System.Serializable]
public struct StatusTable
{
    /// <summary>
    /// 主人公キャラのレベル
    /// </summary>
    public int Lv;

    /// <summary>
    /// そのレベルで習得するスキル
    /// </summary>
    public int GetSkillNo;

    /// <summary>
    /// 主人公キャラのHp増分
    /// </summary>
    public int Hp;

    /// <summary>
    /// 主人公キャラのHp増分
    /// </summary>
    public int Mp;

    /// <summary>
    /// 主人公キャラの魔力増分
    /// </summary>
    public int Str;

    /// <summary>
    /// 主人公キャラの素早さ増分
    /// </summary>
    public int Agi;

    /// <summary>
    /// 主人公キャラの賢さ増分
    /// </summary>
    public int Int;

    /// <summary>
    /// 主人公キャラの元気度増分
    /// </summary>
    public int Vit;

    /// <summary>
    /// 主人公キャラの運のよさ増分
    /// </summary>
    public int Luk;

    /// <summary>
    /// 主人公キャラのクリティカル度増分
    /// </summary>
    public int Cri;

    /// <summary>
    /// 主人公キャラの経験値増分
    /// </summary>
    public int Exp;

    /// <summary>
    /// 主人公キャラの累積経験値
    /// </summary>
    public int Exp_ALL;

    /// <summary>
    /// 状態異常(バトルのみ)
    /// </summary>
    public enum Abnormality
    {
        Normal, Poison, Freeze, Down
    }

    /// <summary>
    /// 主人公キャラの属性
    /// </summary>
    public TableValues.Attribute Atr;
    
    /// <summary>
    /// 主人公キャラの名前
    /// </summary>
    public string Name;
    
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="name"></param>
    /// <param name="lv"></param>
    /// <param name="hp"></param>
    /// <param name="mp"></param>
    /// <param name="str"></param>
    /// <param name="agi"></param>
    /// <param name="_int"></param>
    /// <param name="vit"></param>
    /// <param name="luk"></param>
    /// <param name="cri"></param>
    /// <param name="exp"></param>
    /// <param name="atr"></param>
    public StatusTable(string name, string lv, string hp,
                       string mp, string str, string agi,
                       string _int, string vit, string luk,
                       string cri, string exp, string exp_all, string atr, string getskillNo)
    {
        Name = name;
        Lv = int.Parse(lv);
        Hp = int.Parse(hp);
        Mp = int.Parse(mp);
        Str = int.Parse(str);
        Agi = int.Parse(agi);
        Int = int.Parse(_int);
        Vit = int.Parse(vit);
        Luk = int.Parse(luk);
        Cri = int.Parse(cri);
        Exp = int.Parse(exp);
        Exp_ALL = int.Parse(exp_all);
        Atr = TableValues.GetAttributeFormStr(atr);
        if (!string.IsNullOrEmpty(getskillNo))
            GetSkillNo = int.Parse(getskillNo);
        else
            GetSkillNo = 0;
    }
}
