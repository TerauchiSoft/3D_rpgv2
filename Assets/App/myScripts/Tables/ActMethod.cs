﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{
    public class ActMethod : MonoBehaviour
    {
        /// <summary>
        /// スキルの関数、威力計算に使う
        /// </summary>
        public delegate int act(Battle_Status b_st, Battle_Status b_st_tgt);
    }
}