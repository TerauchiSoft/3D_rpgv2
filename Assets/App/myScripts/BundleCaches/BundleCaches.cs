﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.U2D;
using SE;
using Audio;
using Battle;
using MapVariable;

#if UNITY_EDITOR && DEVELOP
using UnityEditor;
#endif

public class BundleCaches {
    // ------------ Cache Members --------------

    private const string DATA_BUNDLE = "/data";
    private const string BGM_BUNDLE = "/audio";
    private const string SE_BUNDLE = "/se";
    private const string AUDIOMIXER_BUNDLE = "/audiomixer";
    private const string EFFEKSEER_BUNDLE = "/effekseer";
    private const string STAGE_BUNDLE = "/stage";
    private const string STAGECOMMON_BUNDLE = "/stagecommon";
    private const string BATTLE_BUNDLE = "/battle";
    private const string MAP_EVENT_ICON_BUNDLE = "/mapeventicon";

    private const string AUDIOMIXER_ASSET = "AudioMixer";
    private const string ICON_ASSET = "IconDatasMaster";
    private const string EVENT_ASSET = "EventPrefMaster";
    private const string AUDIO_TABLE = "audioTable";
    private const string SE_TABLE = "seTable";
    private const string BATTLE_TABLE = "battleGroupTable";
    private const string MAP_EVENT_SPRITEATLAS = "MapEventAtlas";

#if UNITY_EDITOR && DEVELOP
    private const string RESOURCES_PATH = "/App/Resources";
#endif

    private static AudioMixer audioMixer;
    private static IconDatas iconData;
    private static EventInstructionDatas eventInstructionData;
    private static bool EffectLoaded;

    private static string streamingBundlePass;

    // Resources以下のファイルパスを全て保持
    private static string[] resourcesPaths;

    public static void Init()
    {
#if UNITY_EDITOR && DEVELOP
        // Resources以下のリソースパスを取得
        var path = Application.dataPath + RESOURCES_PATH;
        var paths = Directory.EnumerateFileSystemEntries(path, "*", SearchOption.AllDirectories);
        List<string> assetPaths = new List<string>();

        Debug.Log("Resources以下のリソースパスを取得");
        foreach (var p in paths)
        {
            int subIndex = p.IndexOf("Assets/", System.StringComparison.CurrentCulture);
            var pp = p.Substring(subIndex, p.Length - subIndex);
            var data = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(pp);
            if (data != null)
                assetPaths.Add(pp);
        }
        resourcesPaths = assetPaths.ToArray();
#endif
        streamingBundlePass = Application.streamingAssetsPath;
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        streamingBundlePass = StringBuild.GetStringFromArgs(streamingBundlePass, "/Bundles/Mac");
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        streamingBundlePass = StringBuild.GetStringFromArgs(streamingBundlePass, "/Bundles/Windows");
#endif
        // バンドルをキャッシュ
        LoadAudioMixer();
        LoadSEBundle();
        LoadBGMBundle();
        LoadBattleGroupBundle();
    }

    /// <summary>
    /// アセットバンドルをロード。毎回解放。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="bundleName"></param>
    /// <param name="assetName"></param>
    /// <returns></returns>
    private static T LoadAsset<T>(string bundleName, string assetName, Action<T> callback = null) where T : UnityEngine.Object
    {
        Debug.Log("LoadAsset " + bundleName + " " + assetName);
#if UNITY_EDITOR && DEVELOP
        // Resourcesフォルダ以下からファイル名を検索して取得
        if (string.IsNullOrEmpty(assetName))
            return null;

        foreach (var p in resourcesPaths)
        {
            if (!p.Contains(assetName))
                continue;

            var data = AssetDatabase.LoadAssetAtPath<T>(p);
            if (data == null)
                continue;
            callback?.Invoke(data);
            return data;
        }
        return null;
#else
        // バンドルからロード
        var bundle = AssetBundle.LoadFromFile(bundleName);
        var data = bundle.LoadAsset(assetName, typeof(T)) as T;
        callback?.Invoke(data);
        bundle.Unload(false); // ここでfalseにすることでアセットバンドルのみ解放
        return data;
#endif
    }

    /// <summary>
    /// AudioMixerを取得
    /// </summary>
    /// <returns></returns>
    public static AudioMixer LoadAudioMixer()
    {
        if (audioMixer == null)
        {
            string bundlePath = StringBuild.GetStringFromArgs(streamingBundlePass, AUDIOMIXER_BUNDLE);
            LoadAsset<AudioMixer>(bundlePath, AUDIOMIXER_ASSET, data => audioMixer = data);
        }
        return audioMixer;
    }

    /// <summary>
    /// エフェクトのロード処理
    /// SkillTable読み込み後のバトル前などに呼ぶ。今はゲーム起動時のSkillTableでスキルロード時に行なっている。
    /// </summary>
    /// <param name="table"></param>
    public static void LoadEffects(SkillTable table)
    {
#if UNITY_EDITOR && DEVELOP
        // Resources.Loadから読み出し
        if (EffectLoaded == false)
        {
            for (int i = 0; i < table.enm_skills.Count; i++)
                EffekseerSystem.LoadEffect(table.enm_skills[i].EffectName);
            for (int i = 0; i < table.ply_skills.Count; i++)
                EffekseerSystem.LoadEffect(table.ply_skills[i].EffectName);
        }
#else
        if (EffectLoaded == false)
        {
            var bundle = AssetBundle.LoadFromFile(StringBuild.GetStringFromArgs(streamingBundlePass, EFFEKSEER_BUNDLE));

            for (int i = 0; i < table.enm_skills.Count; i++)
                EffekseerSystem.LoadEffect(table.enm_skills[i].EffectName, bundle);
            for (int i = 0; i < table.ply_skills.Count; i++)
                EffekseerSystem.LoadEffect(table.ply_skills[i].EffectName, bundle);

            bundle.Unload(false);
        }
#endif
    }

    /// <summary>
    /// マップの表示オプジェクトのプレハブマスターデータを取得
    /// </summary>
    /// <param name="hierarchy"></param>
    /// <returns></returns>
    public static MapHierarchyMasterDatas LoadMapHierarchyMasterDatas(int hierarchy)
    {
        string bundleNameCommon = StringBuild.GetStringFromArgs(streamingBundlePass, STAGECOMMON_BUNDLE);
        var bundle = AssetBundle.LoadFromFile(bundleNameCommon);

        string bundlePath = StringBuild.GetStringFromArgs(streamingBundlePass, STAGE_BUNDLE, hierarchy.ToString());
        string assetName = StringBuild.GetStringFromArgs("H", hierarchy.ToString(), "Master");
        var data = LoadAsset<MapHierarchyMasterDatas>(bundlePath, assetName);

        bundle.Unload(false);
        return data;
    }

    /// <summary>
    /// アイコンのマスターデータを取得
    /// </summary>
    /// <returns></returns>
    public static IconDatas LoadIconDatas()
    {
        if (iconData == null)
        {
            var path = StringBuild.GetStringFromArgs(streamingBundlePass, DATA_BUNDLE);
            LoadAsset<IconDatas>(path, ICON_ASSET, data => iconData = data);
        }
        return iconData;
    }

    /// <summary>
    /// イベント処理に必要なプレハブマスターデータを取得
    /// </summary>
    /// <returns></returns>
    public static EventInstructionDatas LoadEventInstructionDatas()
    {
        if (eventInstructionData == null)
        {
            var path = StringBuild.GetStringFromArgs(streamingBundlePass, DATA_BUNDLE);
            LoadAsset<EventInstructionDatas>(path, EVENT_ASSET, data => eventInstructionData = data);
        }
        return eventInstructionData;
    }

    /// <summary>
    /// 効果音データをロード
    /// </summary>
    public static void LoadSEBundle()
    {
        var path = StringBuild.GetStringFromArgs(streamingBundlePass, SE_BUNDLE);
        LoadAsset<SETable>(path, SE_TABLE, data => SEManager.Instance.Init(data.SEDatas));
    }

    /// <summary>
    /// bgmをロード
    /// </summary>
    public static void LoadBGMBundle()
    {
        var path = StringBuild.GetStringFromArgs(streamingBundlePass, BGM_BUNDLE);
        LoadAsset<AudioTable>(path, AUDIO_TABLE, data => AudioManager.Instance.Init(data.AudioDatas));
    }

    /// <summary>
    /// バトルグループデータをロード
    /// </summary>
    public static void LoadBattleGroupBundle()
    {
        var path = StringBuild.GetStringFromArgs(streamingBundlePass, BATTLE_BUNDLE);
        LoadAsset<BattleGroupMaster>(path, BATTLE_TABLE, data => BattlePrefabLoader.Instance.Init(data.BattleGroups));
    }

    /// <summary>
    /// マップイベントのアイコンデータをロード
    /// </summary>
    /// <param name="callback"></param>
    public static void LoadMapEventIconBundle(Action<SpriteAtlas> callback)
    {
        var path = StringBuild.GetStringFromArgs(streamingBundlePass, MAP_EVENT_ICON_BUNDLE);
        LoadAsset<SpriteAtlas>(path, MAP_EVENT_SPRITEATLAS, atlas => callback(atlas));
    }
}
