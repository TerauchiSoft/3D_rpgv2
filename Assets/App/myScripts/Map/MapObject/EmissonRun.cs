﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissonRun : MonoBehaviour {
    private Material mt;
    private float max = 1.0f;
    private float r = 0;
    // Use this for initialization
	void Start () {
        mt = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        r += 0.1f;
        if (r > 6.28f)
            r = 0;
        float val = max * Mathf.Sin(r) / 4 + max / 4 + 0.25f;

        mt.SetColor("_EmissionColor", new Color(val, val, 0));
        mt.SetColor("_Color", new Color(val, val, val, val));
    }
}
