﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 画面に表示させるUIの表示非表示。
/// </summary>
public class FrontUIView : MonoBehaviour {
    private const string DISPLAY_ANIME = "MapArea_Appear";
    private const string HIDE_ANIME = "MapArea_Remove";

    [SerializeField]
    private Animator animator;
    private CanvasGroup canvasGroup;
    static FrontUIView instance;
    public static FrontUIView Instance
    {
        get { 
            if (instance == null)
            {
                instance = FindObjectOfType<FrontUIView>();
            }
            return instance;
        }
    }
    // Use this for initialization
    void Awake () {
        instance = this;
        canvasGroup = GetComponent<CanvasGroup>();

    }
	
    /// <summary>
    /// UI表示を反転させる。
    /// </summary>
    /// <returns><c>true</c>, if front UIV iew was reversed, <c>false</c> otherwise.</returns>
    public bool ReverseFrontUIView()
    {
        canvasGroup.alpha = (canvasGroup.alpha > 0) ? 0 : 1;
        return canvasGroup.alpha > 0;
    }

    /// <summary>
    /// Config設定時、ロード時、マップ画面などを表示するか
    /// </summary>
    public void SetFrontUIView(int isView)
    {
        canvasGroup.alpha = isView;
    }

    /// <summary>
    /// 表示アニメーションを再生する
    /// </summary>
    /// <param name="toVisible"></param>
    public void DisplayAnime(bool toVisible)
    {
        if (toVisible)
            animator.Play(DISPLAY_ANIME);
        else
            animator.Play(HIDE_ANIME);
    }

    /// <summary>
    /// セーブ時、キャッシュを取得。
    /// </summary>
    /// <returns>The front UIV iew.</returns>
    public int GetFrontUIView()
    {
        return (canvasGroup.alpha > 0) ? 1 : 0;
    }
}
