﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapVariable;

namespace Minimap
{
    /// <summary>
    /// ミニマップのマス
    /// </summary>
    public class MinimapMass : MonoBehaviour
    {
        private const int WALL_NUM = 4;

        private const int DOOR = 2;
        private const int LOCK_DOOR = 3;

        private const int HIDE_WALL = 4;
        private const int NOLOCK_WALL = 5;
        private const int ONE_PASS_WALL1 = 6;
        private const int ONE_PASS_WALL2 = 7;

        private const int ONE_PASS_DOOR1 = 8;
        private const int ONE_PASS_DOOR2 = 9;

        private const int DEFAULT_IMAGE_SPRITE = 0;

        private readonly Color FLOOR_COLOR = new Color(0.4f, 0.7f, 0.5f, 0.75f);
        private readonly Color FLOOR_COLOR2 = new Color(0.25f, 0.25f, 0.25f, 0.5f);

        [SerializeField]
        private GameObject viewChangeObj;
        [SerializeField]
        private SpriteRenderer mapFloor;
        [SerializeField]
        private SpriteRenderer iconImage;
        [SerializeField]
        private SpriteRenderer[] walls = new SpriteRenderer[WALL_NUM];
        [SerializeField]
        private SpriteRenderer[] doors = new SpriteRenderer[WALL_NUM];

        [SerializeField]
        private ParticleSystem particle;

        private int[] array = new int[WALL_NUM];

        /// <summary>
        /// マスのデータを設定する
        /// </summary>
        /// <param name="up"></param>
        /// <param name="right"></param>
        /// <param name="down"></param>
        /// <param name="left"></param>
        /// <param name="floor"></param>
        public void SetMassData(byte up, byte right, byte down, byte left, byte floor)
        {
            Sprite defaultSprite = MapEventIconLoader.Instance.GetSprite(DEFAULT_IMAGE_SPRITE);
            // アイコン
            iconImage.sprite = defaultSprite;
            iconImage.color = Color.clear;

            mapFloor.sprite = defaultSprite;
            // 床
            if (floor < 32)
            {
                mapFloor.color = FLOOR_COLOR;
            }
            else
            {
                mapFloor.color = FLOOR_COLOR2;
            }

            array[0] = up;
            array[1] = right;
            array[2] = down;
            array[3] = left;

            for (int direction = 0; direction < WALL_NUM; direction++)
            {
                walls[direction].sprite = defaultSprite;
                doors[direction].sprite = defaultSprite;
                // 壁
                if (array[direction] > 0 && array[direction] != HIDE_WALL && array[direction] != NOLOCK_WALL/* && !(array[direction] == ONE_PASS_WALL1 && (direction == 0 || direction == 3)) && !(array[direction] == ONE_PASS_WALL2 && (direction == 1 || direction == 2))*/)
                {
                    walls[direction].color = Color.yellow;
                }
                else
                {
                    walls[direction].color = Color.clear;
                }

                // 扉
                if (array[direction] == DOOR || array[direction] == LOCK_DOOR || (array[direction] == ONE_PASS_DOOR1 && (direction == 0 || direction == 3)) || (array[direction] == ONE_PASS_DOOR2 && (direction == 1 || direction == 2)))
                {
                    doors[direction].color = Color.blue;
                }
                else
                {
                    doors[direction].color = Color.clear;
                }
            }
        }

        /// <summary>
        /// イベントアイコンを設定する
        /// </summary>
        /// <param name="iconNo">アイコンのID 階段 回復の泉など</param>
        public void SetEventIcon(int iconNo)
        {
            if (iconNo == 0)
            {
                iconImage.color = Color.clear;
                return;
            }

            // TODO:回転床も宝箱もアイコングラフィックは全てIconLoaderから読むように設定するようにする
            Sprite sprite = MapEventIconLoader.Instance.GetSprite(iconNo);
            if (sprite != null)
            {
                iconImage.sprite = sprite;
                iconImage.color = Color.white;
            }
            else
            {
                iconImage.color = Color.clear;
            }
        }

        /// <summary>
        /// マス表示をするか
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="isEffect"></param>
        public void ToActive(bool isActive, bool isEffect = false)
        {
            bool prev = viewChangeObj.activeSelf;
            viewChangeObj.SetActive(isActive);
            if (isActive && isEffect && !prev)
            {
                particle.Play();
            }
        }
    }
}
