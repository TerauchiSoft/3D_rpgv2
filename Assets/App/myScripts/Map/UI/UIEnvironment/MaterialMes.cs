﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// 錬成のアイテム所持数、アイテム説明文を表示する。
/// </summary>
public class MaterialMes : MonoBehaviour
{
    public TextMeshProUGUI _text_itemName;
    public TextMeshProUGUI _text_itemEx;
    public TextMeshProUGUI _text_numArestaHave;
    public TextMeshProUGUI _text_numErinaHave;
    public TextMeshProUGUI _text_numPetilHave;
    public Image _img_MesWin;
    public Vector2 _offsetPos;

    /// <summary>
    /// MaterialTrayから。
    /// </summary>
    /// <param name="flame"></param>
    public void ViewItemMat(RecipeFlame flame) {
        var item = flame.item;
        if (item.ItemID == 0) {
            HideItemMes();
            return;
        }

        var rect = transform as RectTransform;
        var flameRect = flame.transform as RectTransform;
        rect.anchoredPosition = flameRect.anchoredPosition + _offsetPos;
        _img_MesWin.color = Color.white;
        _text_itemName.text = item.ItemName;
        _text_itemEx.text = item.ExplanatoryText;
        
        var num = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.アレスタ);
        _text_numArestaHave.text = (num <= 0) ? 0.ToString() : num.ToString();

        num = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.エリナ);
        _text_numErinaHave.text = (num <= 0) ? 0.ToString() : num.ToString();

        num = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.ペティル);
        _text_numPetilHave.text = (num <= 0) ? 0.ToString() : num.ToString();
        
    }

    /// <summary>
    /// MaterialTrayから。
    /// </summary>
    /// <param name="flame"></param>
    public void HideItemMes() {
        _img_MesWin.color = Color.clear;
    }
}
