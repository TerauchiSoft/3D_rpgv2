﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 歩行音などマップの効果音の再生。
/// </summary>
public class Map_Sound : Singleton<Map_Sound>, IAudioSaveLoad
{
    AudioSource audioSource;
    public AudioClip battleStart;
    public AudioClip[] sounds;
    /*
     * 0 ~ 4: 連続した足音
     * 5: 壁にぶつかった音
     * 6: 決定キー, メニュー開き
     * 7: キャンセルキー
     * 8: カーソル移動
     * 9: 選択不能
     */
    readonly int Conc = 5;

    // Use this for initialization
    void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        instance = this;
        instance.audioSource = instance.GetComponent<AudioSource>();
        DontDestroyOnLoad(this);
    }

    int WalkFoot = 0;
    public void WalkSoundPlay()
    {
        SoundPlay(Instance.WalkFoot);
        Instance.WalkFoot = (Instance.WalkFoot < 4) ? Instance.WalkFoot + 1 : 0;
    }

    public void DecisionSoundPlay()
    {
        SoundPlay(6);
    }

    public void CancelSoundPlay()
    {
        SoundPlay(7);
    }

    public void CursorMoveSoundPlay()
    {
        SoundPlay(8);
    }

    public void DontSelectPlay()
    {
        SoundPlay(9);
    }

    public void EquipPlay()
    {
        SoundPlay(10);
    }

    public void WallHitPlay() {
        SoundPlay(Instance.Conc);
    }

    public void PlayStartBattle() {
        SoundPlay(Instance.battleStart);
    }

    public void SoundPlay(int sounds_select)
    {
        Instance.audioSource.clip = Instance.sounds[sounds_select];
        Instance.audioSource.Play();
    }

    public void SoundPlay(AudioClip audio)
    {
        Instance.audioSource.clip = audio;
        Instance.audioSource.Play();
    }

    // --------------- セーブロード -------------------

    /// <summary>
    /// コンフィグ変更時、ロード時、ボリュームを変更。
    /// </summary>
    /// <param name="volume">Volume.</param>
    public void SetVolume(int volume)
    {
        audioSource.volume = volume * 0.01f;
    }

    /// <summary>
    /// セーブ時に現在のボリューム値を取得。
    /// </summary>
    /// <returns>The volume cache.</returns>
    public int GetVolumeCache()
    {
        return (int)(audioSource.volume * 100);
    }
}
