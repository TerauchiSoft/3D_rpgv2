﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BGM、SEの設定項目のセーブロード。
/// </summary>
public interface IAudioSaveLoad {
    /// <summary>
    /// コンフィグ変更時、ロード時、ボリュームを変更。
    /// </summary>
    /// <param name="volume">Volume.</param>
    void SetVolume(int volume);

    /// <summary>
    /// セーブ時に現在のボリューム値を取得。
    /// </summary>
    /// <returns>The volume cache.</returns>
    int GetVolumeCache();
}
