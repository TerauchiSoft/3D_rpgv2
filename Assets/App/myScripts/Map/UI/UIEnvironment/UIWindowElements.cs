﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using DG.Tweening;

/// <summary>
/// UIの選択ウィンドウの機能を提供する。
/// </summary>
[System.Serializable]
public class UIWindowElements : MapComponent
{
    /// <summary>
    /// 一応個別に見ることは出来る。
    /// </summary>
    public bool isChoicing;
    /// <summary>
    /// キャンセルボタンが有効か。
    /// </summary>
    public bool isCanselEnabled = true;
    /// <summary>
    /// 全体を選択するに固定しているか。(キャラの全員選択。)
    /// </summary>
    public bool isAllSelect = false;
    /// <summary>
    /// 最後に開かれたUIWindowElements
    /// </summary>
    public static UIWindowElements uiWindowElements;
    /// <summary>
    /// 最大のインデックス数。
    /// </summary>
    public int MaxIndex = 0;
    /// <summary>
    /// 最小のインデックス数。タイトルなどをインデックスにしたくない時などに使用。
    /// </summary>
    public int MinIndex = 0;
    /// <summary>
    /// 今選択しているインデックス。
    /// </summary>
    public int SelectedIndex = 0;
    /// <summary>
    /// 決定されたインデックス。終了時に値を代入する。
    /// </summary>
    public int DecisionIndex = 0;
    public enum Mode { YesNo, CharaSelect, NormalSelect, ShopItemSelect }
    /// <summary>
    /// どの用途にウィンドウを使用するか決めて制御する。
    /// </summary>
    public Mode mode;
    /// <summary>
    /// カーソル位置に使用。
    /// </summary>
    public RectTransform rect_Cursor;
    /// <summary>
    /// カーソル。色を変化させる。
    ///TODO:本来アニメーションでの対応をするべきである。
    /// </summary>
    private Image img_Cursor;
    /// <summary>
    /// テキスト。
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI[] text;
    /// <summary>
    /// テキストの内容。text[n].textに初期化時に設定する。
    /// </summary>
    [SerializeField]
    private string[] strs;
    /// <summary>
    /// キャラセレクトのとき、居ないメンバーを除外する。
    /// </summary>
    [SerializeField]
    private List<GameObject> charaElm = new List<GameObject>();

    private RectTransform Rect_content;

    // エフェクト表示をするrawImage
    [SerializeField]
    private RawImage[] effectRawImages;

    private System.Action callBack;

    /// <summary>
    /// 引数からテキストをセット。
    /// </summary>
    /// <param name="vs"></param>
    /// <param name="callback"></param>
    public void InitTexts(string[] vs = null, System.Action callback = null)
    {
        callBack = callback;

        if (vs != null)
            strs = vs;

        if (text == null || text.Length == 0 ||
            strs == null || strs.Length == 0 ||
            strs.Length != text.Length)
            return;

        for(int i = 0; i < text.Length; i++)
        {
            text[i].text = strs[i];
        }
    }

    /// <summary>
    /// いないキャラクターを非表示にする。
    /// </summary>
    private void DisableChara()
    {
        for (int i = 0; i < PlayerDatas.Instance.p_chara.Length; i++) {
            bool isjoined = PlayerDatas.Instance.p_chara[i].isJoined;
            charaElm[i].SetActive(isjoined);
            charaElm[i + 3].SetActive(isjoined);
        }
    }

    public override void Awake()
    {
        AddContainerIndex();
        initAncorPos = rect_Cursor.anchoredPosition;

    }

    private void OnEnable()
    {
        img_Cursor = rect_Cursor.GetComponent<Image>();
        InitTexts();
        if (mode == Mode.ShopItemSelect)
            Rect_content = GetComponentInChildren<ContentSizeFitter>().GetComponent<RectTransform>();
    }

    public void ButtonOnClick(int n)
    {
        if (n == -1)
            n = SelectedIndex;

        DecisionIndex = n;
        isChoicing = false;
        Map_Sound.Instance.DecisionSoundPlay();
        HideObject(true);
        return;
    }

    private Vector2 initAncorPos;
    public Image color_selectCursor;
    private float time = Mathf.PI / 2;
    private float A = 0.22f;
    private Color col_Cursor = new Color(0.9882f, 1.0f, 0.3608f, 0.25f);
    private void changeColor()
    {
        time += (Mathf.PI / 8) / Mathf.PI;
        if (time >= 2 * Mathf.PI) time = 0;
        float alp = A * Mathf.Sin(time);
        col_Cursor.a = alp + 0.24f;
        img_Cursor.color = col_Cursor;
    }
    
    private IEnumerator DelaySetActive(bool toActive) {
        yield return 0;
        gameObject.SetActive(toActive);
    }

    /// <summary>
    /// 非表示
    /// </summary>
    public void HideObject(bool toHide)
    {
        if (toHide == false)
            return;

        isChoicing = false;
        if (callBack != null)
            callBack();
        StartCoroutine(DelaySetActive(false));
    }
    /// <summary>
    /// 再表示
    /// </summary>
    public void ShowObject() {
        isChoicing = true;
        gameObject.SetActive(true);
    }

    /// <summary>
    /// 再選択
    /// </summary>
    public void ToActiveChoicing() {
        ShowObject();
    }

    private void SetUpIdxRangeForShop()
    {
        MinIndex = 0;
        MaxIndex = items.Length - 1;
        isChoicing = true;
    }

    private bool ButtonProcessing(bool toHide = true)
    {
        if (PlayerController.Instance.isFire1 == true)
        {
            DecisionIndex = SelectedIndex;
            isChoicing = false;
            Map_Sound.Instance.DecisionSoundPlay();
            HideObject(toHide);
            return true;
        }
        else if (isCanselEnabled && PlayerController.Instance.isFire2 == true)
        {
            DecisionIndex = -1;
            isChoicing = false;
            Map_Sound.Instance.CancelSoundPlay();
            HideObject(toHide);
            return true;
        }
        return false;
    }

    private void ArrowProcessing(System.Action act)
    {
        // up
        if (PlayerController.Instance.Virtical > 0.5f || PlayerController.Instance.Horizontal < -0.5f)
        {
            if (SelectedIndex == MinIndex)
                SelectedIndex = MaxIndex;
            else
                SelectedIndex--;
            Map_Sound.Instance.CursorMoveSoundPlay();

            if (mode == Mode.CharaSelect)
                GotoExistCharaIdx(-1);

            if (act != null)
                act.Invoke();
        }
        // down
        else if (PlayerController.Instance.Virtical < -0.5f || PlayerController.Instance.Horizontal > 0.5f)
        {
            if (SelectedIndex == MaxIndex)
                SelectedIndex = MinIndex;
            else
                SelectedIndex++;
            Map_Sound.Instance.CursorMoveSoundPlay();

            if (mode == Mode.CharaSelect)
                GotoExistCharaIdx(1);

            if (act != null)
                act.Invoke();
        }
    }

    private void GotoExistCharaIdx(int sign)
    {
        int timeout = 100;
        while (timeout > 0)
        {

            if (SelectedIndex > PlayerDatas.Instance.p_chara.Length - 1)
                SelectedIndex = 0;

            if (SelectedIndex < 0)
                SelectedIndex = PlayerDatas.Instance.p_chara.Length - 1;

            if (PlayerDatas.Instance.p_chara[SelectedIndex].isJoined == false)
            {
                SelectedIndex += sign;
            }

            if (SelectedIndex > PlayerDatas.Instance.p_chara.Length - 1)
                SelectedIndex = 0;

            if (SelectedIndex < 0)
                SelectedIndex = PlayerDatas.Instance.p_chara.Length - 1;

            if (PlayerDatas.Instance.p_chara[SelectedIndex].isJoined == true)
            {
                return;
            }
            timeout--;
        }
    }

    private readonly int MAXCHARAINDEX = 3;
    /// <summary>
    /// 選択時ウィンドウをHideするかどうか。
    /// </summary>
    /// <param name="b"></param>
    public void SetIsHideWhenDecided(bool b) { isHideWhenDecided = b; }
    private bool isHideWhenDecided = true;
    private bool isFirst = true;
    private void CharaSelect(Mode md)
    {
        if (isFirst)
        {
            DisableChara();
            MaxIndex = MAXCHARAINDEX;
        }

        bool pressed = ButtonProcessing(isHideWhenDecided);
        if (pressed)
            return;

        if (!isAllSelect) // 全員選択でないか
            ArrowProcessing(null);
        
        float x = initAncorPos.x - rect_Cursor.sizeDelta.x * (int)SelectedIndex;
        rect_Cursor.anchoredPosition = new Vector2(x, rect_Cursor.anchoredPosition.y);
    }

    private void YesNoSelect()
    {
        bool pressed = ButtonProcessing();
        if (pressed)
            return;

        ArrowProcessing(null);

        float y = initAncorPos.y + rect_Cursor.sizeDelta.y * (int)SelectedIndex;
        rect_Cursor.anchoredPosition = new Vector2(rect_Cursor.anchoredPosition.x, y);
    }

    public ItemElement nowItemElement; 
    private void ShopItemSelect()
    {
        bool pressed = ButtonProcessing(false);
        if (pressed)
            return;

        ArrowProcessing(() => nowItemElement = itemElements[SelectedIndex]);
        //TODO:ショップに対応させましょう。
        float y = initAncorPos.y + rect_Cursor.sizeDelta.y * (int)SelectedIndex;
        rect_Cursor.anchoredPosition = new Vector2(rect_Cursor.anchoredPosition.x, y);
    }

    // 選択肢の処理
    private void Update()
    {
        changeColor();
        if (isChoicing == false)
            return;

        // キャラクターの選択
        if (mode == Mode.CharaSelect)
        {
            CharaSelect(mode);
        }
        // 選択肢の表示
        else if (mode == Mode.NormalSelect)
        {
            YesNoSelect();
        }
        // YesかNoか
        else if (mode == Mode.YesNo)
        {
            YesNoSelect();
        }
        // ショップでのアイテムの選択
        else if (mode == Mode.ShopItemSelect)
        {
            ShopItemSelect();
        }
    }

    // -------------------------------------------

    Item[] items;
    public ItemElement[] itemElements;
    [SerializeField] GameObject pref_ItemForShop;
    /// <summary>
    /// ショップ専用
    /// </summary>
    public void DeleteItemList()
    {
        var list = Rect_content.GetComponentsInChildren<ItemElement>();
        foreach (var a in list)   // list[0] = content(ディレクトリなオブジェクト)
            Destroy(a.gameObject);
        items = new Item[0];
    }

    /// <summary>
    /// ショップ専用
    /// </summary>
    public void AddItemElements(Item[] its)
    {
        items = its;
        itemElements = new ItemElement[items.Length];
        for (int i = 0; i < items.Length; i++)
        {
            var ob = Instantiate(pref_ItemForShop, Rect_content);
            var so = ob.GetComponent<ItemElement>();
            so.icon.sprite = items[i].IconSprite;
            so.text.text = items[i].ItemName;
            so.item = items[i];
            itemElements[i] = so;
        }
        SetUpIdxRangeForShop();
        SelectedIndex = MinIndex;
        nowItemElement = itemElements[MinIndex];
    }


    /// <summary>
    /// uiにpref.GetComponent<UIWindowElements>()の参照をセット。
    /// すでに参照がある場合はui.ShowObjectのみを実行する。
    /// </summary>
    /// <param name="pref"></param>
    /// <param name="ui"></param>
    public static UIWindowElements LoadUIWindow(GameObject pref, UIWindowElements ui, Transform trs)
    {
        if (ui != null)
        {
            Destroy(ui.gameObject);
        }
        var ob = Instantiate(pref, trs);
        ui = ob.GetComponent<UIWindowElements>();
        return ui;
    }

    /// <summary>
    /// インデックス値を固定にする。
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public UIWindowElements ToIndexLock(int i)
    {
        MaxIndex = i;
        MinIndex = i;
        return this;
    }

    /// <summary>
    /// インデックス値を全体に固定にする。
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public UIWindowElements SetIsAllSelectAtCharaSlct(bool isall)
    {
        isAllSelect = isall;
        if (isAllSelect)
        {
            MaxIndex = 0;
            MinIndex = 0;

        }
        return this;
    }

    /// <summary>
    /// エフェクト表示用のrawImageを表示する
    /// </summary>
    /// <param name="bits">1b:アレスタ 10b:エリナ 100b:ペティル</param>
    /// <param name="rendererTexture">カメラ撮影物を移したテクスチャ</param>
    public void ViewEffectRawImage(int bits, RenderTexture renderTexture)
    {
        for (int i = 0; i < effectRawImages.Length; i++)
        {
            bool toActive = ((bits & (1 << i)) > 0);
            effectRawImages[i].gameObject.SetActive(toActive);
            if (toActive)
                effectRawImages[i].texture = renderTexture;
        }
    }
}
