﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class TextAreaStatic : MonoBehaviour
{
    public static TextAreaStatic _textAreaStatic;

    // --------------- 表示関係 ------------

    private GameObject ob_TextWin;
    private TextWin textWin;

    private void Awake()
    {
        _textAreaStatic = this;
        ob_TextWin = this.gameObject;
        textWin = this.GetComponent<TextWin>();
        ob_TextWin.SetActive(false);
    }
    // ----------- メニューを開く ------------------

    // -------------- イベントを開く ---------------------

    public static void StartEvent() {

    }

    // -------------- テキストエリア関係 -----------------
    
    public static void TextAreaOpen() {
        _textAreaStatic.ob_TextWin.SetActive(true);
    }

    public static void TextAreaClose() {
        _textAreaStatic.ob_TextWin.SetActive(false);
    }

    /// <summary>
    /// メッセージ再生ウェイトなし
    /// </summary>
    /// <param name="str"></param>
    public static void StartStatusMes(string str) {
        _textAreaStatic.textWin.StartTextPlayNoSpan(str);
    }

}