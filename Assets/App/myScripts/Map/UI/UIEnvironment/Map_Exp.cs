﻿using UnityEngine;
using UnityEngine.UI;

public class Map_Exp : MonoBehaviour
{
    /// <summary>
    /// レベルアップ演出中、Update停止
    /// </summary>
    /// <value><c>true</c> if is now level up; otherwise, <c>false</c>.</value>
    public static bool IsNowLevelUp { get; set; }

    public int PlayerID = 0;        // 0 ~ 2 -1でダミー
    public bool isValueChange;
    private Slider slider;
    private PlayerCharaData pd;
    int Exp { get { return pd.EXP - pd.EXP_BTM; } }
    int max_Exp { get { return pd.EXP_UPR - pd.EXP_BTM; } }

    // Use this for initialization
    void Start () {
        if (PlayerID == -1)
            pd = new PlayerCharaData();

        isValueChange = false;
        slider = GetComponent<Slider>();
        if (PlayerID == -1)
            pd = new PlayerCharaData();
        else
            pd = PlayerDatas.Instance.p_chara[PlayerID];
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerID == -1)
            return;

        if (!IsNowLevelUp)
            ExpValueChange();
    }


    public void ExpValueChange()
    {
        if (PlayerID == -1)
            return;

        if (slider.maxValue != max_Exp)
            slider.maxValue = max_Exp;


        if (slider.value != Exp)
        {
            isValueChange = true;
            if (Exp > slider.value)
            {
                var diff = Exp - slider.value;
                if (diff > 10000)
                {
                    slider.value += 5000;
                }
                else if (diff > 1000)
                {
                    slider.value += 500;
                }
                else if (diff > 100)
                {
                    slider.value += 50;
                }
                else if (diff > 30)
                {
                    slider.value += 15;
                }
                else
                {
                    slider.value++;
                }
            }
            if (Exp < slider.value)
            {
                slider.value--;
            }
        }
        else
        {
            isValueChange = false;
        }

        if (slider.value == slider.maxValue)
            LevelUp();
    }

    // ---------- レベルアップ演出 ------------

    System.Action _levelUpMesFromBattle;
    public void AddLevelUpAct(System.Action act)
    {
        _levelUpMesFromBattle = act;
    }

    public GameObject _pref_LVUP;
    void LevelUp()
    {
        IsNowLevelUp = true;
        _levelUpMesFromBattle.Invoke(); // IsNowLevelUp = false
        Instantiate(_pref_LVUP, transform);
        slider.maxValue = max_Exp;
        slider.minValue = 0;
        slider.value = 0;
    }
}
