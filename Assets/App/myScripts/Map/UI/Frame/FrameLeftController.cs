﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

/// <summary>
/// 左のフレームコントローラー
/// </summary>
public class FrameLeftController : Singleton<FrameLeftController>
{
    private const float MOVING_TIME = 0.5f;
    private const string FLOOR = "F";
    private const string BASEMENT = "B";

    private bool isHided;
    private bool isShowed;

    [SerializeField]
    private GameObject frame;

    [SerializeField]
    private Transform HidePositionTransform;

    [SerializeField]
    private Transform ShowPositionTransform;

    private Tweener tweener;

    [SerializeField]
    private TextMeshProUGUI moneyText;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// フレームを出現させるアニメーション
    /// </summary
    public void ShowFrame()
    {
        if (isShowed)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        frame.SetActive(true);
        frame.transform.position = HidePositionTransform.position;
        tweener = frame.transform.DOMove(ShowPositionTransform.position, MOVING_TIME);
        isShowed = true;
        isHided = false;
    }

    /// <summary>
    /// フレームを隠すアニメーション
    /// </summary>
    public void HideFrame()
    {
        if (isHided)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        tweener = frame.transform.DOMove(HidePositionTransform.position, MOVING_TIME)
            .OnComplete(() =>
            {
                frame.SetActive(false);
            });
        isHided = true;
        isShowed = false;
    }

    /// <summary>
    /// お金テキストを設定
    /// </summary>
    /// <param name="money">お金の数</param>
    public void SetMoney(int money)
    {
        moneyText.text = money.ToString() + TableValues.MONEY_NAME;
    }
}
