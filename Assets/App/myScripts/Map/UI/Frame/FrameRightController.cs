﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

/// <summary>
/// 右のフレームコントローラー
/// </summary>
public class FrameRightController : Singleton<FrameRightController>
{
    private const string FLOOR = "F";
    private const string BASEMENT = "B";
    private const float MOVING_TIME = 0.5f;

    private bool isShowed;
    private bool isHided;

    [SerializeField]
    private GameObject frame;

    [SerializeField]
    private Transform HidePositionTransform;

    [SerializeField]
    private Transform ShowPositionTransform;

    private Tweener tweener;

    [SerializeField]
    private TextMeshProUGUI dungeonName;

    [SerializeField]
    private TextMeshProUGUI floorText;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// フレームを出現させるアニメーション
    /// </summary>
    public void ShowFrame()
    {
        if (isShowed)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        frame.SetActive(true);
        frame.transform.position = HidePositionTransform.position;
        tweener = frame.transform.DOMove(ShowPositionTransform.position, MOVING_TIME);
        isShowed = true;
        isHided = false;
    }

    /// <summary>
    /// フレームを隠すアニメーション
    /// </summary>
    public void HideFrame()
    {
        if (isHided)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        tweener = frame.transform.DOMove(HidePositionTransform.position, MOVING_TIME)
            .OnComplete(() =>
            {
                frame.SetActive(false);
            });
        isHided = true;
        isShowed = false;
    }

    /// <summary>
    /// ダンジョンの名前を設定
    /// </summary>
    /// <param name="dungeon">ダンジョン名文字列</param>
    public void SetDungeonName(string dungeon)
    {
        dungeonName.text = dungeon;
    }

    /// <summary>
    /// ダンジョンの階層を設定
    /// </summary>
    /// <param name="floor"></param>
    public void SetDungeonFloor(int floor, string floorName)
    {
        floorText.text = floorName + floor.ToString();
    }
}
