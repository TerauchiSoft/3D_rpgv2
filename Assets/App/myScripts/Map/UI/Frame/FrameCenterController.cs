﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// センターフレームのコントローラー
/// </summary>
public class FrameCenterController : Singleton<FrameCenterController>
{
    private bool isShowed;
    private bool isHided;

    [SerializeField]
    private GameObject frame;

    [SerializeField]
    private GameObject arrow;

    [SerializeField]
    private Transform HidePositionTransform;

    [SerializeField]
    private Transform ShowPositionTransform;

    private float MOVING_TIME = 0.5f;
    private Tweener tweener;

    [SerializeField]
    private Animator arrowAnimator;

    private const string ARROW_ANIME = "Arrow_";

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// フレームを出現させるアニメーション
    /// </summary>
    public void ShowFrame()
    {
        if (isShowed)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        frame.SetActive(true);
        frame.transform.position = HidePositionTransform.position;
        tweener = frame.transform.DOMove(ShowPositionTransform.position, MOVING_TIME);
        isShowed = true;
        isHided = false;
    }

    /// <summary>
    /// フレームを隠すアニメーション
    /// </summary>
    public void HideFrame()
    {
        if (isHided)
            return;

        if (tweener != null)
        {
            tweener.Kill();
            tweener = null;
        }
        tweener = frame.transform.DOMove(HidePositionTransform.position, MOVING_TIME)
            .OnComplete(() =>
            {
                frame.SetActive(false);
            });
        isHided = true;
        isShowed = false;
    }

    /// <summary>
    /// 方鉱石の方向を指定してアニメーションさせる
    /// </summary>
    /// <param name="direction"></param>
    private void ArrowAnimation(int direction)
    {
        arrowAnimator.Play(ARROW_ANIME + direction.ToString());
    }
}
