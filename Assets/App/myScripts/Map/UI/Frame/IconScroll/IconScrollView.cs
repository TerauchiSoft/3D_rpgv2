﻿using System;
using System.Linq;
using EasingCore;
using UnityEngine;
using FancyScrollView;
using System.Collections.Generic;

namespace IconScroll
{
    /// <summary>
    /// アイコンをスクロールさせる機構
    /// </summary>
    public class IconScrollView : FancyScrollView<IconScrollData, IconContext>
    {
        private int nowSelectIndex = 0;
        private int iconNum;
        public int SelectIndex { get { return nowSelectIndex % iconNum; } }

        private const float DEFAULT_INTERVAL = 0.16f;
        private const float ONE_ICON_INTERVAL = 0.4f;
        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;
        private Item[] items;
        private IconScrollElement[] iconScrollElements;
        private List<IconScrollData> iconScrollDatas = new List<IconScrollData>();

        protected override GameObject CellPrefab => cellPrefab;

        void Awake()
        {
            Context.OnCellClicked = SelectCell;
        }

        /// <summary>
        /// アイコンデータを初期化
        /// </summary>
        /// <param name="items"></param>
        public void Init(Item[] items)
        {
            iconNum = items.Length;

            if (items.Length == 0)
            {
                return;
            }

            if (items.Length == 1)
            {
                cellInterval = ONE_ICON_INTERVAL;
            }
            else
            {
                cellInterval = DEFAULT_INTERVAL;
            }
            gameObject.SetActive(true);

            Refresh();
            scroller.OnValueChanged(UpdatePosition);
            scroller.OnSelectionChanged(UpdateSelection);

            this.items = items;
            iconScrollDatas.Clear();
            for (int i = 0; i < items.Length; i++)
            {
                iconScrollDatas.Add(new IconScrollData(items[i]));
            }
            iconScrollElements = GetComponentsInChildren<IconScrollElement>();
            UpdateData(iconScrollDatas);
        }

        /// <summary>
        /// スクローラーでのインデックス変更時に位置を更新する
        /// </summary>
        /// <param name="index"></param>
        void UpdateSelection(int index)
        {
            if (Context.SelectedIndex == index)
            {
                return;
            }

            Context.SelectedIndex = index;
            Refresh();
        }

        /// <summary>
        /// アイテムデータを流し込む
        /// </summary>
        /// <param name="items"></param>
        public void UpdateData(IList<IconScrollData> items)
        {
            UpdateContents(items);
            scroller.SetTotalCount(items.Count);
        }

        /// <summary>
        /// アイコンの選択
        /// </summary>
        /// <param name="index"></param>
        public void SelectCell(int index)
        {
            if (index < 0 || index >= ItemsSource.Count || index == Context.SelectedIndex)
            {
                return;
            }

            nowSelectIndex = index;
            UpdateSelection(index);
            scroller.ScrollTo(index, 0.35f, Ease.OutCubic);
        }
    }
}
