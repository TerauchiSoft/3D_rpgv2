﻿using UnityEngine;
using UnityEngine.UI;
using FancyScrollView;
using System;

namespace IconScroll
{
    /// <summary>
    /// アイコンスクロール要素
    /// </summary>
    public class IconScrollElement : FancyScrollViewCell<IconScrollData, IconContext>
    {
        private ItemFlame itemFlame;
        public ItemFlame  ItemFlame {
            get
            {
                if (itemFlame == null)
                    itemFlame = GetComponentInChildren<ItemFlame>();
                return itemFlame;
            }
        }
        [SerializeField] Animator animator = default;
        private const float CENTER_POSITION = 0.5f;

        static class AnimatorHash
        {
            public static readonly int Scroll = Animator.StringToHash("scroll");
        }

        /// <summary>
        /// ここでアイテムの画像を更新する
        /// </summary>
        /// <param name="itemData"></param>
        public override void UpdateContent(IconScrollData itemData)
        {
            this.ItemFlame.item = itemData.item;
            this.ItemFlame.SetData(this.itemFlame.item);
        }

        public override void UpdatePosition(float position)
        {
            CurrentPosition = position;
            animator.Play(AnimatorHash.Scroll, -1, position);
            animator.speed = 0;
        }

        // GameObject が非アクティブになると Animator がリセットされてしまうため
        // 現在位置を保持しておいて OnEnable のタイミングで現在位置を再設定します
        public float CurrentPosition { get; private set; } = 0;

        void OnEnable() => UpdatePosition(CurrentPosition);
    }
}
