﻿using System;
using UnityEngine;

namespace IconScroll
{
    /// <summary>
    /// アイコンスクロールのためのデータ
    /// </summary>
    [Serializable]
    public class IconScrollData
    {
        public int Index;
        public Item item;
        public Sprite FlameSprite;    // アイテムの フレーム画像

        public Sprite _alphaIcon;
        protected Color _effectColor = new Color(1, 1, 1, 0.8f);
        protected Color _effectColorNull = new Color(1, 1, 1, 0);

        public IconScrollData(Item item)
        {
            this.item = item;
        }
    }
}