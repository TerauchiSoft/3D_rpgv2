﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBarTray : UITray {
    private PlayerController plycnt;

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    private IEnumerator InitAnime() {
        yield return 0;
        _objectFlame[_nowFlameCursor].PlayAnimeOnCursor();
    }

    public void InitCursor() {
        StartCoroutine(InitAnime());
    }

    public override void OnOpenTray() {
        plycnt = PlayerController.Instance;
    }

    private void Update() {
        if (!_isRunning)
            return;
        
        CursorMove(plycnt.joy_Axis_buf);
        GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}
