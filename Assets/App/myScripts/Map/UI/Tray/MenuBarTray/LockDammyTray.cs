﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UnLock用のダミーのトレイ
/// </summary>
public class LockDammyTray : UITray {
    public override void CursorAnime(int nowidx) { }
    public override void LeaveAnime(int befidx) { }

    public override void OnOpenTray() {
        CloseTray();
    }

    protected override void Start() { }
}
