﻿using System.Linq;
using UnityEngine;

namespace Recipe
{
    /// <summary>
    /// 錬金できるアイテムの種類
    /// </summary>
    public enum RecipeMode
    {
        NONE = -1,
        HP = 0,
        MP,
        HPMP,
        BATTLE,
        WEPON,
        NUM,
    }

    /// <summary>
    /// 錬金アイテムリストの取得クラス
    /// </summary>
    public class RecipeItemGetter
    {
        private Item[] items;
        private Item[][] recipeItems = new Item[(int)RecipeMode.NUM][];
        public Item[][] RecipeItems { get { return recipeItems; } }

        /// <summary>
        /// リスト作成
        /// </summary>
        public RecipeItemGetter()
        {
            items = ItemTable.Instance.Items.ToArray();
            for (RecipeMode mode = RecipeMode.HP; mode <= RecipeMode.WEPON; mode++)
            {
                recipeItems[(int)mode] = GetRecipeitems(mode); 
            }
        }

        /// <summary>
        /// 錬成できるアイテムを取得
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        private Item[] GetRecipeitems(RecipeMode mode)
        {
            if (mode == RecipeMode.HP)
                return items.Where(item => item.effects[0] == TableValues.Effect.HPRecover && item.MaterialItemIDs.Length > 1).ToArray();
            if (mode == RecipeMode.MP)
                return items.Where(item => item.effects[0] == TableValues.Effect.MPRecover && item.MaterialItemIDs.Length > 1).ToArray();
            if (mode == RecipeMode.HPMP)
                return items.Where(item => item.effects[0] == TableValues.Effect.HPMPRecover && item.MaterialItemIDs.Length > 1).ToArray();
            if (mode == RecipeMode.BATTLE)
                return items.Where(item => item.effects[0] >= TableValues.Effect.Attack && item.effects[0] <= TableValues.Effect.CRIDown && item.MaterialItemIDs.Length > 1).ToArray();
            if (mode == RecipeMode.WEPON)
                return items.Where(item => item.kind == TableValues.Kind.Wepon && item.MaterialItemIDs.Length > 1).ToArray();

            Debug.LogError("錬金モードが範囲外です");
            return null;
        }

    }
}