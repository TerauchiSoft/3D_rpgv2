﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRecipeTray : UITray {
    public GameObject _ob_recipeItemSelect;
    public RecipeCompletedTray _recipeCompletedTray;
    public AudioSource _audioSource;

    public override void LeaveAnime(int befidx) {

    }

    public override void CursorAnime(int nowidx) {

    }

    public override void OnOpenTray() {
        _anim = GetComponent<Animator>();
        _anim.Play("PRTPlayRecipe");
        _audioSource.playOnAwake = false;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    public void ToNextOnAnimationEnd() {
        _anim.Play("Stop");
        _audioSource.Stop();
        _ob_recipeItemSelect.SetActive(false);
        _recipeCompletedTray.OpenNewTray(this, null);
    }
}
