﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using Recipe;

public class RecipeCompletedTray : UITray
{
    public GameObject _ob_recipeItemSelect;

    public TextMeshProUGUI _text_ItemName;
    public ItemFlame _itemFlame;
    public GameObject _ob_kohkasen;

    private PlayerController _plycnt;

    public override void LeaveAnime(int befidx) {

    }

    public override void CursorAnime(int nowidx) {

    }
    
    private void ItemSelectToView() {
        _ob_recipeItemSelect.SetActive(true);
    }

    public override void OnOpenTray() {
        _plycnt = PlayerController.Instance;
        _plycnt.ResetInput(120);
        _ob_kohkasen.SetActive(true);

        _itemFlame.SetData(RecipeValue.ToRecipeItem);
        _text_ItemName.text = _itemFlame.item.ItemName;

        DecMaterialItem();
        AddRenseiHandoverItem();
    }

    private void DecMaterialItem() {
        var materials = RecipeValue.NowSelectItem.MaterialItemIDs;
        foreach (var m in materials) {
            PlayerDatas.Instance.DecMaterialItem(m, 1);
        }
    }

    private void AddRenseiHandoverItem() {
        PlayerDatas.Instance.HandoverRenseiItem(RecipeValue.ToRecipeItem, RecipeValue.ToRecipeItem.ItemNum, (int)RecipeValue.HandoverChara);
    }

    public override void GotoNextTray(bool isgoto, Action act = null) {
        if (!isgoto)
            return;

        ItemSelectToView();
        GotoBackTray(isgoto);
        _current.GotoBackTray(isgoto);
        _current.GotoBackTray(isgoto);
    }

    // Update is called once per frame
    void Update() {
        if (!_isRunning)
            return;

        this.GotoNextTray(_plycnt.isFire1);
    }
}
