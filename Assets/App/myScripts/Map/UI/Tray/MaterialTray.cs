﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class MaterialTray : UITray {
    public RecipeFlame[] _iconFlames;
    public GenericFlame _readyFlame;
    public Transform _trsIconTray;

    public MaterialMes _matMes;
    private PlayerController plycnt;

    public Animator _anim_CantRensei;

    public bool _canRensei = false;

    private void SetItemText() {
        if (_nowFlameCursor >= _iconFlames.Length)
            return;

        _matMes.ViewItemMat(_iconFlames[_nowFlameCursor]);
    }

    private void HideItemText() {
        _matMes.HideItemMes();
    }

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx]._anim.Play("UnZoom");
        HideItemText();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx]._anim.Play("Zoom");
        SetItemText();
    }

    /// <summary>
    /// トレーにアイテムをセット。
    /// アイテムスプライトと、エフェクトスプライト、個数をセット。
    /// </summary>
    public override void OnOpenTray() {
        _nowFlameCursor = 0;
        plycnt = PlayerController.Instance;
        _canRensei = CheckCanRensei();
        _readyFlame._image.color = (_canRensei == true) ? Color.white : new Color(1, 1, 1, 0.5f);
        CursorAnime(_nowFlameCursor);
    }

    protected override void ChangeIdxFromCursorMove(int numOfline) {
        _nowFlameCursor += numOfline;
        if (_nowFlameCursor > _numFlame - 1) {
            _nowFlameCursor = _numFlame - 1;
        } else if (_nowFlameCursor == _numFlame - 1) {
            _nowFlameCursor = 0;
        } else if (_nowFlameCursor < 0) {
            _nowFlameCursor--;
        }
    }
    
    // 錬成可能か判定
    private bool CheckCanRensei() {
        foreach (var rf in _iconFlames) {
            var itm = rf.item;
            if (itm.ItemID == 0)
                continue;

            if (itm.ItemNum <= 0) {
                return false;
            }
        }
        return true;
    }

    private int GetCanRenseiNum() {
        List<int> nums = new List<int>();
        foreach (var fl in _iconFlames) {
            var item = fl.item;
            if (item.ItemNum == 0 || item.ItemID == 0)
                continue;

            var arsthave = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.アレスタ);
            var erinhave = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.エリナ);
            var petlhave = PlayerDatas.Instance.GetHaveItemNum(item, PlayerCharaData.Name.ペティル);
            var sum = arsthave + erinhave + petlhave;
            nums.Add(sum);
        }
        return nums.Min();
    }

    private int _NumOfLine = 5;
    private void CursorMoveOnMatTray(Vector2 input) {
        if (input.x == 0 && input.y != 0) {
            base.CursorMove(input, _NumOfLine);
            return;
        }
        base.CursorMove(input);
    }

    public override void GotoNextTray(bool isgoto, Action act = null) {
        if (_nowFlameCursor != 10 || !isgoto)
            return;

        if (_canRensei) {
            Map_Sound.Instance.DecisionSoundPlay();
            _readyFlame.SetDecideIdx(GetCanRenseiNum());
            _readyFlame.OpenTray(this, act);
        } else {
            Map_Sound.Instance.DontSelectPlay();
        }
    }

    public override void GotoBackTray(bool isgoto) {
        if (isgoto) {
            LeaveAnime(_nowFlameCursor);
            _readyFlame._image.color = new Color(1, 1, 1, 0.5f);
        }
        base.GotoBackTray(isgoto);
    }

    // Update is called once per frame
    void Update () {
        if (!_isRunning)
            return;

        CursorMoveOnMatTray(plycnt.joy_Axis_buf);
        this.GotoNextTray(plycnt.isFire1);
        this.GotoBackTray(plycnt.isFire2);
    }
}
