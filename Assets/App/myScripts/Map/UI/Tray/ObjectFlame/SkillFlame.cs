﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SkillFlame : ObjectFlame
{
    public Image _img_effect;   // アイテムの効果画像
    public Image _img_icon;    // アイテムの画像
    public Image _img_flame;    // アイテムの フレーム画像
    public Skill skill; // スキルデータ

    public Sprite _alphaIcon;
    private Color _effectColor = new Color(1, 1, 1, 0.8f);
    private Color _effectColorNull = new Color(1, 1, 1, 0);
    /// <summary>
    /// スキルデータのセット。
    /// </summary>
    /// <param name="o"></param>
    public override void SetData(object o)
    {
        skill = (Skill)o;
        _img_icon.sprite = (skill.IconSprite != null) ? skill.IconSprite : _alphaIcon;
        if (skill.IconEffectSprite != null) {
            _img_effect.sprite = skill.IconEffectSprite;
            _img_effect.color = _effectColor;
        } else {
            _img_effect.sprite = _alphaIcon;
            _img_effect.color = _effectColorNull;
        }
    }

    public override void HiLightOnIconSelect() {
    }

    public override void DisLightOnIconSelect() {
    }

}
