﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConfigFlame : ObjectFlame
{
    public override void PlayAnimeOnCursor()
    {
        _anim.Play("OnCursor");
    }

    public override void PlayAnimeLeaveCursor()
    {
        _anim.Play("Leave");
    }

    public override void SetData(object o)
    { }

    public override void DisLightOnIconSelect()
    { }

    public override void HiLightOnIconSelect()
    { }
}
