﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// セーブ機能が開放されているかいないかで表示を変更する。
/// </summary>
public class SaveUnlock : MonoBehaviour
{
    [SerializeField] private bool isDebugUnlock;

    [SerializeField] GenericFlame flame;
    [SerializeField] UITray gotoTrue;
    [SerializeField] UITray gotoFalse;
    [SerializeField] TextMeshProUGUI saveText;
    const int SAVEFLAG = 102;
    const string SAVE = "セーブ";
    const string UNSAVE = "???";

	// Use this for initialization
	void OnEnable () {
        var isUnlock = FlagObject.Instance.GetFlag(SAVEFLAG) | isDebugUnlock;
        if (isUnlock == true) {
            saveText.text = SAVE;
            flame._gotoTray = gotoTrue;
        } else {
            saveText.text = UNSAVE;
            flame._gotoTray = gotoFalse;
        }
	}

    [ContextMenu("有効にする")]
    public void ToEnableSave()
    {
        isDebugUnlock = true;
        OnEnable();
    }
}
