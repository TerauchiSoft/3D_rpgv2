﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// 錬金機能が開放されているかいないかで文字表示をかえる。
/// </summary>
public class RecipeUnlock : MonoBehaviour
{
    [SerializeField] private bool isDebugUnlock;

    [SerializeField] GenericFlame flame;
    [SerializeField] UITray gotoTrue;
    [SerializeField] UITray gotoFalse;
    [SerializeField] TextMeshProUGUI recipeText;
    const int RECIPEFLAG = 119;
    const string RECIPE = "錬金";
    const string UNRECIPE = "???";

    // Use this for initialization
    void OnEnable() {
        var isUnlock = FlagObject.Instance.GetFlag(RECIPEFLAG) | isDebugUnlock;
        if (isUnlock == true) {
            recipeText.text = RECIPE;
            flame._gotoTray = gotoTrue;
        } else {
            recipeText.text = UNRECIPE;
            flame._gotoTray = gotoFalse;
        }
    }

    [ContextMenu("有効にする")]
    public void ToEnableRecipe()
    {
        isDebugUnlock = true;
        OnEnable();
    }
}
