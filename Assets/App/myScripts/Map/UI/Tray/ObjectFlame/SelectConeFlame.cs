﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class SelectConeFlame : ObjectFlame
{
    private RectTransform _rectTtrs;


    public override void SetData(object o) {
        return;
    }

    public override void PlayAnimeLeaveCursor() {
        base._anim.Play("Leave");
    }

    public override void PlayAnimeOnCursor() {
        base._anim.Play("OnCursor");
    }

    public override void HiLightOnIconSelect() {
        PlayAnimeOnCursor();
    }

    public override void DisLightOnIconSelect() {
        PlayAnimeLeaveCursor();
    }

    private void Start() {
        _rectTtrs = gameObject.GetComponent<RectTransform>();
        _anim = GetComponent(typeof(Animator)) as Animator;
    }

    // debug
    private void Update() {
        if (!_isSelect)
            return;

        _isSelect = false;
        HiLightOnIconSelect();
        DOVirtual.DelayedCall(0.5f, () => DisLightOnIconSelect());
    }
}
