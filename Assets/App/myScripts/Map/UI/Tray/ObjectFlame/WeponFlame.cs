﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeponFlame : ItemFlame {
    /// <summary>
    /// アイテムデータのセット。
    /// </summary>
    /// <param name="o"></param>
    public override void SetData(object o)
    {
        item = (Item)o;
        _img_icon.sprite = (item.IconSprite != null) ? item.IconSprite : _alphaIcon;
        if (item.IconEffectSprite != null)
        {
            _img_effect.sprite = item.IconEffectSprite;
            _img_effect.color = _effectColor;
        }
        else
        {
            _img_effect.sprite = _alphaIcon;
            _img_effect.color = _effectColorNull;
        }
        _text_num.text = "";
    }

    public override void HiLightOnIconSelect() {
    }

    public override void DisLightOnIconSelect() {
    }

}
