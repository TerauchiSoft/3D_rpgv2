﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RecipeFlame : ObjectFlame
{
    public int _initItemId;     // アイテムIDのアイテムを読み込む。

    public Image _img_effect;   // アイテムの効果画像
    public Image _img_icon;    // アイテムの画像
    public Image _img_flame;    // アイテムの フレーム画像
    public Item item;           // アイテムデータ

    public Sprite _alphaIcon;
    private Color _noItemColor = new Color(0.6f, 0.6f, 0.6f, 0.44f);
    private Color _effectColor = new Color(1, 1, 1, 0.8f);
    private Color _effectColorNull = new Color(1, 1, 1, 0);
    /// <summary>
    /// アイテムデータのセット。
    /// 自分でやる。
    /// </summary>
    /// <param name="o"></param>
    public override void SetData(object o)
    {
        item = (Item)o;

        _img_icon.sprite = (item.IconSprite != null) ? item.IconSprite : _alphaIcon;
        if (item.IconEffectSprite != null) {
            _img_effect.sprite = item.IconEffectSprite;
            _img_effect.color = _effectColor;
        } else {
            _img_effect.sprite = _alphaIcon;
            _img_effect.color = _effectColorNull;
        }

        if (item.ItemNum <= 0 || item.IconSprite == null) {
            _img_flame.color = Color.gray;
            _img_icon.color = _noItemColor;
            _img_effect.color = _noItemColor;
        } else {
            _img_flame.color = Color.white;
            _img_icon.color = Color.white;
            _img_effect.color = _effectColor;
        }

    }

    private void Awake() {
        var itm = ItemTable.Instance.GetItemForId(_initItemId);
        SetData(itm);
    }

    public override void HiLightOnIconSelect() {
    }

    public override void DisLightOnIconSelect() {
    }

}
