﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectUseTray : SelectTray
{
    public override void CursorMove(Vector2 input, int numOfline = 1) {
        base.CursorMove(input, numOfline);
    }

    // Update is called once per frame
    void Update() {
        if (!_isRunning)
            return;

        this.CursorMove(plycnt.joy_Axis_buf);
        GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}