﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumHandoverTray : SelectTray
{
    // Update is called once per frame
    void Update() {
        if (!_isRunning)
            return;

        CursorMove(plycnt.joy_Axis_buf);
        GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}