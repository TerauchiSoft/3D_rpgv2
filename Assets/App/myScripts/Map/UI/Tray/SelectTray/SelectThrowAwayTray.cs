﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectThrowAwayTray : SelectTray
{
    public int _num;
    public int _maxNum;
    public TextMeshProUGUI _text_Num;
    public Image _img_Cursor;

    private Item _item;

    /// <summary>
    /// CharaSelectMesから呼ばれる。渡すアイテム情報、渡す人の情報を設定。
    /// </summary>
    /// <param name="item"></param>
    public void InitThrowAwayStatus(Item item) {
        _item = item;
        _maxNum = _item.ItemNum;
        _img_Cursor.color = Color.clear;
    }

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        Map_Sound.Instance.CursorMoveSoundPlay();
        StartCoroutine(DelayAnim(1, nowidx));
    }

    private IEnumerator DelayAnim(int delay, int nowidx) {
        yield return delay;
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    protected override void ChangeIdxFromCursorMove(int numOfline) {
        if (numOfline > 0) {
            LeaveAnime(1);
            CursorAnime(1);
        } else {
            LeaveAnime(0);
            CursorAnime(0);
        }
    }

    private void ChangeNum(int number) {
        _num = Mathf.Clamp(_num + number, 0, _maxNum);
    }

    /// <summary>
    /// 数値の変更。
    /// </summary>
    /// <param name="input"></param>
    /// <param name="numOfline"></param>
    public override void CursorMove(Vector2 input, int numOfline = 1) {
        if (input.y > 0 || input.x < 0) {
            this.ChangeIdxFromCursorMove(-numOfline);
            ChangeNum(1);
        }
        if (input.y < 0 || input.x > 0) {
            this.ChangeIdxFromCursorMove(numOfline);
            ChangeNum(-1);
        }

        _text_Num.text = _num.ToString();
    }

    public override void OnCloseTray() {
        if (_decideIdx != -1) {
            _decideIdx = _num;
        }
    }

    // Update is called once per frame
    void Update() {
        if (!_isRunning)
            return;

        this.CursorMove(plycnt.joy_Axis_buf);
        GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }

    // 強引にCharaSelectUIのカーソルを非表示に。
    private void LateUpdate() {
        _img_Cursor.color = Color.clear;
    }
}