﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace Battle
{
    /// <summary>
    /// バトル中ステータスの状態を表示する。全員のステータスを表示させる。
    /// </summary>
    public class StatusFukidashiTray : UITray {
        [SerializeField]
        private GameObject[] _fukidashis;
        [SerializeField]
        private TextMeshProUGUI[] _texts;
        private PlayerCharacter[] _pcs;
        public override void LeaveAnime(int befidx)
        {

        }

        public override void CursorAnime(int nowidx)
        {

        }

        /// <summary>
        /// 初期化、表示
        /// </summary>
        public override void OnOpenTray()
        {
            _pcs = BattleMain.Instance._playerCharacter.ToArray();
            for (int i = 0; i < _fukidashis.Length; i++)
            {
                _fukidashis[i].SetActive(false);
            }
            for (int i = 0; i < _fukidashis.Length && i < _pcs.Length; i++)
            {
                if (_pcs[i].PlayerCharaName == (PlayerCharaData.Name)i && _pcs[i].BattleStatus.Hp > 0)
                {
                    _fukidashis[i].SetActive(true);
                    _texts[i].text = GetText(_pcs[i].PlayerCharaName);
                }
            }
        }

        /// <summary>
        /// ステータス表示用テキストを取得
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string GetText(PlayerCharaData.Name name)
        {
            /*
             * 体力 : もう限界...！
             * 魔力 : まんたん！
             * 
             * 状態 : 健康
             * 魔物は強そうだよ！*/

            string str = BattleMessage.Instance.GetFukidashiMes(name);
            return str;
        }

        /// <summary>
        /// 前の選択に戻る
        /// </summary>
        /// <param name="isgoto"></param>
        public override void GotoBackTray(bool isgoto)
        {
            EndProcessManual(); // キャンセル時はPlayerActSelecterの（）=> EndSelecting()を実行しない。
            gameObject.SetActive(false);
        }

        private void Update()
        {
            if (!_isRunning)
                return;

            if (PlayerController.Instance.isFire1 || PlayerController.Instance.isFire2)
                this.GotoBackTray(true);
        }
    }
}