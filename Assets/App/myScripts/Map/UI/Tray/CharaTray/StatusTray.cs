﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatusTray : UITray
{
    // --------------- override ------------------

    public override void LeaveAnime(int befidx) {
        _coneTest[befidx].color = Color.clear;
    }

    public override void CursorAnime(int nowidx) {
        _coneTest[_nowFlameCursor].color = Color.white;
    }

    public override void GotoNextTray(bool isgoto, System.Action act = null) {
        if (!isgoto)
            return;

        _objectFlame[_nowFlameCursor].OpenTray(this, () => {
            gameObject.SetActive(true);
            act?.Invoke();
        });
        gameObject.SetActive(false);
    }
    
    public override void PlayRemoveAnim() {
        _parent.PlayRemoveAnim();
    }

    /// <summary>
    /// 決定されたキャラクターのデータをセットする。
    /// </summary>
    /// <param name="decideIdx"></param>
    private void LoadStatusChara(int idx)
    {
        _text_hps.text = CommonData.GetHpStr(idx);
        _text_mps.text = CommonData.GetMpStr(idx);
    }

    /// <summary>
    /// Statusを開くたびに更新
    /// </summary>
    public override void OnOpenTray()
    {
        LoadStatusChara(CharaTray._selectCharaIdx);
    }

    // --------------------------------------------

    public TextMeshProUGUI[] _coneTest;
    public TextMeshProUGUI _text_hps;
    public TextMeshProUGUI _text_mps;

    private void Update() {
        if (!_isRunning)
            return;

        var plycny = PlayerController.Instance;
        CursorMove(plycny.joy_Axis_buf);
        GotoNextTray(plycny.isFire1, () => LoadStatusChara(CharaTray._selectCharaIdx));
        GotoBackTray(plycny.isFire2);
    }
}
