﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkillTray : UITray{
    public Transform _trsIconTray;
    public SkillFlame[] _iconFlames;
    public UseBookAct _useAct;

    public TextMeshProUGUI _skillName;
    public TextMeshProUGUI _skillText;

    private PlayerController plycnt;
    
    public TextMeshProUGUI _text_skillName;
    public TextMeshProUGUI _text_skillExplanatory;

    private void SetSkillText() {
        _skillName.text = _iconFlames[_nowFlameCursor].skill.SkillName;
        _skillText.text = _iconFlames[_nowFlameCursor].skill.ExplanatoryText;
    }

    public override void LeaveAnime(int befidx) {
        _iconFlames[befidx]._anim.Play("UnZoom");
    }

    public override void CursorAnime(int nowidx) {
        _iconFlames[nowidx]._anim.Play("Zoom");
        SetSkillText();
    }

    /// <summary>
    /// トレーにアイテムをセット。
    /// アイテムスプライトと、エフェクトスプライト、個数をセット。
    /// </summary>
    public override void OnOpenTray() {
        plycnt = PlayerController.Instance;
        _iconFlames = _trsIconTray.GetComponentsInChildren<SkillFlame>();
        var playerskills = PlayerDatas.Instance.p_chara[CharaTray._selectCharaIdx].Ply_skl;
        for (int i = 0; i < _iconFlames.Length; i++) {
            if (i < playerskills.Length) {
                _iconFlames[i].SetData(playerskills[i]);
            } else {
                _iconFlames[i].SetData(Skill.GetNullSkill());
            }
        }
        CursorAnime(_nowFlameCursor);
    }

    private int _NumOfLine = 8;
    public override void CursorMove(Vector2 input, int numOfLine = 1) {
        if (input.x == 0 && input.y != 0) {
            base.CursorMove(input, _NumOfLine);
            LoadItemInfomationOnChangeCursor(_nowFlameCursor);
            return;
        }
        base.CursorMove(input);
        LoadItemInfomationOnChangeCursor(_nowFlameCursor);
    }

    public override void GotoNextTray(bool isgoto, System.Action act = null)
    {
        if (!isgoto)
            return;

        var skl = _iconFlames[_nowFlameCursor].skill;
        if (skl.SkillID == 0)
            return;

        Map_Sound.Instance.DecisionSoundPlay();
        _useAct.RunSkill((PlayerCharaData.Name)CharaTray._selectCharaIdx, skl, this);
    }

    /// <summary>
    /// カーソル変更時にアイテム情報を読み込み、右トレイに表示。
    /// </summary>
    private void LoadItemInfomationOnChangeCursor(int idx)
    {
        var skl = _iconFlames[idx].skill;
        _text_skillName.text = skl.SkillName;
        _text_skillExplanatory.text = skl.ExplanatoryText;
    }

    private void Update() {
        if (!_isRunning)
            return;

        this.CursorMove(plycnt.joy_Axis_buf);
        this.GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}
