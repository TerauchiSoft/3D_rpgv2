﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using IconScroll;

/// <summary>
/// 武器の装備。
/// </summary>
public class WeponTray : UITray
{
    private int nowSelectedIcon = 0;
    private int numIcon;

    public Transform _trsIconTray;
    [SerializeField]
    private GameObject copyItemFlameObject;
    private Item[] weponItems;
    public WeponFlame _weponFlame;
    public GameObject _prefWeponParticle;

    // 武器表示
    public IconScrollView _itemScrollView;

    public TextMeshProUGUI _text_itemName;
    public TextMeshProUGUI _text_itemExplanatory;

    [SerializeField] private WeponMessageBehaviour weponMessageBehaviour;
    [SerializeField] private WeponCharaEffect weponCharaEffect;

    private Item selectedItem;

    public TextMeshProUGUI _itemName;
    public TextMeshProUGUI _itemText;

    private PlayerController plycnt;

    private PlayerCharaData charaData;
    private StatusClass nowStatus;
    private StatusClass ifEquitedStatus;
    const string DIALOG_MES = "持ち物がいっぱい！";

    public override void LeaveAnime(int befidx)
    {
    }

    public override void CursorAnime(int nowidx)
    {
    }

    /// <summary>
    /// トレーにアイテムをセット。
    /// アイテムスプライトと、エフェクトスプライト、個数をセット。
    /// </summary>
    public override void OnOpenTray()
    {
        // デバッグ用
        //var aresta = CommonData.Instance.Aresta;
        //aresta.Wepon = ItemTable.Instance.GetItemFromItemID((ItemTable.ItemID)430);
        //for(int i = 401; i <= 424; i++)
        //{
        //    PlayerDatas.Instance.AddTestItemID = i;
        //    PlayerDatas.Instance.AddTestItem();
        //}

        plycnt = PlayerController.Instance;

        ViewEqWepon();
        SetScrollItem();
        _itemScrollView.SelectCell(nowSelectedIcon);

        var selectedIdx = CharaTray._selectCharaIdx;
        // 主人公キャラのデータ
        charaData = PlayerDatas.Instance.p_chara[selectedIdx];
        // 現在ステータス
        nowStatus = new StatusClass(charaData.STR, charaData.AGI, charaData.INT, charaData.VIT, charaData.LUK, charaData.CRI);
        // もし選択中の武器を装備した時のステータス
        ifEquitedStatus = new StatusClass(charaData.Str, charaData.Agi, charaData.Int, charaData.Vit, charaData.Luk, charaData.Cri);
        if (weponItems != null && weponItems.Length > 0)
            ifEquitedStatus = ifEquitedStatus.Add(weponItems[0].weponStatus.status);

        SetSelectitem();
    }

    /// <summary>
    /// 選択アイテムを設定
    /// </summary>
    private void SetSelectitem()
    {
        if (weponItems == null || weponItems.Length == 0)
        {
            LoadItemInfomationOnChangeCursor(-1);
            return;
        }

        selectedItem = weponItems[nowSelectedIcon];
        _itemScrollView.SelectCell(nowSelectedIcon);
        LoadItemInfomationOnChangeCursor(nowSelectedIcon);
    }

    /// <summary>
    /// 武器装備表示。
    /// </summary>
    private void ViewEqWepon()
    {
        var wepon = PlayerDatas.Instance.p_chara[CharaTray._selectCharaIdx].Wepon;
        if (wepon.ItemID != 0)
            _weponFlame.SetData(wepon);
        else
            _weponFlame.SetData(Item.GetNullItem());
    }

    /// <summary>
    /// キャラクター所持武器アイテムのロード
    /// </summary>
    private Item[] LoadWeponItems()
    {
        int id = CharaTray._selectCharaIdx;
        var player = PlayerDatas.Instance.p_chara[id];
        var items = player.Ply_item.Where(item => item.kind == TableValues.Kind.Wepon);
        return items.ToArray();
    }

    /// <summary>
    /// 所持アイテム表示更新
    /// </summary>
    private void SetScrollItem()
    {
        weponItems = LoadWeponItems();
        _itemScrollView.Init(weponItems);
        numIcon = weponItems.Length;
        if (nowSelectedIcon < 0 || nowSelectedIcon >= numIcon)
            nowSelectedIcon = 0;
    }

    /// <summary>
    /// ステータス上昇エフェクトを表示
    /// </summary>
    private void PlayStatusEffect()
    {
        _isRunning = false;
        weponCharaEffect.SpawnEffect(charaData, nowStatus.Sub(ifEquitedStatus), () => _isRunning = true);
    }

    /// <summary>
    /// 選択されている武器を装備する。
    /// </summary>
    private void Equip()
    {
        if (selectedItem.kind != TableValues.Kind.Wepon || selectedItem.ItemNum == 0)
            return;

        if (_nowFlameCursor == weponItems.Length - 1) // 装備をはずず武器を選択
        {
            UnEquip();
        }
        else                                           // 入れかえる武器を選択する 
        {
            SwapEquip();
        }
        nowStatus = charaData.Ply_st;
        // 武器情報表示
        ViewEqWepon();

        // 能力増減エフェクト
        PlayStatusEffect();

        // スクロールアイテム設定
        SetScrollItem();

        // アイテム選択
        SetSelectitem();
        _itemScrollView.SelectCell(nowSelectedIcon);
    }

    /// <summary>
    /// 装備解除のみ
    /// </summary>
    private void UnEquip()
    {
        var selectedIdx = CharaTray._selectCharaIdx;
        if (PlayerDatas.Instance.CanAddItem(selectedItem, 1, selectedIdx))
        {
            charaData.Wepon = Item.GetNullItem();
            charaData.AddItem(selectedItem);
            Map_Sound.Instance.EquipPlay();
        }
        else
        {
            Dialog.Instance.ViewDialogMessage(DIALOG_MES);
            Map_Sound.Instance.CancelSoundPlay();
        }

        if (weponItems != null && weponItems.Length > 0)
            ifEquitedStatus = weponItems[selectedIdx].weponStatus.status;
    }

    /// <summary>
    /// 装備武器の入れ替え
    /// </summary>
    private void SwapEquip()
    {
        var selectedIdx = CharaTray._selectCharaIdx;
        var wepon = charaData.Wepon.ItemID != 0 ? charaData.Wepon : Item.GetNullItem();
        wepon.ItemNum = 1;

        // 装備アイテムを複数所持しているとき武器をアイテム欄に戻せるかチェック
        if (wepon.ItemID != 0 && selectedItem.ItemNum > 1 && PlayerDatas.Instance.CanAddItem(wepon, 1, selectedIdx) == false)
        {
            Dialog.Instance.ViewDialogMessage(DIALOG_MES);
            Map_Sound.Instance.CancelSoundPlay();
            return;
        }

        selectedItem.ItemNum = 1;
        charaData.Wepon = selectedItem;

        charaData.DecItem(selectedItem.ItemID);
        charaData.AddItem(wepon);

        Instantiate(_prefWeponParticle, _weponFlame.transform);
        if (weponItems != null && weponItems.Length > 0)
            ifEquitedStatus = weponItems[selectedIdx].weponStatus.status;
        Map_Sound.Instance.EquipPlay();
    }

    /// <summary>
    /// 情報更新
    /// </summary>
    private void LoadItemInfomationOnChangeCursor(int idx)
    {
        if (idx == -1 || weponItems == null || weponItems.Length == 0)
        {
            _text_itemName.text = string.Empty;
            _text_itemExplanatory.text = string.Empty;
            weponMessageBehaviour.Clear();
            return;
        }

        var itm = weponItems[idx];
        _text_itemName.text = itm.ItemName;
        _text_itemExplanatory.text = itm.ExplanatoryText;

        weponMessageBehaviour.ShowAbirityChangeMessage(CharaTray._selectCharaIdx, nowStatus.Sub(ifEquitedStatus).GetValueChangeEffect());
    }

    // ------------ Update内のoverrideメソッド -------------------

    /// <summary>
    /// カーソル移動
    /// </summary>
    /// <param name="input"></param>
    /// <param name="numOfline"></param>
    public override void CursorMove(Vector2 input, int numOfline = 1)
    {
        if (input.x == 0 && input.y == 0)
            return;

        int index = nowSelectedIcon;
        if (input.y > 0 || input.x < 0)
        {
            index -= numOfline;
        }
        if (input.y < 0 || input.x > 0)
        {
            index += numOfline;
        }

        if (index >= numIcon || index < 0)
            return;

        nowSelectedIcon = index;
        SetSelectitem();
        Map_Sound.Instance.CursorMoveSoundPlay();
    }

    /// <summary>
    /// アイテムを装備するか、装備を外すか。()
    /// </summary>
    /// <param name="isgoto">If set to <c>true</c> isgoto.</param>
    /// <param name="act">Act.</param>
    public override void GotoNextTray(bool isgoto, System.Action act = null)
    {
        if (!isgoto)
            return;

        Equip();
    }

    /// <summary>
    private void Update() {
        if (!_isRunning)
            return;

        this.CursorMove(plycnt.joy_Axis_buf);
        this.GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}
