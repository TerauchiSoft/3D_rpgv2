﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ステータス増分のキャラエフェクトを表示
/// </summary>
public class WeponCharaEffect : MonoBehaviour {
    private const float deleteTime = 2.0f;

    private Coroutine coroutine;

    [SerializeField] private WeponMessageBehaviour weponMessageBehaviour;

    [SerializeField] private GameObject strEffectPref;
    [SerializeField] private GameObject agiEffectPref;
    [SerializeField] private GameObject intEffectPref;
    [SerializeField] private GameObject vitEffectPref;
    [SerializeField] private GameObject lukEffectPref;
    [SerializeField] private GameObject criEffectPref;

    /// <summary>
    /// ステータス増減エフェクトスポーン
    /// </summary>
    /// <param name="status">Status.</param>
    public void SpawnEffect(PlayerCharaData chara, StatusClass status, Action callBack)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
        coroutine = StartCoroutine(SpawningEffect(chara, status, callBack));
    }

    /// <summary>
    /// ステータス増減エフェクトスポーン
    /// </summary>
    /// <returns>The effect.</returns>
    /// <param name="status">Chara.</param>
    private IEnumerator SpawningEffect(PlayerCharaData chara, StatusClass status, Action callBack)
    {
        weponMessageBehaviour.ShowOnEquitMessage(chara);

        if (status.Str > 0)
            yield return ShowEffect(strEffectPref);
        if (status.Agi > 0)
            yield return ShowEffect(agiEffectPref);
        if (status.Vit > 0)
            yield return ShowEffect(vitEffectPref);
        if (status.Luk > 0)
            yield return ShowEffect(lukEffectPref);
        if (status.Cri > 0)
            yield return ShowEffect(criEffectPref);
        if (status.Str > 0)
            yield return ShowEffect(strEffectPref);
        if (status.Agi > 0)
            yield return ShowEffect(agiEffectPref);
        if (status.Vit > 0)
            yield return ShowEffect(vitEffectPref);
        if (status.Luk > 0)
            yield return ShowEffect(lukEffectPref);
        if (status.Cri > 0)
            yield return ShowEffect(criEffectPref);

        callBack();
    }

    /// <summary>
    /// *Usrをプレイヤー名に置き変えたものを取得
    /// </summary>
    /// <returns>The equit message.</returns>
    /// <param name="chara">Chara.</param>
    private string GetEquitMessage(PlayerCharaData chara)
    {
        var mes = chara.Wepon.weponStatus.equitMes.Replace("*Usr", chara.PlayerName).Replace("/", "\n");
        Debug.Log(mes);
        return mes;
    }

    /// <summary>
    /// テキストとゲームオブジェクトを表示
    /// </summary>
    /// <param name="effectObj"></param>
    /// <returns></returns>
    private IEnumerator ShowEffect(GameObject effectObj)
    {
        if (effectObj != null)
        {
            var obj = Instantiate(effectObj);
            Destroy(obj, deleteTime);
        }
        yield return new WaitForSeconds(0.01f);
    }
}
