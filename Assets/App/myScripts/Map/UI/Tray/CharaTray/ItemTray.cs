﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemTray : UITray {
    public Transform _trsIconTray;
    public ItemFlame[] _iconFlames;
    public UseBookAct _useAct;

    public TextMeshProUGUI _itemName;
    public TextMeshProUGUI _itemText;

    private PlayerController plycnt;

    public TextMeshProUGUI _text_itemName;
    public TextMeshProUGUI _text_itemExplanatory;

    private void SetItemText() {
        _itemName.text = _iconFlames[_nowFlameCursor].item.ItemName;
        _itemText.text = _iconFlames[_nowFlameCursor].item.ExplanatoryText;
    }

    public override void LeaveAnime(int befidx) {
        _iconFlames[befidx]._anim.Play("UnZoom");
    }

    public override void CursorAnime(int nowidx) {
        _iconFlames[nowidx]._anim.Play("Zoom");
        SetItemText();
    }
    
    /// <summary>
    /// トレーにアイテムをセット。
    /// アイテムスプライトと、エフェクトスプライト、個数をセット。
    /// </summary>
    public override void OnOpenTray() {
        plycnt = PlayerController.Instance;
        _iconFlames = _trsIconTray.GetComponentsInChildren<ItemFlame>();
        var playeritems = PlayerDatas.Instance.p_chara[CharaTray._selectCharaIdx].Ply_item;
        for (int i = 0; i < _iconFlames.Length; i++) {
            if (i < playeritems.Length) {
                _iconFlames[i].SetData(playeritems[i]);
            } else {
                _iconFlames[i].SetData(Item.GetNullItem());
            }
        }
        CursorAnime(_nowFlameCursor);
    }

    /// <summary>
    /// アイテムを選択して次に移る。
    /// </summary>
    /// <param name="isgoto">If set to <c>true</c> isgoto.</param>
    /// <param name="act">Act.</param>
    public override void GotoNextTray(bool isgoto, Action act = null) {
        if (!isgoto)
            return;
        var item = _iconFlames[_nowFlameCursor].item;
        if (item.ItemID == 0)
            return;

        Map_Sound.Instance.DecisionSoundPlay();
        _useAct.RunItem((PlayerCharaData.Name)CharaTray._selectCharaIdx, item, this);
    }

    /// <summary>
    /// カーソル変更時にアイテム情報を読み込み、右トレイに表示。
    /// </summary>
    private void LoadItemInfomationOnChangeCursor(int idx) {
        var itm = _iconFlames[idx].item;
        _text_itemName.text = itm.ItemName;
        _text_itemExplanatory.text = itm.ExplanatoryText;
    }

    private int _NumOfLine = 8;
    public override void CursorMove(Vector2 input, int numOfLine = 1) {
        if (input.x == 0 && input.y != 0) {
            base.CursorMove(input, _NumOfLine);
            LoadItemInfomationOnChangeCursor(_nowFlameCursor);
            return;
        }
        base.CursorMove(input);
        LoadItemInfomationOnChangeCursor(_nowFlameCursor);
    }

    private void Update() {
        if (!_isRunning)
            return;
        
        this.CursorMove(plycnt.joy_Axis_buf);
        this.GotoNextTray(plycnt.isFire1);
        GotoBackTray(plycnt.isFire2);
    }
}
