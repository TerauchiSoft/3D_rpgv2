﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Minimap;
using MapVariable;

/// <summary>
/// マスごとに実行される処理
/// </summary>
public class MassExection : SingletonForData<MassExection>
{
    // バトル移行管理
    private MapToBattle mapTpBattle;

    /// <summary>
    /// マスごとに実行される処理
    /// </summary>
    /// <param name="val"></param>
    /// <param name="isBattle"></param>
    /// <param name="isStep"></param>
    public void Exection(Vector3 val, bool isBattle, bool isStep)
    {
        // バトル移行
        if (mapTpBattle == null)
            mapTpBattle = MonoBehaviour.FindObjectOfType<MapToBattle>();

        var cd = CommonData.Instance;

        // 前回の座標に今の座標をセット
        cd.SetBeforeLocation(cd.Posx, cd.Posy, cd.Posd, cd.Floor, cd.Hierarchy);
        cd.SetBeforeFlameLocation(cd.IntPos);

        // イベント、バトル用の1フレーム前の座標
        cd.SetBeforeFlameLocation(cd.IntPos);
        
        // 外部参照用座標に代入
        cd.SetLocation((int)val.x, (int)val.y, (int)val.z);
        MinimapManager.Instance.UpdateVisible();
        Debug.LogFormat("Location x:{0} y:{1} z:{2}", (int)val.x, (int)val.y, (int)val.z);
        if (isStep && MapVariableManager.Instance.isInited)
        {
            MapVariableManager.Instance.SetMapVariable(cd.Floor, cd.Posx, cd.Posy);
            MapVariableManager.Instance.ExecuteForVariable();
        }

        // イベント判定
        if (Map_EventAnalysisExecution.Instance.MapEventExcection())
            return;

        // バトル判定
        if (isBattle)
            mapTpBattle.ToBattle();

        // メニュー許可
        if (!Map_EventAnalysisExecution.Instance.IsEventRunning)
            PlayerController.Instance.isMenuAllow = true;

        return;
    }
}
