﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPrefabType {
	// マップprefabの種類
	public const int MAP_PREFAB_TYPE_NONE = 0; // なぜか0も壁
	public const int MAP_PREFAB_TYPE_WALL = 1; // 壁
	public const int MAP_PREFAB_TYPE_TILES = 2; // 床
	public const int MAP_PREFAB_TYPE_SPACE = 3; // 空間

	public const int WALL_TOP = 14; // 天井

	// 壁の種類
	public const int TYPE_NONE = 0; // 何もない
	public const int TYPE_WALL = 1; // 壁
	public const int TYPE_DOOR = 2; // 扉
	public const int TYPE_ONEWAY_WALL = 3; // 一方通行の壁
	public const int TYPE_ONEWAY_DOOR = 4; // 一方通行の扉
}
