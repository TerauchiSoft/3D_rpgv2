﻿using System.Collections;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
using UnityEngine;
using MapVariable;
using Minimap;

/// <summary>
/// 現在のマップの情報。
/// マップのロード。
/// </summary>
[System.Serializable]
public class MapTest_Legacy : MonoBehaviour
{
    // 読込完了
    public bool isEndRead = false;
    public static MapTest_Legacy Instance { get; private set; }
    /*~----------------------------------/
    :                                    :
    :      3Dで実体をもつデータ          :
    :                                    :
    /~----------------------------------*/

    readonly int LAYER_WALLANDWALLOTHER = 14;
    // 全オブジェクト
    [SerializeField]
    private List<GameObject> MapObjects;
    // 縦壁
    [SerializeField]
    private List<GameObject> _VerticalWallObjects;
    // 縦壁
    [SerializeField]
    private List<GameObject> _HorizontalWallObjects;
    // 床
    [SerializeField]
    private List<GameObject> _floorObjects;

    // combine
    [SerializeField]
    private GameObject ob_walls;
    [SerializeField]
    private Combine combine;

    // combine以外
    [SerializeField]
    private GameObject ob_wallsOther;

    /// <summary>
    /// 壁オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Walls;

    /// <summary>
    /// 床オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Tiles;

    /// <summary>
    /// 空間オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Space;

    // 壁の角のプレハブ
    [SerializeField]
    private GameObject _prefab_CornerR;
    [SerializeField]
    private GameObject _prefab_CornerL;
    //マップデータ配列
    /*  test.byte
     *  
     *  indexByte(3Byte)
     *  mapData(100Byte)
     *  
     *  total 103Bytes
     */

    //イベントマップデータ配列
    /*  test.byte
     *  
     *  indexByte(2Byte)
     *  iventmapData(100Byte)
     *  
     *  total 103Bytes
     */


    /*~----------------------------------/
    :                                    :
    :        マップ内部のデータ          :
    :                                    :
    /~----------------------------------*/
    // このマップの階層
    [SerializeField]
    private string Map_NowHierarchyName;
    [SerializeField]
    private int Map_NowHierarchy;

    private const string EVENT_FILE_NAME_HEADER = "EVH";
    private const string EVENT_FILE_EXTENSION = ".dat";

    /// <summary>
    /// 迷宮の名前を取得
    /// </summary>
    /// <returns></returns>
    public string GetMap_HierarchyName()
    {
        return Map_NowHierarchyName;
    }

    /// <summary>
    /// 迷宮イベントのファイル名を取得
    /// </summary>
    /// <returns></returns>
    public string GetMap_EventFileName()
    {
        return EVENT_FILE_NAME_HEADER + Map_NowHierarchy.ToString() + EVENT_FILE_EXTENSION;
    }

    /// <summary>
    /// 階層番号を取得
    /// </summary>
    /// <returns>1 ~ </returns>
    public int GetMap_HierarchyNum()
    {
        return Map_NowHierarchy;
    }

    [SerializeField]
    private int Map_NumFloor;
    
    [SerializeField]
    private int Map_NowFloor;

    public int GetMap_Floor()
    {
        return Map_NowFloor;
    }

    //マップサイズ。
    [SerializeField]
    private Vector2Int Map_Size;
    
    public Vector2Int GetMap_Size()
    {
        return Map_Size;
    }

    //縦壁マップデータ格納配列
    /// <summary>
    /// 縦壁
    /// </summary>
    public List<Byte> WallDatas1 { get; private set; }

    //横壁マップデータ格納配列
    /// <summary>
    /// 横壁
    /// </summary>
    public List<Byte> WallDatas2 { get; private set; }

    //床データ1格納配列
    /// <summary>
    /// 床
    /// </summary>
    public List<Byte> FloorDatas1 { get; private set; }

    //床データ2格納配列
    /// <summary>
    /// 空間
    /// </summary>
    public List<Byte> FloorDatas2 { get; private set; }

    //踏破したかしていないか
    // 階層<フロア<マップ領域>>
    private List<List<List<Byte>>> isStepfloors;
    private bool[][] isFloorFirstLoads;

    //イベントマップデータ格納配列
    private List<Byte> iv_buf;

    ///<summary>ゲーム起動時に読み込む階層。1 ~</summary>
    public int _loadLayerAtAwake = 1;

    /*------------------------------------------
     |
     |
     |         各種publicメソッド群
     |
     |
     /-----------------------------------------*/

    /// <summary>
    /// ロード用。
    /// </summary>
    /// <param name="isStep">Is step.</param>
    /// <param name="isFloorFirstLoad">Is floor first load.</param>
    public static void LoadIsStepFloors(List<List<List<Byte>>> isStep, bool[][] isFloorFirstLoad)
    {
        IsStepFloors = isStep;
        IsFloorFirstLoads = isFloorFirstLoad;
    }

    /// <summary>
    /// 全ての踏破リスト。
    /// </summary>
    /// <value>The is step floors.</value>
    public static List<List<List<Byte>>> IsStepFloors
    {
        get { return Instance.isStepfloors; }
        set { Instance.isStepfloors = value; }
    }

    /// <summary>
    /// 全ての踏破初期化フラグ。
    /// </summary>
    /// <value>The is step floors.</value>
    public static bool[][] IsFloorFirstLoads
    {
        get { return Instance.isFloorFirstLoads; }
        set { Instance.isFloorFirstLoads = value; }
    }

    /// <summary>
    /// フロアごとの踏破リスト取得
    /// </summary>
    /// <param name="floor"></param>
    /// <returns></returns>
    public Byte[] GetStepfloorsForFloor(int hierarchy, int floor)
    {
        return isStepfloors[hierarchy - 1][floor - 1].ToArray();
    }

    /// <summary>
    /// 踏破セット、リセット
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val">true = 踏破セット, false = 踏破リセット</param>
    public void SetIsStepfloors(int x, int y, int hierarchy, int floor, bool val)
    {
        try
        {
            // フロア数を入力
            isStepfloors[hierarchy - 1][floor - 1][x + GetMap_Size().x * y] = Convert.ToByte(val);
            //print("floor: " + floor.ToString() + "width:" + GetMap_Size().x + " xy: (" + x.ToString() + ", " + y.ToString() + ")");
        }
        catch
        {
            throw new Exception("踏破フラグの境界の境界を超えました。 floor:" + floor.ToString() + "width:" + GetMap_Size().x + " xy:(" + x.ToString() + ", " + y.ToString() + ")");
        }
    }


    public void InitDatas(int hierarchy, int floor) {
        ResetDatas();
        StepDataListCreate();
        //読み込み
        MapLoad(hierarchy, floor);
    }


    public static void MapLoad(int hierarchy, int floor)
    {
        Instance.DeleteMapObject();
        Instance.ResetDatas();
        Instance.MapDataLoad(hierarchy, floor);
        CommonData.Instance.IntPos.Hierarchy = hierarchy;
        CommonData.Instance.IntPos.Floor = floor;
        MapVariableManager.Instance.Init(CommonData.Instance.Hierarchy);
        MinimapManager.Instance.MapLoad();
        // TODO:マップ読み込み済みかでCameraMove内で分岐できるようにしてよ
        CameraMove.Instance.enabled = true;
    }

    // イベントデータ取得
    public List<Byte> GetIventMapData()
    {
        return iv_buf;
    }

    // マップオブジェクト全削除
    public void DeleteMapObject() {
        if (MapObjects.Count == 0) return;
        foreach (var a in MapObjects) {
            Destroy(a);
        }

        MapObjects.Clear();
    }



    /*------------------------------------------
     |
     |
     |         各種privateメソッド群
     |
     |
     /-----------------------------------------*/


    // -------------- 初期化関係 --------------------

    // ----------- データリセット関係 ---------

    public void ResetDatas() {
        DeleteMapObject();
        for (int i = 0; i < isReadHeads.Length; i++)
            isReadHeads[i] = false;
        MapObjects.Clear();

        WallDatas1.Clear();
        WallDatas2.Clear();
        FloorDatas1.Clear();
        FloorDatas2.Clear();
        iv_buf.Clear();

        _VerticalWallObjects.Clear();
        _HorizontalWallObjects.Clear();

        prefab_Walls = new GameObject[0];
        prefab_Tiles = new GameObject[0];
        prefab_Space = new GameObject[0];
        _prefab_CornerL = null;
        _prefab_CornerR = null;
    }

    private void MemoryAlloc() {
        MapObjects = new List<GameObject>();

        WallDatas1 = new List<byte>();
        WallDatas2 = new List<byte>();
        FloorDatas1 = new List<byte>();
        FloorDatas2 = new List<byte>();
        iv_buf = new List<byte>();
    }

    // ------- 層ごとのオブジェクトの読み込み --------

    private void LoadPrefabs(int hierarchy) {
        //MapHierarchyMasterDatas mapMaster = Resources.Load<MapHierarchyMasterDatas>(path);
        var mapMaster = BundleCaches.LoadMapHierarchyMasterDatas(hierarchy);
        prefab_Walls = (GameObject[])mapMaster._pref_Walls.Clone();
        prefab_Tiles = (GameObject[])mapMaster._pref_Tiles.Clone();
        prefab_Space = (GameObject[])mapMaster._pref_Space.Clone();
        _prefab_CornerL = (GameObject)mapMaster._pref_CornerL;
        _prefab_CornerR = (GameObject)mapMaster._pref_CornerR;
    }
    
    // ------- データ読み込み関係 --------

    void MapDataLoad(int hierarchy, int floor) {
        isEndRead = false;

        Map_NowHierarchy = hierarchy;
        Map_NowFloor = floor;

        // 階層ごとのマップデータ取得
        LoadPrefabs(hierarchy);

        // マップデータ、イベントマップ読込
        string filename = "H" + hierarchy.ToString();
        ReadFile(filename, floor);

        isEndRead = true;

        //読み込み終了後、カメラオブジェクト群の有効化 ポストプロセスカメラ
        GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);

        //セット
        print("マップフロア:" + Map_NowFloor);
        print("マップサイズ:" + Map_Size);
        SetMap();

        Resources.UnloadUnusedAssets();
    }

    private bool[] isReadHeads = new bool[6];
    /// <summary>
    /// 1層分のファイルを見て、1フロアのみ読み込んでいく
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    bool FloorInfoSet(string str) {
        string[] reserved = { "MapName", "FloorName", "Floor", "MapSizeX", "MapSizeY", "[MapData]" };

        for (int i = 0; i < reserved.Length; i++) {
            if (str.Contains(reserved[i]) == true) {
                if (isReadHeads[i] == false) {
                    FloorInfoSetOfMap(i, str);
                    isReadHeads[i] = true;
                }
                return true;
            }
        }
        return false;
    }

    private void SetMapHeaderData(List<string> vs) {
        // 層情報設定
        foreach (var s in vs) {
            FloorInfoSet(s);
        }

    }

    private void ReadMapWallArgs(int idx, int count, List<string> readStrs, List<byte> toWrite) {
        int i = 0, j = 0;
        try {
            for (i = 0; i < count; i++) {
                for (j = 0; j < readStrs[idx + i].Length; j += 2) {
                    byte vx0h = byte.Parse(readStrs[idx + i][j].ToString(), NumberStyles.AllowHexSpecifier);
                    byte v0xh = byte.Parse(readStrs[idx + i][j + 1].ToString(), NumberStyles.AllowHexSpecifier);
                    toWrite.Add((byte)(vx0h * 10 + v0xh));
                }
            }
        } catch {
            var s = readStrs[idx + i][j];
            var ss = readStrs[idx + i][j + 1];
        }
    }

    private void LoadMapWallData(List<string> vs, int floor, int numfloor) {
        int offset = floor - 1;
        int firstLine = 0;

        for (; firstLine < vs.Count; firstLine++)
            if (vs[firstLine].Contains("[MapData]"))
                break;

        int readLine = (firstLine + 1) + offset * Map_Size.y;
        ReadMapWallArgs(readLine, (int)Map_Size.y, vs, WallDatas1);
        readLine = numfloor * Map_Size.y + readLine + (floor - 1);
        ReadMapWallArgs(readLine, (int)Map_Size.y + 1, vs, WallDatas2);
        readLine = numfloor * (Map_Size.y + 1) + readLine + (floor - 1);
        ReadMapWallArgs(readLine, (int)Map_Size.y, vs, FloorDatas1);
        readLine = numfloor * Map_Size.y + readLine + (floor - 1);
        ReadMapWallArgs(readLine, (int)Map_Size.y, vs, FloorDatas2);
        readLine = numfloor * Map_Size.y + readLine + (floor - 1);
    }

    private void SetMapWasLoad() {
        //TODO: 踏破リスト初期化はまた正式に作る。これが暫定正式版
        if (isFloorFirstLoads[Map_NowHierarchy - 1][Map_NowFloor - 1] == false) {
            var bytes = Enumerable.Repeat<byte>(0, (int)(Map_Size.x * Map_Size.y)).ToList();
            // 領域分の到踏マップ
            isStepfloors[Map_NowHierarchy - 1][Map_NowFloor - 1] = bytes;
        }

        isFloorFirstLoads[Map_NowHierarchy - 1][Map_NowFloor - 1] = true;
    }

    //読み込み関数
    bool ReadFile(string filename, int floor) {
        try {
            var allLines = File.ReadLines(Application.streamingAssetsPath + "/MapData/Map/" + filename + ".txt", System.Text.Encoding.UTF8).ToList();
            SetMapHeaderData(allLines); // 層情報の取得
            LoadMapWallData(allLines, floor, Map_NumFloor);
            SetMapWasLoad(); // フロア踏破領域生成、一度でもこの層のフロアを読み込んだかセット
        } catch {
            print("マップの層を読み込めませんでした。:");
        }

        GC.Collect();
        //読み込み終了後、カメラオブジェクト群の有効化
        GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);

        return true;
    }

    void FloorInfoSetOfMap(int i, string str) {
        int n = 0;
        switch (i) {
            //MapName = 名称不明の迷宮
            // 階層名 = MapName
            case 0:
                n = str.IndexOf('=');
                n += 2;
                string hircy = "";
                hircy = str.Substring(n, str.Length - 10);

                Map_NowHierarchyName = hircy;
                break;
            // フロア数セット
            case 2:
                n = str.IndexOf('=');
                n += 2;
                string flr = "";
                flr += str[n];
                if (n < str.Length - 1) flr += str[n + 1];

                Map_NumFloor = int.Parse(flr) + 1;
                break;
            // 横幅
            case 3:
                n = str.IndexOf('=');
                n += 2;
                string x = "";
                x += str[n];
                if (n < str.Length - 1) x += str[n + 1];

                Map_Size.x = int.Parse(x) + 1;
                break;
            // 横幅
            case 4:
                n = str.IndexOf('=');
                n += 2;
                string y = "";
                y += str[n];
                if (n < str.Length - 1) y += str[n + 1];

                Map_Size.y = int.Parse(y) + 1;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 読み込まれた文字列からマップデータ読込
    /// </summary>
    /// <param name="str">ファイルの一行分のバイナリ</param>
    /// <param name="sr">ストリーム</param>
    /// <param name="floor">階</param>
    /// <param name="sizeX">Xのサイズ</param>
    /// <param name="sizeY">Yのサイズ</param>
    /// <param name="list">データの追加先のListbyte</param>
    /// <returns></returns>
    string ReadRepeatRowsMapData(string str, StreamReader sr, int floor, int sizeX, int sizeY, List<byte> list) {
        int skipSize = sizeX * sizeY;
        int skipCnt = floor - 1;
        if (skipCnt > 0) {
            sr.BaseStream.Seek(skipSize - sizeX, SeekOrigin.Current);
            skipCnt--;
            sr.BaseStream.Seek(skipSize * skipCnt, SeekOrigin.Current);
            str = sr.ReadLine();
        }

        for (int j = 0; j < sizeX; j++) {
            for (int i = 0; i < sizeY * 2; i += 2) {
                string s1 = str[i].ToString() + str[i + 1].ToString();
                Byte value = (Byte)Convert.ToInt32(s1, 16);
                list.Add(value);
            }
            str = sr.ReadLine();
        }
        return str;
    }

    // ----------- 踏破関係 ---------------

    private void StepDataListCreate() {
        // 踏破の要素はここで作っておく。
        // 階層
        isStepfloors = new List<List<List<byte>>>();
        for (int i = 0; i < 5; i++) {
            // 階層を5つ作る
            isStepfloors.Add(new List<List<byte>>());
        }
        for (int j = 0; j < 5; j++) {
            for (int n = 0; n < 5; n++) {
                // 階層をフロアを5つ作る
                isStepfloors[j].Add(new List<byte>());
            }
        }
        isFloorFirstLoads = new bool[5][];
        for (int i = 0; i < isFloorFirstLoads.Length; i++)
            isFloorFirstLoads[i] = new bool[5];
    }

    // ------------- 壁設置関係 ---------------

    private void SortVerticalWalls() {
        int M = WallDatas2.Count / Map_Size.y;
        int N = WallDatas2.Count / Map_Size.x;
        List<byte> trs = new List<byte>(WallDatas2.Count);
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                trs[i * N + j] = WallDatas2[j * M + i];
            }
        }
        WallDatas2.Clear();
        WallDatas2 = trs;
    }


    //walldatasのデータをもとに壁を生成
    void SetMap()
    {
        // マップオブジェクト初期化
        DeleteMapObject();
        // 壁置き
        PlaceMapWall();
        
        // 床
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, FloorDatas1, 2);
        // 空間
        //ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas2, 3);
        // 天井
        //ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas1, 20, false);

    }

    void PlaceMapWall()
    {
        // 縦壁
        ArrangementOfMapObjects((int)Map_Size.x + 1, (int)Map_Size.y, WallDatas1, 0, _VerticalWallObjects);
        // 横壁
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y + 1, WallDatas2, 1, _HorizontalWallObjects);

        // コーナー
        SetCornersFromWalls();
    }

    void SetGameObjectConfig(GameObject go)
    {
        go.layer = LAYER_WALLANDWALLOTHER;
        var children = go.GetComponentsInChildren<Transform>().Skip(1);
        foreach (var c in children) {
            c.gameObject.layer = LAYER_WALLANDWALLOTHER;
        }
        MapObjects.Add(go);
    }

    /// <summary>
    /// マップ物の配置
    /// </summary>
    /// <param name="x">横幅</param>
    /// <param name="y">縦幅</param>
    /// <param name="list">データ</param>
    /// <param name="i">データの参照段落(0 ~ 3)</param>
    void ArrangementOfMapObjects(int x_size, int y_size, List<byte> list, int like, List<GameObject> wallContainerVorH = null)
    {
        bool iswall = (wallContainerVorH != null);
        for (int yy = 0; yy < y_size; yy++)
        {
            string str = "";
            for (int xx = 0; xx < x_size; xx++)
            {
                str += list[xx + yy * x_size].ToString("X");
                //1マスの壁データ
                Byte objectData = list[xx + yy * x_size];
                WallSet(xx, -yy, like, objectData, false, wallContainerVorH);
                if (iswall && objectData <= 13)
                    WallSet(xx, -yy, like, objectData, iswall, wallContainerVorH);
            }
        }
    }

    private void RightVrtCornerSpawn()
    {
        var obs = _VerticalWallObjects;
        for (int i = 1; i < obs.Count; i++)
        {
            // 一つ前の要素が並んでいなければコーナーにスポーン
            if ((int)(obs[i - 1].transform.position.z - obs[i].transform.position.z) != 1 ||
                obs[i].transform.position.x != obs[i - 1].transform.position.x)
            {
                var ins = Instantiate(GetHierarchyCorner(false), obs[i].transform.position, obs[i].transform.rotation, ob_wallsOther.transform);
                ins.name = "RightVrt::" + i;
                SetGameObjectConfig(ins);
            }
        }
    }

    private void RightHolCornerSpawn()
    {
        var obs = _HorizontalWallObjects;
        for (int i = 0; i < obs.Count - 1; i++)
        {
            // 一つ後の要素が並んでいなければコーナーにスポーン
            if ((int)(obs[i + 1].transform.position.x - obs[i].transform.position.x) != 1 ||
                obs[i].transform.position.z != obs[i + 1].transform.position.z)
            { 
                var ins = Instantiate(GetHierarchyCorner(false), obs[i].transform.position, obs[i].transform.rotation, ob_wallsOther.transform);
                ins.name = "RightHol::" + i;
                SetGameObjectConfig(ins);
            }
        }
    }

    private void LeftVrtCornerSpawn()
    {
        var obs = _VerticalWallObjects;
        for (int i = 0; i < obs.Count - 1; i++)
        {
            // 一つ後の要素が並んでいなければコーナーにスポーン
            if ((int)(obs[i].transform.position.z - obs[i + 1].transform.position.z) != 1 ||
                obs[i].transform.position.x != obs[i - 1].transform.position.x)
            {
                var ins = Instantiate(GetHierarchyCorner(true), obs[i].transform.position, obs[i].transform.rotation, ob_wallsOther.transform);
                ins.name = "LeftVrt::" + i;
                SetGameObjectConfig(ins);
            }
        }
    }

    private void LeftHolCornerSpawn()
    {
        var obs = _HorizontalWallObjects;
        for (int i = 1; i < obs.Count; i++)
        {
            // 一つ前の要素が並んでいなければコーナーにスポーン
            if ((int)(obs[i].transform.position.x - obs[i - 1].transform.position.x) != 1 ||
                obs[i].transform.position.z != obs[i - 1].transform.position.z)
            {
                var ins = Instantiate(GetHierarchyCorner(true), obs[i].transform.position, obs[i].transform.rotation, ob_wallsOther.transform);
                ins.name = "LeftHol::" + i;
                SetGameObjectConfig(ins);
            }
        }
    }

    /// <summary>
    /// 生成された壁データからコーナーを設置
    /// </summary>
    private void SetCornersFromWalls()
    {
        // 縦壁
        // 一つ前の要素が並んでいなければコーナーにスポーン
        LeftVrtCornerSpawn();
        // 一つ前の要素が並んでいなければコーナーにスポーン
        RightVrtCornerSpawn();

        // 横壁
        LeftHolCornerSpawn();
        // 一つ前の要素が並んでいなければコーナーにスポーン
        RightHolCornerSpawn();
    }

    /// <summary>
    /// 階層ごとのコーナーオブジェクト。
    /// </summary>
    /// <returns></returns>
    private GameObject GetHierarchyCorner(bool isLeft) {
        if (isLeft) {// isLeft
            return _prefab_CornerL;
        } else {    // isRight
            return _prefab_CornerR;
        }
    }

    /// <summary>
    /// 各種プレハブオブジェクトの取得
    /// </summary>
    /// <returns></returns>
    private GameObject GetSetObject(int like, int data)
    {
        if (like == 0 || like == 1)
        {
            return prefab_Walls[data];
        }
        else if (like == 2)
        {
            return prefab_Tiles[data];
        }
        else if (like == 3)
        {
            return prefab_Space[data];
        }
        else // like == 20
            return prefab_Walls[14];
    }
    
    /// <summary>
    /// オブジェクトのセット。
    /// 壁は2枚重ねる。
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="like"></param>
    /// <param name="w_Data"></param>
    void WallSet(int x, int y, int like, Byte w_Data, bool isreverse, List<GameObject> wallContainerVorH)
    {
        var vec = MapWallChecker.GetWallVector(x, y, w_Data, like);
        var quat = MapWallChecker.GetWallQuat(x, y, w_Data, like, isreverse);
        GameObject setobject;
        setobject = GetSetObject(like, w_Data);

        if (setobject == null) return;

        // 普通の壁はメッシュ結合
        if (like < 2 && w_Data == 01)
        {
            var a = Instantiate(setobject, vec, quat, ob_walls.transform);
            SetGameObjectConfig(a);
            if (wallContainerVorH != null) // いらない
                wallContainerVorH.Add(a);
            //combine.MeshCombine(a);
        }
        else if (like >= 2 || (like < 2 && w_Data != 00))
        {
            var b = Instantiate(setobject, vec, quat, ob_wallsOther.transform);
            SetGameObjectConfig(b);
        }
        return;
    }

    /*********************************
     * 
     *          マップの判定
     * 
     * *******************************/
    /// <summary>
    /// 前に壁があるか？
    /// </summary>
    /// <returns>The wall kind.</returns>
    /// <param name="po">Po.</param>
    /// <param name="diffDirection">Diff direction.</param>
    public int GetWallKind(Position po, int diffDirection)
    {
        var dir = (diffDirection + po.Direction) & 3;
        var pos = WallRefSet(po, dir);
        var data = CheckisWallInFrontFromData(pos.X, pos.Y, dir);
        return data;
    }

    /// <summary>
    /// 判定用座標取得
    /// </summary>
    /// <returns>The reference set.</returns>
    /// <param name="po">Po.</param>
    /// <param name="direction">Direction.</param>
    Position WallRefSet(Position po, int direction)
    {
        var pos = new Position(po.X, po.Y, po.Direction, po.Floor, po.Hierarchy);
        switch (direction)
        {
            case 1:
                pos.X = pos.X + 1;
                break;
            case 2:
                pos.Y = pos.Y + 1;
                break;
            case 0:
            default://d = 3
                break;
        }
        return pos;
    }

    /// <summary>
    /// 対応した向きの壁情報を取得
    /// </summary>
    /// <returns>The wall in front from data.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    /// <param name="d">D.</param>
    int CheckisWallInFrontFromData(int x, int y, int d)
    {
        if (d == 0 || d == 2)
        {
            return WallDatas2[x + y * (int)Map_Size.x];
        }
        else
        {
            return WallDatas1[x + y * ((int)Map_Size.x + 1)];
        }
    }

    /*----------------------------------------
     |
     |
     |          Awake 
     |  
     |    
     |--------------------------------------- */

    // Use this for initialization
    void Awake() {
        MemoryAlloc();
        Instance = this;
        print("起動");
        InitDatas(_loadLayerAtAwake, 1);
    }

}
