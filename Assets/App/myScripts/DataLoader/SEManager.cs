﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace SE
{
    /// <summary>
    /// オーディオデータ
    /// </summary>
    [Serializable]
    public class SEData
    {
        public string Name;
        public AudioClip Clip;

        /// <summary>
        /// 効果音名と効果音を設定
        /// </summary>
        /// <param name="seName"></param>
        /// <param name="seClip"></param>
        public SEData(string seName, AudioClip seClip)
        {
            Name = seName;
            Clip = seClip;
        }
    }

    /// <summary>
    /// 効果音データを保持、再生など
    /// </summary>
    public class SEManager : SingletonForData<SEManager>
    {
        private List<SEData> seDatas;

        /// <summary>
        /// 初期化
        /// </summary>
        public void Init(SEData[] datas)
        {
            seDatas = datas.ToList();
        }

        /// <summary>
        /// オーディオクリップを取得する
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public AudioClip GetSE(string name)
        {
            var data = seDatas.FirstOrDefault(SEData => SEData.Name == name);
            return data.Clip;
        }
    }
}