﻿using System;
using UnityEngine;
using SE;

/// <summary>
/// Resources用のサウンドデータマスターデータ
/// </summary>
[Serializable]
[CreateAssetMenu(menuName = "Utility/Create SE Table", fileName = "ParameterTable")]
public class SETable : ScriptableObject
{
    public SEData[] SEDatas;
}
