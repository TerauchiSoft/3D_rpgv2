﻿using System;
using UnityEngine;

/// <summary>
/// イベント命令で使用するプレハブの収納。Resourcesフォルダ内に保存。
/// 外部からResources.Loadで参照を得る。
/// </summary>
[CreateAssetMenu]
public class EventInstructionDatas : ScriptableObject
{
    [SerializeField]
    public GameObject[] evtext = new GameObject[0];
    [SerializeField]
    public GameObject[] evcharaview = new GameObject[0];
    [SerializeField]
    public GameObject[] evcharaname = new GameObject[0];
    [SerializeField]
    public GameObject[] evchoiceobject = new GameObject[0];
    [SerializeField]
    public GameObject[] evifobject = new GameObject[0];
    [SerializeField]
    public GameObject[] evcallobject = new GameObject[0];
    [SerializeField]
    public GameObject[] evshop = new GameObject[0];
    [SerializeField]
    public GameObject[] evwarp = new GameObject[0];
}
