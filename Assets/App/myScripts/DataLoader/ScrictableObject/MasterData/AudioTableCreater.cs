﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;
using Audio;
using SE;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
/// <summary>
/// オーディオテーブルを作成する
/// </summary>
public class AudioTableCreate
{
    private static string AUDIO_PATH = "/App/Resources/Audios/BGMs";
    private static string SE_PATH = "/App/Resources/Audios/SEs";
    private static string RESOURCES_PATH = "/App/Resources/Tables";

    /// <summary>
    /// オーディオデータのテーブルを作成
    /// </summary>
    [MenuItem("Utility/オーディオテーブルを作成")]
    public static void CreateAudioTable()
    {
        string path = Application.dataPath + AUDIO_PATH;
        Debug.Log(path);
        var paths = Directory.EnumerateFileSystemEntries(path, "*", SearchOption.AllDirectories);
        var datas = new List<AudioData>();

        foreach (var p in paths)
        {
            int subIndex = p.IndexOf("Assets/", System.StringComparison.CurrentCulture);
            var pp = p.Substring(subIndex, p.Length - subIndex);
            Debug.Log(pp);
            var data = AssetDatabase.LoadAssetAtPath<AudioClip>(pp);
            if (data == null)
                continue;
            AudioData audioData = new AudioData(data.name, data);
            datas.Add(audioData);
        }

        AudioTable table = new AudioTable();
        table.AudioDatas = datas.ToArray();

        string dirName = Application.dataPath + RESOURCES_PATH;
        if (!Directory.Exists(dirName))
            Directory.CreateDirectory(dirName);

        string tablePath = "Assets" + RESOURCES_PATH;
        tablePath += "/audioTable.asset";
        Debug.Log(tablePath);
        AssetDatabase.CreateAsset(table, tablePath);
    }


    /// <summary>
    /// 効果音データのテーブルを作成
    /// </summary>
    [MenuItem("Utility/効果音テーブルを作成")]
    public static void CreateSETable()
    {
        string path = Application.dataPath + SE_PATH;
        Debug.Log(path);
        var paths = Directory.EnumerateFileSystemEntries(path, "*", SearchOption.AllDirectories);
        var datas = new List<SEData>();

        foreach (var p in paths)
        {
            int subIndex = p.IndexOf("Assets/", System.StringComparison.CurrentCulture);
            var pp = p.Substring(subIndex, p.Length - subIndex);
            var data = AssetDatabase.LoadAssetAtPath<AudioClip>(pp);
            if (data == null)
                continue;
            SEData seData = new SEData(data.name, data);
            datas.Add(seData);
        }

        SETable table = new SETable();
        table.SEDatas = datas.ToArray();

        string dirName = Application.dataPath + RESOURCES_PATH;
        if (!Directory.Exists(dirName))
            Directory.CreateDirectory(dirName);

        string tablePath = "Assets" + RESOURCES_PATH;
        tablePath += "/seTable.asset";
        Debug.Log(tablePath);
        AssetDatabase.CreateAsset(table, tablePath);
    }
}

#endif