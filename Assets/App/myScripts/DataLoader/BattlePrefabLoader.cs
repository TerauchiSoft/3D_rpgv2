﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// オーディオデータを保持、再生など
    /// </summary>
    public class BattlePrefabLoader : SingletonForData<BattlePrefabLoader>
    {
        private List<BattlePrefabSource> battlePrefabs;

        /// <summary>
        /// 敵グループプレハブを設定する
        /// </summary>
        /// <param name="datas"></param>
        public void Init(BattlePrefabSource[] datas)
        {
            battlePrefabs = datas.ToList();
        }

        /// <summary>
        /// 敵グループを取得する
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BattlePrefabSource GetBattlePrefab(string name)
        {
            var data = battlePrefabs.FirstOrDefault(battlePrefab => battlePrefab.GroupName == name);
            return data;
        }
    }
}