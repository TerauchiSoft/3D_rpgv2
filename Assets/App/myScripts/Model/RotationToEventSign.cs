﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 常にこちらを向く疑似ビルボード。
///TODO:板ポリにして結合ビルボード化
/// </summary>
[System.Serializable]
public class RotationToEventSign : MonoBehaviour {
    private static Transform tgtTrs;

    private void Start()
    {
        if (tgtTrs == null)
        {
            //tgtTrs = GameObject.FindGameObjectWithTag("CharaVire_CameraMoveObject").transform;
            tgtTrs = Camera.main.transform.parent;
        }
    }

    private void Update()
    {
        transform.rotation = tgtTrs.rotation;
    }

}
