﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 常にこちらを向く疑似ビルボード。
///TODO:板ポリにして結合ビルボード化
/// </summary>
[System.Serializable]
public class RotationToBillboard : MonoBehaviour {
    //private static CameraAct camact;
    [SerializeField]
    private static Transform tgtTrs;

    private void Start()
    {
        if (tgtTrs == null)
            tgtTrs = Camera.main.transform;
    }

    private int q;
    private void Update()
    {
        //if (q != (int)transform.rotation.eulerAngles.y)
        //{
            transform.LookAt(tgtTrs);
            //q = (int)transform.rotation.eulerAngles.y;
        //}
    }

}
