﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// ワープさせます。Wait値で光のフェードアウトの速度を加減します。
/// </summary>
[System.Serializable]
public class WarpObject : MapComponent {
    private const string MASK = "BlackMask";
    private const float FADEIN_WAIT = 0.8f;

    private bool warp2;
    public static bool isWarping = false;
    [SerializeField]
    private float waitTimeToWarpComplete;
    private float WarpStartCount;
    [SerializeField]
    private GameObject pref_WarpFade;
    [SerializeField]
    private Vector2Int warpVec;
    public void SetInit(float wait, Vector2Int vec, System.Action act = null)
    {
        isWarping = true;
        waitTimeToWarpComplete = Time.time + wait;
        WarpStartCount = Time.time + (wait / 2f);
        warpVec = vec;
        callBack = act;
    }

    private System.Action callBack = null;

    public void SetWarpVec(Vector2Int vec)
    {
        warpVec = vec;
    }

    public override void Awake()
    {
        isWarping = true;
        //Instantiate(pref_WarpFade, gameObject.transform);
        //AddContainerIndex();// 自身で削除する
    }


    private void Update()
    {
        if (Time.time > waitTimeToWarpComplete)
        {
            //MapComponentsContainar.DeleteOneComponent(Name.Warp);
            isWarping = false;
            Map_WallView.map_wallview.SetIsMapView(true);
            var sp = GameObject.Find(MASK).GetComponent<Image>();
            sp.DOColor(Color.clear, FADEIN_WAIT).OnComplete(() => {
                CameraMove.Instance.Move_Arrow = CameraMove.Arrow.NONE;
                PlayerController.Map_InputConfigSet(true);
            });
            Destroy(gameObject);
        }

        if (!warp2 && Time.time > WarpStartCount)
        {
            MapEvent.ForceBreak();
            Map_WallView.ToLock(true);  // 踏破記録機能のロック

            if (callBack != null)
                callBack.Invoke();
            /*if (_floor > 15)
                {
                    #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                    #elif UNITY_STANDALONE
                    Application.Quit();
                    #endif
                    return;
                }

                Map_WallView.ToLock(true);  // 踏破記録機能のロック

                var nowhierarchy = MapTest_Legacy.map.GetMap_HierarchyNum();
                MapTest_Legacy.MapLoad(_hierarchy, _floor);
                Map_EventAnalysisExecution.MapEventFloorCheck();
                Map_Audio.AudioPlay(_floor - 1, _hierarchy);
                if (_hierarchy != nowhierarchy)
                    Map_EventAnalysisExecution.LoadMapEvents();
                PlayerController.Map_InputConfigSet();
                //Map_WallView.MapObjectViewForIsMapView();

                Map_WallView.ToLock(false);  // 踏破記録機能ロック解除
             */            
            CommonData.Instance.Warp(warpVec);
            Map_WallView.map_wallview.SetIsMapView(false);
            
            DOVirtual.DelayedCall(0.4f, () => Map_WallView.ToLock(false));  // 踏破記録機能ロック解除

            warp2 = true;
        }
    }
}
