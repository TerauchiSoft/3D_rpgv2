﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// マップイベントに使う動的なコンポーネントを全てここに格納する。
/// 各マップイベントからアクセスするため、MapManagerにアタッチ。
/// </summary>
public class MapComponentsContainar : Singleton<MapComponentsContainar>
{
    [SerializeField]
    public List<GameObject> components = new List<GameObject>(); 

    public void AddComponentObject(GameObject obj)
    {
        for (int i = 0; i < Instance.components.Count; i++)
        {
            if (Instance.components[i] == null)   // 空きがあればいれたい
            {
                Instance.components[i] = obj;
                return;
            }
        }
        Instance.components.Add(obj);
    }

    /// <summary>
    /// すべてのイベントコンポーネントを削除する
    /// </summary>
    public void DeleteAll()
    {
        foreach (var ob in Instance.components)
        {
            if (ob != null)
                Destroy(ob);
        }
        Instance.components.Clear();
    }

    /// <summary>
    /// 一つのイベントコンポーネントを削除する
    /// </summary>
    /// <param name="name"></param>
    public void DeleteOneComponent(MapComponent.Name name)
    {
        var coms = Instance.components;
        for (int i = 0; i < coms.Count; i++)
        {
            var com = coms[i].GetComponent<MapComponent>();
            if (com != null && com.comname == name)
            {
                Destroy(com.gameObject);
                coms.RemoveAt(i);
                return;
            }
        }
    }

    /// <summary>
    /// 一つのイベントコンポーネントを取得する
    /// </summary>
    /// <returns></returns>
    public MapComponent GetOneComponent(MapComponent.Name name)
    {
        var coms = Instance.components;
        for (int i = 0; i < coms.Count; i++)
        {
            if (coms[i] != null)
            {
                var com = coms[i].GetComponent<MapComponent>();
                if (com != null && com.comname == name)
                {
                    return com;
                }
            }
        }
        return null;
    }

    public bool ExistComponent(string tag)
    {
        foreach (var t in Instance.components)
        {
            if (t.tag == tag)
            {
                return true;
            }
        }
        return false;
    }

    private void Start()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }
}
