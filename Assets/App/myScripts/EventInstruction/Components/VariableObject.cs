﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VariableObject : Singleton<VariableObject>, ILoadableObject<IEnumerable<int>>
{
    public bool isEndRead = false;

    [SerializeField]
    private List<int> vals = new List<int>();
    [SerializeField]
    private List<string> valnames = new List<string>();

    public List<int> Vals { get { return vals; } set { vals = value; } }

    //読み込み関数
    private void ReadVariable()
    {
        try
        {
            string[] strs = EventFileLoadcs.ReadFile("Variables", "variable.dat");

            for (int i = 1; i < strs.Length; i++)
            {
                vals.Add(0);
                valnames.Add(strs[i].Split(':')[1]);
            }
            isEndRead = true;
        }
        catch
        {
            throw new System.Exception("変数が読み込めません。");
        }
    }

    public void Load(IEnumerable<int> variables)
    {
        this.vals = variables.ToList();
    }

    // Use this for initialization
    void Awake ()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        instance = this;
        ReadVariable();
        DontDestroyOnLoad(this);
    }

    public void SetVal(int valNo, int value)
    {
        vals[valNo] = value;
    }

    public void SetValArray(int startValNo, int[] Ary)
    {
        for (int i = 0; i < Ary.Length; i++)
        {
            vals[startValNo + i] = Ary[i];
            Debug.Log("変数:"  + (startValNo + i).ToString() + "=" + vals[startValNo + i]);
        }
    }

    public int GetVal(int valNo)
    {
        return vals[valNo];
    }

    public int[] GetValArray(int startValNo, int length)
    {
        int[] ret = new int[length];
        for (int i = 0; i < length; i++)
            ret[i] = vals[startValNo + i];
        
        return ret;
    }
}
