﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FlagObject : Singleton<FlagObject>, ILoadableObject<IEnumerable<bool>>
{
    public bool isEndRead = false;

    [SerializeField]
    private List<bool> flags = new List<bool>();
    [SerializeField]
    private List<string> flagnames = new List<string>();

    public List<bool> Flags { get { return Instance.flags; } set { Instance.flags = value; } }

    //読み込み関数
    private void ReadFlag()
    {
        try
        {
            string[] strs = EventFileLoadcs.ReadFile("Variables", "flag.dat");
            for (int i = 1; i < strs.Length; i++)
            {
                flags.Add(false);
                flagnames.Add(strs[i].Split(':')[1]);
            }
            isEndRead = true;
        }
        catch
        {
           throw new System.Exception("フラグが読み込めません");
        }
    }

    public void Load(IEnumerable<bool> flags)
    {
        this.flags = flags.ToList();
    }

    // Use this for initialization
    void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        instance = this;
        ReadFlag();
        DontDestroyOnLoad(this);
    }

    public void SetFlag(int flagNo, bool flag)
    {
        flags[flagNo] = flag;
    }

    public bool GetFlag(int flagNo)
    {
        return flags[flagNo];
    }
}
