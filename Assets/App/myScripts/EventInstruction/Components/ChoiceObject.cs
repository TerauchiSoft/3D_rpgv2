﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// 同じゲームオブジェクトにMapEventがアタッチされているので、
/// EVBranchから命令をスタートさせることができる。
/// </summary>
public class ChoiceObject : MapComponent
{
    private PlayerController plycnt;
    public bool isChoicing = true;
    public Mode JumpBranch;

    /// <summary>
    /// 選択肢選択カーソル。
    /// </summary>
    public RectTransform selectCursor;
    private float initAncorPos;
    public Image color_selectCursor;
    private Color cursorCol = new Color(0.9882f, 1.0f, 0.3608f, 0.25f);
    private float time = Mathf.PI / 2;
    private float A = 0.22f;

    // Inspecter表示用。
    [SerializeField]
    private Instruction[] instructions;

    /// <summary>
    /// 選択肢
    /// </summary>
    public TextMeshProUGUI[] texts;

    /// <summary>
    /// EVBranchの自分のMode、または今選んでいる選択肢化を表す。
    /// </summary>
    public enum Mode { b0 = 0, b1, b2, b3, bC };

    // 今選んでいる選択肢
    public Mode selectBranch = Mode.b0;

    // キャンセルで選ぶ選択肢
    public Mode cancelBranch = Mode.b1;

    // イベントコンポーネントの初期化
    public override void Awake()
    {
        MapEvent.choiceObjects.Add(this);
        plycnt = PlayerController.Instance;
        plycnt.SetKeyDownMode(true);
        initAncorPos = selectCursor.anchoredPosition.y;
        base.comname = Name.Choice;
        base.AddContainerIndex();
        isChoicing = true;
    }

    private void changeColor()
    {
        time += (Mathf.PI / 8) / Mathf.PI;
        if (time >= 2 * Mathf.PI) time = 0;
        float alp = A * Mathf.Sin(time);
        cursorCol.a = alp + 0.24f;
        color_selectCursor.color = cursorCol;

    }

    private bool IsStrExist()
    {
        if (texts[(int)selectBranch].text == "")
            return false;
        else
            return true;
    }

    private void SelectIdxAdjustment(bool isDown)
    {
        if (IsStrExist() == true)
            return;

        if (isDown == true)
            selectBranch = Mode.b0;
        else
        {
            for (int i = 3; i != 0; i--)
            {
                if (texts[i].text != "")
                {
                    selectBranch = (Mode)i;
                    return;
                }
            }
        }
    }

    private void DoSelectBranch()
    {
        if (plycnt.isFire1 == true)
        {
            JumpBranch = selectBranch;
            isChoicing = false;
            Map_Sound.Instance.DecisionSoundPlay();

            var texts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
            var images = gameObject.GetComponentsInChildren<Image>();
            foreach (var a in texts)
                a.color = new Color(255, 255, 255, 0);
            foreach (var a in images)
                a.color = new Color(255, 255, 255, 0);


            return;
        }

        if (plycnt.Virtical > 0)
        {
            if (selectBranch == Mode.b0)
                selectBranch = Mode.b3;
            else
                selectBranch--;
            SelectIdxAdjustment(false);
            Map_Sound.Instance.CursorMoveSoundPlay();
        }
        else if (plycnt.Virtical < 0)
        {
            if (selectBranch == Mode.b3)
                selectBranch = Mode.b0;
            else
                selectBranch++;
            SelectIdxAdjustment(true);
            Map_Sound.Instance.CursorMoveSoundPlay();
        }

        float y = initAncorPos + selectCursor.sizeDelta.y * (int)selectBranch;
        selectCursor.anchoredPosition = new Vector2(selectCursor.anchoredPosition.x, y);
    }

    // 選択肢の処理
    private void Update()
    {
        if (isChoicing == false)
            return;

        changeColor();
        DoSelectBranch();
    }

    /// <summary>
    /// EVChoiceから呼び出す。
    /// </summary>
    /// <param name="cancelbr"></param>
    public void RunInit(string[] strs, Mode cancelbr)
    {
        for (int i = 0; i < strs.Length; i++)
            texts[i].text = strs[i];

        cancelBranch = cancelbr;
    }

    /// <summary>
    /// EVBranchから呼び出す。
    /// </summary>
    /// <param name="inst"></param>
    public void StartInstruction(Instruction[] inst, string[] sources)
    {
        instructions = inst;
        var evt = GetComponent<MapEvent>();
        evt.evSources = sources;
        evt.RunEventFromComponents(inst);
        // EventRun()で実行する時に追加される。
        //MapEvent.childEvent.Add(gameObject);
    }
}
