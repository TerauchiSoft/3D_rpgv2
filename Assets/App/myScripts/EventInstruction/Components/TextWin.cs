﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

/// <summary>
/// テキストウィンドウの操作クラス
/// </summary>
public class TextWin : MapComponent
{
    private const string ANIME_PROMPT = "Prompt";
    private const string ANIME_FADEOUT = "FadeOut";
    private const string ANIME_APPEAR = "Appear";
    private const string ANIME_REVIEW = "Review";
    private const string ANIME_STATE_FADEIN = "FadeIn";

    private const string ALPHA0_TAG = "<alpha=#00>";

    /// <summary>
    /// メニュー用のウィンドウ。trueでコンポーネントのものとは分けられる。
    /// trueで永久不滅。
    /// </summary>
    public bool isMenuWindow = false;

    /// <summary>
    /// バトル用のウィンドウ。勝手に閉じない。
    /// </summary>
    public bool isBattleWindow = false;

    public enum State
    { Wait, Prompt }

    public static State state;

    // 操作対象のGameObject
    private GameObject ob;

    // メッセージ
    private TextMeshProUGUI textmesh;

    // ウィンドウのアニメーション
    private Animator anim_texwin;

    // 上側か下側か
    public string Position = "Top";
    private const string POSITION_NAME_MAP = "Map";
    private const string POSITION_NAME_TOP = "Top";
    private const string POSITION_NAME_BOTTOM = "Bottom";
    private const string CHARA_NAME = "CharaName";

    private const string OBJ_INFOMES = "InfoMes";
    private const string OBJ_MES = "Mes";

    // 連続表示のスパンデフォルト(フレーム)
    public static readonly int ViewSpanDefault = 4;

    // 連続表示のスパン(フレーム)
    public static int ViewSpan = 4;

    // 連続表示のカウント
    public static int ViewCount = 4;

    // ボタンでスキップ可能か
    public static bool isAllowSkip = true;

    // メッセージ再生中か
    public static bool isMesPlaying = false;

    // 外部のイベントへのメッセージ送り許可 外部のwhileループなどで進ませる。
    public static bool isCanSendMes = false;

    // プロンプトに移行しない
    public static bool isLockNext = false;

    // ディレイ
    //private static int delay = 0;
    //private static readonly int delayflame = 0;

    private void Update()
    {
        if (state == State.Prompt)
            MesSendPhase();
    }

    private void MesSendPhase()
    {
        if (isLockNext == false && PlayerController.Instance.isFire1)
            isCanSendMes = true;
    }


    // 位置、連続文字表示の時間間隔
    public override void Awake()
    {
        if (!isMenuWindow)
        {
            base.comname = Name.TextWin;
            base.AddContainerIndex();
            TextUIViewHierarchySort();
        }
        textmesh = GetComponentInChildren<TextMeshProUGUI>();
        anim_texwin = GetComponentInParent<Animator>();
        ob = gameObject;
        SetViewSpanAndPosition(POSITION_NAME_MAP, 3);
    }

    private void Start()
    {
        PlayerController.Instance.isKeyDownMode = true;
    }

    /// <summary>
    /// CharaNameを手前に表示する
    /// </summary>
    private void TextUIViewHierarchySort()
    {
        this.transform.SetAsLastSibling();
        var cn = MapComponentsContainar.Instance.GetOneComponent(Name.CharaName);
        if (cn != null)
            cn.transform.SetAsLastSibling();
    }

    public void SetViewSpanAndPosition(string po, int tm)
    {
        Position = po;
        SetViewSpan(tm);

        // 操作オブジェクト取得
        if (po == POSITION_NAME_TOP)
            ob = GameObject.Find(OBJ_INFOMES);
        if (po == POSITION_NAME_BOTTOM)
            ob = GameObject.Find(OBJ_MES);
    }
    
    public void SetViewSpanAndPosition(GameObject go, int tm)
    {
        Position = POSITION_NAME_MAP;
        SetViewSpan(tm);

        // 操作オブジェクト取得
        ob = go;

    }

    /// <summary>
    /// 表示間隔の設定
    /// </summary>
    /// <param name="tm"></param>
    public void SetViewSpan(int tm)
    {
        ViewSpan = tm;
        ViewCount = tm;
    }

    public void StartTextPlay(string st)
    {
        PlayerController.Map_EventConfigSet();
        TextInitToPlay(st);
        Debug.Log(st);
    }

    public void StartTextPlayBattle(string st, int? span = null, bool? isallowakip = null)
    {
        if (span != null && span == -1)
        {
            StartTextPlayNoSpan(st);
            return;
        }

        if (span != null)
            ViewSpan = (int)span;
        if (isallowakip != null)
            isAllowSkip = (bool)isallowakip;

        PlayerController.Instance.ResetInput(20);
        TextInitToPlay(st);
        Debug.Log(st);
    }

    /// <summary>
    /// メッセージを瞬間表示
    /// </summary>
    /// <param name="st">St.</param>
    /// <param name="forIncIdxAct">For inc index act.</param>
    /// <param name="isStaticClose">If set to <c>true</c> is static close.</param>
    public void StartTextPlayNoSpan(string st, System.Action forIncIdxAct = null, bool isStaticClose = true)
    {
        TextPlayNoSpan(st, forIncIdxAct, isStaticClose);
    }

    private void TextInitToPlay(string st)
    {
        StartCoroutine(TextPlay(st));
    }

    public void ShowAnimation(bool isNoSpan)
    {
        if (gameObject.activeSelf == false)
            gameObject.SetActive(true);

        if (!isNoSpan)
            StartingTextAnimation();
        else
            SetMesPlayInitNoSpan();
    }

    async void HidingWindow(bool isFadeout)
    {
        anim_texwin.Play(ANIME_PROMPT);
        await System.Threading.Tasks.Task.Delay(33);
        if (isFadeout)
            anim_texwin.Play(ANIME_FADEOUT);
        else
           anim_texwin.Play(ANIME_REVIEW);
    }

    /// <summary>
    /// テキストを隠す
    /// </summary>
    public void HideAnimation(bool isFadeout = true)
    {
        HidingWindow(isFadeout);
    }

    private void EndTextSetContinuity()
    {
        PlayerController.Instance.SetInputDelay();
        SetViewSpan(ViewSpanDefault);
        if (isLockNext == false)
        {
            PromptTextAnimation();
            isMesPlaying = false;
        }
        state = State.Prompt;
    }

    public void LockNext()
    {
        isLockNext = true;
    }

    public void UnLockNext()
    {
        isLockNext = false;
        PromptTextAnimation();
        isMesPlaying = false;
    }

    private void StartingTextAnimation()
    {
        var sts = anim_texwin.GetCurrentAnimatorStateInfo(0);

        if (sts.IsName(ANIME_STATE_FADEIN) != true)
            anim_texwin.Play(ANIME_APPEAR, 0);
    }

    private void PromptTextAnimation()
    {
        anim_texwin.Play(ANIME_PROMPT, 0);
    }

    private void EndingTextAnimation()
    {
        anim_texwin.Play(ANIME_REVIEW, 0);
    }

    private void SetMesPlayInit()
    {
        StartingTextAnimation();
        isCanSendMes = false;
        state = State.Wait;
        isMesPlaying = true;
    }

    private void SetMesPlayInitNoSpan()
    {
        isCanSendMes = false;
        isMesPlaying = true;
    }
    
    /// <summary>
    /// 連続文字表示のメソッド
    /// </summary>
    /// <returns></returns>
    private IEnumerator TextPlay(string st)
    {
        int strcnt = 1;
        SetMesPlayInit();
        textmesh.text = ALPHA0_TAG + st;
        // 全ての文字を読み込むまで繰り返す
        while (strcnt <= st.Length)
        {
            if (isAllowSkip == true && PlayerController.Instance.isFire1 == true)
            {
                textmesh.text = st;
                break;
            }

            // 一文字の時間間隔待ち
            if (ViewCount > 0)
            {
                if (isAllowSkip == true && PlayerController.Instance.isFire1 == true)
                {
                    textmesh.text = st;
                    break;
                }

                ViewCount--;

                // 一回休み
                yield return 0;
                continue;
            }

            // 表示させる文字を1すすめる
            string first = st.Substring(0, strcnt);
            string latter = st.Substring(strcnt, st.Length - strcnt);
            textmesh.text = StringBuild.GetStringFromArgs(first, ALPHA0_TAG, latter);

            // 文字間隔リセット
            ViewCount = ViewSpan;

            // 次の文字
            strcnt++;

            yield return 0;
        }
        EndTextSetContinuity();
    }
    
    /// <summary>
    /// 瞬間表示のメソッド。
    /// メッセージが消えるとActionをInvokeさせる。
    /// </summary>
    /// <returns></returns>
    private void TextPlayNoSpan(string st, System.Action forIncIdxAct = null, bool isStaticMesClose = true)
    {
        gameObject.SetActive(true);
        TextUIViewHierarchySort();
        SetMesPlayInitNoSpan();
        textmesh.text = st;
        EndTextSetContinuity();
        if (isBattleWindow == true)
        {
            PromptTextAnimation();
            return;
        }

        if (isStaticMesClose == false)
            return;

        PlayerController.GotoInputWaitMode(() =>
        {
            //TextAreaStatic.TextAreaClose();
            gameObject.SetActive(false);
            forIncIdxAct?.Invoke();
        });
    }
    
}