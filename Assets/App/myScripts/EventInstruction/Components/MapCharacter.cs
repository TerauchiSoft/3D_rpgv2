﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCharacter : MonoBehaviour
{
    [System.Serializable]
    public struct MapCharacter_dat
    {
        public Sprite subSprites;
        public Vector2 subSpritesDiff;
    }

    public int _charaID;   // 必ずイベントクリエイターと同じIDをつけること。
    public Sprite main_sprite;
    public Sprite[] subSprites = new Sprite[18];
    public Vector2[] subSpritesDiff = new Vector2[18];
    public Vector2 diffVec;
}
