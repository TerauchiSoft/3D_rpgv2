﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 各サブクラスのAwakeで、comnameの設定と、AddContainerIndex()を呼び出してください。
/// </summary>
public class MapComponent : MonoBehaviour
{
    protected MapComponentsContainar container;
    public enum Name
    {
        None, TextWin, CharaView, CharaName, Choice, Branch, If, Call, Flag, Variable, Shop, Warp
    }
    public Name comname = Name.None;

    protected void AddContainerIndex()
    {
        //container = GameObject.Find("MapManager").GetComponent<MapComponentsContainar>();
        MapComponentsContainar.Instance.AddComponentObject(gameObject);
    }

    public virtual void Awake()
    {
        AddContainerIndex();
    }

}