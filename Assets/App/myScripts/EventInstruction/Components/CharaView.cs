﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using DG.Tweening;

public class ShowCharaObject
{
    public GameObject _object;
    public SpriteRenderer _sprite;
    public MapCharacter _mapCharacter;
}

/// <summary>
/// EVCharaでキャラを表示させるときに、ここのCharactersプレハブから表示させる。
/// </summary>
public class CharaView : MapComponent
{
    private List<Tweener> tweeners = new List<Tweener>();

    // debug
    private void Update() {
        // debug
        if (fo == null)
            return;
        //print("FACEID : " + id);
        var p = mc.subSpritesDiff[id];
        fo.transform.localPosition = mc.diffVec + p;
    }

    public const int LAYER = 15;
    public static bool isWaiting = false;
    public GameObject[] Characters;
    [SerializeField]
    private ShowCharaObject[] showCharaObject = new ShowCharaObject[100];

    public override void Awake() {
        base.comname = Name.CharaView;
        base.AddContainerIndex();
    }

    // 呼び出し時に顔があれば一旦削除する。
    private void DeleteFace(GameObject charaobj, SpriteRenderer sprite, int mode, int tim) {
        var faceobs = charaobj.GetComponentsInChildren<Transform>();
        for (int i = 1; faceobs != null && i < faceobs.Length; i++) {
            var ob = faceobs[i];
            sprite.DOColor(new Color(1.0f, 1.0f, 1.0f, 0), GetTimeWait(tim)).OnComplete(() => Destroy(ob.gameObject));
        }
    }

    public GameObject fo;    // debug
    public MapCharacter mc;     // debug
    public int id;     // debug
    private void SetFace(GameObject charaobj, MapCharacter chara, int faceid, int lblid) {
        // 元絵ソート値
        showCharaObject[lblid]._sprite.sortingOrder = 0;
        showCharaObject[lblid]._object.GetComponent<SortingGroup>().sortingOrder = lblid - GetMaxNextIdx();

        // 顔の空オブジェクト
        GameObject ob = new GameObject();
        ob.layer = LAYER;
        ob.transform.parent = showCharaObject[lblid]._object.transform;
        ob.transform.localRotation = Quaternion.identity;
        ob.transform.localScale = Vector3.one;
        var p = chara.subSpritesDiff[faceid];
        ob.transform.localPosition = chara.diffVec + p;

        // 顔のソート値
        //SortingGroup sortingGroup = ob.AddComponent<SortingGroup>();
        //sortingGroup.sortingLayerName = "CharaView";
        //sortingGroup.sortingOrder = -lblid;

        // スプライトを空オブジェクトにセット
        Sprite face = chara.subSprites[faceid];
        SpriteRenderer faceins = ob.AddComponent<SpriteRenderer>();
        faceins.sprite = face;
        faceins.color = new Color(1.0f, 1.0f, 1.0f, 0);
        faceins.sortingLayerName = "CharaView";
        faceins.sortingOrder = 1;

        fo = ob;      // debug
        mc = chara;     // debug
        id = faceid;    // dabug
    }

    // vs[7] = フェードイン　瞬間表示　左から　右から　下から　フェードイン(左から　右から　下から)
    // vs[8] = 時間
    // vs[9] = ウェイト
    private void SetTweenAnimation(GameObject charaobj, int[] vs) {
        if (charaobj != null) {
            // 出現の移動アニメーション。
            SetMoveAnimation(vs, charaobj);
            // キャラの色を変更する。
            SetSpriteAnimation(vs, charaobj);
        }
    }

    private void SetMoveAnimation(int[] vs, GameObject obj) {
        int mode = vs[7], tim = vs[8], iswait = vs[9];
        int x = vs[2], y = vs[3], z = vs[4];

        Vector3 initPos = GetInitPos(mode, x, y, z);
        obj.transform.localPosition = initPos;
        obj.transform.DOMove(obj.transform.position, GetTimeWait(tim));
    }

    private void SetSpriteAnimation(int[] vs, GameObject obj) {
        int mode = vs[7], tim = vs[8], iswait = vs[9];

        var spobs = obj.GetComponentsInChildren<SpriteRenderer>();
        float t = GetColorWaitTim(mode, tim);
        for (int i = 0; spobs != null && i < spobs.Length; i++) {
            spobs[i].DOColor(new Color(1.0f, 1.0f, 1.0f, 1.0f), t);
        }
    }

    private Vector3 GetBasePos(int x, int y, int z) {
        Vector3 vec = new Vector3(0.0f, 0.0f, 0.0f);
        vec.x = (x - BASEX) / (4 * BASEX);
        vec.y = (BASEY - y) / (4 * BASEY) - 0.41f;
        vec.z = (BASEZ + z) * 0.01f;
        return vec;
    }

    private const float BASEX = 776f;
    private const float BASEY = 360f;
    private const float BASEZ = 5;  // カメラからの距離0.0x
    private Vector3 GetInitPos(int mode, int x, int y, int z) {
        Vector3 vec = GetBasePos(x, y, z);
        switch (mode) {
            case 0: // fadein
            default:
                return vec;
            case 1: // appear
                return vec;
            case 2: // l -> c
            case 5: // fade l -> c
                vec.x -= 1.0f;
                return vec;// new Vector3(-650, y - 360.0f, z);
            case 3: // r -> c
            case 6: // fade r -> c
                vec.x += 1.0f;
                return vec;//new Vector3(2570, y - 360.0f, z);
            case 4: // d -> c
            case 7: // fade d -> c
                vec.y -= 1.0f;
                return vec;//new Vector3(x, -650 - 360.0f, z);
        }
    }

    private float GetColorWaitTim(int mode, int tim) {
        switch (mode) {
            default:
            case 0: // fadein
            case 5: // fade l -> c
            case 6: // fade r -> c
            case 7: // fade d -> c
                return GetTimeWait(tim);
            case 1: // appear
            case 2: // l -> c
            case 3: // r -> c
            case 4: // d -> c
                return 0;
        }
    }

    /// <summary>
    /// *0.1秒の時間を取得
    /// </summary>
    /// <param name="cnt"></param>
    /// <returns></returns>
    private float GetTimeWait(int cnt) {
        return 0.1f * cnt;
    }
    //
    //
    //
    //              0         1    2  3  4      5         6       7         8     9
    // CharaView(キャラID, 表情ID, x, y, z, ラベル番号, 倍率, 出現方法ID, 時間, wait)
    /// <summary>
    /// EVCharaから呼び出される。
    /// Z値は実質的に使用しなくなった。
    /// </summary>
    /// <param name="vs"></param>
    public void ShowCharacter(int[] vs) {
        isWaiting = true;
        int sortIdx = SpawnObject(vs[0], vs[7], vs[8]);
        // vs[5]が現在のレイヤー的な
        GameObject charaobj = showCharaObject[sortIdx]._object;
        MapCharacter chara = showCharaObject[sortIdx]._mapCharacter;
        SpriteRenderer sprite = showCharaObject[sortIdx]._sprite;

        DeleteFace(charaobj, sprite, vs[7], vs[8]);
        SetFace(charaobj, chara, vs[1], sortIdx);
        SetTweenAnimation(charaobj, vs);
        MapComponentsContainar.Instance.AddComponentObject(charaobj);

        if (vs[9] != 0) {
            DOVirtual.DelayedCall(GetTimeWait(vs[8]), () => isWaiting = false);
        } else {
            isWaiting = false;
        }
    }

    private int GetMaxNextIdx() {
        for (int i = 0; i < showCharaObject.Length; i++) {
            if (showCharaObject[i] == null)
                return i;
        }
        throw new System.Exception("キャラクター配列の最後のインデックスが取得できませんでした。");
    }

    private int SeachCharaAryIdx(int charaid, int maxnext) {
        for (int i = 0; i < maxnext; i++) {
            if (showCharaObject[i] != null && showCharaObject[i]._mapCharacter._charaID == charaid)
                return i;
        }
        return -1;
    }

    private int SortCharaAry(int charaid) {
        int maxnext = GetMaxNextIdx();
        if (maxnext >= showCharaObject.Length - 1) // >= Length - 1 なのがミソ
            return -1;

        int charaIdx = SeachCharaAryIdx(charaid, maxnext);
        ShowCharaObject insertChara = null;
        // 既存のキャラクター検索
        if (charaIdx != -1)
            insertChara = showCharaObject[charaIdx];

        bool hasBacked = false;
        for (int i = 0; i <= maxnext; i++) {
            // 挿入するキャラクターにあたったら一番後ろ(手前に描画)に持っていく
            if (hasBacked == false && showCharaObject[i] != null && showCharaObject[i] == insertChara) {
                showCharaObject[maxnext] = showCharaObject[i];
                showCharaObject[i] = null;
                hasBacked = true;
            }

            // 詰める
            if (i - 1 >= 0 && showCharaObject[i - 1] == null) {
                showCharaObject[i - 1] = showCharaObject[i];
                showCharaObject[i] = null;
            }
        }

        if (insertChara == null) {
            return GetMaxNextIdx();
        } else {
            charaIdx = SeachCharaAryIdx(charaid, maxnext);
            return charaIdx;
        }
    }

    private void SortSpriteOrder() {
        int maxnext = GetMaxNextIdx();

        for (int i = 0; i < maxnext; i++) {
            if (showCharaObject[i] != null) {
                showCharaObject[i]._sprite.sortingOrder = 0;
                showCharaObject[i]._object.GetComponent<SortingGroup>().sortingOrder = i - maxnext;
            }
        }

    }

    private int SpawnObject(int charaid, int mode, int tim) {
        int sortIdx = SortCharaAry(charaid);
        SortSpriteOrder();

        if (showCharaObject[sortIdx] != null) {
            var delob = showCharaObject[sortIdx]._object;
            var sps = delob.GetComponentsInChildren<SpriteRenderer>();
            float t = GetColorWaitTim(mode, tim);
            foreach (var a in sps)
                a.DOColor(new Color(1f, 1f, 1f, 0), t);
            Destroy(delob, t + 0.2f);
        }

        Transform cmotr = GameObject.FindGameObjectWithTag("CharaView_CameraMoveObject").transform;
        var ob = Instantiate(Characters[charaid], cmotr);
        ob.layer = LAYER;
        var sp = ob.GetComponent<SpriteRenderer>();
        var mapchara = ob.GetComponent<MapCharacter>();
        sp.sortingOrder = sortIdx;
        sp.color = new Color(1f, 1f, 1f, 0);
        showCharaObject[sortIdx] = new ShowCharaObject { _object = ob, _sprite = sp, _mapCharacter = mapchara };

        return sortIdx;
    }

    private void SetDeleteAnimation(ShowCharaObject showob, int mode, int tim) {
        var ob = showob._object;
        var sps = ob.GetComponentsInChildren<SpriteRenderer>();
        Vector3 vec = ob.transform.position;
        switch (mode) {
            case 0: // fadeout
                SetDODeleteAnime(sps, 0, tim);
                break;
            case 1: // appear
                SetDODeleteAnime(sps, 0, 0);
                break;
            case 2: // -> left
                SetDODeleteAnime(sps, 1.0f, tim);
                vec.x -= 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
            case 3: // -> right
                SetDODeleteAnime(sps, 1.0f, tim);
                vec.x += 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
            case 4: // -> down
                SetDODeleteAnime(sps, 1.0f, tim);
                vec.y -= 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
            case 5: // -> fade left
                SetDODeleteAnime(sps, 0, tim);
                vec.x -= 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
            case 6: // -> fade right
                SetDODeleteAnime(sps, 0, tim);
                vec.x += 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
            case 7: // -> fade down
                SetDODeleteAnime(sps, 0, tim);
                vec.y -= 1.0f;
                ob.transform.DOMove(vec, GetTimeWait(tim));
                break;
        }

    }

    private void SetDODeleteAnime(SpriteRenderer[] sps, float alpha, int tim) {
        foreach (var o in sps) {
            var ob = o;
            ob.DOColor(new Color(1.0f, 1.0f, 1.0f, alpha), GetTimeWait(tim)).OnComplete(() => {
                Destroy(ob.gameObject);
            });
        }
    }

    // CharaDelete(キャラID, 消去方法, 時間, Wait)
    /// <summary>
    /// CharaDelから呼び出される。
    /// </summary>
    public void DeleteCharacter(int[] vs) {
        isWaiting = true;

        int charaIdx = SeachCharaAryIdx(vs[0], 90); // debug
        if (charaIdx == -1) {
            isWaiting = false;
            return;
        }

        SetDeleteAnimation(showCharaObject[charaIdx], vs[1], vs[2]);
        showCharaObject[charaIdx] = null;

        if (vs[3] != 0)
            DOVirtual.DelayedCall(GetTimeWait(vs[2]), () => isWaiting = false);
        else
            isWaiting = false;
    }

    /// <summary>
    /// CharaFrontから呼び出す。
    /// </summary>
    /// <param name="charaid"></param>
    public void SortToFront(int charaid) {
        int sortIdx = SortCharaAry(charaid);
        SortSpriteOrder();
    }

    /// <summary>
    /// CharaBackから呼び出す。
    /// </summary>
    /// <param name="charaid"></param>
    public void SortToBack(int charaid) {
        int sortIdx = SortCharaAry(charaid);
        ShowCharaObject back = showCharaObject[sortIdx];
        if (back == null)
            return;

        ShowCharaObject show = showCharaObject[0];
        showCharaObject[sortIdx] = show;
        showCharaObject[0] = back;
        SortSpriteOrder();
    }
}
