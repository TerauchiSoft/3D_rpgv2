﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharaName : MapComponent
{
    private TextMeshProUGUI textmes;

    public override void Awake()
    {
        base.comname = Name.CharaName;
        base.AddContainerIndex();
    }

    public void SetCharaName(string name)
    {
        textmes = GetComponent<TextMeshProUGUI>();
        textmes.text = name;
    }
}
