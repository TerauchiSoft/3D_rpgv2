﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestStatusView : MonoBehaviour
{
    [SerializeField]
    private CommonData cd;
    [SerializeField]
    private GameObject prefab_HpText;
    [SerializeField]
    private GameObject prefab_MpText;

    private Text text_Hp;
    private Text text_Mp;

    // Use this for initialization
    void Start ()
    {
        cd = CommonData.Instance;

        /*
        text_Hp = Instantiate(prefab_HpText, GameObject.Find("Panel").transform).GetComponent<Text>();
        text_Mp = Instantiate(prefab_MpText, GameObject.Find("Panel").transform).GetComponent<Text>();
        */

        StartCoroutine(SetHpMpText());
    }
	
    IEnumerator SetHpMpText()
    {
        bool isTextSeted = false;
        while (isTextSeted == false)
        {
            if (cd.isEndRead == false)
            {
                yield return 0;
                continue;
            }

            //text_Hp.text = PlayerDatas.Instance.p_chara[0].HP.ToString() + " / " + PlayerDatas.Instance.p_chara[0].M_HP.ToString();
            //text_Mp.text = PlayerDatas.Instance.p_chara[0].MP.ToString() + " / " + PlayerDatas.Instance.p_chara[0].M_MP.ToString();

            isTextSeted = true;
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
