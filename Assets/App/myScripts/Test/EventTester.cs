﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class EventTester : MonoBehaviour {
    public GameObject _mapManager;
    List<MapEvent> _mapEvents;
    public Dropdown _dropdown_EventSelect;
    public Sprite _img;
    public Text _changeText;

    IEnumerator LoadEvents()
    {
        while (Map_EventAnalysisExecution._isEndRead == false)
        {
            yield return 0;
        }
        List<Dropdown.OptionData> datas = new List<Dropdown.OptionData>();
        _mapEvents = FindObjectOfType<Map_EventAnalysisExecution>().events;
        foreach(var e in _mapEvents)
        {
            Dropdown.OptionData dat = new Dropdown.OptionData(e.eventName, _img);
            datas.Add(dat);
        }
        _dropdown_EventSelect.AddOptions(_mapEvents.Select(e => e.eventName).ToList());
        _dropdown_EventSelect.value = 0;
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadEvents());
    }

    MapEvent _runEvent = null;
    string eName;

    public void OnValueChange()
    {
        eName = _dropdown_EventSelect.options[_dropdown_EventSelect.value].text;
        //_changeText.text = eName;
    }

    public void OnPointerDown()
    {
        //try
        //{
        //    var mapui = FindObjectOfType<Map_UI>();
        //    if (mapui != null)
        //        mapui.gameObject.SetActive(false);
        //}
        //catch
        //{
        //
        //}
    }

    public void OnSubmit()
    {
        _runEvent = null;
        try
        {
            string eName = _dropdown_EventSelect.options[_dropdown_EventSelect.value].text;
            _runEvent = _mapEvents.FirstOrDefault(e => e.eventName == eName);
        }
        catch
        {
            return;
        }
        if (_runEvent != null)
        {
            var cond = _runEvent.GetComponent<RunEventConditionChacker>();
            if (cond.CheckEventCondition())
            {
                MapEvent.ForceBreak();
                _runEvent.RunEvent();
            }
        }
    }

    public void OnSubmitForce()
    {
        _runEvent = null;
        try
        {
            _runEvent = _mapEvents.FirstOrDefault(e => e.eventName == eName);
        }
        catch
        {
            return;
        }
        if (_runEvent != null)
        {
            MapEvent.ForceBreak();
            _runEvent.RunEvent();
        }
    }
}
