﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ViewUseMagicCost : MonoBehaviour {
    [SerializeField]
    private MagicElement magicElm;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private Sprite[] sprites = new Sprite[3];
    [SerializeField]
    private Color[] colors = new Color[3];
    [SerializeField]
    private Image img;
    [SerializeField]
    private ParticleSystem ps;

    private readonly string nousemp = "魔力は使わないよ";
    private readonly string mpLess = "魔力足りないよ";
    private readonly string[] vs = { "魔力ちょっと使うよ", "魔力それなりに使うよ", "魔力結構使うよ", "魔力かなり使うよ" };
    private string GetMpCostMes(int mpcost, int maxmp, int mp)
    {
        if (mpcost == 0)
            return nousemp;

        if (mp < mpcost)
            return mpLess;

        var p = (float)mpcost / maxmp;
        if (p < 0.1f)
            return vs[0];
        else if (p < 0.3f)
            return vs[1];
        else if (p < 0.5f)
            return vs[2];
        else
            return vs[3];
        
    }

    // UseBookActからも
    public void SetMpCostMes(int charaid, ref Skill skl)
    {
        try
        {
            var mp = PlayerDatas.Instance.p_chara[charaid].MP;
            var mmp = PlayerDatas.Instance.p_chara[charaid].M_MP;
            var mpcost = skl.mpCost;
            text.SetText(GetMpCostMes(mpcost, mmp, mp));
        }
        catch
        {
            throw new System.Exception("MagicStoneでプレイヤーデータを取得できませんでした。:" + charaid.ToString());
        }
    }

    public void SpawnMagicStone(int charaid, ref Skill skl)
    {
        try
        {
            magicElm.SetCharaIdx(charaid);
            img.sprite = sprites[charaid];
            img.SetNativeSize();
            var part = ps.main;
            part.startColor = colors[charaid];
            part.loop = true;
            part.playOnAwake = true;
            SetMpCostMes(charaid, ref skl);
        }
        catch
        {
            throw new System.Exception("MagicStoneの指定IDが配列の境界外でした。");
        }
    }
}
