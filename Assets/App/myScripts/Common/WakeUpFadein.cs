﻿using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using System.Collections;

public class WakeUpFadein : MonoBehaviour {
    [SerializeField] Image img;

    void Start()
    {
        StartCoroutine(Fadeout());
        Destroy(gameObject, 5.0f);
    }

    IEnumerator Fadeout()
    {
        yield return new WaitForSeconds(1.5f);
        var time = Time.time + 1.0f;
        while (Time.time < time)
        {
            img.color = new Color(0, 0, 0, time - Time.time);
            yield return 0;
        }
        img.color = Color.clear;
    }
}
