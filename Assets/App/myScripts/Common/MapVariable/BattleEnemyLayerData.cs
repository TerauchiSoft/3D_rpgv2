﻿using System;
using System.Collections.Generic;

namespace MapVariable
{
    /// <summary>
    /// マップ上の敵グループレイヤーデータ
    /// </summary>
    public class BattleEnemyLayerData
    {
        public int Index;
        public List<string> Enemys = new List<string>();

        /// <summary>
        /// 敵のデータを初期化する
        /// </summary>
        /// <param name="index"></param>
        /// <param name="line"></param>
        public BattleEnemyLayerData(int index, string line)
        {
            Index = index;
            var enemyNames = line.Split(',');
            for (int i = 0; i < enemyNames.Length; i++)
            {
                Enemys.Add(enemyNames[i]);
            }
        }
    }
}
