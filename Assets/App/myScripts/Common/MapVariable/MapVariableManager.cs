﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace MapVariable
{
    /// <summary>
    /// マップ上の変数をマネージする
    /// </summary>
    public class MapVariableManager : SingletonForData<MapVariableManager>
    {
        private const string MAP_ICONSPATH = "/MapData/Variable/mapIcon.dat";
        private const string ENEMY_LAYERPATH = "/MapData/Variable/enemyLayer.dat";
        private const string TALK_PATH = "/MapData/Variable/talk.dat";

        private const string MAP_TABLEPATH = "{0}/MapData/Variable/H{1}.mdata";

        private const string MAP_FLOORNUM = "FloorNum = ";
        private const string MAPFLOOR_LABEL = "Floor = ";
        private const string MAPSIZE_X_LABEL = "MapSizeX = ";
        private const string MAPSIZE_Y_LABEL = "MapSizeY = ";
        private const string MAPDATA_LABEL = "[MapData]";

        private const int MODE_NUM = 5;
        private const string MAPICONID_TAGNAME = "[MapIconID]";
        private const string ENEMY_PERCENT_TAGNAME = "[ENEMY_PERCENT]";
        private const string ENEMY_LAYER_TAGNAME = "[ENEMY_LAYER]";
        private const string TALK_TAGNAME = "[TALK]";
        private const string DOOR_LOCK_MAP_TAGNAME = "[DOOR_LOCK_MAP]";
        private readonly string[] TAGNAMES = { MAPICONID_TAGNAME, ENEMY_PERCENT_TAGNAME, ENEMY_LAYER_TAGNAME, TALK_TAGNAME, DOOR_LOCK_MAP_TAGNAME };

        private const byte NO_DATA = 255;

        private int currentMapHierarchy = -1;
        private int nowFloor = -1;
        private int nowX = -1;
        private int nowY = -1;
        private Vector2Int currentMapSize;

        // ラベル値
        private Dictionary<int, string> mapIconNameList;
        private List<BattleEnemyLayerData> battleEnemyGroupList;
        private Dictionary<int, int> talkList;

        // 現在のマップの変数値など
        public List<List<byte>> CurrentMapIconMap { get; private set; } = new List<List<byte>>();
        public List<List<byte>> CurrentEnemyPercentMap { get; private set; } = new List<List<byte>>();
        public List<List<byte>> CurrentEnemyLayerMap { get; private set; } = new List<List<byte>>();
        public List<List<byte>> CurrentTalkMap { get; private set; } = new List<List<byte>>();
        public List<List<byte>> CurrentDoorLockMap { get; private set; } = new List<List<byte>>();

        // 読み込んだ階層
        private int currentHierarchy;

        // 現在のマスの値
        private byte currentMapIcon = NO_DATA;
        private byte currentEnemyPercent = NO_DATA;
        private byte currentEnemyLayer = NO_DATA;
        private byte currentTalk = NO_DATA;
        private byte currentDoorLock = NO_DATA;

        // 敵出現値の累積値
        public byte EnemyCumulativeProbability { get; private set; }

        private bool isTableRead;
        public bool isInited { get; private set; }

        private System.Random rand = new System.Random();

        /// <summary>
        /// 初期化 マップロード時
        /// </summary>
        public void Init(int hierarchy)
        {
            if (currentMapHierarchy == hierarchy)
                return;

            LoadVariableTables();

            string path = string.Format(MAP_TABLEPATH, Application.streamingAssetsPath, hierarchy);
            // 階層変更時に読み込む
            string[] text = File.ReadAllLines(path, System.Text.Encoding.UTF8);

            int index = 0;
            var maps = new List<List<byte>>[] { CurrentMapIconMap, CurrentEnemyPercentMap, CurrentEnemyLayerMap, CurrentTalkMap, CurrentDoorLockMap };
            while (index < text.Length)
            {
                string line = text[index];
                if (line.Contains(MAPSIZE_X_LABEL))
                {
                    string flootText = line.Replace(MAPSIZE_X_LABEL, "");
                    currentMapSize.x = Convert.ToInt32(flootText);
                }
                else if (line.Contains(MAPSIZE_Y_LABEL))
                {
                    string flootText = line.Replace(MAPSIZE_Y_LABEL, "");
                    currentMapSize.y = Convert.ToInt32(flootText);
                }

                for (int i = 0; i < TAGNAMES.Length; i++)
                {
                    if (!line.Contains(TAGNAMES[i]))
                        continue;

                    List<byte> bytes = null;
                    (index, bytes) = LoadTagMap(index, text, currentMapSize);
                    maps[i].Add(bytes);
                    break;
                }
                index++;
            }

            // ログ
            //int dd = 0;
            //foreach (var mm in maps)
            //{
            //    Debug.LogError(TAGNAMES[dd]);
            //    int flor = 0;
            //    foreach (var m in mm)
            //    {
            //        Debug.LogError("floor ; " + flor);
            //        string s = "";
            //        int ii = 0;
            //        foreach (var l in m)
            //        {
            //            s += l.ToString("00");
            //            if (ii > 0 && (1 + ii) % currentMapSize.x == 0)
            //                s += Environment.NewLine;
            //            ii++;
            //        }
            //        Debug.Log(s);
            //        flor++;
            //    }
            //    dd++;
            //}
            currentMapHierarchy = hierarchy;
            isInited = true;
        }

        /// <summary>
        /// タグのバイトマップをロードする
        /// </summary>
        /// <param name="index"></param>
        /// <param name="text"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private (int, List<byte>) LoadTagMap(int index, string[] text, Vector2Int size)
        {
            index++;
            List<byte> bytes = new List<byte>();
            for (int y = 0; y < size.y; y++)
            {
                string line = text[index];
                for (int x = 0; x < size.x; x++)
                {
                    string byteText = line.Substring(x * 2, 2);
                    byte value = Convert.ToByte(byteText);
                    bytes.Add(value);
                }
                index++;
            }
            index--;
            return (index, bytes);
        }

        /// <summary>
        /// マップ変数のテーブルを読み込む
        /// </summary>
        private void LoadVariableTables()
        {
            if (isTableRead)
                return;

            // マップアイコンテーブル読み込み
            int i = 0;
            try
            {
                string[] strs = File.ReadAllLines(Application.streamingAssetsPath + MAP_ICONSPATH, System.Text.Encoding.UTF8);
                mapIconNameList = new Dictionary<int, string>();
                //foreach (var v in strs) Debug.Log(v);
                for (i = 1; i < strs.Length; i++)
                {
                    string[] ss = strs[i].Split(':');
                    string iconName = ss[1].Split(',')[0];
                    if (string.IsNullOrWhiteSpace(iconName))
                        continue;
                    int index = Convert.ToInt16(ss[0]);
                    Debug.Log("ssindex : " + index + "iconName" + iconName);
                    mapIconNameList.Add(index, iconName);
                }
                Debug.Log("LoadVariableTables Init");
                MapEventIconLoader.Instance.Init(mapIconNameList);
            }
            catch
            {
                Debug.LogError(string.Format("マップアイコン読み込みエラー {0}行目", i));
                throw;
            }

            // バトルレイヤーテーブル読み込み
            try
            {
                string[] strs = File.ReadAllLines(Application.streamingAssetsPath + ENEMY_LAYERPATH, System.Text.Encoding.UTF8);
                battleEnemyGroupList = new List<BattleEnemyLayerData>();
                //foreach (var v in strs) Debug.Log(v);
                for (i = 2; i < strs.Length; i++)
                {
                    var index = Convert.ToInt16(strs[i].Split(':')[0]);
                    var enemys = strs[i].Split(':')[1];
                    var dat = new BattleEnemyLayerData(index, enemys);
                    battleEnemyGroupList.Add(dat);
                    index++;
                }
            }
            catch
            {
                Debug.LogError(string.Format("マップアイコン読み込みエラー {0}行目", i));
                throw;
            }

            // 会話テーブル読み込み
            i = 0;
            try
            {
                string[] strs = File.ReadAllLines(Application.streamingAssetsPath + TALK_PATH, System.Text.Encoding.UTF8);
                talkList = new Dictionary<int, int>();
                //foreach (var v in strs) Debug.Log(v);
                for (i = 2; i < strs.Length; i++)
                {
                    string[] ss = strs[i].Split(':');
                    if (string.IsNullOrWhiteSpace(ss[1]))
                        continue;
                    int index = Convert.ToInt16(ss[0]);
                    int talkNo = Convert.ToInt16(ss[1].Split(',')[0]);
                    talkList.Add(index, talkNo);
                }
            }
            catch
            {
                Debug.LogError(string.Format("会話イベント読み込みエラー {0}行目", i));
                throw;
            }
            isTableRead = true;
        }

        /// <summary>
        /// 指定したマスのマップ変数を設定する
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetMapVariable(int floor, int x, int y)
        {
            // 敵確率増大
            // 敵グループ設定
            // 会話イベント発生メソッドの実行 メニューの「会話」から確認できる
            // かんぬきイベントを出現させる
            // 　以上の条件の設定

            //Debug.Log("SetMapVariable S");
            if (floor == nowFloor && x == nowX && y == nowY)
                return;
            nowFloor = floor;
            nowX = x;
            nowY = y;
            
            floor--;
            int index = x + y * currentMapSize.y;
            currentMapIcon = CurrentMapIconMap[floor][index];
            currentEnemyPercent = CurrentEnemyPercentMap[floor][index];
            currentEnemyLayer = CurrentEnemyLayerMap[floor][index];
            currentTalk = CurrentTalkMap[floor][index];
            currentDoorLock = CurrentDoorLockMap[floor][index];

            Debug.Log("currentMapIcon : " + currentMapIcon);
            Debug.Log("currentEnemyPercent : " + currentEnemyPercent);
            Debug.Log("currentEnemyLayer : " + currentEnemyLayer);
            Debug.Log("currentTalk : " + currentTalk);
            Debug.Log("currentDoorLock : " + currentDoorLock);
        }

        /// <summary>
        /// マップ変数をリセット
        /// </summary>
        public void ResetMapVariable()
        {
            currentMapIcon = NO_DATA;
            currentEnemyPercent = NO_DATA;
            currentEnemyLayer = NO_DATA;
            currentTalk = NO_DATA;
            currentDoorLock = NO_DATA;
        }

        /// <summary>
        /// 取得した変数の処理を実行
        /// </summary>
        public void ExecuteForVariable()
        {
            // 敵出現値の累積
            EnemyCumulativeProbability += currentEnemyPercent;
        }

        /// <summary>
        /// 敵出現累積値のリセット
        /// </summary>
        public void ResetEnemyCumulativeProbability()
        {
            EnemyCumulativeProbability = 0;
        }

        /// <summary>
        /// バトルグループ名を取得する
        /// </summary>
        public string GetBattleGroup()
        {
            var dat = battleEnemyGroupList[currentEnemyLayer];
            if (dat == null || dat.Enemys == null || dat.Enemys.Count == 0)
                return string.Empty;
            
            var count = dat.Enemys.Count;
            return dat.Enemys[rand.Next(count)];
        }
    }
}