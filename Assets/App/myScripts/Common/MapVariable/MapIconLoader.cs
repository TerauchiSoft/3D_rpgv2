﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace MapVariable
{
    /// <summary>
    /// マップ(地図の)アイコンのローダー
    /// </summary>
    public class MapEventIconLoader : SingletonForData<MapEventIconLoader>
    {
        private Dictionary<int, Sprite> mapEventIcons = new Dictionary<int, Sprite>();

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="datas"></param>
        public void Init(Dictionary<int, string> datas)
        {
            Debug.Log("MapEventIconLoader Init");
            BundleCaches.LoadMapEventIconBundle(atlas => SetIconSprites(datas, atlas));
        }

        /// <summary>
        /// スプライトアトラスからスプライトを設定
        /// </summary>
        /// <param name="spriteAtlas"></param>
        public void SetIconSprites(Dictionary<int, string> tableData, SpriteAtlas spriteAtlas)
        {
            foreach (var t in tableData)
            {
                Sprite sprite = spriteAtlas.GetSprite(t.Value);
                if (sprite != null)
                    mapEventIcons.Add(t.Key, sprite);
            }
        }

        /// <summary>
        /// スプライトを取得
        /// </summary>
        /// <param name="iconNo"></param>
        /// <returns></returns>
        public Sprite GetSprite(int iconNo)
        {
            if (mapEventIcons.ContainsKey(iconNo))
            {
                Sprite sprite = mapEventIcons[iconNo];
                return sprite;
            }
            return null;
        }
    }
}