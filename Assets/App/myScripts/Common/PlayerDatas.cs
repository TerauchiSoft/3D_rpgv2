﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// プレイヤー全体に関わる処理を行う。
/// アイテムの追加、渡す、回復処理など。
/// </summary>
[Serializable]
public class PlayerDatas : Singleton<PlayerDatas>
{
    public bool isEndRead;
    public PlayerCharaData[] p_chara;

    /// <summary>
    /// 所持できるキャラクターにアイテムを追加。
    /// </summary>
    /// <param name="itm"></param>
    public bool AddItemForAll(Item itm)
    {
        foreach(var chara in p_chara)
        {
            if (chara.GetCharaHaveItemSpace(itm))
            {
                chara.AddItem(itm, true);
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// アイテムを取得できるかチェックする。-1で全員
    /// </summary>
    /// <returns><c>true</c>, if add item was caned, <c>false</c> otherwise.</returns>
    /// <param name="itm">Itm.</param>
    /// <param name="itemnum">Itemnum.</param>
    /// <param name="charaidx">Charaidx.</param>
    public bool CanAddItem(Item itm, int itemnum, int charaidx = -1)
    {
        itm.ItemNum = itemnum;
        if (charaidx != -1)
        {
            return p_chara[charaidx].GetCharaHaveItemSpace(itm);
        }

        foreach (var chara in p_chara)
        {
            if (chara.GetCharaHaveItemSpace(itm))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// キャラがアイテムを所持している個数を得る。
    /// 未所持の場合0を返す。
    /// </summary>
    /// <param name="itm"></param>
    /// <param name="charaNo"></param>
    /// <returns></returns>
    public int GetHaveItemNum(Item ckitm, PlayerCharaData.Name name = PlayerCharaData.Name.NULL) {
        var idx = (int)name;
        // 全体検索(HIT時にreturn)
        // idx == -1 = PlayerCharaData.Name.NULL
        if (idx == -1) {
            for (int i = 0; i < p_chara.Length; i++) {
                var itmNum = p_chara[i].GetHaveItem(ckitm);
                if (itmNum > 0)
                    return itmNum;
            }
            return -1;
        } else {
            // キャラ個別検索
            return p_chara[idx].GetHaveItem(ckitm);
        }
    }

    /// <summary>
    /// takeキャラがgiveキャラに渡す。
    /// </summary>
    /// <returns><c>true</c>, if item was handovered, <c>false</c> otherwise.</returns>
    /// <param name="item">Item.</param>
    /// <param name="num">Number.</param>
    /// <param name="takeCharaIdx">Take chara index.</param>
    /// <param name="giveCharaIdx">Give chara index.</param>
    public bool HandoverItem(Item item, int num, int takeCharaIdx, int giveCharaIdx = -1)
    {
        var isNullItem = GetHaveItemNum(Item.GetNullItem(), (PlayerCharaData.Name)giveCharaIdx);
        var isHaveItemNum = GetHaveItemNum(item, (PlayerCharaData.Name)giveCharaIdx);

        if (isHaveItemNum > 0 || isNullItem >= 0)
        {
            if (giveCharaIdx != -1)
                p_chara[takeCharaIdx].DecItem(item, num);
            item.ItemNum = num;
            p_chara[giveCharaIdx].AddItem(item);
            return true;
        }
        return false;
    }

    /// <summary>
    /// 錬成でgiveキャラに渡す。
    /// </summary>
    /// <returns><c>true</c>, if item was handovered, <c>false</c> otherwise.</returns>
    /// <param name="item">Item.</param>
    /// <param name="num">Number.</param>
    /// <param name="takeCharaIdx">Take chara index.</param>
    /// <param name="giveCharaIdx">Give chara index.</param>
    public bool HandoverRenseiItem(Item item, int num, int giveCharaIdx = -1)
    {
        var isNullItem = GetHaveItemNum(Item.GetNullItem(), (PlayerCharaData.Name)giveCharaIdx);
        var isHaveItemNum = GetHaveItemNum(item, (PlayerCharaData.Name)giveCharaIdx);

        if (isHaveItemNum > 0 || isNullItem >= 0)
        {
            item.ItemNum = num;
            p_chara[giveCharaIdx].AddItem(item);
            return true;
        }
        return false;
    }

    /// <summary>
    /// アイテムを渡す
    /// </summary>
    /// <returns><c>true</c>, if item was handobered, <c>false</c> otherwise.</returns>
    /// <param name="itemID">Item identifier.</param>
    /// <param name="num">Number.</param>
    /// <param name="takeCharaIdx">Take chara index.</param>
    /// <param name="giveCharaIdx">Give chara index.</param>
    public bool HandoberItem(int itemID, int num, int takeCharaIdx, int giveCharaIdx = -1) {
        var item = ItemTable.Instance.GetItemForId(itemID);
        return HandoverItem(item, num, takeCharaIdx,　giveCharaIdx);
    }

    /// <summary>
    /// Item構造体で捨てる
    /// </summary>
    /// <param name="charaIdx">Chara index.</param>
    /// <param name="item">Item.</param>
    /// <param name="num">Number.</param>
    public void ThrowItem(int charaIdx, Item item, int num) {
        p_chara[charaIdx].DecItem(item, num);
    }

    /// <summary>
    /// アイテムIDで捨てる
    /// </summary>
    /// <param name="charaIdx">Chara index.</param>
    /// <param name="itemId">Item identifier.</param>
    /// <param name="num">Number.</param>
    public void ThrowItem(int charaIdx, int itemId, int num) {
        p_chara[charaIdx].DecItem(itemId, num);
    }

    /// <summary>
    /// 錬金で素材を減らす
    /// </summary>
    /// <param name="itemId">Item identifier.</param>
    /// <param name="num">Number.</param>
    public void DecMaterialItem(int itemId, int num) {
        Item itm = ItemTable.Instance.GetItemForId(itemId);
        for (int i = 0; i < p_chara.Length; i++) {
            var NumHave = GetHaveItemNum(itm, (PlayerCharaData.Name)i);
            if (NumHave > 0) {
                ThrowItem(i, itm, 1);
                return;
            }
        }
    }

    /// <summary>
    /// 全回復の処理
    /// </summary>
    public void CareFull(int charaid = -1)
    {
        if (charaid == -1)
        {
            foreach (var chara in p_chara)
            {
                chara.CareFull();
            }
        }
        else
        {
            try
            {
                p_chara[charaid].CareFull();
            }
            catch
            {
                return;
            }
        }

    }

    const int PLAYERNUM = 3;
    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        p_chara = new PlayerCharaData[PLAYERNUM];
        for (int i = 0; i < p_chara.Length; i++)
        {
            p_chara[i] = new PlayerCharaData();                       // class
            p_chara[i].Ply_st = new StatusClass();                    // class
            p_chara[i].Ply_skl = new Skill[PlayerCharaData.NumSkill]; // struct
            p_chara[i].Ply_item = new Item[PlayerCharaData.NumItem];  // struct
        }
        instance = this;
        isEndRead = true;
    }

    // ------------- debug -----------------

    public int AddTestItemID;
    [ContextMenu("AddItem")]
    public void AddTestItem()
    {
        var item = ItemTable.Instance.GetItemForId(AddTestItemID);
        AddItemForAll(item);
    }
}


/// <summary>
/// プレイヤー個々を見て実行する。
/// </summary>
[Serializable]
public class PlayerCharaData
{
    //public bool isTest = true;
    /// <summary>
    /// 名前
    /// </summary>
    public string PlayerName;

    public enum Name { アレスタ = 0, エリナ, ペティル, NULL = -1 }
    public Name name = Name.アレスタ;
    public void SetNameFromStringName() { name = (Name)Enum.Parse(typeof(Name), PlayerName); }

    public bool isEndRead = false;

    /// <summary>
    /// 仲間にいるか？
    /// </summary>
    public bool isJoined;

    /// <summary>
    /// プレイヤーの能力
    /// </summary>
    public StatusClass Ply_st;
    /// <summary>
    /// スキルの最大数
    /// </summary>
    public const int NumSkill = 24;
    /// <summary>
    /// プレイヤーのスキル
    /// </summary>
    public Skill[] Ply_skl = new Skill[NumSkill];
    /// <summary>
    /// アイテムの最大数
    /// </summary>
    public const int NumItem = 24;
    /// <summary>
    /// プレイヤーのアイテム
    /// </summary>
    public Item[] Ply_item = new Item[NumItem];
    /// <summary>
    /// 装備中の武器。プレイヤーのアイテム欄とは別。
    /// </summary>
    public Item Wepon;
    /// <summary>
    /// 武器の装備。アイテムインデックスのアイテムを
    /// Weponにセット、装備していた武器をアイテムに戻す。
    /// </summary>
    /// <param name="itemIdx"></param>
    /// <returns></returns>
    public bool ChangeWepon(int itemIdx) {
        if (Ply_item[itemIdx].kind != TableValues.Kind.Wepon)
            return false;

        var Witem = Ply_item[itemIdx];
        Ply_item[itemIdx] = Wepon;
        Wepon = Witem;
        return true;
    }
    /// <summary>
    /// アイテムが持てるキャラクターかチェック
    /// </summary>
    /// <returns></returns>
    public bool GetCharaHaveItemSpace(Item item)
    {
        foreach (var i in Ply_item)
        {
            if (i.ItemID == 0 || i.ItemID == item.ItemID)
                return true;
        }
        return false;
    }
    /// <summary>
    /// アイテムの追加(同じ種類のアイテムはまとめる)
    /// 追加する個数は予めItemNumに指定しておく。
    /// </summary>
    /// <param name="itm"></param>
    public int AddItem(Item itm, bool doReSort = false)
    {
        // すでにあるアイテムグループにまとめる
        for (int i = 0; i < Ply_item.Length; i++)
        {
            if (itm.ItemID == Ply_item[i].ItemID)
            {
                Ply_item[i].ItemNum = Ply_item[i].ItemNum + itm.ItemNum;
                if (Ply_item[i].ItemNum <= 0)
                {
                    ReSortItemIdx();
                    return 0;// アイテムが無くなったときは0を返す
                }
                return Ply_item[i].ItemNum;
            }
        }

        // 空白に入れる
        for (int i = 0; i < Ply_item.Length; i++)
        {
            if (Ply_item[i].ItemID == 0)
            {
            Ply_item[i] = itm;
            return Ply_item[i].ItemNum;
            }
        }

        // アイテムがいっぱいのときは-1を返す。
        return -1;
    }
    /// <summary>
    /// アイテムの整列。ID順。
    /// </summary>
    public void ReSortItemIdx()
    {
        IEnumerable<Item> q = Ply_item.Where(x => x.ItemID != 0).OrderBy(x => x.ItemID);
        Item[] aliveitem = q.ToArray();
        Ply_item = new Item[NumItem];
        for(int i = 0; i < NumItem; i++)
        {
            if (i < aliveitem.Length && aliveitem[i].ItemID != 0)
                Ply_item[i] = aliveitem[i];
            else
                return;
        }
    }
    /// <summary>
    /// アイテムの削除
    /// 削除する個数も指定。
    /// </summary>
    /// <param name="itm"></param>
    public int DecItem(Item itm, int n = 1, bool doReSort = false)
    {
        itm.SetItemNum(n);
        for (int i = 0; i < Ply_item.Length; i++)
        {
            if (itm.ItemID == Ply_item[i].ItemID)
            {
                Ply_item[i].ItemNum = Ply_item[i].ItemNum - itm.ItemNum;
                if (Ply_item[i].ItemNum <= 0)
                {
                    Ply_item[i] = new Item();
                    Ply_item[i].ItemNum = 0;
                }
                if (doReSort)
                    ReSortItemIdx();
                return Ply_item[i].ItemNum;
            }
        }
        return 0;
    }

    public int DecItem(int ItemId, int n = 1, bool doResort = false) {
        Item itm = ItemTable.Instance.GetItemForId(ItemId);
        return DecItem(itm, n, doResort);
    }

    public void SetItemRange(Item[] items)
    {
        for (int i = 0; i < Ply_item.Length; i++)
        {
            if (i < items.Length && items[i].ItemID != 0)
                Ply_item[i] = items[i];
            else
            {
                Ply_item[i] = new Item();
                Ply_item[i].ItemNum = 0;
                Ply_item[i].ItemID = 0;
            }
        }
    }

    public int GetHaveItem(Item ckitm) {
        foreach (var item in Ply_item) {
            if (ckitm.ItemID == item.ItemID) {
                return ckitm.ItemNum;
            }
        }
        return -1;
    }

    public void CareFull()
    {
        HP = M_HP;
        MP = M_MP;
    }

    public PlayerCharaData()
    {
        Ply_item = new Item[NumItem];
        Ply_skl = new Skill[NumItem];
    }

    // ------------ 能力値 ---------------

    // Ply_stの外からアクセスする用
    /// <summary>レベル</summary>
    public int LV
    {
        get { return Ply_st.Lv; }
        set { Ply_st.Lv = value; }
    }
    /// <summary>
    /// 習得スキル。
    /// </summary>
    /// <value>The skill.</value>
    public int SKILL
    {
        get { return Ply_st.GetSkillNo; }
        set { Ply_st.GetSkillNo = value; }
    }
    /// <summary>Hp</summary>
    public int HP
    {
        get { return Ply_st.Hp; }
        set { Ply_st.Hp = value; }
    }
    /// <summary>最大Hp</summary>
    public int M_HP
    {
        get { return Ply_st.M_Hp; }
        set { Ply_st.M_Hp = value; }
    }

    public void AddHp(int n)
    {
        Ply_st.Hp += n;
        Ply_st.Hp = Mathf.Clamp(Ply_st.Hp, 0, Ply_st.M_Hp);
    }

    public void DecHP(int n)
    {
        Ply_st.Hp -= n;
        Ply_st.Hp = Mathf.Clamp(Ply_st.Hp, 0, Ply_st.M_Hp);
    }

    /// <summary>Mp</summary>
    public int MP
    {
        get { return Ply_st.Mp; }
        set { Ply_st.Mp = value; }
    }
    /// <summary>最大Mp</summary>
    public int M_MP
    {
        get { return Ply_st.M_Mp; }
        set { Ply_st.M_Mp = value; }
    }

    public void AddMp(int n)
    {
        Ply_st.Mp += n;
        Ply_st.Mp = Mathf.Clamp(Ply_st.Mp, 0, Ply_st.M_Mp);
    }

    public void DecMP(int n)
    {
        Ply_st.Mp -= n;
        Ply_st.Mp = Mathf.Clamp(Ply_st.Mp, 0, Ply_st.M_Mp);
    }

    /// <summary>魔力</summary>
    public int Str
    {
        get { return Ply_st.Str; }
        set { Ply_st.Str = value; }
    }
    /// <summary>素早さ</summary>
    public int Agi
    {
        get { return Ply_st.Agi; }
        set { Ply_st.Agi = value; }
    }
    /// <summary>賢さ</summary>
    public int Int
    {
        get { return Ply_st.Int; }
        set { Ply_st.Int = value; }
    }
    /// <summary>元気度</summary>
    public int Vit
    {
        get { return Ply_st.Vit; }
        set { Ply_st.Vit = value; }
    }
    /// <summary>運のよさ</summary>
    public int Luk
    {
        get { return Ply_st.Luk; }
        set { Ply_st.Luk = value; }
    }
    /// <summary>クリティカル度</summary>
    public int Cri
    {
        get { return Ply_st.Cri; }
        set { Ply_st.Cri = value; }
    }

    /// <summary>総合魔力</summary>
    public int STR
    {
        get { return Ply_st.Str + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Str : 0); }
    }
    /// <summary>総合素早さ</summary>
    public int AGI
    {
        get { return Ply_st.Agi + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Agi : 0); }
    }
    /// <summary>総合賢さ</summary>
    public int INT
    {
        get { return Ply_st.Int + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Int : 0); }
    }
    /// <summary>総合元気度</summary>
    public int VIT
    {
        get { return Ply_st.Vit + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Vit : 0); }
    }
    /// <summary>総合運のよさ</summary>
    public int LUK
    {
        get { return Ply_st.Luk + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Luk : 0); }
    }
    /// <summary>総合クリティカル度</summary>
    public int CRI
    {
        get { return Ply_st.Cri + (Wepon.ItemID != 0 ? Wepon.weponStatus.status.Cri : 0); }
    }/// <summary>属性</summary>
    public TableValues.Attribute ATR
    {
        get { return Ply_st.Atr; }
        set
        {
            if (value.GetTypeCode() == TypeCode.Int32) Ply_st.Atr = (TableValues.Attribute)value;
            else Ply_st.Atr = value;
        }
    }

    /// <summary>経験値</summary>
    public int EXP
    {
        get { return Ply_st.Exp; }
        set { Ply_st.Exp = value; }
    }
    /// <summary>現レベルでの経験値の底</summary>
    public int EXP_BTM
    {
        get { return Ply_st.Exp_buttom; }
        set { Ply_st.Exp_buttom = value; }
    }
    /// <summary>現レベルでの経験値の上限</summary>
    public int EXP_UPR
    {
        get { return Ply_st.Exp_upper; }
        set { Ply_st.Exp_upper = value; }
    }
    /// <summary>所持金(アレスタのみ)</summary>
    public int G
    {
        get { return Ply_st.G; }
        set { Ply_st.G = value; }
    }
    /// <summary>疲労度</summary>
    public int Fatigue
    {
        get { return Ply_st.Fatigue; }
        set { Ply_st.Fatigue = value; }
    }

}

/// <summary>
/// 能力値クラス
/// 能力、属性
/// </summary>
[System.Serializable]
public class StatusClass
{
    // 能力
    public int Lv;
    public int GetSkillNo;
    public int Hp;
    public int M_Hp;
    public int Mp;
    public int M_Mp;
    public int Str;
    public int Agi;
    public int Int;
    public int Vit;
    public int Luk;
    public int Cri;
    public int Fatigue;
    public TableValues.Attribute Atr;

    public int B_Str;
    public int B_Agi;
    public int B_Int;
    public int B_Vit;
    public int B_Luk;
    public int B_Cri;

    // 主人公、敵に適用
    // 累積値
    public int Exp;
    // 現在レベルの底値
    public int Exp_buttom;
    // 現在レベルの上限値
    public int Exp_upper;
    // レベルごとの上限値
    public int[] Exps;
    // アレスタ、敵に適用
    public int G;

    public StatusClass() { }

    /// <summary>
    /// 基礎能力比較用
    /// </summary>
    /// <param name="str">String.</param>
    /// <param name="agi">Agi.</param>
    /// <param name="ini">Ini.</param>
    /// <param name="vit">Vit.</param>
    /// <param name="luk">Luk.</param>
    /// <param name="cri">Cri.</param>
    public StatusClass(int str, int agi, int ini, int vit, int luk, int cri)
    {
        Str = str;
        Agi = agi;
        Int = ini;
        Vit = vit;
        Luk = luk;
        Cri = cri;
    }

    /// <summary>
    /// 能力値を足し合わせる
    /// </summary>
    /// <param name="add">Add.</param>
    public StatusClass Add(StatusClass add)
    {
        var str = Str += add.Str;
        var agi = this.Agi += add.Agi;
        var vit = this.Vit += add.Vit;
        var luk = this.Luk += add.Luk;
        var cri = this.Cri += add.Cri;

        return new StatusClass { Str = str, Agi = agi, Vit = vit, Luk = luk, Cri = cri };
    }

    /// <summary>
    /// 能力値の引き算を求める
    /// </summary>
    /// <param name="sub">Sub.</param>
    public StatusClass Sub(StatusClass sub)
    {
        var str = this.Str -= sub.Str;
        var agi = this.Agi -= sub.Agi;
        var vit = this.Vit -= sub.Vit;
        var luk = this.Luk -= sub.Luk;
        var cri = this.Cri -= sub.Cri;

        return new StatusClass { Str = str, Agi = agi, Vit = vit, Luk = luk, Cri = cri };
    }

    /// <summary>
    /// 能力変化の情報を取得
    /// </summary>
    /// <returns></returns>
    public TableValues.Effect[] GetValueChangeEffect()
    {
        List<TableValues.Effect> effects = new List<TableValues.Effect>();
        if (this.Str > 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Agi > 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Vit > 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Luk > 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Cri > 0)
            effects.Add(TableValues.Effect.STRUp);

        if (this.Str < 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Agi < 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Vit < 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Luk < 0)
            effects.Add(TableValues.Effect.STRUp);
        if (this.Cri < 0)
            effects.Add(TableValues.Effect.STRUp);

        return effects.ToArray();
    }
}

/*
 * 能力値の説明

全てintに収まりさえすればよい、わかりやすさのため0~255の値を取る。
・属性5つ(火→氷→地→雷→風→火(→は右の属性に対して強いことを表す。))
・LV(0 ~ 63)
・HP(ファジーパラメーターで表示)
・M_HP(マックス数値)
・MP(ファジーパラメーターで表示)
・M_MP(マックス数値)
・STR	力　　　魔法攻撃で相手に与えるダメージの基本値に影響
・AGI	素早さ	先制攻撃や相手の攻撃をよける確率に影響
・INT	賢さ	魔法攻撃ダメージや魔法攻撃の被ダメ軽減に影響
・VIT	元気度	被ダメ軽減に影響
・LUK	運	アイテムドロップ率や攻撃を避ける確率に影響
・CRI	技力	クリティカルヒット成功率に影響
・Fatigue 疲労度　0 ~ 100 、0のときに覚醒コマンドを実行できる。
　　　　　　　　　また、疲労度が大きいと能力が少し下がる。
　　　　　　　　　

・B_STR　バトル時のSTR(バトルが終わればもとに戻る)
・B_AGI　バトル時のAGI
・B_INT　バトル時のINT
・B_VIT　バトル時のVIT
・B_LUK　バトル時のLUK
・B_CRI　バトル時のCRI

敵キャラ独自のパラメーター
・EXP　経験値　キャラクターの持つ経験値
・G　お金　キャラクターの持つお金
 */