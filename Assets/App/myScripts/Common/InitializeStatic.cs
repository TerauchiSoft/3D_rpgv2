﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アセットバンドル、Resources以下の辞書的登録
/// TODO:cdObjectのアタッチから変更するのがいいと思われる
/// </summary>
public class InitializeStatic : MonoBehaviour {
    private void Awake()
    {
        BundleCaches.Init();
    }
}
