﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitScreenSize : MonoBehaviour {
    public int ScreenWidth = 1920;
    public int ScreenHeight = 1080;

    private void Start()
    {
        // PC向けビルドだったらサイズ変更
        if (Application.platform == RuntimePlatform.WindowsPlayer ||
        Application.platform == RuntimePlatform.OSXPlayer ||
        Application.platform == RuntimePlatform.LinuxPlayer)
        {
            Screen.SetResolution(ScreenWidth, ScreenHeight, FullScreenMode.FullScreenWindow, 0);
        }
    }
}
