﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/*
 * 
　　所持アイテム Item[]           ----            
　　所持キャラ
　　所持武器
　・イベント
　・音量設定
　・所持金
　・フラグ
　・変数
　・フロア                       ----        RetPosition
　・現在位置                  ----        RetPosition
　・迷宮名　プレイ時間　記録時間
 */

/// <summary>
/// セーブデータの表示に使う情報。メンバはclassでもstructでもSerializable属性をつける必要がある。
/// </summary>
[System.Serializable]
public class SaveInfo
{
    public string dungeonName;
    public int floor;
    public int playTimeH;
    public int playTimeM;
    public int playTimeS;
    public int saveTimeH;
    public int saveTimeM;

    public void SetCaches() {
        dungeonName = CommonData.Instance.GetDungeonName(CommonData.Instance.Hierarchy);
        floor = CommonData.Instance.Floor;
        //playTimeH
        //playTimeH
        //playTimeH
        //playTimeH
        //playTimeH
    }

    public void SetDataFromCaches()
    { }
}

[System.Serializable]
public class SaveEvent
{
    public EventInfo mainEvent;
    public EventInfo[] subQuest;

    public void SetCaches()
    {
        mainEvent = CommonData.Instance.mainEvent;
        subQuest = CommonData.Instance.subQuest.ToArray();
    }

    public void SetDataFromCaches()
    {
        CommonData.Instance.mainEvent = mainEvent;
        CommonData.Instance.subQuest = subQuest.ToList();
    }
}

[System.Serializable]
public class SavePosition
{
    public Position returnPos;
    public Map map;

    [System.Serializable]
    public struct Map
    {
        public Hierarchy[] hierarchy;
    }

    [System.Serializable]
    public struct Hierarchy
    {
        public Floor[] floors;
    }

    [System.Serializable]
    public struct Floor
    {
        public Byte[] isStep;
        public bool isFirstLoad;
    }

    public void SetCaches()
    {
        returnPos = CommonData.Instance.IntPos;

        List<List<List<Byte>>> isStep = MapTest_Legacy.IsStepFloors;
        bool[][] isFirstLoad = MapTest_Legacy.IsFloorFirstLoads;

        map = new SavePosition.Map();
        map.hierarchy = new Hierarchy[isStep.Count];
        for (int i = 0; i < isStep.Count; i++)
        {
            map.hierarchy[i].floors = new Floor[isStep[i].Count];
            for (int j = 0; j < isStep[i].Count; j++)
            {
                map.hierarchy[i].floors[j].isFirstLoad = isFirstLoad[i][j];

                map.hierarchy[i].floors[j].isStep = new Byte[isStep[i][j].Count];
                for (int k = 0; k < isStep[i][j].Count; k++)
                    map.hierarchy[i].floors[j].isStep[k] = isStep[i][j][k];
            }
        }
    }

    public void SetDataFromCaches()
    {
        CommonData.Instance.SetLocation(returnPos);
        MapVariable.MapVariableManager.Instance.SetMapVariable(returnPos.Floor, returnPos.X, returnPos.Y);
        CommonData.Instance.SetReturnLocation(returnPos);
        MapTest_Legacy.MapLoad(returnPos.Hierarchy, returnPos.Floor);
        CameraMove.InitPosition();

        var floor = CommonData.Instance.IntPos.Floor;
        int HIERARCHYNUM = map.hierarchy.Length;

        List<List<List<Byte>>> isStep = new List<List<List<byte>>>(HIERARCHYNUM);
        bool[][] isFirstLoad = new bool[HIERARCHYNUM][];
        for (int i = 0; i < HIERARCHYNUM; i++)
        {
            int FLOORNUM = map.hierarchy[i].floors.Length;

            isStep.Add(new List<List<byte>>(FLOORNUM));
            isFirstLoad[i] = new bool[FLOORNUM];
            for (int j = 0; j < FLOORNUM; j++)
            {
                isFirstLoad[i][j] = map.hierarchy[i].floors[j].isFirstLoad;

                int MASUNUM = map.hierarchy[i].floors[j].isStep.Length;
                isStep[i].Add(new List<byte>(MASUNUM));
                for (int k = 0; k < MASUNUM; k++)
                    isStep[i][j].Add(map.hierarchy[i].floors[j].isStep[k]);
            }
        }

        MapTest_Legacy.IsStepFloors = isStep;
        MapTest_Legacy.IsFloorFirstLoads = isFirstLoad;
    }
}

[System.Serializable]
public class SaveVariableFlag
{
    public List<bool> flags;
    public List<int> variables;

    public void SetCaches() {
        flags = FlagObject.Instance.Flags;
        variables = VariableObject.Instance.Vals;
    }

    public void SetDataFromCaches()
    {
        FlagObject.Instance.Flags = flags;
        VariableObject.Instance.Vals = variables;
    }
}

[System.Serializable]
public class SaveMoney
{
    public int money;

    public void SetCaches()
    {
        money = PlayerDatas.Instance.p_chara[0].G;
    }

    public void SetDataFromCaches()
    {
        PlayerDatas.Instance.p_chara[0].G = money;
    }
}

[System.Serializable]
public class SaveVolume
{
    public float volume;

    public void SetCaches() { }
    public void SetDataFromCaches() { }
}

[System.Serializable]
public class SaveCharacterPossessions
{
    [System.Serializable]
    public struct Element
    {
        public int id;
        public int num;
    }

    const int CHARANUM = 3;
    const int ITEMNUM = 24, SKILLNUM = 24;
    public Element[] arestaItems = new Element[ITEMNUM];
    public Element[] erinaItems = new Element[ITEMNUM];
    public Element[] petilItems = new Element[ITEMNUM];
    public Element[] arestaSkills = new Element[SKILLNUM];
    public Element[] erinaSkills = new Element[SKILLNUM];
    public Element[] petilSkills = new Element[SKILLNUM];
    public Element[] equipment = new Element[CHARANUM];

    public void SetCaches()
    {
        var players = PlayerDatas.Instance.p_chara;
        int idx = 0;
        Item[][] haveItems = new Item[CHARANUM][];
        Skill[][] haveSkills = new Skill[CHARANUM][];
        foreach (var p in players)
        {
            equipment[idx].id = p.Wepon.ItemID;
            equipment[idx].num = p.Wepon.ItemNum;
            haveItems[idx] = p.Ply_item;
            haveSkills[idx] = p.Ply_skl;

            idx++;
        }

        for (int i = 0; i < ITEMNUM; i++)
        {
            arestaItems[i] = new Element { id = haveItems[0][i].ItemID, num = haveItems[0][i].ItemNum };
            erinaItems[i] = new Element { id = haveItems[1][i].ItemID, num = haveItems[1][i].ItemNum };
            petilItems[i] = new Element { id = haveItems[2][i].ItemID, num = haveItems[2][i].ItemNum };

            arestaSkills[i] = new Element { id = haveSkills[0][i].SkillID };
            erinaSkills[i] = new Element { id = haveSkills[1][i].SkillID };
            petilSkills[i] = new Element { id = haveSkills[2][i].SkillID };
        }
    }

    public void SetDataFromCaches()
    {
        var players = PlayerDatas.Instance.p_chara;
        int idx = 0;
        foreach (var p in players)
        {
            p.Wepon = ItemTable.Instance.GetItemForId(equipment[idx].id);
            p.Wepon.ItemNum = equipment[idx].num;
        }

        for (int i = 0; i < CHARANUM; i++)
        {
            var items = GetItemElements(i);
            var skills = GetSkillElements(i);
            for (int j = 0; j < ITEMNUM; j++)
            {
                players[i].Ply_item[j] = ItemTable.Instance.GetItemForId(items[j].id);
                players[i].Ply_item[j].ItemNum = items[j].num;
                players[i].Ply_skl[j] = SkillTable.GetSkillForId(skills[j].id, false);
            }
        }
    }

    public Element[] GetItemElements(int chara)
    {
        switch (chara)
        {
            case 0: return arestaItems;
            case 1: return erinaItems;
            case 2: return petilItems;
            default: throw new Exception("セーブデータのアイテムインデックス数がおかしいです。");
        }
    }

    public Element[] GetSkillElements(int chara)
    {
        switch (chara)
        {
            case 0: return arestaSkills;
            case 1: return erinaSkills;
            case 2: return petilSkills;
            default: throw new Exception("セーブデータのアイテムインデックス数がおかしいです。");
        }
    }
}


[System.Serializable]
public class SaveCharaStatuses
{
    const int CHARANUM = 3;
    public bool[] isJoin = new bool[CHARANUM];
    public StatusClass[] statuses = new StatusClass[CHARANUM];
    public int[] lv = new int[CHARANUM];
    public long[] exp = new long[CHARANUM];

    public void SetCaches()
    {
        var players = PlayerDatas.Instance.p_chara;
        int idx = 0;
        foreach (var p in players)
        {
            isJoin[idx] = p.isJoined;
            statuses[idx] = p.Ply_st;
            lv[idx] = statuses[idx].Lv;
            exp[idx] = statuses[idx].Exp;

            idx++;
        }
    }

    public void SetDataFromCaches()
    {
        var players = PlayerDatas.Instance.p_chara;
        int idx = 0;
        foreach (var p in players)
        {
            p.isJoined = isJoin[idx];
            p.Ply_st = statuses[idx];

            idx++;
        }
    }
}


[System.Serializable]
public class SaveConfig
{
    public int viewUI;
    public int bgmVol;
    public int seVol;

    public void SetCaches()
    {
        viewUI = FrontUIView.Instance.GetFrontUIView();
        bgmVol = Map_Audio.Instance.GetVolumeCache();
        seVol = Map_Sound.Instance.GetVolumeCache();
    }

    public void SetDataFromCaches()
    {
        FrontUIView.Instance.SetFrontUIView(viewUI);
        Map_Audio.Instance.SetVolume(bgmVol);
        //Battle.BattleMusic.Instance.SetVolume(bgmVol);
        Map_Sound.Instance.SetVolume(seVol);
        //Battle.BattleSound.Instance.SetVolume(seVol);
    }
}