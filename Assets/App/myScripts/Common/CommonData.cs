﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Battle;

// Jsonにしようかなあ...
// 直列化

/// <summary>
/// 座標、体力、フラグなどの共通データは全てここに入れる。 
/// 高頻度の使用、キャッシュが必要なときもここに入れています。
/// その場合は各クラス内にCommonData cd;を宣言しています。
/// </summary>
[Serializable]
public class CommonData : Singleton<CommonData>
{
    /*~----------------------------------/
    :                                    :
    :    マップに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 外部から座標を参照するときに使用。
    /// CameraMoveクラスで移動フレーム終了後に代入されていく。
    /// </summary>
    public Position IntPos { get { return intPos; } private set { intPos = value; } }
    [SerializeField]
    private Position intPos = new Position();
    public int Posx { get { return IntPos.X; } }
    public int Posy { get { return IntPos.Y; } }
    public int Posd { get { return IntPos.Direction; } }
    public int Floor { get { return IntPos.Floor; } }
    public int Hierarchy { get { return IntPos.Hierarchy; } }

    /// <summary>
    /// 1フレーム前の座標
    /// </summary>
    public Position BeforeFlamePos { get { return beforeFlamePos; } private set { beforeFlamePos = value; } }
    [SerializeField]
    private Position beforeFlamePos = new Position();

    /// <summary>
    /// 前回の座標
    /// 外部から座標を参照するときに使用
    /// </summary>
    public Position BeforePosition { get { return beforePosition; } private set { beforePosition = value; } }
    [SerializeField]
    private Position beforePosition = new Position();

    /// <summary>
    /// MapToBattle->バトルからの復帰用マップ位置座標を一時的に記憶する。
    /// OnEnable時に再読み込みする。セーブからの復帰用に使用することにした。
    /// </summary>
    public Position RetPosition { get { return retPosition; } private set { retPosition = value; } }
    [SerializeField]
    private Position retPosition = new Position(-1, -1, 0, 1, 1);

    /// <summary>
    /// 現座標と前回座標の比較
    /// </summary>
    public bool CheckintPosEqBeforePos { get { return IntPos.X == BeforePosition.X && IntPos.Y == BeforePosition.Y; } }

    /// <summary>
    /// プレイヤーに近接するオブジェクトをスポーンするときのスポーン先。
    /// CameraMoveから設定される。
    /// </summary>
    public Transform transformForSpawnToPlayer;

    /// <summary>
    /// イベントコマンド場所移動ワープ
    /// </summary>
    /// <param name="vec"></param>
    public void Warp(Vector2Int vec)
    {
        transformForSpawnToPlayer.position = new Vector3(vec.x, transformForSpawnToPlayer.position.y, -vec.y);
        CommonData.Instance.SetLocation(vec.x, vec.y, Posd);
        CommonData.Instance.SetBeforeLocation(vec.x, vec.y, Posd);
    }

    public void WarpWithFloor(Vector3Int vec)
    {

    }

    public void LoadMapPositions()
    {

    }

    /// <summary>
    /// フロアの位置をセット
    /// </summary>
    /// <param name="position"></param>
    public void SetLocation(Position position)
    {
        IntPos.SetCopy(position);
    }

    /// <summary>
    /// フロアの位置をセット
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="d"></param>
    /// <param name="floor"></param>
    /// <param name="hierarchy"></param>
    public void SetLocation(int x, int y, int d, int floor = -1, int hierarchy = -1)
    {
        IntPos.X = x;
        IntPos.Y = y;
        IntPos.Direction = d;
        if (floor != -1)
            IntPos.Floor = floor;
        if (hierarchy != -1)
            IntPos.Hierarchy = hierarchy;
    }

    /// <summary>
    /// 前回の位置をセット
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="d"></param>
    /// <param name="floor"></param>
    /// <param name="hierarchy"></param>
    public void SetBeforeLocation(int x, int y, int d, int floor = -1, int hierarchy = -1)
    {
        Debug.Log("SetBeforeLocation");
        BeforePosition.X = x;
        BeforePosition.Y = y;
        BeforePosition.Direction = d;
        if (floor != -1)
            BeforePosition.Floor = floor;
        if (hierarchy != -1)
            BeforePosition.Hierarchy = hierarchy;
    }

    /// <summary>
    /// 前回フレームの座標を代入
    /// </summary>
    /// <param name="position"></param>
    public void SetBeforeFlameLocation(Position position)
    {
        BeforeFlamePos.SetCopy(position);
    }

    /// <summary>
    /// 記憶位置に復帰させる
    /// </summary>
    public void SetLocationFromReturnPos()
    {
        IntPos.SetCopy(RetPosition);
    }

    /// <summary>
    /// バトル、ロードからの復帰場所を設定
    /// </summary>
    /// <param name="position"></param>
    public void SetReturnLocation(Position position)
    {
        RetPosition.SetCopy(position);
    }

    /*~----------------------------------/
    :                                    :
    :    キャラクターに関係するデータ    :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 仲間の数
    /// </summary>
    public int PlayerNum
    {
        get
        {
            int a = 0;
            foreach (var p in PlayerDatas.Instance.p_chara)
                a += Convert.ToInt32(p.isJoined == true);

            return a;
        }
    }

    /// <summary>
    /// アレスタのデータ
    /// </summary>
    public PlayerCharaData Aresta { get { return PlayerDatas.Instance.p_chara[0]; } set { PlayerDatas.Instance.p_chara[0] = value; } }
    /// <summary>
    /// エリナのデータ
    /// </summary>
    public PlayerCharaData Erina { get { return PlayerDatas.Instance.p_chara[1]; } set { PlayerDatas.Instance.p_chara[1] = value; } }
    /// <summary>
    /// ペテルのデータ
    /// </summary>
    public PlayerCharaData Petil { get { return PlayerDatas.Instance.p_chara[2]; } set { PlayerDatas.Instance.p_chara[2] = value; } }

    /*~----------------------------------/
    :                                    :
    :    ダンジョンに関係するデータ      :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// ゴールド
    /// </summary>
    public int Gold { get { return Aresta.G; } set { Aresta.G = value; } }

    /// <summary>
    /// ダンジョン名を取得
    /// </summary>
    /// <param name="hierarchy"></param>
    /// <returns></returns>
    public string GetDungeonName(int hierarchy)
    {
        return TableValues.DUNGEON_NAMES[hierarchy - 1];
    }

    /// <summary>
    /// フロア前の接頭語を取得
    /// </summary>
    /// <returns></returns>
    public string GetFloorName(int hierarchy)
    {
        return TableValues.DUNGEON_FLOOR_NAMES[hierarchy - 1];
    }

    /*~----------------------------------/
    :                                    :
    :    フラグに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 1F
    /// (6, 9)
    /// アレスタ「いっくよー！」
    /// の終了判定
    /// </summary>
    public bool isStartMesAfter = false;

    /*~----------------------------------/
    :                                    :
    :    バトルに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 敵出現時、どの敵キャラグループを出現させるかを記憶してバトルシーンで配置
    /// </summary>
    public string BattleEnemyGroupName;

    const int NumPlayer = 3;


    /// <summary>
    /// プレイヤーデータからバトルステータスに
    /// </summary>
    /// <param name="bp"></param>
    /// <param name="idx"></param>
    public void SetCommonStatusToBattlePlayer(Battle_Status bp, int idx)
    {
        bp.ObjectID = idx;
        // ダミーデータの場合
        if (bp.isDammy)
        {
            bp.CharaName = "";
            bp.Lv = 0;
            bp.Hp = 0;
            bp.M_Hp = 0;
            bp.Mp = 0;
            bp.M_Mp = 0;
            bp.Str = 0;
            bp.Agi = 0;
            bp.Int = 0;
            bp.Vit = 0;
            bp.Luk = 0;
            bp.Cri = 0;
            bp.Exp = 0;
            bp.G = 0;
            bp.Fatigue = 0;
            return;
        }

        var pd = PlayerDatas.Instance.p_chara[idx];
        bp.CharaName = pd.PlayerName;
        bp.Lv = pd.LV;
        bp.Hp = pd.HP;
        bp.M_Hp = pd.M_HP;
        bp.Mp = pd.MP;
        bp.M_Mp = pd.M_MP;
        bp.Str = pd.Str;
        bp.Agi = pd.Agi;
        bp.Int = pd.Int;
        bp.Vit = pd.Vit;
        bp.Luk = pd.Luk;

        // 武器
        bp.w_Str = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Str : 0);
        bp.w_Agi = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Agi : 0);
        bp.w_Cri = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Cri : 0);
        bp.w_Int = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Int : 0);
        bp.w_Luk = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Luk : 0);
        bp.w_Vit = (pd.Wepon.ItemID != 0 ? pd.Wepon.weponStatus.status.Vit : 0);

        bp.Cri = pd.CRI;
        bp.Exp = pd.EXP;
        bp.G = pd.G;
        bp.Fatigue = 0;
    }
    
    /// <summary>
    /// バトルデータからプレイヤーデータに
    /// </summary>
    /// <param name="bp">Bp.</param>
    /// <param name="idx">Index.</param>
    public static void SetBattleStatusToCommonPlayer(Battle_Status bp, int idx)
    {
        // ダミーデータの場合
        if (bp.isDammy)
        {
            return;
        }

        var pd = PlayerDatas.Instance.p_chara[idx];
        pd.LV = bp.Lv;
        pd.HP = bp.Hp;
        pd.M_HP = bp.M_Hp;
        pd.MP = bp.Mp;
        pd.M_MP = bp.M_Mp;
        pd.Str = bp.Str;
        pd.Agi = bp.Agi;
        pd.Int = bp.Int;
        pd.Vit = bp.Vit;
        pd.Luk = bp.Luk;
        pd.Cri = bp.Cri;
        pd.EXP = bp.Exp;
        pd.G = bp.G;
    }

    /*~-----------------------------------/
     * 
     *          状態メッセージ群
     * 
     * ----------------------------------*/

    /// <summary>
    /// Hpごとの状態メッセージ取得
    /// </summary>
    /// <returns></returns>
    public static string GetHpStr(int charaidx)
    {
        PlayerCharaData data = PlayerDatas.Instance.p_chara[charaidx];
        return GetHpStr(data);
    }

    /// <summary>
    /// Hpごとの状態メッセージ取得
    /// </summary>
    /// <returns></returns>
    public static string GetHpStr(PlayerCharaData data)
    {
        string[] strs = hpstrsplyr;
        float hp_per_mhp = data.HP / (float)data.M_HP;
        string EnmName = data.name.ToString();
        return StageCalculation(12, hp_per_mhp, strs, EnmName);
    }

    static string[] hpstrsplyr = { "闘志満々！" ,
                             "とっても元気だよ！",
                             "元気、元気！！",
                             "元気！",
                             "まだいけるよ！",
                             "ちょっと苦しくなってきたよ",
                             "くらくらするよ",
                             "結構きいてきたみたい...。",
                             "痛い...。",
                             "苦しい...。",
                             "ふぅ...ふぅ...。",
                             ".......。" };

    /// <summary>
    /// Mpごとの状態メッセージ取得
    /// 主人公専用
    /// </summary>
    /// <returns></returns>
    public static string GetMpStr(int charaidx)
    {
        PlayerCharaData data = PlayerDatas.Instance.p_chara[charaidx];
        return GetMpStr(data.MP, data.M_HP, data.PlayerName);
    }

    /// <summary>
    /// Mpごとの状態メッセージ取得。MPTextで使用。
    /// 主人公専用
    /// </summary>
    /// <returns></returns>
    public static string GetMpStrBattle(Battle_Status bs)
    {
        return GetMpStr(bs.Mp, bs.M_Mp, bs.CharaName);
    }

    /// <summary>
    /// Mpごとの状態メッセージ取得
    /// 主人公専用
    /// </summary>
    /// <returns></returns>
    public static string GetMpStr(int MP, int M_MP, string name)
    {
        string[] strs = mpstrsplyr;
        float mp_per_mmp = MP / (float)M_MP;
        string EnmName = name;
        return StageCalculation(12, mp_per_mmp, strs, EnmName);
    }

    static string[] mpstrsplyr = {  "魔力満タン",
                                    "魔力まだまだある!",
                                    "魔力まだある!",
                                    "魔力ちょっと減ってきた",
                                    "魔力減ってきた",
                                    "魔力半分くらい",
                                    "魔力半分切ってる",
                                    "魔力少ない",
                                    "魔力あんまりない",
                                    "魔力もうほとんどない",
                                    "魔力全然ない...",
                                    "魔力空っぽ...。"
                                    };

    private static string StageCalculation(int n, float m, string[] strs, string en)
    {
        for (int i = n; i > 0; i--)
        {
            if (m > 0.0833333f * i)
            {
                print("d / hp : " + m);
                return en + strs[n - i];
            }
        }
        return en + strs[strs.Length - 1];
    }

    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み完了
    /// </summary>
    public bool isEndRead = false;
    private void Awake()
    {
        // テスト起動用。
        if (instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        // -- 各種データコンポーネント
        instance = this.GetComponent<CommonData>();
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// 読み込んだデータから主人公パラメータを生成
    /// </summary>
    private void Start()
    {
        StartCoroutine(DelayInit());
    }

    /// <summary>
    /// テーブル読み込み完了後初期化。
    /// </summary>
    /// <returns>The init.</returns>
    private IEnumerator DelayInit()
    {
        while (PlayerTable.Instance == null || ItemTable.Instance == null || SkillTable.Instance == null || PlayerDatas.Instance == null ||
               PlayerTable.Instance.isEndRead == false || ItemTable.Instance.isEndRead == false || SkillTable.Instance.isEndRead == false || PlayerDatas.Instance.isEndRead == false)
        {
            yield return null;
        }

        Debug.Log("テーブル初期化");
        PlayerDataSet();
        isEndRead = true;
    }

    public bool _joinAresta = true;
    public bool _joinErina = true;
    public bool _joinPetil = true;
    /// <summary>
    /// lvは0から始まる
    /// </summary>
    /// <param name="lv"></param>
    public void PlayerDataSet(int lv = 0)
    {
        if (lv != 0) lv--;

        var plytbl = PlayerTable.Instance;
        var playercharas = PlayerDatas.Instance.p_chara;
        var skltbl = SkillTable.Instance;

        // 主人公のパラメーターセット
        for (int i = 0; i < playercharas.Length; i++)
        {
            //TODO: 直す
            StatusTable[] dat;
            if (i == 0)
            {
                dat = plytbl.st_Aresta.ToArray();
                //TODO: テスト用なので消す // debug
                playercharas[0].Ply_skl[0] = skltbl.ply_skills.First(sk => sk.SkillID == 201 - 200);

                playercharas[1].Ply_skl[0] = skltbl.ply_skills.First(sk => sk.SkillID == 210 - 200);
                playercharas[1].Ply_skl[1] = skltbl.ply_skills.First(sk => sk.SkillID == 211 - 200);
                playercharas[1].Ply_skl[2] = skltbl.ply_skills.First(sk => sk.SkillID == 204 - 200);
                //pd.p_chara[1].Ply_skl[1] = skltbl.ply_skills.First(sk => sk.SkillID == 203 - 200);
                //pd.p_chara[1].Ply_skl[2] = skltbl.ply_skills.First(sk => sk.SkillID == 204 - 200);

                playercharas[2].Ply_skl[0] = skltbl.ply_skills.First(sk => sk.SkillID == 217 - 200);
                playercharas[2].Ply_skl[1] = skltbl.ply_skills.First(sk => sk.SkillID == 220 - 200);
                //pd.p_chara[2].Ply_skl[1] = skltbl.ply_skills.First(sk => sk.SkillID == 203 - 200);
                //pd.p_chara[2].Ply_skl[2] = skltbl.ply_skills.First(sk => sk.SkillID == 204 - 200);
            }
            else if (i == 1)
                dat = plytbl.st_Erina.ToArray();
            else// if (i == 2)
                dat = plytbl.st_Petil.ToArray();

            playercharas[0].isJoined = _joinAresta;
            playercharas[1].isJoined = _joinErina;
            playercharas[2].isJoined = _joinPetil;

            playercharas[i].PlayerName = dat[lv].Name;
            playercharas[i].SetNameFromStringName();
            playercharas[i].LV = dat[lv].Lv;
            playercharas[i].HP = dat[lv].Hp;
            playercharas[i].M_HP = dat[lv].Hp;
            playercharas[i].MP = dat[lv].Mp;
            playercharas[i].M_MP = dat[lv].Mp;
            playercharas[i].Str = dat[lv].Str;
            playercharas[i].Agi = dat[lv].Agi;
            playercharas[i].Int = dat[lv].Int;
            playercharas[i].Vit = dat[lv].Vit;
            playercharas[i].Luk = dat[lv].Luk;
            playercharas[i].Cri = dat[lv].Cri;
            playercharas[i].EXP = dat[lv].Exp_ALL - dat[lv].Exp;
            playercharas[i].EXP_BTM = dat[lv].Exp_ALL - dat[lv].Exp;
            playercharas[i].EXP_UPR = dat[lv].Exp_ALL;
            playercharas[i].SKILL = dat[lv].GetSkillNo;
        }
    }

    /// <summary>
    /// バトル後のレベルアップ
    /// </summary>
    /// <param name="playerId">Player identifier.</param>
    public void LvUp(int playerId)
    {
        var plytbl = PlayerTable.Instance;
        var playerdatas = PlayerDatas.Instance;
        var skltbl = SkillTable.Instance;

        var ply = PlayerDatas.Instance.p_chara[playerId];
        List<StatusTable> dat;
        if (playerId == 0)
            dat = plytbl.st_Aresta;
        else if (playerId == 1)
            dat = plytbl.st_Erina;
        else if (playerId == 2)
            dat = plytbl.st_Petil;
        else throw new System.IndexOutOfRangeException("レベルアップさせるキャラクターがいません。");

        var lv = ply.LV;
        ply.LV = dat[lv].Lv;
        ply.HP = dat[lv].Hp;
        ply.M_HP = dat[lv].Hp;
        ply.MP = dat[lv].Mp;
        ply.M_MP = dat[lv].Mp;
        ply.Str = dat[lv].Str;
        ply.Agi = dat[lv].Agi;
        ply.Int = dat[lv].Int;
        ply.Vit = dat[lv].Vit;
        ply.Luk = dat[lv].Luk;
        ply.Cri = dat[lv].Cri;
        //ply.EXP = dat[lv].Exp_ALL - dat[lv].Exp;
        ply.EXP_BTM = dat[lv].Exp_ALL - dat[lv].Exp;
        ply.EXP_UPR = dat[lv].Exp_ALL;
        ply.SKILL = dat[lv].GetSkillNo;
    }

    /// <summary>
    /// LevelUpのあとにスキルを習得させる。
    /// 習得スキルの名前を返す。
    /// </summary>
    /// <returns>The skill name.</returns>
    /// <param name="playerId">Player identifier.</param>
    public string GetSkillName(int playerId)
    {
        var playerdatas = PlayerDatas.Instance;
        // スキルを習得するレベルではない
        var getSkill = playerdatas.p_chara[playerId].SKILL;
        if (getSkill == 0)
            return null;

        var skill = SkillTable.GetSkillForId(getSkill - 200, false);

        // スキルをすでに習得している
        foreach (var ps in playerdatas.p_chara[playerId].Ply_skl)
        {
            if (skill.SkillID == ps.SkillID)
                return null;
        }

        // 空白にスキルを当てはめる
        int SkillNum = playerdatas.p_chara[playerId].Ply_skl.Length;
        for (int i = 0; i < SkillNum; i++)
        {
            if (playerdatas.p_chara[playerId].Ply_skl[i].SkillID == 0)
            {
                playerdatas.p_chara[playerId].Ply_skl[i] = skill;
                return playerdatas.p_chara[playerId].Ply_skl[i].SkillName;
            }
        }
        throw new Exception("習得できるスキルがありませんでした。");
    }

    /*~-----------------------------
     * 
     *      トレイ表示するイベント
     * 
     * -----------------------------*/
    public EventInfo mainEvent = new EventInfo { eventTitle = "迷宮を駆け抜けよう", eventExplanatory = "迷宮の奥地のアスタリスクを手に入れる。" };
    public List<EventInfo> subQuest = new List<EventInfo>();
}
