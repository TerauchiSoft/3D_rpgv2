﻿using UnityEngine;

/// <summary>
/// セーブデータをロードする
/// </summary>
public class SaveLoader : MonoBehaviour
{
    public static bool isLoad;
    public static int loadNo;

    public static void SetLoadNo(int loadNo)
    {
        SaveLoader.isLoad = true;
        SaveLoader.loadNo = loadNo;
    }
    // Use this for initialization
    void Start()
    {
        if (isLoad == false)
            return;

        SaveToJson.Instance.LoadData(loadNo);
    }
}
