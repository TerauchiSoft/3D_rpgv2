﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 位置クラス
/// </summary>
[System.Serializable]
public class Position
{
    /// <summary>
    /// 上右下左 0, 1, 2, 3
    /// </summary>
    public int Direction;
    /// <summary>
    /// 0~
    /// </summary>
    public int X;
    /// <summary>
    /// 0~
    /// </summary>
    public int Y;
    public Vector2Int XY { get { return new Vector2Int(X, Y); } }
    public Vector3Int XYF { get { return new Vector3Int(X, Y, Floor); } }
    /// <summary>
    /// 1~
    /// </summary>
    public int Floor;
    /// <summary>
    /// 1~
    /// </summary>
    public int Hierarchy;

    public override bool Equals(object o)
    {
        var pos = (Position)o;
        bool b = (pos.X == X) && (pos.Y == Y) && (pos.Direction == Direction) && (pos.Hierarchy == Hierarchy) && (pos.Floor == Floor);
        return b;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    /// <summary>
    /// デフォルトコンストラクタ
    /// </summary>
    public Position() { }

    /// <summary>
    /// (x, y, 方向{0 ~ 3})
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="d"></param>
    /// <param name="fl"></param>
    /// <param name="hr"></param>
    public Position(int x, int y, int d, int fl, int hr)
    {
        this.X = x;
        this.Y = y;
        this.Direction = d;
        this.Hierarchy = hr;
        this.Floor = fl;
    }

    /// <summary>
    /// コピー
    /// </summary>
    /// <param name="pos"></param>
    public void SetCopy(Position pos)
    {
        this.X = pos.X;
        this.Y = pos.Y;
        this.Direction = pos.Direction;
        this.Hierarchy = pos.Hierarchy;
        this.Floor = pos.Floor;
    }

    /// <summary>
    /// カメラの位置をデータから取得。
    /// </summary>
    /// <returns>The world vec.</returns>
    public Vector3 GetWorldVec()
    {
        return new Vector3(X, 0, -Y);
    }
}