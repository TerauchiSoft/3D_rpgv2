﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFromDataTray : SaveToDataTray
{
    // ---------------- UITray -------------------

    public override void LeaveAnime(int befidx)
    {
    }

    public override void CursorAnime(int nowidx)
    {
    }

    public override void OnOpenTray()
    {
        var idx = _current._decideIdx;
        Load(idx);
        loadCanvas.isDone = true;
        continueSelect.GotoMap();
    }

    // --------------- LoadFormData -----------------

    [SerializeField] LoadCanvas loadCanvas;
    [SerializeField] TitleCommandController continueSelect;
    void Load(int idx)
    {
        SaveLoader.SetLoadNo(idx);
    }
}
