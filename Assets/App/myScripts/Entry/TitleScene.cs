﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;
using UniRx;

/// <summary>
/// タイトル画面までの遷移をコントロール
/// </summary>
public class TitleScene : MonoBehaviour {
    private const int DELAY_MILSEC = 1000;
    private const int DELAY_ANIME_MILSEC = 500;
    private const float FADE_TIME = 1.2f;
    private const float WAIT_TIME = 2.5f;

    private bool isCompleted;
    private bool isFadeCompleted;
    private bool isViewText;
    [SerializeField] private Image circleLogo;
    [SerializeField] private CanvasGroup title;
    [SerializeField] private TitleCommandController titleCommandController;

    /// <summary>
    /// シーン移動セット
    /// </summary>
    private void GotoMethod()
    {
        title.DOFade(0.0f, FADE_TIME);
    }

	// Use this for initialization
	void Start ()
    {
        SetCircleLogoFadeInOut();
        SetGoTitle();
    }

    /// <summary>
    /// circleロゴのフェードインアウトを設定
    /// </summary>
    private void SetCircleLogoFadeInOut()
    {
        Observable.Timer(TimeSpan.FromMilliseconds(DELAY_MILSEC))
            .Subscribe(_ =>
            {
                // サークルロゴ
                circleLogo.DOFade(1.0f, FADE_TIME);
            });

        Observable.Timer(TimeSpan.FromMilliseconds(DELAY_MILSEC * 4 + FADE_TIME))
            .Subscribe(_ =>
            {
                // サークルロゴ
                circleLogo.DOFade(0.0f, FADE_TIME);
            });
    }

    /// <summary>
    /// タイトル画面に移行セット
    /// </summary>
    private void SetGoTitle()
    {
        Observable.Timer(TimeSpan.FromMilliseconds(DELAY_MILSEC * 6 + FADE_TIME * 2))
            .Subscribe(_ =>
            {
                // title
                title.DOFade(1.0f, FADE_TIME / 2f).OnComplete(() => isFadeCompleted = true);
            });
    }

    private void Update()
    {
        if (!isFadeCompleted || isCompleted)
            return;

        // 入力でtitleコマンド選択に移る
        if (PlayerController.Instance.IsButtonAndAxisIsNoZero || PlayerController.Instance.isFire1 || PlayerController.Instance.isFire2 || PlayerController.Instance.isFire3)
        {
            titleCommandController.Init();
            isCompleted = true;
        }
    }
}
