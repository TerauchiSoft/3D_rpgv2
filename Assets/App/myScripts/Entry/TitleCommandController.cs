﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UniRx;

/// <summary>
/// タイトルのコマンドを選択する
/// </summary>
public class TitleCommandController : MonoBehaviour
{
    private bool isControl;

    private const int NEWGAME_POINTER_INDEX = 0;
    private const int CONTINUE_POINTER_INDEX = 1;
    private const string MAP_SCENE = "Map_EventDevelop";

    private const float MOVE_DULATION = 0.2f;
    private const float FADE_DULATION = 1.5f;

    private Tweener movePointerTweener;

    [SerializeField] private int selectedIndex;
    [SerializeField] private Transform cursorTransform;
    [SerializeField] private Transform[] elements;
    [SerializeField] private GameObject pushEnyButton;
    [SerializeField] private GameObject commandMenu;
    [SerializeField] private LoadCanvas loadCanvas;
    [SerializeField] private CanvasGroup titleCanvasGroup;
    [SerializeField] private CanvasGroup saveCanvasGroup;

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        Map_Sound.Instance.DecisionSoundPlay();
        // セーブデータチェック
        if (SaveToJson.Instance.GetSavingMaxIdx() == -1)
        {
            selectedIndex = NEWGAME_POINTER_INDEX;
            cursorTransform.position = elements[NEWGAME_POINTER_INDEX].position;
        }
        else
        {
            selectedIndex = CONTINUE_POINTER_INDEX;
            cursorTransform.position = elements[CONTINUE_POINTER_INDEX].position;
        }

        pushEnyButton.SetActive(false);
        commandMenu.SetActive(true);
        SetCursorPosition();

        Observable.Timer(TimeSpan.FromSeconds(0.1f))
            .Subscribe(_ => isControl = true)
            .AddTo(gameObject);
    }

    /// <summary>
    /// カーソル位置を更新
    /// </summary>
    public void SetCursorPosition()
    {
        if (movePointerTweener == null)
        {
            movePointerTweener = cursorTransform.DOMove(elements[selectedIndex].position, MOVE_DULATION);
        }
        else
        {
            movePointerTweener.Kill();
            movePointerTweener = null;
            movePointerTweener = cursorTransform.DOMove(elements[selectedIndex].position, MOVE_DULATION);
        }
    }

    /// <summary>
    /// マップに移動する
    /// </summary>
    public void GotoMap()
    {
        // TODO:マップ戦以後メニュー開かないよう対策が必要
        titleCanvasGroup.DOFade(0.0f, FADE_DULATION);
        saveCanvasGroup.DOFade(0.0f, FADE_DULATION).OnComplete(() => SceneManager.LoadScene(MAP_SCENE));
    }

    /// <summary>
    /// コマンドを決定する
    /// </summary>
    private void DecitionCommand()
    {
        isControl = false;
        switch (selectedIndex)
        {
            case 0: // 物語をはじめる
                GotoMap();
                break;
            case 1: // 続きからはじめる
                loadCanvas.Wakeup();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// インデックスを変更する
    /// </summary>
    /// <param name="addIndex"></param>
    private void AddIndex(int addIndex)
    {
        selectedIndex = addIndex + selectedIndex;
        if (selectedIndex >= elements.Length)
        {
            selectedIndex = 0;
        }
        else if (selectedIndex < 0)
        {
            selectedIndex = elements.Length - 1;
        }
    }

    /// <summary>
    /// キー入力の移動
    /// </summary>
    private void Navigate()
    {
        if (!isControl)
            return;

        float y = PlayerController.Instance.joy_Axis_buf.y;
        if (PlayerController.Instance.isFire1)
        {
            Map_Sound.Instance.DecisionSoundPlay();
            DecitionCommand();
        }
        else if (y > 0)
        {
            // Up
            Map_Sound.Instance.CursorMoveSoundPlay();
            AddIndex(-1);
            SetCursorPosition();
        }
        else if (y < 0)
        {
            // down
            Map_Sound.Instance.CursorMoveSoundPlay();
            AddIndex(1);
            SetCursorPosition();
        }
    }

    private void Update()
    {
        Navigate();    
    }
}
