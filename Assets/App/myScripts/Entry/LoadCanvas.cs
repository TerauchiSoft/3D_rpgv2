﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCanvas : MonoBehaviour {
    [SerializeField] TitleCommandController continueSelect;
    [SerializeField] SaveTray saveTray;
    public bool isDone;
    public void Wakeup()
    {
        gameObject.SetActive(true);
        saveTray.OpenNewTray(null, null);
    }

    public void ReturnCanvas()
    {
        saveTray.gameObject.SetActive(false);
        gameObject.SetActive(false);
        continueSelect.Init();
    }

    void Update()
    {
        if (!isDone && PlayerController.Instance.isFire2)
        {
            ReturnCanvas();
        }
    }
}
