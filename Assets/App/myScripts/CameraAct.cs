﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraAct : MonoBehaviour {
    [SerializeField]
    private Transform billboardParent;
    [SerializeField]
    private Combine combine;

    public void AddBillboard(Transform billtrs)
    {
        billtrs.parent = billboardParent;
        combine.MeshCombine(billtrs.gameObject);
    }

    private void LookToCameraBillboard()
    {
        billboardParent.LookAt(transform);
    }

	// Update is called once per frame
	void Update () {
        //LookToCameraBillboard();

    }
}
