﻿using System.IO;
using UnityEngine;
using DG.Tweening;
using Minimap;

/// <summary>
/// セーブデータ全ての初期化、読み出し、セーブ。
/// </summary>
public class SaveToJson : Singleton<SaveToJson> {
    private const string PATH_SAVE_DATA_DIRECTORY = "/save";
    private int numSaveFile;

    private SaveData saveData;
    public const int MIN_DATA_NUM = 3; // セーブデータの最大
    private ISavingText<SaveData>[] savingTexts = new ISavingText<SaveData>[MIN_DATA_NUM];

    /// <summary>
    /// セーブファイルの最後のファイルのインデックスを返す
    /// </summary>
    /// <returns></returns>
    public int GetSavingMaxIdx()
    {
        var now = 0;
        var idx = -1;
        foreach (var s in savingTexts)
        {
            if (s != null && File.Exists(s.FilePath))
            {
                idx = now;
            }
            now++;
        }
        return idx;
    }

    // Use this for initialization
    void Start () {
        InitSaveData();
    }
	
    void InitSaveData()
    {
        // ディレクトリがなければ作成。
        string dir = Application.streamingAssetsPath + PATH_SAVE_DATA_DIRECTORY;
        if (!File.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        numSaveFile = Directory.GetFiles(dir, "saveData*.json", SearchOption.AllDirectories).Length;
        if (numSaveFile < MIN_DATA_NUM)
        {
            numSaveFile = MIN_DATA_NUM;
        }

        for (int i = 0; i < numSaveFile; i++)
        {
            savingTexts[i] = new SavingText<SaveData>();
            savingTexts[i].InitSetting(i);
        }
    }

    /// <summary>
    /// 全てのデータのセーブ。セーブデータがなければ新しく作成する。
    /// </summary>
    [ContextMenu("Save")]
    public void SaveToData(int idx)
    {
        if (savingTexts == null)
        {
            savingTexts = new ISavingText<SaveData>[idx + 1];
        }

        if (savingTexts[idx] == null)
        {
            savingTexts[idx] = new SavingText<SaveData>();
            savingTexts[idx].InitSetting(idx);
        }
        if (saveData == null)           // nullで入ると思うけど念の為
            saveData = new SaveData();
        saveData.SetCaches();
        savingTexts[idx].Save(saveData);
        saveData = null;
    }

    /// <summary>
    /// セーブデータを取得するのみ。
    /// </summary>
    /// <returns>The save data.</returns>
    /// <param name="n">N.</param>
    public SaveData GetSaveData(int n)
    {
        return savingTexts[n].Load();
    }

    /// <summary>
    /// 全てのデータのロード。ロードデータがなければ初期セーブデータを読み込む。
    /// タイミングがオブジェクトを待っていたりするので少し遅い、取り扱い注意
    /// </summary>
    [ContextMenu("Load")]
    public void LoadData(int n)
    {
        saveData = GetSaveData(n);
        saveData.LoadCaches();
        saveData = null;

        MinimapManager.Instance.UpdateVisible();
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
}
