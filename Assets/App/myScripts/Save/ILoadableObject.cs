﻿using System;
/// <summary>
/// ロード可能なオブジェクト。
/// </summary>
public interface ILoadableObject<T>
{
    void Load(T data);
}