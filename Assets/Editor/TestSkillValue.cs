﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert; // AssertはNUnitのではなくUnityのものを使う
using System.Collections.Generic;
using UnityEngine;
using Battle;

public class TestSkillValue
{
    // ---------- SkillValue -----------

    private Battle_Status GetTestStatus()
    {
        var ob = new GameObject();
        var status = ob.AddComponent<Battle_Status>();
        status.PlyOrEnm = Battle_Status.PLYorENM.PLY;
        status.CharaName = "アレスタ";
        status.Hp = 100;
        status.M_Hp = 100;
        status.Mp = 100;
        status.M_Mp = 100;
        status.B_Str = 100;
        status.B_Agi = 100;
        status.B_Int = 100;
        status.B_Vit = 100;
        status.B_Cri = 100;
        status.B_Luk = 100;

        status.TargetObject = TableValues.Tgt.Enemy;
        status.SelectedTgtName = "敵";
        status.SelectedActNum = 0;
        var enmob = new GameObject();
        status.EnmStatus = new List<Battle_Status>();
        status.EnmStatus.Add(enmob.AddComponent<Battle_Status>());
        var enm = status.EnmStatus[0];
        enm.M_Hp = 100;
        enm.Hp = 100;
        enm.PlyOrEnm = Battle_Status.PLYorENM.ENM;
        enm.CharaName = "敵";
        enm.Mp = 100;
        enm.M_Mp = 100;
        enm.B_Str = 200;
        enm.B_Agi = 200;
        enm.B_Int = 200;
        enm.B_Vit = 100;
        enm.B_Cri = 200;
        enm.B_Luk = 200;

        return status;
    }

    private Skill GetSkl()
    {
        return new Skill
        {
            DamageFormula = "ply_amo * (ply_str + ply_int) / (2 * enm_int) * ( 1 + (ply_lv / ply_amo/2 ) )",
            skl_method = (bs, tgbs) => { return 50; },
            effects = new TableValues.Effect[] { TableValues.Effect.Attack }
        };
    }

    [Test]
    public void Attack()
    {
        var aresta = GetTestStatus();
        UnityEngine.Debug.Log("前：" + aresta.EnmStatus[0].Hp);

        // ActMethod.act mtd = Skill.GetSkillMethod(201);
        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.Attack };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta.EnmStatus[0]);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta.EnmStatus[0]);

        UnityEngine.Debug.Log("攻撃後：" + aresta.EnmStatus[0].Hp);
    }

    [Test]
    public void MPDown()
    {
        var aresta = GetTestStatus();
        UnityEngine.Debug.Log("MP前：" + aresta.EnmStatus[0].Mp);

        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.MPDown };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta.EnmStatus[0]);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta.EnmStatus[0]);

        UnityEngine.Debug.Log("MP攻撃後：" + aresta.EnmStatus[0].Mp);
    }

    [Test]
    public void HPRecover()
    {
        var aresta = GetTestStatus();
        aresta.Hp = 1;
        UnityEngine.Debug.Log("HP前：" + aresta.Hp);

        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.HPRecover };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta);

        UnityEngine.Debug.Log("回復後：" + aresta.Hp);
    }

    [Test]
    public void AGIDown()
    {
        var aresta = GetTestStatus();
        aresta.Hp = 1;
        UnityEngine.Debug.Log("AGI前：" + aresta.B_Agi);

        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.AGIDown };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta);

        UnityEngine.Debug.Log("AGId後：" + aresta.B_Agi);
    }

    [Test]
    public void AgiUp()
    {
        var aresta = GetTestStatus();
        aresta.Hp = 1;
        UnityEngine.Debug.Log("AGIu前：" + aresta.B_Agi);

        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.AGIUp };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta);

        UnityEngine.Debug.Log("AGIu後：" + aresta.B_Agi);
    }

    [Test]
    public void INIUp()
    {
        var aresta = GetTestStatus();
        aresta.Hp = 1;
        UnityEngine.Debug.Log("前：" + aresta.B_Int);

        Skill skill = GetSkl();
        skill.effects = new TableValues.Effect[] { TableValues.Effect.STRUp };

        // 一般化
        var dam = skill.skl_method.Invoke(aresta, aresta);
        SkillValue skillValue = SkillValue.CreateSkillValue(skill.effects[0]);
        skillValue._calculatedAmount = dam;
        skillValue.RunSkillActionToTgt(aresta);

        UnityEngine.Debug.Log("StrUp後：" + aresta.B_Int);
    }

}