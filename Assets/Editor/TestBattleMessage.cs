﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert; // AssertはNUnitのではなくUnityのものを使う
using System.Collections.Generic;
using Battle;

public class NewEditModeTest
{
    [Test]
    public void SimplePasses()
    {
        Assert.AreEqual(2, 1 + 1);
    }

    [UnityTest]
    public IEnumerator EnumeratorPasses()
    {
        yield return null;
        Assert.AreEqual(2, 1 + 1);
    }

    // ---------- Battle Message -----------

    private Battle_Status GetTestStatus()
    {
        var status = new Battle_Status();
        status.PlyOrEnm = Battle_Status.PLYorENM.PLY;
        status.CharaName = "アレスタ";
        status.Hp = 500;
        status.M_Hp = 500;
        status.Mp = 500;
        status.M_Mp = 500;

        status.TargetObject = TableValues.Tgt.Enemy;
        status.SelectedTgtName = "敵";
        status.SelectedActNum = 0;
        status.EnmStatus = new System.Collections.Generic.List<Battle_Status> {
            new Battle_Status { M_Hp = 500,
            Hp = 500,PlyOrEnm = Battle_Status.PLYorENM.ENM,
            CharaName = "敵"
            }};

        return status;
   }


    [Test]
    public void BattleMessageHp()
    {
        Battle_Status battle_status = GetTestStatus();
        BattleMessage battlemessage = new BattleMessage();

        battle_status.Hp = 550;
        var hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 500;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 400;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 300;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 200;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 100;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 20;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Hp = 0;
        hpmessages = battlemessage.GetHpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);
    }

    [Test]

    public void BattleMessageMp()
    {
        Battle_Status battle_status = GetTestStatus();
        BattleMessage battlemessage = new BattleMessage();

        battle_status.Mp = 550;
        var hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 500;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 400;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 300;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 200;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 100;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 20;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);

        battle_status.Mp = 0;
        hpmessages = battlemessage.GetMpStr(battle_status);
        UnityEngine.Debug.Log(hpmessages);
    }

    [Test]
    public void BattleMessageDamage()
    {
        Battle_Status battle_status = GetTestStatus();
        BattleMessage battlemessage = new BattleMessage();
        var damageses = battlemessage.GetDamageStr(battle_status, 550);
        UnityEngine.Debug.Log(damageses);

        damageses = battlemessage.GetDamageStr(battle_status, 450);
        UnityEngine.Debug.Log(damageses);

        damageses = battlemessage.GetDamageStr(battle_status, 350);
        UnityEngine.Debug.Log(damageses);

        damageses = battlemessage.GetDamageStr(battle_status, 250);
        UnityEngine.Debug.Log(damageses);


        damageses = battlemessage.GetDamageStr(battle_status, 150);
        UnityEngine.Debug.Log(damageses);

        damageses = battlemessage.GetDamageStr(battle_status, 50);
        UnityEngine.Debug.Log(damageses);

        damageses = battlemessage.GetDamageStr(battle_status, 00);
        UnityEngine.Debug.Log(damageses);
    }

    [Test]
    public void BattleMessageStrength()
    {
        // 雑
        List<Battle_Status> battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 6 }, new Battle_Status { Lv = 6 }, };
        List<Battle_Status> battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };
        BattleMessage battlemessage = new BattleMessage();

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 8 }, new Battle_Status { Lv = 8 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 10 }, new Battle_Status { Lv = 10 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 11 }, new Battle_Status { Lv = 11 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };


        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 13 }, new Battle_Status { Lv = 13 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 15 }, new Battle_Status { Lv = 15 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };

        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 17 }, new Battle_Status { Lv = 17 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };
        battle_statuses_player = new List<Battle_Status> { new Battle_Status { Lv = 19 }, new Battle_Status { Lv = 19 }, };
        battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12 }, new Battle_Status { Lv = 12 }, };
    }

    [Test]
    public void BattleAppearMessage()
    {
        BattleMessage btlms = new BattleMessage();

        for (int i = 0; i < 12; i++)
        {
            var battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12, CharaName = "アビゲイル" } };
            var mes = btlms.GetAppearMessage(battle_statuses_enemies.ToArray());
            UnityEngine.Debug.Log(mes);
        }

        for (int i = 0; i < 12; i++)
        {
            var battle_statuses_enemies = new List<Battle_Status> { new Battle_Status { Lv = 12, CharaName = "ジェイコブ" }, new Battle_Status { Lv = 12, CharaName = "オスカー" }, new Battle_Status { Lv = 12, CharaName = "ガミジン" } };
            var mes = btlms.GetAppearMessage(battle_statuses_enemies.ToArray());
            UnityEngine.Debug.Log(mes);
        }
    }
}