cd `dirname $0`



function download() {
ftp -n << END
    open terainast.html.xdomain.jp
    user terainast.html.xdomain.jp Passtsu0
    get $1 $2
    close
    bye
END
    echo "pull"
    echo $1
    echo $2
}

local_paths=()
remote_paths=()
can="OK"

while read row; do
  local=`echo ${row} | cut -d ',' -f 1`
  remote=`echo ${row} | cut -d ',' -f 2`
  remote_to_local=`echo ${row} | cut -d ',' -f 4 | tr -d '\r'`

  if [ $remote_to_local = $can ]; then
    local_paths+=( $PWD$local )
    remote_paths+=( $remote )
  fi
done < TableUpdateListUnity.csv

idx=0
for loc in ${local_paths[@]}
do
  download ${remote_paths[$idx]} $loc
  let idx++
done

download /VERSION.txt $PWD"/VERSION.txt"
